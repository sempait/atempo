USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[ItemTurnoSala_Delete_PorTurnoSala]    Script Date: 05/28/2015 20:46:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ItemTurnoSala_Delete_PorTurnoSala]

	@pIDTurnoSala int

AS
BEGIN

	DELETE FROM ItemTurnoSala 
	WHERE IDTurnosSala = @pIDTurnoSala

END
