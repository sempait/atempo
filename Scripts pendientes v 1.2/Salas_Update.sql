USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[Gastos_Update]    Script Date: 12/30/2014 01:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Salas_Update]

@pUniqueID int,
@pResourceName varchar(50),
@pPrecioSala float



AS
BEGIN

	UPDATE  Salas
	
	 SET ResourceName=@pResourceName,
	  PrecioSala=@pPrecioSala
	  WHERE UniqueID=@pUniqueID
END

