USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[OperacionCajadiaria_Close]    Script Date: 03/02/2015 23:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[OperacionCajadiaria_Close]

@pFechaOperacion DATETIME,
@pFechaOperacionPost DATETIME

AS
BEGIN


DECLARE @CajaInicialPos Float,
@CajaInicial float

SET @CajaInicialPos = 9128329
SET @CajaInicial=0


--Consigo el valor de el resultado de la caja del dia anterior
declare SaldoCaja cursor for
SELECT Resultado
FROM ViResultadoCajaDiaria
WHERE CONVERT(VARCHAR,Fecha,103)=CONVERT(VARCHAR,@pFechaOperacion,103)

open SaldoCaja
fetch SaldoCaja INTO @CajaInicial

close SaldoCaja
deallocate SaldoCaja

--Verifico si la caja inicial del dia posterior esta en 0, si es asi hago un INSERT sino un UPDATE

declare CajaInicial cursor for
SELECT CajaInicial
FROM CajaDiaria
WHERE CONVERT(VARCHAR,FechaOperacion,103)=CONVERT(VARCHAR,@pFechaOperacionPost,103)

open CajaInicial
fetch CajaInicial INTO @CajaInicialPos

close CajaInicial
deallocate CajaInicial

IF (@CajaInicialPos=9128329)
BEGIN
INSERT INTO CajaDiaria(FechaOperacion,DescripcionOperacion,CajaInicial)VALUES(@pFechaOperacionPost,'',@CajaInicial)
END
ELSE
BEGIN
UPDATE CajaDiaria SET CajaInicial=@CajaInicial
WHERE CONVERT(VARCHAR,FechaOperacion,103)=CONVERT(VARCHAR,@pFechaOperacionPost,103)
END



END


	


