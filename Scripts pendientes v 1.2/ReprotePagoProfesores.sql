USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[ReportePagoProfesores_SelectDatos]    Script Date: 03/17/2015 01:22:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ReportePagoProfesores_SelectDatos]

@pFechaDesde datetime,
@pFechaHasta datetime

AS
BEGIN
	
	SELECT profesor,modalidad,CONVERT(vARCHAR,fecha,103) as fecha,curso,horas,horas*importe as importe,pocentajePlus
	FROM PagoProfesoresView
	WHERE fecha >= @pFechaDesde AND fecha <= @pFechaHasta
	ORDER BY fecha
	

END