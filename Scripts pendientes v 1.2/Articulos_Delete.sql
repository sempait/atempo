USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[Articulos_Delete]    Script Date: 06/15/2015 19:44:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Articulos_Delete]

	@pIDArticulo int

AS
BEGIN

	DELETE FROM Articulos WHERE IDArticulo = @pIDArticulo

END
