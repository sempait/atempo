USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[TiposArticulos_Delete]    Script Date: 06/15/2015 19:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[TiposArticulos_Delete]

	@pIDTipoArticulo int

AS
BEGIN
	DELETE FROM TiposArticulos WHERE IDTipoArticulo = @pIDTipoArticulo
END
