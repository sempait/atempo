USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[Salas_SelectOne]    Script Date: 03/02/2015 23:43:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Salas_SelectOnePorResourceID]

	@pResourceID int

AS
BEGIN

	SELECT *
	FROM Salas S
	WHERE S.ResourceID = @pResourceID	

END