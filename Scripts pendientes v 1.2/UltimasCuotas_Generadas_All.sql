USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[UltimasCuotas_Generadas_All]    Script Date: 03/17/2015 00:58:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[UltimasCuotas_Generadas_All]

	
AS
BEGIN

Select MAX(PM.MesPago) AS MesPago,MAX(PM.AñoPago) AS AñoPago,PM.IDInscripcion,I.FechaFinInscripcion
FROM PagosMensuales PM 
INNER JOIN Inscripciones I
ON PM.IDInscripcion = I.IDInscripcion
WHERE PM.AñoPago=YEAR(GETDATE()) AND I.FechaFinInscripcion IS NULL
GROUP BY PM.IDInscripcion,I.FechaFinInscripcion

--Select MAX(PM.MesPago) AS MesPago,MAX(PM.AñoPago) AS AñoPago,PM.IDInscripcion,I.FechaFinInscripcion
--FROM PagosMensuales PM 
--INNER JOIN Inscripciones I
--ON PM.IDInscripcion = I.IDInscripcion
--WHERE I.FechaFinInscripcion IS NULL
--GROUP BY PM.IDInscripcion,I.FechaFinInscripcion,pm.MesPago

END


