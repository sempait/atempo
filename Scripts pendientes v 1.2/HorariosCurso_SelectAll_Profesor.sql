USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[HorariosCurso_SelectAll_Profesor]    Script Date: 06/07/2015 21:51:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[HorariosCurso_SelectAll_Profesor]

	@pIDProfesor int

AS
BEGIN

	SELECT *
	FROM HorariosCurso HC
	INNER JOIN Cursos C
	ON HC.IDCurso = C.IDCurso
	WHERE HC.IDProfesor = @pIDProfesor
	AND HC.FechaBaja IS NULL

END
