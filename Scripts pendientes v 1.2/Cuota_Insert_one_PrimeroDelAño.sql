USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[Cuota_Insert_one_PrimeroDelAño]    Script Date: 03/17/2015 00:57:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Cuota_Insert_one_PrimeroDelAño]

	@pIDInscripcion int

AS
BEGIN

INSERT 
INTO PagosMensuales(IDInscripcion,MesPago,AñoPago) 
VALUES(@pIDInscripcion,1,YEAR(GETDATE()))

END
