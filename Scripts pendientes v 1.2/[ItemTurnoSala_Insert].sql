USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[ItemTurnoSala_Insert]    Script Date: 05/28/2015 20:46:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ItemTurnoSala_Insert]

	@pIDTurnoSala int,
	@pIDArticulo int,
	@pCantidadItemTurnoSala int

AS
BEGIN

	INSERT INTO ItemTurnoSala
	(
		IDTurnosSala,
		IDArticulo,
		CantidadItemTurnoSala
	)
	VALUES
	(
		@pIDTurnoSala,
		@pIDArticulo,
		@pCantidadItemTurnoSala
	)

END
