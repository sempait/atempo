USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[HorariosCurso_SelectAll_Asistencia]    Script Date: 06/07/2015 21:54:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[HorariosCurso_SelectAll_Asistencia]

	@pIDProfesor int,
	@pFechaAsistencia Datetime,
	@pNombreDia varchar(10)

AS
BEGIN

	

	SELECT *
	FROM HorariosCurso HC
	INNER JOIN Cursos C
	ON HC.IDCurso = C.IDCurso
	WHERE HC.IDProfesor = @pIDProfesor
	AND HC.DiaCurso = @pNombreDia
	AND HC.IDHorarioCurso NOT IN (
								SELECT HCC.IDHorarioCurso
								FROM AsistenciasProfesores ASP LEFT JOIN HorariosCurso HCC
								ON HCC.IDHorarioCurso=ASP.IDHorariosCurso
								WHERE HCC.IDProfesor=@pIDProfesor
								AND ASP.FechaAsistenciaProfesor=@pFechaAsistencia
								AND HCC.DiaCurso = @pNombreDia
								)
	AND HC.FechaBaja IS NULL						



END
