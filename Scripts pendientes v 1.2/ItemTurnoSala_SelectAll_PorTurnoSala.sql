USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[ItemTurnoSala_SelectAll_PorTurnoSala]    Script Date: 06/15/2015 23:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ItemTurnoSala_SelectAll_PorTurnoSala]

	@pIDTurnoSala int

AS
BEGIN

	SELECT IDItemTurnoSala, IDTurnosSala, its.IDArticulo as codigo, ar.DescripcionArticulo as descripcion, 
	CantidadItemTurnoSala as cantidad, hpa.PrecioArticulo as precio, hca.CostoArticulo as costo, ar.StockArticulo as stock
	FROM ItemTurnoSala as its
	INNER JOIN Articulos as ar
	ON ar.IDArticulo = its.IDArticulo
	INNER JOIN HistoricoPreciosArticulos as hpa
	ON hpa.IDArticulo = its.IDArticulo
	INNER JOIN HistoricoCostosArticulos as hca
	ON hca.IDArticulo = its.IDArticulo
	WHERE IDTurnosSala = @pIDTurnoSala AND FechaHastaPrecioArticulo IS NULL AND FechaHastaCostoArticulo IS NULL

END
