USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[Gastos_SelectOne]    Script Date: 12/30/2014 00:54:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Salas_SelectOne]

	@pUniqueID int

AS
BEGIN

	SELECT *
	FROM Salas S
	WHERE S.UniqueID = @pUniqueID	

END