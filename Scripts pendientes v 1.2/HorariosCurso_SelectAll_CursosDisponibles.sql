USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[HorariosCurso_SelectAll_CursosDisponibles]    Script Date: 07/04/2015 13:17:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[HorariosCurso_SelectAll_CursosDisponibles]

	@pIDCurso int,
	@pTipoCurso varchar(50)
	

AS
BEGIN

	SELECT HC.IDHorarioCurso,HC.DiaCurso,HC.HoraDesdeCurso,HC.HoraHastaCurso,P.IDProfesor, P.NombreYApellidoProfesor,C.IDCurso, C.DescripcionCurso,HC.CantidadAlumnos
	FROM HorariosCurso HC 
	LEFT JOIN Profesores P 
	ON P.IDProfesor=HC.IDProfesor 
	LEFT JOIN Cursos C 
	ON C.IDCurso=HC.IDCurso
	LEFT JOIN Inscripciones I ON I.IDHorariosCurso = HC.IDHorarioCurso
	WHERE HC.IDCurso = @pIDCurso
	AND HC.CantidadAlumnos < (CASE @pTipoCurso
									--Cuando el curso es Individual (ID = 1) la cantidad máxima de alumnos es 1
								   WHEN 'I' THEN 1
								    --Cuando el curso es Grupal (ID=2) la cantidad máxima de alumnos se encuentra definida en la tabla Parámetros
								   WHEN 'G' THEN (SELECT CantidadMaximaAlumnos FROM Parametros)
								   ELSE 0
							  END)

	AND P.FechaBajaProfesor IS NULL AND
	
	I.FechaFinInscripcion IS NULL AND
	
	HC.FechaBaja IS NULL

END
