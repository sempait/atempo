USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[HorariosCurso_Delete]    Script Date: 07/04/2015 13:25:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[HorariosCurso_Delete]

	@pIDHorarioCurso int
	

AS
DECLARE 
@pIDInscripcion int
BEGIN

	UPDATE HorariosCurso SET FechaBaja = GetDate()
	WHERE IDHorarioCurso = @pIDHorarioCurso
	
	
	--DOY DE BAJA LAS INSCRIPCIONES QUE CORRESPONDIAN A ESTE HORARIO CURSO.
	declare cursorInscForHorarioCurso cursor for
    SELECT IDInscripcion FROM Inscripciones
    WHERE IDHorariosCurso= @pIDHorarioCurso

open cursorInscForHorarioCurso
FETCH cursorInscForHorarioCurso INTO @pIDInscripcion

WHILE @@FETCH_STATUS=0
BEGIN
UPDATE Inscripciones SET FechaFinInscripcion= GETDATE()
WHERE IDInscripcion=@pIDInscripcion


FETCH NEXT FROM cursorInscForHorarioCurso INTO @pIDInscripcion
END

END
