USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[Cuota_Insert_one]    Script Date: 09/25/2015 10:34:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Cuota_Insert_one]
	@pMesPago int,
	@pAñoPago int,
	@pIDInscripcion int

AS
BEGIN

INSERT 
INTO PagosMensuales(IDInscripcion,MesPago,AñoPago) 
VALUES(@pIDInscripcion,@pMesPago,@pAñoPago)

END