USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[Inscripciones_Insert]    Script Date: 09/25/2015 10:34:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Inscripciones_Insert]

@pIDAlumno int,
	@pIDHorarioCurso int,
	@pModalidad varchar(1),
	@pFechaInicio datetime
AS
DECLARE 

@Individual BIT
BEGIN

IF(@pModalidad='I')
SET @Individual = 1
ELSE
SET @Individual = 0

EXECUTE [dbo].[HorariosCursos_Update_AumentarCantidadAlumnos] @pIDHorarioCurso

 
	INSERT INTO Inscripciones
	(
		FechaInicioInscripcion,
		IDAlumno,
		IDHorariosCurso,
		Modalidad
		
	)
	VALUES
	(
		@pFechaInicio,
		@pIDAlumno,
		@pIDHorarioCurso,
		@pModalidad
	)
	
	UPDATE HorariosCurso
	SET Individual=@Individual
	
	WHERE IDHorarioCurso = @pIDHorarioCurso
	
	
	INSERT INTO PagosMensuales
	(
	IDInscripcion,
	MesPago,
	AñoPago
	)
	
	VALUES
	(
	@@IDENTITY,
	MONTH(@pFechaInicio),
	YEAR(@pFechaInicio)
	
	
	)

END

