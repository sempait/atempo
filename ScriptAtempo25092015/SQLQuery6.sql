USE [EscuelaDeMusica]
GO
/****** Object:  StoredProcedure [dbo].[Alumnos_SelectAll_ConCuotasPendientes]    Script Date: 09/25/2015 11:14:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Alumnos_SelectAll_ConCuotasPendientes]

AS
BEGIN

	DECLARE @Mes int = MONTH(GETDATE())
	DECLARE @Anio int = YEAR(GETDATE())

	SELECT DISTINCT A.*
	FROM Alumnos A
	INNER JOIN Inscripciones I
	ON A.IDAlumno = I.IDAlumno
	LEFT JOIN PagosMensuales PM
	ON I.IDInscripcion = PM.IDInscripcion
	WHERE I.FechaFinInscripcion IS NULL
	--AND PM.MesPago = @Mes
	--AND PM.AñoPago = @Anio
	AND PM.FechaPagoMensual IS NULL
	AND A.FechaBaja IS NULL

END
