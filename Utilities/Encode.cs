﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Utilities
{
    public static class Encode
    {
        public static string GetMD5(string String)
        {
            StringBuilder oStringBuilder = new StringBuilder();

            byte[] Stream = MD5CryptoServiceProvider.Create().ComputeHash(new ASCIIEncoding().GetBytes(String));

            for (int i = 0; i < Stream.Length; i++) 
                oStringBuilder.AppendFormat("{0:x2}", Stream[i]);

            return oStringBuilder.ToString();
        }
    }
}
