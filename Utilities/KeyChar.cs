﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Utilities
{
    public class KeyChar
    {
        public static bool OnlyDigits(KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
                return true;
            else
                return false;
        }

        public static bool OnlyDigits(KeyEventArgs e)
        {
            if (Char.IsDigit((char)e.KeyCode) || Char.IsControl((char)e.KeyCode))
                return true;
            else
                return false;
        }

        public static bool OnlyLettersAndSpaces(KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar) || Char.IsControl(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar))
                return true;
            else
                return false;
        }

        public static bool OnlyLettersAndDigits(KeyPressEventArgs e)
        {
            if (Char.IsLetterOrDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
                return true;
            else
                return false;
        }
    }
}
