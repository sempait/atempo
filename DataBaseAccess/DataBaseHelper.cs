﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;
using System.Configuration;

namespace DataBaseAccess
{
    public class DataBaseHelper
    {
        private static DataBaseHelper _oDataBaseHelper = null;

        protected DataBaseHelper()
        {
            oSqlCommand = new SqlCommand();
        }

        public static DataBaseHelper Instance
        {
            get
            {
                if (_oDataBaseHelper == null)
                    _oDataBaseHelper = new DataBaseHelper();

                return _oDataBaseHelper;
            }
        }

        private SqlConnection oConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["EscuelaDeMusicaConnectionString"].ConnectionString);

        private SqlCommand oSqlCommand;

        public SqlCommand Command
        {
            get
            {
                return oSqlCommand;
            }
        }

        public void AddParameter(string VariableName, object ParameterValue)
        {
            oSqlCommand.Parameters.Add(new SqlParameter(VariableName, ParameterValue));
        }

        public DataTable ExecuteDataTable(string StoreProcedureName)
        {
            DataTable oDataTable = null;

            try
            {
                using (TransactionScope oTransactionScope = new TransactionScope())
                {
                    oConnection.Open();

                    oSqlCommand.CommandText = StoreProcedureName;
                    oSqlCommand.CommandType = CommandType.StoredProcedure;
                    oSqlCommand.Connection = oConnection;

                    SqlDataReader oDataReader = oSqlCommand.ExecuteReader();

                    oDataTable = new DataTable();

                    oDataTable.Load(oDataReader);

                    oTransactionScope.Complete();

                    oConnection.Close();
                }
            }
            catch (Exception oException)
            {
                oConnection.Close();
               throw oException;
            }
            finally
            {
                oSqlCommand.Parameters.Clear();
            }

            return oDataTable;
        }

        public object ExecuteScalar(string StoreProcedureName)
        {
            object o = null;

            try
            {
                using (TransactionScope oTransactionScope = new TransactionScope())
                {
                    oConnection.Open();

                    oSqlCommand.CommandText = StoreProcedureName;
                    oSqlCommand.CommandType = CommandType.StoredProcedure;
                    oSqlCommand.Connection = oConnection;

                    o = oSqlCommand.ExecuteScalar();
                    oTransactionScope.Complete();

                    oConnection.Close();
                }
            }
            catch (Exception oException)
            {
                throw oException;
            }
            finally
            {
                oConnection.Close();
                oSqlCommand.Parameters.Clear();
            }

            return o;
        }

        public object ExecuteNonQuery(string StoreProcedureName)
        {
            object o = null;

            try
            {
                using (TransactionScope oTransactionScope = new TransactionScope())
                {
                    oConnection.Open();

                    oSqlCommand.CommandText = StoreProcedureName;
                    oSqlCommand.CommandType = CommandType.StoredProcedure;
                    oSqlCommand.Connection = oConnection;

                    o = oSqlCommand.ExecuteNonQuery();
                    oTransactionScope.Complete();

                    oConnection.Close();
                }
            }
            catch (Exception oException)
            {
                oConnection.Close();
                throw oException;
            }
            finally
            {
                oConnection.Close();
                oSqlCommand.Parameters.Clear();
            }

            return o;
        }
    }
}
