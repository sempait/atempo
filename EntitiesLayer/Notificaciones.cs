﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class Notificaciones : Entity
    {
        public string Notificacion { set; get; }

        public DateTime FechaPublicacionNotificacion { set; get; }
    }
}
