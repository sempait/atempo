﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.Bar
{
    public class Ventas : Entity
    {
        public DateTime Hora { set; get; }

        public DateTime Fecha { set; get; }

        public float Importe { set; get; }

        private List<Detalles> oDetalles = new List<Detalles>();

        public List<Detalles> Detalles { set { oDetalles = value; } get { return oDetalles; } }
    }
}
