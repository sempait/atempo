﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.Bar
{
    public class GastosBar:Entity
    {
        public string DescripcionGasto { set; get; }

        public float Improte { set; get; }

        public Boolean AftectaCaja { set; get; }

        public DateTime FechaGasto { set; get; }
    }
}
