﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.Bar.DataMapperClasses
{
    public class ArticulosDataMapper : DataMapperClass<Articulos>
    {
        #region DataTable Variables

        private string Descripcion = "DescripcionArticulo";
        private string IDTipoArticulo = "IDTipoArticulo";
        private string Stock = "StockArticulo";
        private string FechaBaja = "FechaBajaArticulo";
        private string IDDetalle = "IDDetalle";
        private string IDHistoricoPrecioArticulo = "IDHistoricoPrecioArticulo";
        private string IDHistoricoCostoArticulo = "IDHistoricoCostoArticulo";

        #endregion

        public ArticulosDataMapper()
        {
            ID = "IDArticulo";
        }

        #region Methods

        public override Articulos DataMapping(Articulos Articulo, DataRow oDataRow, General.Clases Clase)
        {
            if (Articulo.ID == 0)
            {
                Articulo.ID = GetValue<int>(oDataRow, ID);
                Articulo.Stock = GetValue<int>(oDataRow, Stock);
                Articulo.FechaBaja = GetValue<DateTime?>(oDataRow, FechaBaja);
                Articulo.DescripcionArticulo = GetValue<string>(oDataRow, Descripcion);
                Articulo.TipoArticulo = GetEntity<TiposArticulos, TiposArticulosDataMapper>(oDataRow, IDTipoArticulo, Clase, General.Clases.TiposArticulos, General.Clases.Articulos);
            }

            CompleteEntity<HistoricoCostoArticulosDataMapper, HistoricoCostosArticulos, List<HistoricoCostosArticulos>>(Articulo.HistoricoCostoArticulo, oDataRow, IDHistoricoCostoArticulo, Clase, General.Clases.HistoricoCostosArticulos, General.Clases.Articulos);
            CompleteEntity<HistoricoPreciosArticulosDataMapper, HistoricoPreciosArticulos, List<HistoricoPreciosArticulos>>(Articulo.HistoricoPrecioArticulo, oDataRow, IDHistoricoPrecioArticulo, Clase, General.Clases.HistoticoPreciosArticulos, General.Clases.Articulos);
            CompleteEntity<DetallesDataMapper, Detalles, List<Detalles>>(Articulo.Detalles, oDataRow, IDDetalle, Clase, General.Clases.Detalles, General.Clases.Articulos);

            return Articulo;
        }

        #endregion
    }
}
