﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.Bar.DataMapperClasses
{
    public class GastosBarDataMapper:DataMapperClass<GastosBar>
    {
        #region DataTable Variables

        private string DescripcionGasto = "DescripcionGasto";
        private string Improte = "Improte";
        private string AftectaCaja = "AftectaCaja";
        private string FechaGasto = "FechaGasto";

        #endregion

           #region Methods
        
        public GastosBarDataMapper()
        {
            ID = "Id_Gasto";
        }


         public override GastosBar DataMapping(GastosBar gasto, DataRow oDataRow, General.Clases Clase)
        {
            if (gasto.ID == 0)
            {
                gasto.ID = GetValue<int>(oDataRow, ID);
                gasto.FechaGasto = GetValue<DateTime>(oDataRow,FechaGasto);
                gasto.DescripcionGasto = GetValue<string>(oDataRow, DescripcionGasto);
                gasto.Improte = GetValue<float>(oDataRow, Improte);
                gasto.AftectaCaja = GetValue<Boolean>(oDataRow, AftectaCaja);
            }

           
            return gasto;
        }
        #endregion
    }
}
