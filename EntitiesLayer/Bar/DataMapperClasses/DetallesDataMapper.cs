﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.Bar.DataMapperClasses
{
    public class DetallesDataMapper : DataMapperClass<Detalles>
    {
        #region DataTable Variables

        private string Cantidad = "CantidadDetalle";
        private string IDArticulo = "IDArticulo";
        private string IDVenta = "IDVenta";

        #endregion

        public DetallesDataMapper ()
        {
            ID = "IDDetalle";
        }

        #region Methods

        public override Detalles DataMapping(Detalles Detalle, DataRow oDataRow, General.Clases Clase)
        {
            if (Detalle.ID == 0)
            {
                Detalle.ID = GetValue<int>(oDataRow,ID);
                Detalle.Cantidad = GetValue<int>(oDataRow,Cantidad);
                Detalle.Articulo = GetEntity<Articulos,ArticulosDataMapper>(oDataRow,IDArticulo,Clase,General.Clases.Articulos,General.Clases.Detalles);
                Detalle.Venta = GetEntity<Ventas,VentasDataMapper>(oDataRow,IDVenta,Clase,General.Clases.Ventas,General.Clases.Detalles);
            }

            return Detalle;
        }

        #endregion
    }
}
