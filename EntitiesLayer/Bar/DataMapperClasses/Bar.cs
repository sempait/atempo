﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.Bar.DataMapperClasses
{
    public class Bar
    {
        public enum Clases
        {
            Articulos,
            Detalles,
            HistoricoCostosArticulos,
            HistoticoPreciosArticulos,
            TiposArticulos,
            Ventas,
            Ninguna
        }
    }
}
