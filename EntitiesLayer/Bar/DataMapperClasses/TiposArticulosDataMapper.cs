﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.Bar.DataMapperClasses
{
    public class TiposArticulosDataMapper : DataMapperClass<TiposArticulos>
    {
        #region DataTable Variables

        private string Descripcion = "DescripcionTipoArticulo";
        private string FechaBaja = "FechaBajaTipoArticulo";
        private string IDArticulo = "IDArticulo";

        #endregion

        public TiposArticulosDataMapper()
        {
            ID = "IDTipoArticulo";
        }

        #region Methods

        public override TiposArticulos DataMapping(TiposArticulos TipoArticulo, DataRow oDataRow, General.Clases Clase)
        {
            if (TipoArticulo.ID == 0)
            {
                TipoArticulo.ID = GetValue<int>(oDataRow, ID);
                TipoArticulo.FechaBaja = GetValue<DateTime?>(oDataRow, FechaBaja);
                TipoArticulo.DescripcionTipoArticulo = GetValue<string>(oDataRow, Descripcion);
            }

            CompleteEntity<ArticulosDataMapper, Articulos, List<Articulos>>(TipoArticulo.Articulos, oDataRow, IDArticulo, Clase, General.Clases.Articulos, General.Clases.TiposArticulos);

            return TipoArticulo;
        }

        #endregion
    }
}
