﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.Bar.DataMapperClasses
{
    public class VentasDataMapper : DataMapperClass<Ventas>
    {
        #region DataTable Variables

        private string Hora = "HoraVenta";
        private string Fecha = "FechaVenta";
        private string IDDetalle = "IDDetalle";
        private string Importe = "ImporteTotalVenta";


        #endregion

        public VentasDataMapper()
        {
            ID = "IDVenta";
        }

        #region Methods

        public override Ventas DataMapping(Ventas Venta, DataRow oDataRow, General.Clases Clase)
        {
            if (Venta.ID == 0)
            {
                Venta.ID = GetValue<int>(oDataRow, ID);
                Venta.Hora = GetValue<DateTime>(oDataRow, Hora);
                Venta.Fecha = GetValue<DateTime>(oDataRow, Fecha);
                Venta.Importe = GetValue<float>(oDataRow, Importe);


            }

            CompleteEntity<DetallesDataMapper, Detalles, List<Detalles>>(Venta.Detalles, oDataRow, IDDetalle, Clase, General.Clases.Detalles, General.Clases.Ventas);

            return Venta;
        }

        #endregion
    }
}
