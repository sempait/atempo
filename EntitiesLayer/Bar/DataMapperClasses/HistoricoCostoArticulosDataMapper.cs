﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.Bar.DataMapperClasses
{
    public class HistoricoCostoArticulosDataMapper : DataMapperClass<HistoricoCostosArticulos>
    {
        #region DataTable Variables

        private string FechaHasta = "FechaHastaCostoArticulo";
        private string Costo = "CostoArticulo";
        private string IDArticulo = "IDArticulo";

        #endregion

        public HistoricoCostoArticulosDataMapper()
        {
            ID = "IDHistoricoCostoArticulo";
        }

        #region Methods

        public override HistoricoCostosArticulos DataMapping(HistoricoCostosArticulos HistoricoCostoArticulo, DataRow oDataRow, General.Clases Clase)
        {
            if (HistoricoCostoArticulo.ID == 0)
            {
                HistoricoCostoArticulo.ID = GetValue<int>(oDataRow, ID);
                HistoricoCostoArticulo.Costo = GetValue<float>(oDataRow, Costo);
                HistoricoCostoArticulo.FechaHasta = GetValue<DateTime?>(oDataRow, FechaHasta);
                HistoricoCostoArticulo.Articulo = GetEntity<Articulos, ArticulosDataMapper>(oDataRow, IDArticulo, Clase, General.Clases.Articulos, General.Clases.HistoricoCostosArticulos);
            }

            return HistoricoCostoArticulo;
        }

        #endregion
    }
}
