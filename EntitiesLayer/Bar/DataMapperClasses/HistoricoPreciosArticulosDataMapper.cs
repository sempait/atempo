﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.Bar.DataMapperClasses
{
    public class HistoricoPreciosArticulosDataMapper : DataMapperClass<HistoricoPreciosArticulos>
    {
        #region DataTable Variables

        private string FechaHasta = "FechaHastaPrecioArticulo";
        private string Precio = "PrecioArticulo";
        private string IDArticulo = "IDArticulo";

        #endregion

        public HistoricoPreciosArticulosDataMapper()
        {
            ID = "IDHistoricoPrecioArticulo";
        }

        #region Methods

        public override HistoricoPreciosArticulos DataMapping(HistoricoPreciosArticulos HistoricoPrecioArticulo, DataRow oDataRow, General.Clases Clase)
        {
            if (HistoricoPrecioArticulo.ID == 0)
            {
                HistoricoPrecioArticulo.ID = GetValue<int>(oDataRow, ID);
                HistoricoPrecioArticulo.Precio = GetValue<float>(oDataRow, Precio);
                HistoricoPrecioArticulo.FechaHasta = GetValue<DateTime?>(oDataRow, FechaHasta);
                HistoricoPrecioArticulo.Articulo = GetEntity<Articulos, ArticulosDataMapper>(oDataRow, IDArticulo, Clase, General.Clases.Articulos, General.Clases.HistoticoPreciosArticulos);
            }

            return HistoricoPrecioArticulo;
        }

        #endregion
    }
}
