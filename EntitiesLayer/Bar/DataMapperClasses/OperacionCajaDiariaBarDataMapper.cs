﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.Bar.DataMapperClasses
{
   public class OperacionCajaDiariaBarDataMapper: DataMapperClass<OperacionCajaDiariaBar>
    {

        #region DataTable Variables

        private string DescripcionOperacion="DescripcionOperacion";
        private string Egreso = "Egreso";
        private string Ingreso = "Ingreso";
        private string FechaOperacion = "FechaOperacion";

        #endregion

        #region Methods
        
        public OperacionCajaDiariaBarDataMapper()
        {
            ID = "Id_Caja_Bar";
        }


         public override OperacionCajaDiariaBar DataMapping(OperacionCajaDiariaBar operacion, DataRow oDataRow, General.Clases Clase)
        {
            if (operacion.ID == 0)
            {
                operacion.ID = GetValue<int>(oDataRow, ID);
                operacion.FechaOperacion = GetValue<DateTime?>(oDataRow, FechaOperacion);
                operacion.DescripcionOperacion = GetValue<string>(oDataRow, DescripcionOperacion);
                operacion.Egreso= GetValue<float>(oDataRow, Egreso);
                operacion.Ingreso = GetValue<float>(oDataRow, Ingreso);
            }

           
            return operacion;
        }
        #endregion
    }
}
