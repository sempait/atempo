﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.Bar
{
    public class Articulos : Entity
    {
        public string DescripcionArticulo { set; get; }

        public TiposArticulos TipoArticulo { set; get; }

        public int Stock { set; get; }

        private List<Detalles> oDetalles = new List<Detalles>();

        public List<Detalles> Detalles { set { oDetalles = value; } get { return oDetalles; } }

        private List<HistoricoPreciosArticulos> oHistoricosPreciosArticulos = new List<HistoricoPreciosArticulos>();

        public List<HistoricoPreciosArticulos> HistoricoPrecioArticulo { set { oHistoricosPreciosArticulos = value; } get { return oHistoricosPreciosArticulos; } }

        private List<HistoricoCostosArticulos> oHistoricoCostosArticulos = new List<HistoricoCostosArticulos>();

        public List<HistoricoCostosArticulos> HistoricoCostoArticulo { set { oHistoricoCostosArticulos = value; } get { return oHistoricoCostosArticulos; } }

        public DateTime? FechaBaja { set; get; }
    }
}
