﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.Bar
{
    public class Detalles : Entity
    {
        public int Cantidad { set; get; }

        public Articulos Articulo { set; get; }

        public Ventas Venta { set; get; }
    }
}
