﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.Bar
{
    public class TiposArticulos : Entity
    {
        public string DescripcionTipoArticulo { set; get; }

        private List<Articulos> oArticulos = new List<Articulos>();

        public List<Articulos> Articulos { set { oArticulos = value; } get { return oArticulos; } }

        public DateTime? FechaBaja { set; get; }
    }
}
