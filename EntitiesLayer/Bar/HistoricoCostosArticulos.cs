﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.Bar
{
    public class HistoricoCostosArticulos : Entity
    {
        public DateTime? FechaHasta { set; get; }

        public float Costo { set; get; }

        public Articulos Articulo { set; get; }
    }
}
