﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.Bar
{
    public class HistoricoPreciosArticulos : Entity
    {
        public DateTime? FechaHasta { set; get; }

        public float Precio { set; get; }

        public Articulos Articulo { set; get; }
    }
}
