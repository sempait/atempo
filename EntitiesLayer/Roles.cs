﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class Roles : Entity
    {
        public string DescripcionRol { set; get; }

        public bool Activo { set; get; }

        private List<Usuarios> oUsuarios = new List<Usuarios>();

        public List<Usuarios> Usuarios { set { oUsuarios = value; } get { return oUsuarios; } }

        public DateTime? FechaBaja { set; get; }
    }
}
