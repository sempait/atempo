﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class General
    {
        public enum Clases
        {
            Usuarios,
            Roles,
            Alumnos,
            AsistenciasAlumnos,
            AsistenciasProfesores,
            Cursos,
            CursosProfesores,
            HistoricoPrecios,
            HorariosCursos,
            HorariosDisponibilidad,
            Inscripciones,
            PagosMensuales,
            Profesores,
            TiposCurso,
            Notificacion,
            ItemTurnoSala,
            TurnosSala,
            Articulos,
            Detalles,
            HistoricoCostosArticulos,
            HistoticoPreciosArticulos,
            TiposArticulos,
            Ventas,
            Ninguna
        }
    }
}
