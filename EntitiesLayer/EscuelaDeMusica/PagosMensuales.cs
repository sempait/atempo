﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace EntitiesLayer
{
    public class PagosMensuales : Entity
    {
        public int Mes { set; get; }

        public int Anio { set; get; }

        public DateTime? FechaPago { set; get; }

        public float MontoAbonado { set; get; }

        public Inscripciones Inscripcion { set; get; }

        public string MesString
        {
            get
            {
                string mes = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Mes);
                return mes.Replace(mes[0], mes[0].ToString().ToUpper().ToCharArray()[0]);
            }
        }

        public float MontoMes { set; get; }
    }
}
