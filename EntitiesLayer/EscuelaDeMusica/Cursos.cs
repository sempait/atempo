﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class Cursos : Entity
    {
        public string DescripcionCurso { set; get; }

        public TiposCurso TipoCurso { set; get; }

        public float PrecioGrupal {set;get;}

        public float PrecioIndividual {set;get;}

        private List<HorariosCursos> oHorariosCursos = new List<HorariosCursos>();

        public List<HorariosCursos> HorariosCursos { set { oHorariosCursos = value; } get { return oHorariosCursos; } }

        private List<CursosProfesores> oCursosProfesores = new List<CursosProfesores>();

        public List<CursosProfesores> CursosProfesores { set { oCursosProfesores = value; } get { return oCursosProfesores; } }

        public DateTime? FechaBaja { set; get; }

        public string DescripcionCursoConTipo 
        { 
            get 
            {
                if (TipoCurso != null)
                    return DescripcionCurso + "(" + TipoCurso.DescripcionTipoCurso + ")";
                else
                    return DescripcionCurso;
            } 
        }
    }
}
