﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class AsistenciasAlumnos : Entity
    {
        public int Dia { set; get; }

        public int Mes { set; get; }

        public int Anio { set; get; }

        public bool Asistencia { set; get; }

        public Inscripciones Inscripcion { set; get; }
    }
}
