﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class Profesores : Entity
    {
        public string NombreYApellidoProfesor { set; get; }
        public string Direccion { set; get; }
        public string Telefono { set; get; }
        public string Email { set; get; }
        public string Ciudad { set; get; }
        public DateTime? FechaBaja { set; get; }
        public string DNI { set; get; }
        public DateTime? FechaNacimiento { set; get; }

        private List<CursosProfesores> oCursosProfesores = new List<CursosProfesores>();

        public List<CursosProfesores> CursosProfesores { set { oCursosProfesores = value; } get { return oCursosProfesores; } }
    }
}
