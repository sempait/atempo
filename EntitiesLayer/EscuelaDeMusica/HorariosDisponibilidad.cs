﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class HorariosDisponibilidad : Entity
    {
        public string Dia { set; get; }

        public TimeSpan HoraDesde { set; get; }

        public TimeSpan HoraHasta { set; get; }

        public Profesores Profesor { set; get; }
    }
}
