﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class Alumnos : Entity
    {
        public string NombreYApellido { set; get; }

        public string Email { set; get; }

        public string Telefono { set; get; }

        public string NombreYApellidoPadre { set; get; }

        public string NombreYApellidoMadre { set; get; }

        public string TrabajoPadre { set; get; }

        public string TrabajoMadre { set; get; }

        public string TelefonoTrabajoPadre { set; get; }

        public string TelefonoTrabajoMadre { set; get; }

        public string Comentario { set; get; }

        public string TelefonoPadre { set; get; }

        public string TelefonoMadre { set; get; }

        public string Domicilio { set; get; }

        public string Tutor { set; get; }

        public string TelefonoTutor { set; get; }

        public string DNI { set; get; }

        public string Colegio { set; get; }

        public DateTime? FechaNacimiento { set; get; }

        private List<Inscripciones> oInscripciones = new List<Inscripciones>();
        public List<Inscripciones> Inscripciones { get { return oInscripciones; } set { oInscripciones = value; } }
    }
}
