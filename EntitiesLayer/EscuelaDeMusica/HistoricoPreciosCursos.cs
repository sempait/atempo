﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class HistoricoPreciosCursos : Entity
    {
        public float Precio { set; get; }

        public DateTime? FechaHasta { set; get; }

        public Cursos Curso { set; get; }
    }
}
