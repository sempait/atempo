﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class CursosProfesores : Entity
    {
        public Cursos Curso { set; get; }

        public Profesores Profesor { set; get; }

        public DateTime? FechaBaja { set; get; }
    }
}
