﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class AsistenciasProfesores : Entity
    {
        public DateTime FechaAsistencia { set; get; }

        public HorariosCursos HorarioCurso { set; get; }
    }
}
