﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class TiposCurso : Entity
    {
        public string DescripcionTipoCurso { set; get; }

        private List<Cursos> oCursos = new List<Cursos>();

        public List<Cursos> Cursos { set { oCursos = value; } get { return oCursos; } }

        public DateTime? FechaBaja { set; get; }
    }
}
