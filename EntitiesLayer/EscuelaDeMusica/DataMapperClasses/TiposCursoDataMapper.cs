﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class TiposCursoDataMapper : DataMapperClass<TiposCurso>
    {
        #region DataTable Variables

        private string Descripcion = "DescripcionTipoCurso";
        private string IDCurso = "IDCurso";

        #endregion

        public TiposCursoDataMapper()
        {
            ID = "IDTipoCurso";
        }

        #region Methods

        public override TiposCurso DataMapping(TiposCurso TipoCurso, DataRow oDataRow, General.Clases Clase)
        {
            if (TipoCurso.ID == 0)
            {
                TipoCurso.ID = GetValue<int>(oDataRow, ID);
                TipoCurso.DescripcionTipoCurso = GetValue<string>(oDataRow, Descripcion);
            }

            CompleteEntity<CursosDataMapper, Cursos, List<Cursos>>(TipoCurso.Cursos, oDataRow, IDCurso, Clase, General.Clases.Cursos, General.Clases.TiposCurso);

            return TipoCurso;
        }

        #endregion
    }
}
