﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class CursosProfesoresDataMapper : DataMapperClass<CursosProfesores>
    {
        #region DataTable Variables

        private string IDCurso = "IDCurso";
        private string IDProfesor = "IDProfesor";
        private string FechaBaja = "FechaBajaCursoProfesor";

        #endregion

        public CursosProfesoresDataMapper()
        {
            ID = "IDCursoProfesor";
        }

        #region Methods

        public override CursosProfesores DataMapping(CursosProfesores CursoProfesor, DataRow oDataRow, General.Clases Clase)
        {
            if (CursoProfesor.ID == 0)
            {
                CursoProfesor.ID = GetValue<int>(oDataRow, ID);
                CursoProfesor.FechaBaja = GetValue<DateTime?>(oDataRow, FechaBaja);
                CursoProfesor.Curso = GetEntity<Cursos, CursosDataMapper>(oDataRow, IDCurso, Clase, General.Clases.Cursos, General.Clases.CursosProfesores);
                CursoProfesor.Profesor = GetEntity<Profesores, ProfesoresDataMapper>(oDataRow, IDProfesor, Clase, General.Clases.Profesores, General.Clases.CursosProfesores);
            }

            return CursoProfesor;
        }

        #endregion
    }
}
