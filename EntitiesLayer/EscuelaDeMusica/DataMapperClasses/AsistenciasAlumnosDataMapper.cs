﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class AsistenciasAlumnosDataMapper : DataMapperClass<AsistenciasAlumnos>
    {
        #region DataTable Variables

        private string Dia = "DiaAsistenciaAlumno";
        private string Mes = "MesAsistenciaAlumno";
        private string Anio = "AñoAsistenciaAlumno";
        private string Asistencia = "AsistenciaAlumno";
        private string IDInscripcion = "IDInscripcion";

        #endregion

        public AsistenciasAlumnosDataMapper()
        {
            ID = "IDAsistenciaAlumno";
        }

        #region Methods

        public override AsistenciasAlumnos DataMapping(AsistenciasAlumnos AsistenciaAlumno, DataRow oDataRow, General.Clases Clase)
        {
            if (AsistenciaAlumno.ID == 0)
            {
                AsistenciaAlumno.ID = GetValue<int>(oDataRow, ID);
                AsistenciaAlumno.Dia = GetValue<int>(oDataRow, Dia);
                AsistenciaAlumno.Mes = GetValue<int>(oDataRow, Mes);
                AsistenciaAlumno.Anio = GetValue<int>(oDataRow, Anio);
                AsistenciaAlumno.Asistencia = GetValue<bool>(oDataRow, Asistencia);
                AsistenciaAlumno.Inscripcion = GetEntity<Inscripciones, InscripcionesDataMapper>(oDataRow, IDInscripcion, Clase, General.Clases.Inscripciones, General.Clases.AsistenciasAlumnos);
            }

            return AsistenciaAlumno;
        }

        #endregion
    }
}
