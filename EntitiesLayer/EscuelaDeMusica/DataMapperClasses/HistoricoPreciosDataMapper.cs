﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class HistoricoPreciosCursosDataMapper : DataMapperClass<HistoricoPreciosCursos>
    {
        #region DataTable Variables

        private string Precio = "PrecioCurso";
        private string FechaHasta = "FechaHastaPrecioCurso";
        private string IDCurso = "IDCurso";

        #endregion

        public HistoricoPreciosCursosDataMapper()
        {
            ID = "IDHistoricoPrecio";
        }

        #region Methods

        public override HistoricoPreciosCursos DataMapping(HistoricoPreciosCursos HistoricoPrecioCurso, DataRow oDataRow, General.Clases Clase)
        {
            if (HistoricoPrecioCurso.ID == 0)
            {
                HistoricoPrecioCurso.ID = GetValue<int>(oDataRow, ID);
                HistoricoPrecioCurso.Precio = GetValue<float>(oDataRow, Precio);
                HistoricoPrecioCurso.FechaHasta = GetValue<DateTime?>(oDataRow, FechaHasta);
                HistoricoPrecioCurso.Curso = GetEntity<Cursos, CursosDataMapper>(oDataRow, IDCurso, Clase, General.Clases.Cursos, General.Clases.HistoricoPrecios);
            }

            return HistoricoPrecioCurso;
        }

        #endregion
    }
}
