﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class AsistenciasProfesoresDataMapper :DataMapperClass<AsistenciasProfesores>
    {
        #region DataTable Variables

        private string Dia = "DiaAsistenciaProfesor";
        private string Mes = "MesAsistenciaProfesor";
        private string Año = "AñoAsistenciaProfesor";
        private string Asistencia = "AsistenciaProfesor";

        #endregion

        #region Methods

        public AsistenciasProfesoresDataMapper()
        {
            ID = "IDAsistenciaProfesor";
        }

        public override AsistenciasProfesores DataMapping(AsistenciasProfesores AsistenciaProfesor, DataRow oDataRow, General.Clases Clase)
        {
            if (AsistenciaProfesor.ID == 0)
            {
                AsistenciaProfesor.ID = GetValue<int>(oDataRow, ID);
                AsistenciaProfesor.FechaAsistencia = GetValue<DateTime>(oDataRow, Dia);
                AsistenciaProfesor.HorarioCurso = GetEntity<HorariosCursos, HorariosCursosDataMapper>(oDataRow, "IDHorariosCurso", Clase, General.Clases.HorariosCursos, General.Clases.AsistenciasProfesores);
            }

            return AsistenciaProfesor;
        }

        #endregion
    }
}
