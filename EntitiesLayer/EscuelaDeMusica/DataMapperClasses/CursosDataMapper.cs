﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class CursosDataMapper : DataMapperClass<Cursos>
    {
        #region DataTable Variables

        private string Descripcion = "DescripcionCurso";
        //private string IDTipoCurso = "IDTipoCurso";
        private string FechaBaja = "FechaBajaCurso";
        private string IDHistoricoPrecio = "IDHistoricoPrecioCurso";
        private string IDCursoProfesor = "IDCursoProfesor";
        private string IDHorarioCurso = "IDHorarioCurso";
        private string PrecioGrupal = "PrecioGrupal";
        private string PrecioIndividual = "PrecioIndividual";

        #endregion

        public CursosDataMapper()
        {
            ID = "IDCurso";
            
        }

        #region Methods

        public override Cursos DataMapping(Cursos Curso, DataRow oDataRow, General.Clases Clase)
        {
            if (Curso.ID == 0)
            {
                Curso.ID = GetValue<int>(oDataRow, ID);
                Curso.FechaBaja = GetValue<DateTime?>(oDataRow, FechaBaja);
                Curso.DescripcionCurso = GetValue<string>(oDataRow, Descripcion);
                Curso.PrecioGrupal = GetValue<float>(oDataRow, PrecioGrupal);
                Curso.PrecioIndividual = GetValue<float>(oDataRow, PrecioIndividual);
               // Curso.TipoCurso = GetEntity<TiposCurso, TiposCursoDataMapper>(oDataRow, IDTipoCurso, Clase, General.Clases.TiposCurso, General.Clases.Cursos);
            }

            CompleteEntity<CursosProfesoresDataMapper, CursosProfesores, List<CursosProfesores>>(Curso.CursosProfesores, oDataRow, IDCursoProfesor, Clase, General.Clases.CursosProfesores, General.Clases.Cursos);
            
            CompleteEntity<HorariosCursosDataMapper, HorariosCursos, List<HorariosCursos>>(Curso.HorariosCursos, oDataRow, IDHorarioCurso, Clase, General.Clases.HorariosCursos, General.Clases.Cursos);

            return Curso;
        }

        #endregion
    }
}
