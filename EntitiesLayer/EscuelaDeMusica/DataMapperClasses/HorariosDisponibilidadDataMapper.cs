﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace EntitiesLayer.EscuelaDeMusica
{
    public class HorariosDisponibilidadDataMapper : DataMapperClass<HorariosDisponibilidad>
    {
        #region DataTable Variables

        private string Dia = "DiaDisponibilidad";
        private string HoraDesdeDisponibilidad = "HoraDesdeDisponibilidad";
        private string HoraHastaDisponibilidad = "HoraHastaDisponibilidad";
        private string IDProfesor = "IDProfesor";

        #endregion

        public HorariosDisponibilidadDataMapper()
        {
            ID = "IDHorarioDisponibilidad";
        }

        #region Methods

        public override HorariosDisponibilidad DataMapping(HorariosDisponibilidad HorarioDisponibilidad, DataRow oDataRow, General.Clases Clase)
        {
            if (HorarioDisponibilidad.ID == 0)
            {
                HorarioDisponibilidad.ID = GetValue<int>(oDataRow, ID);
                HorarioDisponibilidad.Dia = GetValue<string>(oDataRow, Dia);
                HorarioDisponibilidad.HoraDesde = GetValue<TimeSpan>(oDataRow, HoraDesdeDisponibilidad);
                HorarioDisponibilidad.HoraHasta = GetValue<TimeSpan>(oDataRow, HoraHastaDisponibilidad);
                HorarioDisponibilidad.Profesor = GetEntity<Profesores, ProfesoresDataMapper>(oDataRow, IDProfesor, Clase, General.Clases.Profesores, General.Clases.HorariosDisponibilidad);
            }

            return HorarioDisponibilidad;
        }

        #endregion
    }
}
