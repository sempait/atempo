﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class InscripcionesDataMapper : DataMapperClass<Inscripciones>
    {
        #region DataTable Variables

        private string FechaInicio = "FechaInicioInscripcion";
        private string FechaFin = "FechaFinInscripcion";
        private string IDAlumno = "IDAlumno";
        private string IDHorarioCurso = "IDHorarioCurso";
        private string IDPagoMensual = "IDPagoMensual";
        private string IDAsistenciaAlumno = "IDAsistenciaAlumno";
        private string Modalidad = "Modalidad";

        #endregion

        #region Methods

        public InscripcionesDataMapper()
        {
            ID = "IDInscripcion";
        }

        public override Inscripciones DataMapping(Inscripciones Inscripcion, DataRow oDataRow, General.Clases Clase)
        {
            if (Inscripcion.ID == 0)
            {
                Inscripcion.ID = GetValue<int>(oDataRow, ID);
                Inscripcion.FechaFin = GetValue<DateTime?>(oDataRow, FechaFin);
                Inscripcion.FechaInicio = GetValue<DateTime>(oDataRow, FechaInicio);
                Inscripcion.Alumno = GetEntity<Alumnos, AlumnosDataMapper>(oDataRow, IDAlumno, Clase, General.Clases.Alumnos, General.Clases.Inscripciones);
                Inscripcion.HorarioCurso = GetEntity<HorariosCursos, HorariosCursosDataMapper>(oDataRow, IDHorarioCurso, Clase, General.Clases.HorariosCursos, General.Clases.Inscripciones);
                Inscripcion.Modalidad= GetValue<string>(oDataRow, Modalidad);
            }

            CompleteEntity<PagosMensualesDataMapper, PagosMensuales, List<PagosMensuales>>(Inscripcion.PagosMensuales, oDataRow, IDPagoMensual, Clase, General.Clases.PagosMensuales, General.Clases.Inscripciones);
            CompleteEntity<AsistenciasAlumnosDataMapper, AsistenciasAlumnos, List<AsistenciasAlumnos>>(Inscripcion.AsistenciasAlumno, oDataRow, IDAsistenciaAlumno, Clase, General.Clases.AsistenciasAlumnos, General.Clases.Inscripciones);

            return Inscripcion;
        }

        #endregion
    }
}
