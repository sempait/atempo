﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class ProfesoresDataMapper : DataMapperClass<Profesores>
    {
        #region DataTable Variables

        private string Ciudad = "Ciudad";
        private string NombreYApellido = "NombreYApellidoProfesor";
        private string FechaBaja = "FechaBajaProfesor";
        private string IDCursoProfesor = "IDCursoProfesor";
        private string Email = "EmailProfesor";
        private string Telefono = "TelefonoProfesor";
        private string Direccion = "DireccionProfesor";
        private string DNI = "DNI";
        private string FechaNacimiento = "FechaNacimiento";

        #endregion

        public ProfesoresDataMapper()
        {
            ID = "IDProfesor";
        }

        #region Methods

        public override Profesores DataMapping(Profesores Profesor, DataRow oDataRow, General.Clases Clase)
        {
            if (Profesor.ID == 0)
            {
                Profesor.ID = GetValue<int>(oDataRow, ID);
                Profesor.Ciudad = GetValue<string>(oDataRow, Ciudad);
                Profesor.NombreYApellidoProfesor = GetValue<string>(oDataRow, NombreYApellido);
                Profesor.FechaBaja = GetValue<DateTime?>(oDataRow, FechaBaja);
                Profesor.Direccion = GetValue<string>(oDataRow, Direccion);
                Profesor.Telefono = GetValue<string>(oDataRow, Telefono);
                Profesor.Email = GetValue<string>(oDataRow, Email);
                Profesor.DNI = GetValue<string>(oDataRow, DNI);
                Profesor.FechaNacimiento = GetValue<DateTime?>(oDataRow, FechaNacimiento);
            }
            
            CompleteEntity<CursosProfesoresDataMapper, CursosProfesores, List<CursosProfesores>>(Profesor.CursosProfesores, oDataRow, IDCursoProfesor, Clase, General.Clases.CursosProfesores, General.Clases.Profesores);

            return Profesor;
        }

        #endregion
    }
}
