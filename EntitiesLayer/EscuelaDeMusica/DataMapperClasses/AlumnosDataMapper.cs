﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class AlumnosDataMapper : DataMapperClass<Alumnos>
    {
        #region DataTable Variables

        private string NombreYApellido = "NombreYApellidoAlumno";
        private string IDInscripcion = "IDInscripcion";
        private string Email = "EmailAlumno";
        private string Telefono = "TelefonoAlumno";
        private string NombreYApellidoPadre = "NombreYApellidoPadre";
        private string NombreYApellidoMadre = "NombreYApellidoMadre";
        private string TrabajoPadre = "TrabajoPadre";
        private string TrabajoMadre = "TrabajoMadre";
        private string TelefonoTrabajoPadre = "TelefonoTrabajoPadre";
        private string TelefonoTrabajoMadre = "TelefonoTrabajoMadre";
        private string Comentario = "Comentario";
        private string TelefonoPadre = "TelefonoPadre";
        private string TelefonoMadre = "TelefonoMadre";
        private string Domicilio = "DomicilioAlumno";
        private string Tutor = "Tutor";
        private string TelefonoTutor = "TelefonoTutor";
        private string DNI = "DNI";
        private string Colegio = "Colegio";
        private string FechaNacimiento = "FechaNacimiento";
        #endregion

        #region Methods

        public AlumnosDataMapper()
        {
            ID = "IDAlumno";
        }

        public override Alumnos DataMapping(Alumnos Alumno, DataRow oDataRow, General.Clases Clase)
        {
            if (Alumno.ID.Equals(0))
            {
                Alumno.ID = GetValue<int>(oDataRow, "IDAlumno");
                Alumno.NombreYApellido = GetValue<string>(oDataRow, NombreYApellido);
                Alumno.Email = GetValue<string>(oDataRow, Email);
                Alumno.Telefono = GetValue<string>(oDataRow, Telefono);
                Alumno.NombreYApellidoPadre = GetValue<string>(oDataRow, NombreYApellidoPadre);
                Alumno.NombreYApellidoMadre = GetValue<string>(oDataRow, NombreYApellidoMadre);
                Alumno.TrabajoMadre = GetValue<string>(oDataRow, TrabajoMadre);
                Alumno.TrabajoPadre = GetValue<string>(oDataRow, TrabajoPadre);
                Alumno.TelefonoPadre = GetValue<string>(oDataRow, TelefonoPadre);
                Alumno.TelefonoMadre = GetValue<string>(oDataRow, TelefonoMadre);
                Alumno.TelefonoTrabajoPadre = GetValue<string>(oDataRow, TelefonoTrabajoPadre);
                Alumno.TelefonoTrabajoMadre = GetValue<string>(oDataRow, TelefonoTrabajoMadre);
                Alumno.Comentario = GetValue<string>(oDataRow, Comentario);
                Alumno.Domicilio = GetValue<string>(oDataRow, Domicilio);
                Alumno.Tutor = GetValue<string>(oDataRow, Tutor);
                Alumno.TelefonoTutor = GetValue<string>(oDataRow, TelefonoTutor);
                Alumno.DNI = GetValue<string>(oDataRow,DNI);
                Alumno.Colegio = GetValue<string>(oDataRow, Colegio);
                Alumno.FechaNacimiento = GetValue<DateTime?>(oDataRow, FechaNacimiento);
            }

            CompleteEntity<InscripcionesDataMapper, Inscripciones, List<Inscripciones>>(Alumno.Inscripciones, oDataRow, IDInscripcion, Clase, General.Clases.Inscripciones, General.Clases.Alumnos);

            return Alumno;
        }

        #endregion
    }
}
