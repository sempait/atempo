﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class PagosMensualesDataMapper : DataMapperClass<PagosMensuales>
    {
        #region DataTable Variables

        private string Mes = "MesPago";
        private string Año = "AñoPago";
        private string FechaPago = "FechaPagoMensual";
        private string MontoAbonado = "MontoAbonadoPagoMensual";
        private string IDInscripcion = "IDInscripcion";
        private string MontoMes = "MontoMes";

        #endregion

        #region Methods

        public PagosMensualesDataMapper()
        {
            ID = "IDPagoMensual";
        }

        public override PagosMensuales DataMapping(PagosMensuales PagoMensual, DataRow oDataRow, General.Clases Clase)
        {
            if (PagoMensual.ID == 0)
            {
                PagoMensual.ID = GetValue<int>(oDataRow, ID);
                PagoMensual.Mes = GetValue<int>(oDataRow, Mes);
                PagoMensual.Anio = GetValue<int>(oDataRow, Año);
                PagoMensual.FechaPago = GetValue<DateTime?>(oDataRow, FechaPago);
                PagoMensual.MontoAbonado = GetValue<float>(oDataRow, MontoAbonado);
                PagoMensual.MontoMes = GetValue<float>(oDataRow, MontoMes);
                PagoMensual.Inscripcion = GetEntity<Inscripciones, InscripcionesDataMapper>(oDataRow, IDInscripcion, Clase, General.Clases.Inscripciones, General.Clases.PagosMensuales);
            }

            return PagoMensual;
        }

        #endregion
    }
}
