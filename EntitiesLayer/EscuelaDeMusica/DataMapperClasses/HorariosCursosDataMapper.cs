﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.EscuelaDeMusica.DataMapperClasses
{
    public class HorariosCursosDataMapper : DataMapperClass<HorariosCursos>
    {
        public HorariosCursosDataMapper()
        {
            ID = "IDHorarioCurso";
        }

        public override HorariosCursos DataMapping(HorariosCursos oTModel, System.Data.DataRow oDataRow, General.Clases Clase)
        {
            if (oTModel.ID == 0)
            {
                oTModel.ID = GetValue<int>(oDataRow, ID);
                oTModel.Dia = GetValue<string>(oDataRow, "DiaCurso");
                oTModel.HoraDesde = GetValue<TimeSpan>(oDataRow, "HoraDesdeCurso");
                oTModel.HoraHasta = GetValue<TimeSpan>(oDataRow, "HoraHastaCurso");
                oTModel.CantidadAlumnos = GetValue<int>(oDataRow, "CantidadAlumnos");
                oTModel.Curso = GetEntity<Cursos, CursosDataMapper>(oDataRow, "IDCurso", Clase, General.Clases.Cursos, General.Clases.HorariosCursos);
                oTModel.Profesor = GetEntity<Profesores, ProfesoresDataMapper>(oDataRow, "IDProfesor", Clase, General.Clases.Profesores, General.Clases.HorariosCursos);
            }
            CompleteEntity<InscripcionesDataMapper,Inscripciones,List<Inscripciones>>(oTModel.Inscripciones,oDataRow,"IDInscripcion",Clase,General.Clases.Inscripciones,General.Clases.HorariosCursos);

            return oTModel;
        }
    }
}
