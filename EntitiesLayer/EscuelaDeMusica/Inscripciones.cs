﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class Inscripciones : Entity
    {
        public DateTime FechaInicio { set; get; }

        public DateTime? FechaFin { set; get; }

        public Alumnos Alumno { set; get; }

        public string Modalidad { set; get; }


        public HorariosCursos HorarioCurso { set; get; }

        private List<PagosMensuales> oPagosMensuales = new List<PagosMensuales>();

        public List<PagosMensuales> PagosMensuales { set { oPagosMensuales = value; } get { return oPagosMensuales; } }

        private List<AsistenciasAlumnos> oAsistenciasAlumnos = new List<AsistenciasAlumnos>();

        public List<AsistenciasAlumnos> AsistenciasAlumno { set { oAsistenciasAlumnos = value; } get { return oAsistenciasAlumnos; } }
    }
}
