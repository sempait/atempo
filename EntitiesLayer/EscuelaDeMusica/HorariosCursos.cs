﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class HorariosCursos : Entity
    {
        public string Dia { get; set; }

        public TimeSpan HoraDesde { get; set; }

        public TimeSpan HoraHasta { get; set; }

        public Cursos Curso { get; set; }

        public Profesores Profesor { get; set; }

        public int CantidadAlumnos { get; set; }

        public int IDHorarioCurso { get; set; }


        public Boolean Individual { get; set; }
        private List<Inscripciones> oInscripciones = new List<Inscripciones>();

        public List<Inscripciones> Inscripciones
        {
            get
            {
                return oInscripciones;
            }
            set
            {
                oInscripciones = value;
            }
        }
    }
}
