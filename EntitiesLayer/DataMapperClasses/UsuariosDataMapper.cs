﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.DataMapperClasses
{
    public class UsuariosDataMapper : DataMapperClass<Usuarios>
    {
        #region DataTable Variables

        private string Usuario = "Usuario";
        private string Password = "Password";
        private string NombreYApellido = "NombreYApellidoUsuario";
        private string IDRol = "IDRol";
        private string Activo = "ActivoUsuario";
        private string FechaBaja = "FechaBajaUsuario";

        #endregion

        public UsuariosDataMapper()
        {
            ID = "IDUsuario";
        }

        #region Methods

        public override Usuarios DataMapping(Usuarios Usuario, DataRow oDataRow, General.Clases Clase)
        {
            if (Usuario.ID == 0)
            {
                Usuario.ID = GetValue<int>(oDataRow, ID);
                Usuario.NombreYApellido = GetValue<string>(oDataRow, NombreYApellido);
                Usuario.Usuario = GetValue<string>(oDataRow, this.Usuario);
                Usuario.Password = GetValue<string>(oDataRow, Password);
                Usuario.FechaBaja = GetValue<DateTime?>(oDataRow, FechaBaja);
                Usuario.Activo = GetValue<bool>(oDataRow, Activo);

                Usuario.Rol = GetEntity<Roles, RolesDataMapper>(oDataRow, IDRol, Clase, General.Clases.Roles, General.Clases.Usuarios);

            }

            return Usuario;
        }

        #endregion
    }
}
