﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.DataMapperClasses
{
    public class RolesDataMapper : DataMapperClass<Roles>
    {
        #region DataTable Variables

        private string Descripcion = "DescripcionRol";
        private string Activo = "ActivoRol";
        private string FechaBaja = "FechaBajaRol";
        private string IDUsuario = "IDUsuario";

        #endregion

        #region Methods

        public RolesDataMapper()
        {
            ID = "IDRol";
        }

        public override Roles DataMapping(Roles Rol, DataRow oDataRow, General.Clases Clase)
        {
            if (Rol.ID == 0)
            {
                Rol.ID = GetValue<int>(oDataRow, ID);
                Rol.DescripcionRol = GetValue<string>(oDataRow, Descripcion);
                Rol.Activo = GetValue<bool>(oDataRow, Activo);
                Rol.FechaBaja = GetValue<DateTime?>(oDataRow, FechaBaja);
            }

            CompleteEntity<UsuariosDataMapper, Usuarios, List<Usuarios>>(Rol.Usuarios, oDataRow, IDUsuario, Clase, General.Clases.Usuarios, General.Clases.Roles);

            if (oDataRow.Table.Columns.Contains(IDUsuario) && Clase != General.Clases.Usuarios)
            {
                UsuariosDataMapper oUsuarioDataMapper = new UsuariosDataMapper();

                Usuarios oUsuario = Rol.Usuarios.Find(e => e.ID.Equals(int.Parse(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][IDUsuario].ToString())));

                if (oUsuario == null)
                    Rol.Usuarios.Add(oUsuarioDataMapper.DataMapping(new Usuarios(), oDataRow, General.Clases.Roles));
                else
                {
                    oUsuarioDataMapper.DataMapping(oUsuario, oDataRow, General.Clases.Roles);
                }
            }

            return Rol;
        }





        #endregion
    }
}
