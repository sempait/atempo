﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.DataMapperClasses
{
    public class NotificacionesDataMapper : DataMapperClass<Notificaciones>
    {
        #region DataTable Variables

        private string FechaPublicacionNotificacion = "FechaPublicacionNotificacion";
        private string Notificacion = "Notificacion";

        #endregion

        #region Methods

        public NotificacionesDataMapper()
        {
            ID = "IDNotificacion";
        }

        public override Notificaciones DataMapping(Notificaciones Notificacion, DataRow oDataRow, General.Clases Clase)
        {
            if (Notificacion.ID == 0)
            {
                Notificacion.ID = GetValue<int>(oDataRow, ID);
                Notificacion.Notificacion = GetValue<string>(oDataRow, this.Notificacion);
                Notificacion.FechaPublicacionNotificacion = GetValue<DateTime>(oDataRow, FechaPublicacionNotificacion);
            }

            return Notificacion;
        }
        #endregion
    }
}
