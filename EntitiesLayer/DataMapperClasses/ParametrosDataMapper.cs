﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.DataMapperClasses
{
    public class ParametrosDataMapper : DataMapperClass<Parametros>
    {
        public ParametrosDataMapper()
        {
            ID = "IDParametros";
        }

        public override Parametros DataMapping(Parametros oTModel, System.Data.DataRow oDataRow, General.Clases Clase)
        {
            if (oTModel.ID == 0)
            {
                oTModel.CantidadMaximaAlumnos = GetValue<int>(oDataRow, "CantidadMaximaAlumnos");
                oTModel.HoraAperturaEscuela = GetValue<TimeSpan>(oDataRow, "EscuelaHoraApertura");
                oTModel.HoraCierreEscuela = GetValue<TimeSpan>(oDataRow, "EscuelaHoraCierre");
                oTModel.HoraAperturaSala = GetValue<TimeSpan>(oDataRow, "SalaHoraApertura");
                oTModel.HoraCierreSala = GetValue<TimeSpan>(oDataRow, "SalaHoraCierre");
                oTModel.HoraAperturaSalaSabado = GetValue<TimeSpan>(oDataRow, "SalaHoraAperturaS");
                oTModel.HoraCierreSalaSabado = GetValue<TimeSpan>(oDataRow, "SalaHoraCierreS");
                oTModel.ID = GetValue<int>(oDataRow, ID);
                oTModel.Direccion = GetValue<string>(oDataRow, "Direccion");
                oTModel.Telefono = GetValue<string>(oDataRow, "Telefono");
                oTModel.Celular = GetValue<string>(oDataRow, "Celular");
                oTModel.Email = GetValue<string>(oDataRow, "Email");
                oTModel.Usuario = GetValue<string>(oDataRow, "UsuarioEmail");
                oTModel.Contrsenia = GetValue<string>(oDataRow, "ContraseniaEmail");
                oTModel.PrecioHoraGrupal = GetValue<float>(oDataRow, "PrecioHoraIndividual");
                oTModel.PrecioHoraIndividual = GetValue<float>(oDataRow, "PrecioHoraGrupal");
                oTModel.PorcentajePlusProfesor = GetValue<Int32>(oDataRow, "PorcentajePlusProfesor");
                oTModel.PrecioHoraSala = GetValue<float>(oDataRow, "PrecioHoraSala");
            }

            return oTModel;
        }
    }
}
