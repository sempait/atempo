﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class Parametros : Entity
    {
        public string Email { get; set; }

        public string Usuario { get; set; }

        public string Contrsenia { get; set; }

        public int CantidadMaximaAlumnos { get; set; }

        public TimeSpan HoraAperturaEscuela { get; set; }

        public TimeSpan HoraCierreEscuela { get; set; }

        public TimeSpan HoraAperturaSala { get; set; }

        public TimeSpan HoraCierreSala { get; set; }

        public TimeSpan HoraAperturaSalaSabado { get; set; }

        public TimeSpan HoraCierreSalaSabado { get; set; }

        public string Direccion { get; set; }

        public string Telefono { get; set; }

        public string Celular { get; set; }

        public float PrecioHoraIndividual { get; set; }

        public float PrecioHoraGrupal { get; set; }

        public int PorcentajePlusProfesor { get; set; }

        public float PrecioHoraSala { get; set; }

    }
}
