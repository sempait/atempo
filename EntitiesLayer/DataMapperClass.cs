﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer
{
    public abstract class DataMapperClass<TModel>
        where TModel : Entity,new ()

    {
        public string ID = string.Empty;

        public virtual List<TModel> DataMapper(DataTable oDataTable)
        {
            List<TModel> Lista = new List<TModel>();

            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                int ID = int.Parse(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][this.ID].ToString());

                TModel oTModel = Lista.Find(e => e.ID.Equals(ID));

                if (oTModel==null)
                    Lista.Add(DataMapping(new TModel(), oDataRow, General.Clases.Ninguna));
                else
                    DataMapping(oTModel, oDataRow, General.Clases.Ninguna);
            }

            return Lista;
        }

        public virtual TModel DataMapping(TModel oTModel, DataRow oDataRow, General.Clases Clase)
        {
            return (TModel)new TModel();
        }

        public T GetValue<T>(DataRow oDataRow, string DataBaseColumnName)
        {
            if (oDataRow.Table.Columns.Contains(DataBaseColumnName))
            {
                if (!(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][DataBaseColumnName] is DBNull))
                {
                    switch (typeof(T).ToString())
                    {
                        case "System.Nullable`1[System.Single]":
                            return (T)Convert.ChangeType(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][DataBaseColumnName].ToString(), typeof(float));
                        case "System.Nullable`1[System.Double]":
                            return (T)Convert.ChangeType(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][DataBaseColumnName].ToString(), typeof(double));
                        case "System.TimeSpan":
                            return (T)Convert.ChangeType(TimeSpan.Parse(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][DataBaseColumnName].ToString()),typeof(T));
                        case "System.Nullable`1[System.DateTime]":
                            return (T)Convert.ChangeType(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][DataBaseColumnName].ToString(), typeof(DateTime));
                        default:
                            return (T)Convert.ChangeType(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][DataBaseColumnName].ToString(), typeof(T));
                    }
                }
            }

            return default(T);
        }

        public TEntity GetEntity<TEntity,TEntityDataMapper>(DataRow oDataRow, string IDEntity, General.Clases Class, General.Clases Equal, General.Clases ThisClass) 
            where TEntity: Entity,new ()
            where TEntityDataMapper : DataMapperClass<TEntity> ,new()
        {
            if (oDataRow.Table.Columns.Contains(IDEntity) && Class != Equal)
            {
                return new TEntityDataMapper().DataMapping(new TEntity(), oDataRow, ThisClass);
            }

            return default(TEntity);
        }

        public void CompleteEntity<TEntityDataMapper, TEntity, TListToFill>(TListToFill TList, DataRow oDataRow, string IDEntity, General.Clases Class, General.Clases Equal, General.Clases ThisClass)
            where TEntity : Entity, new()
            where TEntityDataMapper : DataMapperClass<TEntity>, new()
            where TListToFill : List<TEntity>
        {
            if (oDataRow.Table.Columns.Contains(IDEntity) && Class != Equal)
            {
                TEntityDataMapper oTEntityDataMapper = new TEntityDataMapper();

                TEntity oTEntity = TList.Find(e => e.ID.Equals(int.Parse(oDataRow.Table.Rows[oDataRow.Table.Rows.IndexOf(oDataRow)][IDEntity].ToString())));

                if (oTEntity == null)
                    TList.Add(oTEntityDataMapper.DataMapping(new TEntity(), oDataRow, ThisClass));
                else
                {
                    oTEntityDataMapper.DataMapping(oTEntity, oDataRow, ThisClass);
                }
            }
        }
    }
}
