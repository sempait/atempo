﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.SalaDeEnsayo
{
    public class GastosSala:Entity
    {
        public string DescripcionGasto { set; get; }

        public float Improte { set; get; }

        public Boolean AftectaCaja { set; get; }

        public DateTime FechaGasto { set; get; }
    }
}
