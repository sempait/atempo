﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.Bar;

namespace EntitiesLayer.SalaDeEnsayo
{
    public class ItemTurnoSala : Entity
    {
        public TurnosSala TurnoSala { set; get; }

        public Articulos Articulo { set; get; }

        public int CantidadItemTurnoSala { get; set; }
    }
}
