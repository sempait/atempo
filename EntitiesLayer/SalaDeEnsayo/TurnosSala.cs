﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.SalaDeEnsayo
{
    public class TurnosSala : Entity
    {
        public DateTime? StarDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Subject { get; set; }

        public string Location { get; set; }

        public int ResourceID { get; set; }

        private List<ItemTurnoSala> oItemsTurnoSala = new List<ItemTurnoSala>();

        public List<ItemTurnoSala> ItemsTurnoSala { get { return oItemsTurnoSala; } set { oItemsTurnoSala = value; } }

        public float ImporteTotal { get; set; }
    }
}
