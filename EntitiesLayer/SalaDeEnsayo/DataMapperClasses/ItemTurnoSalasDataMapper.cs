﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.Bar;
using EntitiesLayer.Bar.DataMapperClasses;

namespace EntitiesLayer.SalaDeEnsayo.DataMapperClasses
{
    public class ItemTurnoSalasDataMapper : DataMapperClass<ItemTurnoSalas>
    {
        public ItemTurnoSalasDataMapper()
        {
            ID = "IDItemTurnoSala";
        }

        public override ItemTurnoSalas DataMapping(ItemTurnoSalas oTModel, System.Data.DataRow oDataRow, General.Clases Clase)
        {
            if(oTModel.ID == 0)
            {
                oTModel.ID = GetValue<int>(oDataRow, ID);
                oTModel.CantidadItem = GetValue<int>(oDataRow, "CantidadItem");
                oTModel.Articulo = GetEntity<Articulos, ArticulosDataMapper>(oDataRow, "IDArticulo", Clase, General.Clases.Articulos, General.Clases.ItemTurnoSala);
                oTModel.TurnoSala = GetEntity<TurnosSala, TurnosSalaDataMapper>(oDataRow, "IDTurnoSala", Clase, General.Clases.TurnosSala, General.Clases.ItemTurnoSala);
            }

            return oTModel;
        }
    }
}
