﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace EntitiesLayer.SalaDeEnsayo.DataMapperClasses
{
    public class SalasDataMapper : DataMapperClass<Salas>
    {
        
        #region DataTable Variables

        private string ResourceName = "ResourceName";
        private string PrecioSala = "PrecioSala";
        
        #endregion

        #region Methods
        
        public SalasDataMapper()
        {
            ID = "UniqueID";
        }


        public override Salas DataMapping(Salas operacion, DataRow oDataRow, General.Clases Clase)
        {
            if (operacion.ID == 0)
            {
                operacion.ID = GetValue<int>(oDataRow, ID);
                operacion.ResourceName = GetValue<string>(oDataRow, ResourceName);
                operacion.PrecioSala = GetValue<float>(oDataRow, PrecioSala);
            }

           
            return operacion;
        }
        #endregion
    }
}
