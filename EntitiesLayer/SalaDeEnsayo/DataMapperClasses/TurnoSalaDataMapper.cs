﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.SalaDeEnsayo.DataMapperClasses
{
    public class TurnoSalaDataMapper : DataMapperClass<TurnosSala>
    {
        public TurnoSalaDataMapper()
        {
            ID = "UniqueID";
        }

        public override TurnosSala DataMapping(TurnosSala oTModel, System.Data.DataRow oDataRow, General.Clases Clase)
        {
            if (oTModel.ID == 0)
            {
                oTModel.ID = GetValue<int>(oDataRow, ID);
                oTModel.StarDate = GetValue<DateTime>(oDataRow, "StartDate");
                oTModel.EndDate = GetValue<DateTime?>(oDataRow, "EndDate");
                oTModel.Location = GetValue<string>(oDataRow, "Location");
                oTModel.ResourceID = GetValue<int>(oDataRow, "ResourceID");
                oTModel.Subject = GetValue<string>(oDataRow, "Subject");
                oTModel.ImporteTotal = GetValue<float>(oDataRow, "ImporteTotal");
            }

            CompleteEntity<ItemTurnoSalaDataMapper, ItemTurnoSala, List<ItemTurnoSala>>(oTModel.ItemsTurnoSala, oDataRow, "IDItemTurnoSala", Clase, General.Clases.ItemTurnoSala, General.Clases.TurnosSala);

            return oTModel;
        }
    }
}
