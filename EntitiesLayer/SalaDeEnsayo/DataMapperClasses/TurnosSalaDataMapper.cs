﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer.SalaDeEnsayo.DataMapperClasses
{
    public class TurnosSalaDataMapper : DataMapperClass<TurnosSala>
    {
        public TurnosSalaDataMapper()
        {
            ID = "UniqueID";
        }

        public override TurnosSala DataMapping(TurnosSala oTModel, System.Data.DataRow oDataRow, General.Clases Clase)
        {
            if (oTModel.UniqueID == 0)
            {
                oTModel.UniqueID = GetValue<int>(oDataRow, ID);
                oTModel.Subject = GetValue<string>(oDataRow, "Subject");
                oTModel.StartDate = GetValue<DateTime>(oDataRow, "StartDate");
                oTModel.EndDate = GetValue<DateTime>(oDataRow, "EndDate");
                oTModel.Location = GetValue<string>(oDataRow, "Location");
                oTModel.ResourceID = GetValue<int>(oDataRow, "ResourceID");
                oTModel.ImporteTotal = GetValue<float>(oDataRow, "ImporteTotal");
            }

            CompleteEntity<ItemTurnoSalasDataMapper, ItemTurnoSalas, List<ItemTurnoSalas>>(oTModel.ItemsTurnoSala, oDataRow, "IDItemTurnoSala", Clase, General.Clases.ItemTurnoSala, General.Clases.TurnosSala);

            return oTModel;
        }
    }
}
