﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.Bar;
using EntitiesLayer.Bar.DataMapperClasses;

namespace EntitiesLayer.SalaDeEnsayo.DataMapperClasses
{
    public class ItemTurnoSalaDataMapper : DataMapperClass<ItemTurnoSala>
    {
        public ItemTurnoSalaDataMapper()
        {
            ID = "IDItemTurnoSala";
        }

        public override ItemTurnoSala DataMapping(ItemTurnoSala oTModel, System.Data.DataRow oDataRow, General.Clases Clase)
        {
            if (oTModel.ID == 0)
            {
                oTModel.ID = GetValue<int>(oDataRow, ID);
                oTModel.CantidadItemTurnoSala = GetValue<int>(oDataRow, "CantidadItemTurnoSala");
                oTModel.Articulo = GetEntity<Articulos, ArticulosDataMapper>(oDataRow, "IDArticulo", Clase, General.Clases.Articulos, General.Clases.ItemTurnoSala);
                oTModel.TurnoSala = GetEntity<TurnosSala, TurnoSalaDataMapper>(oDataRow, "UniqueID", Clase, General.Clases.TurnosSala, General.Clases.ItemTurnoSala);
            }

            return oTModel;
        }
    }
}
