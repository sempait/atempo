﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.Bar;

namespace EntitiesLayer.SalaDeEnsayo
{
    public class ItemTurnoSalas : Entity
    {
        public TurnosSala TurnoSala { get; set; }

        public Articulos Articulo { get; set; }

        public int CantidadItem { get; set; }
    }
}
