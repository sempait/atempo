﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class OperacionCajaDiariaSala : Entity
    {
        public float Ingreso { set; get; }

        public float Egreso { set; get; }

        public string DescripcionOperacion { set; get; }

        public DateTime? FechaOperacion { set; get; }
    }
}
