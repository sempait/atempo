﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntitiesLayer
{
    public class Usuarios : Entity
    {
        public string Usuario { set; get; }

        public string Password { set; get; }

        public string NombreYApellido { set; get; }

        public Roles Rol { set; get; }
        
        public bool Activo { set; get; }

        public DateTime? FechaBaja { set; get; }
    }
}
