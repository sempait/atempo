﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;
using EntitiesLayer.DataMapperClasses;

namespace DataLayer
{
    public class NotificacionesData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(Notificaciones oNotificacion)
        {
            DBH.AddParameter("@pNotificacion", oNotificacion.Notificacion);
            DBH.AddParameter("@pFechaPublicacionNotificacion", oNotificacion.FechaPublicacionNotificacion);

            return DBH.ExecuteNonQuery("Notificaciones_Insert");
        }

        public void Update(Notificaciones oNotificacion)
        {
            DBH.AddParameter("@pIDNotificacion", oNotificacion.ID);
            DBH.AddParameter("@pNotificacion", oNotificacion.Notificacion);
            DBH.AddParameter("@pFechaPublicacionNotificacion", oNotificacion.FechaPublicacionNotificacion);

            DBH.ExecuteNonQuery("Notificaciones_Update");
        }

        public Notificaciones SelectOne(int ID)
        {
            DBH.AddParameter("@pIDNotificacion", ID);

            return new NotificacionesDataMapper().DataMapper(DBH.ExecuteDataTable("Notificaciones_SelectOne"))[0];
        }

        public List<Notificaciones> SelectAll()
        {
            return new NotificacionesDataMapper().DataMapper(DBH.ExecuteDataTable("Notificaciones_SelectAll"));
        }

    }
}
