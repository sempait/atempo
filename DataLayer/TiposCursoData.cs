﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class TiposCursoData 
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(TiposCurso oEntity)
        {
            DBH.AddParameter("@pDescripcionTipoCurso", oEntity.DescripcionTipoCurso);

            return DBH.ExecuteScalar("TiposCurso_Insert");
        }

        public void Update(TiposCurso oEntity)
        {
            DBH.AddParameter("@pIDTipoCurso", oEntity.ID);
            DBH.AddParameter("@pDescripcionTipoCurso", oEntity.DescripcionTipoCurso);

            DBH.ExecuteNonQuery("TiposCurso_Update");
        }

        public TiposCurso SelectOne(int ID)
        {
            DBH.AddParameter("@pIDTipoCurso", ID);

            return new TiposCursoDataMapper().DataMapper(DBH.ExecuteDataTable("TiposCurso_SelectOne"))[0];
        }

        public List<TiposCurso> SelectAll()
        {
            return new TiposCursoDataMapper().DataMapper(DBH.ExecuteDataTable("TiposCurso_SelectAll"));
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDTipoCurso", ID);

            DBH.ExecuteNonQuery("TiposCurso_Delete");
        }
    }
}
