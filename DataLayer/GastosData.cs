﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.EscuelaDeMusica;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class GastosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(Gastos oGasto)
        {
            DBH.AddParameter("@pDescripcion", oGasto.DescripcionGasto);
            DBH.AddParameter("@pImporte", oGasto.Improte);
            DBH.AddParameter("@pFechaGasto", oGasto.FechaGasto);
            DBH.AddParameter("@pAfectaBaja", oGasto.AftectaCaja);


            return DBH.ExecuteScalar("Gastos_Insert");

        }

        public object Update(Gastos oGasto)
        {
            DBH.AddParameter("@pId_Gasto", oGasto.ID);
            DBH.AddParameter("@pDescripcion", oGasto.DescripcionGasto);
            DBH.AddParameter("@pImporte", oGasto.Improte);
            DBH.AddParameter("@pFechaGasto", oGasto.FechaGasto);
            DBH.AddParameter("@pAfectaBaja", oGasto.AftectaCaja);

            return DBH.ExecuteNonQuery("Gastos_Update");
        }



        public Gastos SelectOne(int Id_Gasto)
        {
            DBH.AddParameter("@pId_Gasto", Id_Gasto);
            Gastos mGasto = new GastosDataMapper().DataMapper(DBH.ExecuteDataTable("Gastos_SelectOne"))[0];

            return mGasto;

        }

        public object Delete(int Id_Gasto)
        {
            DBH.AddParameter("@pId_Gasto",Id_Gasto);

            return DBH.ExecuteNonQuery("Gastos_Delete");
        }
    }
}
