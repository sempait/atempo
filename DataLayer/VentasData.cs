﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer.Bar;
using EntitiesLayer.Bar.DataMapperClasses;
namespace DataLayer
{
    public class VentasData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(DateTime Fecha, DateTime hora, float importetotal, float costototal)
        {

            DBH.AddParameter("@pFechaVenta", Fecha);
            DBH.AddParameter("@pHoraVenta", hora);
            DBH.AddParameter("@pimportetotalVenta", importetotal);
            DBH.AddParameter("@pcostototalVenta", costototal);
            return DBH.ExecuteNonQuery("Ventas_Insert");
        }

        public List<Ventas> SelectAll()
        {
            return new VentasDataMapper().DataMapper(DBH.ExecuteDataTable("Ventas_SelectAll"));
        }

        public List<Ventas> SelectAllGanancias(DateTime Fecha)
        {
            DBH.AddParameter("@pFechaVenta", Fecha);

            return new VentasDataMapper().DataMapper(DBH.ExecuteDataTable("Ventas_SelectAll_Ganancias"));
        }

        public object InsertItemVenta(int Cantidad, string IdArticulo)
        {
            DBH.AddParameter("@pCantidadDetalle", Cantidad);
            DBH.AddParameter("@pIDArticulo", IdArticulo);

            return DBH.ExecuteNonQuery("Detalles_Insert");
        }

        public object SelectAllItemVenta(int IdVenta)
        {
            DBH.AddParameter("@pIdVenta", IdVenta);

            return new DetallesDataMapper().DataMapper(DBH.ExecuteDataTable("Ventas_SelectAll_Item"));
        }

        public object DeleteVenta(int IdVenta)
        {
            DBH.AddParameter("@pIDVenta", IdVenta);
            return DBH.ExecuteNonQuery("Ventas_Delete");
        }
    }
}
