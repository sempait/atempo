﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class AsistenciaAlumnosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public void InsertAll()
        {
            DBH.ExecuteNonQuery("AsistenciasAlumnos_Insert_All");
        }

        public void Update(AsistenciasAlumnos oEntity)
        {
            DBH.AddParameter("@pIDAsistenciaAlumno", oEntity.ID);
            DBH.AddParameter("@pAsistencia", oEntity.Asistencia);

            DBH.ExecuteNonQuery("AsistenciasAlumnos_Update");
        }

        public List<AsistenciasAlumnos> SelectAll(DateTime oDate)
        {
            DBH.AddParameter("@pDiaAsistenciaAlumno",oDate.Day);
            DBH.AddParameter("@pMesAsistenciaAlumno",oDate.Month);
            DBH.AddParameter("@pAñoAsistenciaAlumno",oDate.Year);

            return new AsistenciasAlumnosDataMapper().DataMapper(DBH.ExecuteDataTable("AsistenciasAlumnos_SelectAll"));
        }
    }
}
