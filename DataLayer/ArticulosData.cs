﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer.Bar;
using EntitiesLayer.Bar.DataMapperClasses;

namespace DataLayer
{
    public class ArticulosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(Articulos oArticulo)
        {
            DBH.AddParameter("@pDescripcionArticulo", oArticulo.DescripcionArticulo);
            DBH.AddParameter("@pIDTipoArticulo", oArticulo.TipoArticulo.ID);
            DBH.AddParameter("@pStockArticulo", oArticulo.Stock);

            return DBH.ExecuteScalar("Articulos_Insert");
        }

        public void Update(Articulos oArticulo)
        {
            DBH.AddParameter("@pIDArticulo", oArticulo.ID);
            DBH.AddParameter("@pDescripcionArticulo", oArticulo.DescripcionArticulo);
            DBH.AddParameter("@pIDTipoArticulo", oArticulo.TipoArticulo.ID);
            DBH.AddParameter("@pStockArticulo", oArticulo.Stock);

            DBH.ExecuteNonQuery("Articulos_Update");
        }

        public List<Articulos> SelectAllEnStock()
        {
            return new ArticulosDataMapper().DataMapper(DBH.ExecuteDataTable("Articulos_SelectAll_EnStock"));
        }


        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDArticulo", ID);

            DBH.ExecuteNonQuery("Articulos_Delete");
        }

        public Articulos SelectOne(int IdArticulo)
        {
            DBH.AddParameter("@pIDArticulo", IdArticulo);

            return new ArticulosDataMapper().DataMapper(DBH.ExecuteDataTable("Articulos_SelectOne"))[0];
        }
    }
}
