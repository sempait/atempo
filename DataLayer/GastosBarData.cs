﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.Bar;
using DataBaseAccess;
using EntitiesLayer.Bar.DataMapperClasses;

namespace DataLayer
{
    public class GastosBarData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(GastosBar oGasto)
        {
            DBH.AddParameter("@pDescripcion", oGasto.DescripcionGasto);
            DBH.AddParameter("@pImporte", oGasto.Improte);
            DBH.AddParameter("@pFechaGasto", oGasto.FechaGasto);
            DBH.AddParameter("@pAfectaBaja", oGasto.AftectaCaja);


            return DBH.ExecuteScalar("GastosBar_Insert");

        }

        public object Update(GastosBar oGasto)
        {
            DBH.AddParameter("@pId_Gasto", oGasto.ID);
            DBH.AddParameter("@pDescripcion", oGasto.DescripcionGasto);
            DBH.AddParameter("@pImporte", oGasto.Improte);
            DBH.AddParameter("@pFechaGasto", oGasto.FechaGasto);
            DBH.AddParameter("@pAfectaBaja", oGasto.AftectaCaja);

            return DBH.ExecuteNonQuery("GastosBar_Update");
        }



        public GastosBar SelectOne(int Id_Gasto)
        {
            DBH.AddParameter("@pId_Gasto", Id_Gasto);
            GastosBar mGasto = new GastosBarDataMapper().DataMapper(DBH.ExecuteDataTable("GastosBar_SelectOne"))[0];

            return mGasto;

        }

        public object Delete(int Id_Gasto)
        {
            DBH.AddParameter("@pId_Gasto", Id_Gasto);

            return DBH.ExecuteNonQuery("GastosBar_Delete");
        }
    }
}
