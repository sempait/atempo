﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataBaseAccess;

namespace DataLayer
{
    public class GananciasBarDataLayer
    {

        DataBaseHelper dbHelper = DataBaseHelper.Instance;
        public DataTable RecuperarOperaciones(DateTime fechaDesde, DateTime fechaHasta)
        {
            DataTable tablaVentas = new DataTable();
            dbHelper.AddParameter("@pFechaDesde", fechaDesde);
            dbHelper.AddParameter("@pFechaHasta", fechaHasta);
            tablaVentas = dbHelper.ExecuteDataTable("VentasBarPorFechas");

            DataTable tablaGastos = new DataTable();
            dbHelper.AddParameter("@pFechaDesde", fechaDesde);
            dbHelper.AddParameter("@pFechaHasta", fechaHasta);
            tablaGastos = dbHelper.ExecuteDataTable("GastosBarPorFechas");

            tablaGastos.Merge(tablaVentas);
            return tablaGastos;

        }
    }
}
