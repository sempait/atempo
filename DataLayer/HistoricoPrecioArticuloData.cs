﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.Bar;
using DataBaseAccess;

namespace DataLayer
{
    public class HistoricoPrecioArticuloData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(HistoricoPreciosArticulos oEntity)
        {

            DBH.AddParameter("@pIDArticulo", oEntity.Articulo.ID);
            DBH.AddParameter("@pPrecioArticulo", oEntity.Precio);

            return DBH.ExecuteScalar("HistoricoPreciosArticulos_Insert");
        }
        
    }
}
