﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;

namespace DataLayer
{
    interface CRUD<TEntity>
        where TEntity : Entity
    {
        object Insert(TEntity oEntity);

        void Update(TEntity oEntity);

        TEntity SelectOne(int ID);

        List<TEntity> SelectAll();

        void Delete(int ID);
    }
}
