﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.DataMapperClasses;

namespace DataLayer
{
    public class RolesData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(Roles oEntity)
        {
            DBH.AddParameter("@pDescripcionRol", oEntity.DescripcionRol);

            return DBH.ExecuteScalar("Roles_Insert");
        }

        public void Update(Roles oEntity)
        {
            DBH.AddParameter("@pDescripcionRol", oEntity.DescripcionRol);

            DBH.ExecuteNonQuery("Roles_Update");
        }

        public Roles SelectOne(int ID)
        {
            DBH.AddParameter("@pIDRol", ID);

            return new RolesDataMapper().DataMapper(DBH.ExecuteDataTable("Roles_SelectOne"))[0];
        }

        public List<Roles> SelectAll()
        {
            return new RolesDataMapper().DataMapper(DBH.ExecuteDataTable("Roles_SelectAll"));
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDRol", ID);

            DBH.ExecuteNonQuery("Roles_Delete");
        }
    }
}
