﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataLayer
{
    public class PagosMensualesData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public void InsertAll()
        {
            DBH.ExecuteDataTable("PagosMensuales_Insert_All");
        }

        public void Update(int IDPagoMensual, float Monto,DateTime FechaPagoCuota)
        {
            DBH.AddParameter("@pIDPagoMensual", IDPagoMensual);
            DBH.AddParameter("@pMontoAbonadoPagoMensual", Monto);
            DBH.AddParameter("@pFechaPagoCuota", FechaPagoCuota);

            DBH.ExecuteNonQuery("PagosMensuales_Update");
        }

        public List<PagosMensuales> SelectAllHorariosCursoDelAlumno(int IDInscripcion)
        {
            DBH.AddParameter("@pIDInscripcion", IDInscripcion);

            return new PagosMensualesDataMapper().DataMapper(DBH.ExecuteDataTable("PagosMensuales_SelectAll_HorariosCursoDelAlumno"));
        }

        public PagosMensuales SelectOne(int IDPagoMensual)
        {
            DBH.AddParameter("@pIDPagoMensual", IDPagoMensual);

            return new PagosMensualesDataMapper().DataMapper(DBH.ExecuteDataTable("PagosMensuales_SelectOne"))[0];
        }

        public DataTable SelectUltimasCuotasGeneradas()
        {
            DataTable dt = new DataTable();
            return dt = DBH.ExecuteDataTable("UltimasCuotas_Generadas_All");


        }

        public void InsertCuota(int IDInscripcion,int mesPago,int añoPago)
        {
            DBH.AddParameter("@pIDInscripcion", IDInscripcion);
            DBH.AddParameter("@pMesPago", mesPago);
            DBH.AddParameter("@pAñoPago", añoPago);
            DBH.ExecuteNonQuery("Cuota_Insert_one");
        }

        public DataTable SelectUltimasDelAño()
        {
            DataTable dt = new DataTable();
            return dt = DBH.ExecuteDataTable("UltimasCuotas_Generadas_DelAño");
        }

        public void InsertPrimeraCuotaDelAño(int IDInscripcion)
        {
            DBH.AddParameter("@pIDInscripcion", IDInscripcion);
            DBH.ExecuteNonQuery("Cuota_Insert_one_PrimeroDelAño");
        }

        public void DeletePagoMensual(int IDCuotaAlumno)
        {
            DBH.AddParameter("@IDPagoMensual", IDCuotaAlumno);
            DBH.ExecuteNonQuery("PagoMensual_Delete");
        }
    }
}
