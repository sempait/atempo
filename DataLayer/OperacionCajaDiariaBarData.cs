﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer.Bar.DataMapperClasses;
using EntitiesLayer;

namespace DataLayer
{
    public class OperacionCajaDiariaBarData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(EntitiesLayer.OperacionCajaDiariaBar mOperacion)
        {
            if (mOperacion.Egreso != 0)
            {
                DBH.AddParameter("@pEgreso", mOperacion.Egreso);
            }
            else
            {
                DBH.AddParameter("@pIngreso", mOperacion.Ingreso);
            }

            DBH.AddParameter("@pDescripcionOperacion", mOperacion.DescripcionOperacion);
            DBH.AddParameter("@pFechaOperacion", mOperacion.FechaOperacion);
            return DBH.ExecuteNonQuery("OperacionCajadiariaBar_Insert");

        }


        public EntitiesLayer.OperacionCajaDiariaBar SelectOne(int Id_Caja)
        {
            DBH.AddParameter("@pId_Caja_Bar", Id_Caja);
            OperacionCajaDiariaBar mOperacion = new OperacionCajaDiariaBarDataMapper().DataMapper(DBH.ExecuteDataTable("OperacionesBar_SelectOne"))[0];

            return mOperacion;

        }



        public object Update(OperacionCajaDiariaBar mOperacion)
        {
            DBH.AddParameter("@pId_Caja_Bar", mOperacion.ID);
            if (mOperacion.Egreso != 0)
                DBH.AddParameter("@pEgreso", mOperacion.Egreso);
            else
                DBH.AddParameter("@pIngreso", mOperacion.Ingreso);

            DBH.AddParameter("@pDescripcionOperacion", mOperacion.DescripcionOperacion);
            DBH.AddParameter("@pFechaOperacion", mOperacion.FechaOperacion);

            return DBH.ExecuteNonQuery("OperacionCajadiariaBar_Update");
        }

        public object Delete(int Id_Caja)
        {
            DBH.AddParameter("@pId_Caja_Bar", Id_Caja);
            return DBH.ExecuteNonQuery("OperacionCajadiariaBar_Delete");
        }

        public object CloseCaja(DateTime FechaOperacion)
        {

            DateTime FechaOperacionPost = FechaOperacion.AddDays(1);
            DBH.AddParameter("@pFechaOperacion", FechaOperacion);
            DBH.AddParameter("@pFechaOperacionPost", FechaOperacionPost);
            return DBH.ExecuteNonQuery("OperacionCajadiariaBar_Close");
        }
    }
}
