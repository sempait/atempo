﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataBaseAccess;

namespace DataLayer
{
    public class PagoProfesoresData
    {
        DataBaseHelper dbHelper = DataBaseHelper.Instance;
        
        public DataTable RecuperarPagoProfesoresDatos(DateTime fechaDesde, DateTime fechaHasta)
        {
            DataTable tabla = new DataTable();
            dbHelper.AddParameter("@pFechaDesde", fechaDesde);
            dbHelper.AddParameter("@pFechaHasta", fechaHasta);
            return tabla = dbHelper.ExecuteDataTable("ReportePagoProfesores_SelectDatos");
        }
    }
}
