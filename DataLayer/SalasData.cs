﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.SalaDeEnsayo;
using DataBaseAccess;
using EntitiesLayer.SalaDeEnsayo.DataMapperClasses;

namespace DataLayer
{
    public class SalasData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public EntitiesLayer.SalaDeEnsayo.Salas SelectOne(int UniqueID)
        {
            DBH.AddParameter("@pUniqueID", UniqueID);
            Salas mSalas = new SalasDataMapper().DataMapper(DBH.ExecuteDataTable("Salas_SelectOne"))[0];

            return mSalas;
        }

        public object Update(Salas oSalas)
        {
            DBH.AddParameter("@pUniqueID", oSalas.ID);
            DBH.AddParameter("@pResourceName", oSalas.ResourceName);
            DBH.AddParameter("@pPrecioSala", oSalas.PrecioSala);

            return DBH.ExecuteNonQuery("Salas_Update");
        }

        public Salas SelectOnePorResourceID(int ResourceID)
        {
            DBH.AddParameter("@pResourceID", ResourceID);
            Salas mSalas = new SalasDataMapper().DataMapper(DBH.ExecuteDataTable("Salas_SelectOnePorResourceID"))[0];

            return mSalas;
        }
    }
}
