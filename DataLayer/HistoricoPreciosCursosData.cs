﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;

namespace DataLayer
{
    public class HistoricoPreciosCursosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(HistoricoPreciosCursos oEntity)
        {
            DBH.AddParameter("@pPrecio", oEntity.Precio);
            DBH.AddParameter("@pIDCurso", oEntity.Curso.ID);

            return DBH.ExecuteScalar("HistoricoPreciosCursos_Insert");
        }

        public void Update(HistoricoPreciosCursos oEntity)
        {
            DBH.AddParameter("@pIDCurso", oEntity.Curso.ID);

            DBH.ExecuteNonQuery("HistoricoPreciosCursos_Update");
        }
    }
}
