﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;
using EntitiesLayer;

namespace DataLayer
{
    public class HorariosCursosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public List<HorariosCursos> SelectAllCursosActuales(int IDCurso,string TipoCurso)
        {
            DBH.AddParameter("@pIDCurso", IDCurso);
            DBH.AddParameter("@pTipoCurso",TipoCurso);
            return new HorariosCursosDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosCurso_SelectAll_CursosDisponibles"));
        }

        public List<HorariosCursos> SelectAllProfesor(int IDProfesor)
        {
            DBH.AddParameter("@pIDProfesor", IDProfesor);
            return new HorariosCursosDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosCurso_SelectAll_Profesor"));
        }

        public void Insert(HorariosCursos oHorarioCurso)
        {
            DBH.AddParameter("@pDiaCurso", oHorarioCurso.Dia);
            DBH.AddParameter("@pHoraDesdeCurso", oHorarioCurso.HoraDesde);
            DBH.AddParameter("@pHoraHastaCurso", oHorarioCurso.HoraHasta);
            DBH.AddParameter("@pIDCurso", oHorarioCurso.Curso.ID);
            DBH.AddParameter("@pIDProfesor", oHorarioCurso.Profesor.ID);
            DBH.AddParameter("@pIndividual", oHorarioCurso.Individual);
            DBH.ExecuteNonQuery("HorariosCurso_Insert");
        }

        public List<HorariosCursos> ValidarHorarioCurso(int IDHorarioDisponibilidad)
        {
            DBH.AddParameter("@pIDHorarioDisponibilidad", IDHorarioDisponibilidad);
            return new HorariosCursosDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosCurso_SelectAll_HorariosDisponibilidad"));
        }

        public void Delete(int IDHorarioCurso)
        {
            DBH.AddParameter("@pIDHorarioCurso", IDHorarioCurso);

            DBH.ExecuteNonQuery("HorariosCurso_Delete");
        }

        public void Update(HorariosCursos oHorarioCurso)
        {
            DBH.AddParameter("@pIDHorarioCurso", oHorarioCurso.ID);
            DBH.AddParameter("@pDiaCurso", oHorarioCurso.Dia);
            DBH.AddParameter("@pHoraDesdeCurso", oHorarioCurso.HoraDesde);
            DBH.AddParameter("@pHoraHastaCurso", oHorarioCurso.HoraHasta);
            DBH.AddParameter("@pIDCurso", oHorarioCurso.Curso.ID);
            DBH.AddParameter("@pIDProfesorCurso", oHorarioCurso.Profesor.ID);

            DBH.ExecuteNonQuery("HorariosCurso_Update");
        }

        public List<HorariosCursos> SelectHorariosCursoAsistencia(DateTime fecha, string NombreDia, string IDProfesor)
        {
            DBH.AddParameter("@pIDProfesor", int.Parse(IDProfesor));
            DBH.AddParameter("@pFechaAsistencia",fecha);
            DBH.AddParameter("@pNombreDia", NombreDia);


            return new HorariosCursosDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosCurso_SelectAll_Asistencia"));
        }

        public HorariosCursos SelectOne(int IDHorarioCurso)
        {
            DBH.AddParameter("@pIDHorarioCurso", IDHorarioCurso);
            HorariosCursos oHorarioCursos = new HorariosCursosDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosCurso_SelectOne"))[0];

            return oHorarioCursos;
        }

      
    }
}
