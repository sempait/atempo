﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class CursosProfesoresData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(CursosProfesores oEntity)
        {
            DBH.AddParameter("@pIDCurso", oEntity.Curso.ID);
            DBH.AddParameter("@pIDProfesor", oEntity.Profesor.ID);

            return DBH.ExecuteScalar("CursosProfesores_Insert");
        }

        public List<CursosProfesores> SelectAll(int IDProfesor)
        {
            DBH.AddParameter("@pIDProfesor", IDProfesor);

            return new CursosProfesoresDataMapper().DataMapper(DBH.ExecuteDataTable("CursosProfesores_SelectAll"));
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDCursoProfesor", ID);

            DBH.ExecuteNonQuery("CursosProfesores_Delete");
        }

        public List<CursosProfesores> SelectAllCursosAsignados(int ID)
        {
            DBH.AddParameter("@pIDProfesor", ID);

            return new CursosProfesoresDataMapper().DataMapper(DBH.ExecuteDataTable("CursosProfesores_SelectAll_CursosAsignados"));
        }
    }
}
