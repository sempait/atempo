﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class AsistenciasProfesoresData 
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;


        public List<AsistenciasProfesores> SelectAll(DateTime oDate)
        {
            DBH.AddParameter("@pFechaAsistenciaProfesor", oDate);

            return new AsistenciasProfesoresDataMapper().DataMapper(DBH.ExecuteDataTable("AsistenciasProfesores_SelectAll"));
        }

        public void Insert(AsistenciasProfesores oAsistenciaProfesor)
        {
            DBH.AddParameter("@pFechaAsistenciaProfesor", oAsistenciaProfesor.FechaAsistencia);
            DBH.AddParameter("@pIDHorarioCurso", oAsistenciaProfesor.HorarioCurso.ID);

            DBH.ExecuteNonQuery("AsistenciasProfesores_Insert");
        }

        public void Delete(int IDAsistenciaProfesor)
        {
            DBH.AddParameter("@pIDAsistenciaProfesor", IDAsistenciaProfesor);

            DBH.ExecuteNonQuery("AsistenciasProfesores_Delete");
        }
    }
}
