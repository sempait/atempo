﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class CursosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(Cursos oEntity)
        {
            DBH.AddParameter("@pDescripcionCurso", oEntity.DescripcionCurso);
            DBH.AddParameter("@pPrecioIndividual", oEntity.PrecioIndividual);
            DBH.AddParameter("@pPrecioGrupal", oEntity.PrecioGrupal);

            return DBH.ExecuteScalar("Cursos_Insert");
        }

        public void Update(Cursos oEntity)
        {
            DBH.AddParameter("@pIDCurso", oEntity.ID);
            DBH.AddParameter("@pDescripcionCurso", oEntity.DescripcionCurso);
            DBH.AddParameter("@pPrecioGrupal", oEntity.PrecioGrupal);
            DBH.AddParameter("@pPrecioIndividual", oEntity.PrecioIndividual);

            DBH.ExecuteNonQuery("Cursos_Update");
        }

        public Cursos SelectOne(int ID)
        {
            DBH.AddParameter("@pIDCurso", ID);

            return new CursosDataMapper().DataMapper(DBH.ExecuteDataTable("Cursos_SelectOne"))[0];
        }

        public List<Cursos> SelectAll()
        {
            return new CursosDataMapper().DataMapper(DBH.ExecuteDataTable("Cursos_SelectAll"));
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDCurso", ID);

            DBH.ExecuteNonQuery("Cursos_Delete");
        }

        public List<Cursos> SelectAllCursosNoAsignados(int IDProfesor)
        {
            DBH.AddParameter("@pIDProfesor", IDProfesor);

            return new CursosDataMapper().DataMapper(DBH.ExecuteDataTable("Cursos_SelectAll_CursosNoAsignados"));
        }
    }
}
