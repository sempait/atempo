﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica;

namespace DataLayer
{
    public class HorariosDisponibilidadData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(HorariosDisponibilidad oEntity)
        {
            DBH.AddParameter("@pDiaDisponibilidad", oEntity.Dia);
            DBH.AddParameter("@pHoraDesdeDisponibilidad", oEntity.HoraDesde);
            DBH.AddParameter("@pHoraHastaDisponibilidad", oEntity.HoraHasta);
            DBH.AddParameter("@pIDProfesor", oEntity.Profesor.ID);

            return DBH.ExecuteScalar("HorariosDisponibilidad_Insert");
        }

        public object Update(HorariosDisponibilidad oHorarioDisponibilidad)
        {
            DBH.AddParameter("@pIDHorarioDisponibilidad", oHorarioDisponibilidad.ID);
            DBH.AddParameter("@pDiaDisponibilidad", oHorarioDisponibilidad.Dia);
            DBH.AddParameter("@pHoraDesdeDisponibilidad", oHorarioDisponibilidad.HoraDesde);
            DBH.AddParameter("@pHoraHastaDisponibilidad", oHorarioDisponibilidad.HoraHasta);

            return DBH.ExecuteScalar("HorariosDisponibilidad_Update");
        }

        public HorariosDisponibilidad SelectOne(int ID)
        {
            DBH.AddParameter("@pIDHorarioDisponibilidad", ID);

            return new HorariosDisponibilidadDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosDisponibilidad_SelectOne"))[0];
        }

        public List<HorariosDisponibilidad> SelectAllProfesor(int IDProfesor)
        {
            DBH.AddParameter("@pIDProfesor", IDProfesor);

            return new HorariosDisponibilidadDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosDisponibilidad_SelectAll_DisponibilidadProfesor"));
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDHorarioDisponibilidad", ID);

            DBH.ExecuteNonQuery("HorariosDisponibilidad_Delete");
        }
    }
}
