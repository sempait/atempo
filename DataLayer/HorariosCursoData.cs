﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class HorariosCursoData : CRUD<HorariosCurso>
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(HorariosCurso oEntity)
        {
            DBH.AddParameter("@pDiaCurso", oEntity.Dia);
            DBH.AddParameter("@pHoraDesdeCurso", oEntity.HoraDesde);
            DBH.AddParameter("@pHoraHastaCurso", oEntity.HoraHasta);
            DBH.AddParameter("@pIDCursoAlumnos", oEntity.CursoAlumnos.ID);

            return DBH.ExecuteScalar("HorariosCurso_Insert");
        }

        public void Update(HorariosCurso oEntity)
        {
            DBH.AddParameter("@pIDHorarioCurso", oEntity.ID);
            DBH.AddParameter("@pDiaCurso", oEntity.Dia);
            DBH.AddParameter("@pHoraDesdeCurso", oEntity.HoraDesde);
            DBH.AddParameter("@pHoraHastaCurso", oEntity.HoraHasta);

            DBH.ExecuteNonQuery("HorariosCurso_Update");
        }

        public HorariosCurso SelectOne(int IDHorarioCurso)
        {
            DBH.AddParameter("@pIDHorarioCurso", IDHorarioCurso);

            return new HorariosCursoDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosCurso_SelectOne"))[0];
        }

        public List<HorariosCurso> SelectAll()
        {
            throw new NotImplementedException();
        }

        public List<HorariosCurso> SelectAll(int IDCursoAlumnos)
        {
            DBH.AddParameter("@pIDCursoAlumnos", IDCursoAlumnos);

            return new HorariosCursoDataMapper().DataMapper(DBH.ExecuteDataTable("HorariosCurso_SelectAll"));
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDHorarioCurso", ID);

            DBH.ExecuteNonQuery("HorariosCurso_Delete");
        }
    }
}
