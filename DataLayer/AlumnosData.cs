﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class AlumnosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(Alumnos oAlumno)
        {
            DBH.AddParameter("@pNombreYApellido", oAlumno.NombreYApellido);
            DBH.AddParameter("@pEmail", oAlumno.Email);
            DBH.AddParameter("@pTelefono", oAlumno.Telefono);
            DBH.AddParameter("@pNombreYApellidoPadre", oAlumno.NombreYApellidoPadre);
            DBH.AddParameter("@pNombreYApellidoMadre", oAlumno.NombreYApellidoMadre);
            DBH.AddParameter("@pTrabajoPadre", oAlumno.TrabajoPadre);
            DBH.AddParameter("@pTrabajoMadre", oAlumno.TrabajoMadre);
            DBH.AddParameter("@pTelTrabajoPadre", oAlumno.TelefonoTrabajoPadre);
            DBH.AddParameter("@pTelTrabajoMadre", oAlumno.TelefonoTrabajoMadre);
            DBH.AddParameter("@pComentario", oAlumno.Comentario);
            DBH.AddParameter("@pTelefonoPadre", oAlumno.TelefonoPadre);
            DBH.AddParameter("@pTelefonoMadre", oAlumno.TelefonoMadre);
            DBH.AddParameter("@pDomicilioAlumno", oAlumno.Domicilio);
            DBH.AddParameter("@pTutor", oAlumno.Tutor);
            DBH.AddParameter("@pTelefonoTutor", oAlumno.TelefonoTutor);
            DBH.AddParameter("@pDNI", oAlumno.DNI);
            DBH.AddParameter("@pColegio", oAlumno.Colegio);
            DBH.AddParameter("@pFechaNacimiento", oAlumno.FechaNacimiento);

            return DBH.ExecuteNonQuery("Alumnos_Insert");
        }

        public void Update(Alumnos oAlumno)
        {
            DBH.AddParameter("@pIDAlumno", oAlumno.ID);
            DBH.AddParameter("@pNombreYApellido", oAlumno.NombreYApellido);
            DBH.AddParameter("@pEmail", oAlumno.Email);
            DBH.AddParameter("@pTelefono", oAlumno.Telefono);
            DBH.AddParameter("@pNombreYApellidoPadre", oAlumno.NombreYApellidoPadre);
            DBH.AddParameter("@pNombreYApellidoMadre", oAlumno.NombreYApellidoMadre);
            DBH.AddParameter("@pTrabajoPadre", oAlumno.TrabajoPadre);
            DBH.AddParameter("@pTrabajoMadre", oAlumno.TrabajoMadre);
            DBH.AddParameter("@pTelTrabajoPadre", oAlumno.TelefonoTrabajoPadre);
            DBH.AddParameter("@pTelTrabajoMadre", oAlumno.TelefonoTrabajoMadre);
            DBH.AddParameter("@pComentario", oAlumno.Comentario);
            DBH.AddParameter("@pTelefonoPadre", oAlumno.TelefonoPadre);
            DBH.AddParameter("@pTelefonoMadre", oAlumno.TelefonoMadre);
            DBH.AddParameter("@pDomicilioAlumno", oAlumno.Domicilio);
            DBH.AddParameter("@pTutor", oAlumno.Tutor);
            DBH.AddParameter("@pTelefonoTutor", oAlumno.TelefonoTutor);
            DBH.AddParameter("@pDNI", oAlumno.DNI);
            DBH.AddParameter("@pColegio", oAlumno.Colegio);
            DBH.AddParameter("@pFechaNacimiento", oAlumno.FechaNacimiento);

            DBH.ExecuteNonQuery("Alumnos_Update");
        }

        public Alumnos SelectOne(int ID)
        {
            DBH.AddParameter("@pIDAlumno", ID);
            Alumnos oAlumno = new AlumnosDataMapper().DataMapper(DBH.ExecuteDataTable("Alumnos_SelectOne"))[0];

            return oAlumno;
        }

        public List<Alumnos> SelectAll()
        {
            return new AlumnosDataMapper().DataMapper(DBH.ExecuteDataTable("Alumnos_SelectAll"));
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDAlumno", ID);

            DBH.ExecuteNonQuery("Alumnos_Delete");
        }

        public List<Alumnos> SelectAllConCuotasPendientes()
        {
            return new AlumnosDataMapper().DataMapper(DBH.ExecuteDataTable("Alumnos_SelectAll_ConCuotasPendientes"));
        }

        public void UpdateBajaAlumno(Alumnos alumnoActual)
        {
            DBH.AddParameter("@pIDAlumno", alumnoActual.ID);

            DBH.ExecuteNonQuery("[Alumnos_Baja]");


        }
    }
}
