﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using System.Data;

namespace DataLayer
{
    public class ItemTurnoSalaData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public void Insert(EntitiesLayer.SalaDeEnsayo.ItemTurnoSala oItemTurnoSala)
        {
            DBH.AddParameter("@pIDTurnoSala", oItemTurnoSala.TurnoSala.ID);
            DBH.AddParameter("@pIDArticulo", oItemTurnoSala.Articulo.ID);
            DBH.AddParameter("@pCantidadItemTurnoSala", oItemTurnoSala.CantidadItemTurnoSala);

            DBH.ExecuteNonQuery("ItemTurnoSala_Insert");
        }

        public void InsertNuevo(int idTurnoSala, int idArticulo, int cantidadItemTurnoSala)
        {
            DBH.AddParameter("@pIDTurnoSala", idTurnoSala);
            DBH.AddParameter("@pIDArticulo", idArticulo);
            DBH.AddParameter("@pCantidadItemTurnoSala", cantidadItemTurnoSala);

            DBH.ExecuteNonQuery("ItemTurnoSala_Insert");
        }

        public DataTable SelectAllPorTurnoSala(int idTurnoSala)
        {
            DataTable tabla = new DataTable();
            DBH.AddParameter("@pIDTurnoSala", idTurnoSala);
            return tabla = DBH.ExecuteDataTable("ItemTurnoSala_SelectAll_PorTurnoSala");
        }

        public void DeletePorTurnoSala(int idTurnoSala)
        {
            DBH.AddParameter("@pIDTurnoSala", idTurnoSala);
            DBH.ExecuteNonQuery("ItemTurnoSala_Delete_PorTurnoSala");
        }
    }
}
