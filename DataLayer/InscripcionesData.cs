﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class InscripcionesData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(Inscripciones oEntity)
        {

            DBH.AddParameter("@pIDAlumno", oEntity.Alumno.ID);
            DBH.AddParameter("@pIDHorarioCurso", oEntity.HorarioCurso.ID);
            DBH.AddParameter("@pModalidad", oEntity.Modalidad);
            DBH.AddParameter("@pFechaInicio", oEntity.FechaInicio);

            return DBH.ExecuteScalar("Inscripciones_Insert");
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDInscripcion", ID);
            DBH.ExecuteNonQuery("Inscripciones_Delete");
        }

        public bool SelectOneValidarUsuarioInscripto(Inscripciones oInscripcion)
        {
            DBH.AddParameter("@pIDAlumno", oInscripcion.Alumno.ID);
            DBH.AddParameter("@pIDHorarioCurso", oInscripcion.HorarioCurso.ID);

            return new InscripcionesDataMapper().DataMapper(DBH.ExecuteDataTable("Inscripciones_SelectOne_ValidarUsuarioInscripto")).Count == 0;
        }

        public List<Inscripciones> SelectAllConCuotasPendientesDelAlumno(int IDAlumno)
        {
            DBH.AddParameter("@pIDAlumno", IDAlumno);

            return new InscripcionesDataMapper().DataMapper(DBH.ExecuteDataTable("Inscripciones_SelectAll_ConCuotasPendientesDelAlumno"));
        }

        public Inscripciones SelectOne(int IDInscripcion)
        {
            DBH.AddParameter("@pIDInscripcion", IDInscripcion);

            return new InscripcionesDataMapper().DataMapper(DBH.ExecuteDataTable("Inscripciones_SelectOne"))[0];
        }

        public object Update(int IDInscripcion,int IDHorarioCurso)
        {
            DBH.AddParameter("@pIDInscripcion", IDInscripcion);
            DBH.AddParameter("@pIDHorarioCurso", IDHorarioCurso);


            return DBH.ExecuteScalar("Inscripciones_Update");
        }

        public object UpdateModalidad(int IdInscripcion, string Modalidad)
        {

            DBH.AddParameter("@pIDInscripcion", IdInscripcion);
            DBH.AddParameter("@pModalidad", Modalidad);

            return DBH.ExecuteScalar("Inscripciones_Update_Modalidad");
        }
    }
}
