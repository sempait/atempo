﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.Bar;
using DataBaseAccess;

namespace DataLayer
{
    public class HistoricoCostosArticuloData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(HistoricoCostosArticulos oEntity)
        {

            DBH.AddParameter("@pIDArticulo", oEntity.Articulo.ID);
            DBH.AddParameter("@pCostoArticulo", oEntity.Costo);

            return DBH.ExecuteScalar("HistoricoCostosArticulos_Insert");
        }
    }
}
