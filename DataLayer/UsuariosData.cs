﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer;
using EntitiesLayer.DataMapperClasses;

namespace DataLayer
{
    public class UsuariosData 
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public List<Usuarios> SelectAll()
        {
            return new UsuariosDataMapper().DataMapper(DBH.ExecuteDataTable("Usuarios_SelectAll"));
        }

        public object Insert(Usuarios oUsuario)
        {
            DBH.AddParameter("@pUsuario", oUsuario.Usuario);
            DBH.AddParameter("@pPassword", oUsuario.Password);
            DBH.AddParameter("@pNombreYApellidoUsuario", oUsuario.NombreYApellido);
            DBH.AddParameter("@pIDRol", oUsuario.Rol.ID);

            return DBH.ExecuteScalar("Usuarios_Insert");
        }

        public Usuarios SelectOne(int ID)
        {
            DBH.AddParameter("@pIDUsuario", ID);

            return new UsuariosDataMapper().DataMapper(DBH.ExecuteDataTable("Usuarios_SelectOne"))[0];
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDUsuario", ID);

            DBH.ExecuteNonQuery("Usuarios_Delete");
        }

        public void Update(Usuarios oUsuario)
        {
            DBH.AddParameter("@pIDUsuario", oUsuario.ID);
            DBH.AddParameter("@pUsuario", oUsuario.Usuario);
            DBH.AddParameter("@pPassword", oUsuario.Password);
            DBH.AddParameter("@pNombreYApellidoUsuario", oUsuario.NombreYApellido);
            DBH.AddParameter("@pIDRol", oUsuario.Rol.ID);

            DBH.ExecuteNonQuery("Usuarios_Update");
        }

        public Usuarios SelectOne_ValidarUsuario(string Usuario, string Password)
        {
            DBH.AddParameter("@pUsuario", Usuario);
            DBH.AddParameter("@pPassword", Password);

            List<Usuarios> oUsuario = new UsuariosDataMapper().DataMapper(DBH.ExecuteDataTable("Usuarios_SelectOne_ValidarUsuario"));

            return oUsuario.Count == 0 ? null : oUsuario[0];
        }


        public Usuarios SelectOneDisponibilidadUsuario(Usuarios pUsuario)
        {
            DBH.AddParameter("@pIDUsuario", pUsuario.ID);
            DBH.AddParameter("@pUsuario", pUsuario.Usuario);

            List<Usuarios> oUsuario = new UsuariosDataMapper().DataMapper(DBH.ExecuteDataTable("Usuarios_SelectOne_DisponibilidadUsuario"));

            return oUsuario.Count == 0 ? null : oUsuario[0];
        }
    }
}
