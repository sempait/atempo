﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.SalaDeEnsayo;
using DataBaseAccess;
using EntitiesLayer.SalaDeEnsayo.DataMapperClasses;

namespace DataLayer
{
    public class GastosSalaData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(GastosSala oGasto)
        {
            DBH.AddParameter("@pDescripcion", oGasto.DescripcionGasto);
            DBH.AddParameter("@pImporte", oGasto.Improte);
            DBH.AddParameter("@pFechaGasto", oGasto.FechaGasto);
            DBH.AddParameter("@pAfectaBaja", oGasto.AftectaCaja);


            return DBH.ExecuteScalar("GastosSala_Insert");

        }

        public object Update(GastosSala oGasto)
        {
            DBH.AddParameter("@pId_Gasto", oGasto.ID);
            DBH.AddParameter("@pDescripcion", oGasto.DescripcionGasto);
            DBH.AddParameter("@pImporte", oGasto.Improte);
            DBH.AddParameter("@pFechaGasto", oGasto.FechaGasto);
            DBH.AddParameter("@pAfectaBaja", oGasto.AftectaCaja);

            return DBH.ExecuteNonQuery("GastosSala_Update");
        }



        public GastosSala SelectOne(int Id_Gasto)
        {
            DBH.AddParameter("@pId_Gasto", Id_Gasto);
            GastosSala mGasto = new GastosSalaDataMapper().DataMapper(DBH.ExecuteDataTable("GastosSala_SelectOne"))[0];

            return mGasto;

        }

        public object Delete(int Id_Gasto)
        {
            DBH.AddParameter("@pId_Gasto", Id_Gasto);

            return DBH.ExecuteNonQuery("GastosSala_Delete");
        }
    }
}
