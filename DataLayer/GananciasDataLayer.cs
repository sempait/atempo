﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using System.Data;

namespace DataLayer
{
    public class GananciasDataLayer
    {
        DataBaseHelper dbHelper = DataBaseHelper.Instance;
        public DataTable RecuperarOperaciones(DateTime fechaDesde, DateTime fechaHasta)
        {
            DataTable tablaPagos = new DataTable();
            dbHelper.AddParameter("@pFechaDesde", fechaDesde);
            dbHelper.AddParameter("@pFechaHasta", fechaHasta);
            tablaPagos = dbHelper.ExecuteDataTable("PagosPorFechas");
            var TotalPagos = tablaPagos.AsEnumerable().Select(r => Convert.ToInt32(r.Field<double>("importe"))).Sum();

            DataRow filaTotalPagos = tablaPagos.NewRow();
            filaTotalPagos["FechaOperacion"] = fechaDesde;
            filaTotalPagos["DescripcionOperacion"] = "Total Pagos";
            filaTotalPagos["TipoOperacion"] = "TP";
            filaTotalPagos["Importe"] = TotalPagos;

            tablaPagos.Rows.Add(filaTotalPagos);

            DataTable tablaGastos = new DataTable();
            dbHelper.AddParameter("@pFechaDesde", fechaDesde);
            dbHelper.AddParameter("@pFechaHasta", fechaHasta);
            tablaGastos = dbHelper.ExecuteDataTable("GastosPorFechas");

            var TotalGastos = tablaGastos.AsEnumerable().Select(r => Convert.ToInt32(r.Field<double>("importe"))).Sum();

            DataRow filaTotalGastos = tablaGastos.NewRow();
            filaTotalGastos["FechaOperacion"] = fechaDesde;
            filaTotalGastos["DescripcionOperacion"] = "Total Gastos";
            filaTotalGastos["TipoOperacion"] = "TG";
            filaTotalGastos["Importe"] = TotalGastos;
            tablaGastos.Rows.Add(filaTotalGastos);


            tablaGastos.Merge(tablaPagos);
            return tablaGastos;

        }


    }
}
