﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer.SalaDeEnsayo.DataMapperClasses;
using EntitiesLayer;

namespace DataLayer
{
    public class OperacionCajaDiariaSalaData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(EntitiesLayer.OperacionCajaDiariaSala mOperacion)
        {
            if (mOperacion.Egreso != 0)
            {
                DBH.AddParameter("@pEgreso", mOperacion.Egreso);
            }
            else
            {
                DBH.AddParameter("@pIngreso", mOperacion.Ingreso);
            }

            DBH.AddParameter("@pDescripcionOperacion", mOperacion.DescripcionOperacion);
            DBH.AddParameter("@pFechaOperacion", mOperacion.FechaOperacion);
            return DBH.ExecuteNonQuery("OperacionCajadiariaSala_Insert");

        }


        public EntitiesLayer.OperacionCajaDiariaSala SelectOne(int Id_Caja_Sala)
        {
            DBH.AddParameter("@pId_Caja_Sala", Id_Caja_Sala);
            OperacionCajaDiariaSala mOperacion = new OperacionCajaDiariaSalaDataMapper().DataMapper(DBH.ExecuteDataTable("OperacionesSala_SelectOne"))[0];

            return mOperacion;

        }



        public object Update(OperacionCajaDiariaSala mOperacion)
        {
            DBH.AddParameter("@pId_Caja_Sala", mOperacion.ID);
            if (mOperacion.Egreso != 0)
                DBH.AddParameter("@pEgreso", mOperacion.Egreso);
            else
                DBH.AddParameter("@pIngreso", mOperacion.Ingreso);

            DBH.AddParameter("@pDescripcionOperacion", mOperacion.DescripcionOperacion);
            DBH.AddParameter("@pFechaOperacion", mOperacion.FechaOperacion);

            return DBH.ExecuteNonQuery("OperacionCajadiariaSala_Update");
        }

        public object Delete(int Id_Caja_Sala)
        {
            DBH.AddParameter("@pId_Caja_Sala", Id_Caja_Sala);
            return DBH.ExecuteNonQuery("OperacionCajadiariaSala_Delete");
        }

        public object CloseCaja(DateTime FechaOperacion)
        {

            DateTime FechaOperacionPost = FechaOperacion.AddDays(1);
            DBH.AddParameter("@pFechaOperacion", FechaOperacion);
            DBH.AddParameter("@pFechaOperacionPost", FechaOperacionPost);
            return DBH.ExecuteNonQuery("OperacionCajadiariaSala_Close");
        }
    }
}
