﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer.Bar;
using EntitiesLayer.Bar.DataMapperClasses;

namespace DataLayer
{
    public class TiposArticulosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(TiposArticulos oTipoArticulo)
        {
            DBH.AddParameter("@pDescripcionTipoArticulo", oTipoArticulo.DescripcionTipoArticulo);

            return DBH.ExecuteNonQuery("TiposArticulos_Insert");
        }

        public void Update(TiposArticulos oTipoArticulo)
        {
            DBH.AddParameter("@pIDTipoArticulo", oTipoArticulo.ID);
            DBH.AddParameter("@pDescripcionTipoArticulo", oTipoArticulo.DescripcionTipoArticulo);

            DBH.ExecuteNonQuery("TiposArticulos_Update");
        }

        public TiposArticulos SelectOne(int ID)
        {
            DBH.AddParameter("@pIDTipoArticulo", ID);

            return new TiposArticulosDataMapper().DataMapper(DBH.ExecuteDataTable("TiposArticulos_SelectOne"))[0]; ;
        }

        public List<TiposArticulos> SelectAll()
        {
            return new TiposArticulosDataMapper().DataMapper(DBH.ExecuteDataTable("TiposArticulos_SelectAll"));
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDTipoArticulo", ID);

            DBH.ExecuteNonQuery("TiposArticulos_Delete");
        }
    }
}
