﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer;
using EntitiesLayer.DataMapperClasses;

namespace DataLayer
{
    public class ParametrosData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public Parametros SelectOneParaPagosMensuales(int Mes, int Anio)
        {
            DBH.AddParameter("@pMes", Mes);
            DBH.AddParameter("@pAnio", Anio);

            return new ParametrosDataMapper().DataMapper(DBH.ExecuteDataTable("Parametros_SelectOne_ParaPagosMensuales"))[0];
        }

        public void Insert(Parametros oParametro)
        {
            DBH.AddParameter("@pCantidadMaximaAlumnos", oParametro.CantidadMaximaAlumnos);
            //DBH.AddParameter("@pHoraApertura", oParametro.HoraApertura);
            //DBH.AddParameter("@pHoraCierre", oParametro.HoraCierre);
            //DBH.AddParameter("@pPrecioHoraGrupal", oParametro.PrecioHoraGrupal);
            //DBH.AddParameter("@pPrecioHoraIndividual", oParametro.PrecioHoraIndividual);

            DBH.ExecuteScalar("Parametros_Insert");
        }

        public Parametros Select(int ID)
        {
            DBH.AddParameter("@pIDParametro", ID);
            Parametros oParametro = new ParametrosDataMapper().DataMapper(DBH.ExecuteDataTable("Parametros_SelectOne"))[0];

            return oParametro;
        }

        public void Update(Parametros oParametrosEdit)
        {
            DBH.AddParameter("@pIDParametro", oParametrosEdit.ID);
            DBH.AddParameter("@pUsuario", oParametrosEdit.Usuario);
            DBH.AddParameter("@pEmail", oParametrosEdit.Email);
            DBH.AddParameter("@pContrasenia", oParametrosEdit.Contrsenia);
            DBH.AddParameter("@pHoraDesdeEscuela", oParametrosEdit.HoraAperturaEscuela);
            DBH.AddParameter("@pHoraHastaEscuela", oParametrosEdit.HoraCierreEscuela);
            DBH.AddParameter("@pHoraDesdeSala", oParametrosEdit.HoraAperturaSala);
            DBH.AddParameter("@pHoraHastaSala", oParametrosEdit.HoraCierreSala);
            DBH.AddParameter("@pHoraDesdeSalaSabado", oParametrosEdit.HoraAperturaSalaSabado);
            DBH.AddParameter("@pHoraHastaSalaSabado", oParametrosEdit.HoraCierreSalaSabado);
            DBH.AddParameter("@pCantMaxAlumnosGrupo", oParametrosEdit.CantidadMaximaAlumnos);
            DBH.AddParameter("@pTelefono", oParametrosEdit.Telefono);
            DBH.AddParameter("@pCelular", oParametrosEdit.Celular);
            DBH.AddParameter("@pDireccion", oParametrosEdit.Direccion);
            DBH.AddParameter("@pPrecioHoraIndividual", oParametrosEdit.PrecioHoraIndividual);
            DBH.AddParameter("@pPrecioHoraGrupal", oParametrosEdit.PrecioHoraGrupal);
            DBH.AddParameter("@pPorcentajePlusProfesor", oParametrosEdit.PorcentajePlusProfesor);
            DBH.AddParameter("@pPrecioHoraSala", oParametrosEdit.PrecioHoraSala);

            DBH.ExecuteNonQuery("Parametros_Update");
        }
    }
}
