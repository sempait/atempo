﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;

namespace DataLayer
{
    public class ProfesoresData 
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public List<Profesores> SelectAll()
        {
            return new ProfesoresDataMapper().DataMapper(DBH.ExecuteDataTable("Profesores_Selectall"));
        }

        public Profesores SelectOne(int ID)
        {
            DBH.AddParameter("@pIDProfesor", ID);

            return new ProfesoresDataMapper().DataMapper(DBH.ExecuteDataTable("Profesores_SelectOne"))[0];
        }

        public object Insert(Profesores oProfesor)
        {
            DBH.AddParameter("@pNombreYApellidoProfesor", oProfesor.NombreYApellidoProfesor);
            DBH.AddParameter("@pTelefonoProfesor", oProfesor.Telefono);
            DBH.AddParameter("@pEmailProfesor", oProfesor.Email);
            DBH.AddParameter("@pDireccionProfesor", oProfesor.Direccion);
            DBH.AddParameter("@pCiudad", oProfesor.Ciudad);
            DBH.AddParameter("@pDNI", oProfesor.DNI);
            DBH.AddParameter("@pFechaNacimiento", oProfesor.FechaNacimiento);
            
            return DBH.ExecuteScalar("Profesores_Insert");
        }

        public void Update(Profesores oProfesor)
        {
            DBH.AddParameter("@pIDProfesor", oProfesor.ID);
            DBH.AddParameter("@pNombreYApellidoProfesor", oProfesor.NombreYApellidoProfesor);
            DBH.AddParameter("@pTelefonoProfesor", oProfesor.Telefono);
            DBH.AddParameter("@pEmailProfesor", oProfesor.Email);
            DBH.AddParameter("@pDireccionProfesor", oProfesor.Direccion);
            DBH.AddParameter("@pCiudad", oProfesor.Ciudad);
            DBH.AddParameter("@pDNI", oProfesor.DNI);
            DBH.AddParameter("@pFechaNacimiento", oProfesor.FechaNacimiento);

            DBH.ExecuteNonQuery("Profesores_Update");
        }

        public void Delete(int ID)
        {
            DBH.AddParameter("@pIDProfesor", ID);

            DBH.ExecuteNonQuery("Profesores_Delete");
        }
    }
}
