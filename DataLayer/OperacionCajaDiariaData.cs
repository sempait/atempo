﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer.EscuelaDeMusica.DataMapperClasses;
using EntitiesLayer;


namespace DataLayer
{
    public class OperacionCajaDiariaData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public object Insert(EntitiesLayer.OperacionCajaDiaria mOperacion)
        {
            if (mOperacion.Egreso != 0)
            {
                DBH.AddParameter("@pEgreso", mOperacion.Egreso);
            }
            else
            {
                DBH.AddParameter("@pIngreso", mOperacion.Ingreso);
            }

            DBH.AddParameter("@pDescripcionOperacion", mOperacion.DescripcionOperacion);
            DBH.AddParameter("@pFechaOperacion", mOperacion.FechaOperacion);
            return DBH.ExecuteNonQuery("OperacionCajadiaria_Insert");

        }


        public EntitiesLayer.OperacionCajaDiaria SelectOne(int Id_Caja)
        {
            DBH.AddParameter("@pId_Caja", Id_Caja);
            OperacionCajaDiaria mOperacion = new OperacionCajaDiariaDataMapper().DataMapper(DBH.ExecuteDataTable("Operaciones_SelectOne"))[0];

            return mOperacion;

        }



        public object Update(OperacionCajaDiaria mOperacion)
        {
            DBH.AddParameter("@pId_Caja", mOperacion.ID);
            if (mOperacion.Egreso != 0)
                DBH.AddParameter("@pEgreso", mOperacion.Egreso);
            else
                DBH.AddParameter("@pIngreso", mOperacion.Ingreso);

            DBH.AddParameter("@pDescripcionOperacion", mOperacion.DescripcionOperacion);
            DBH.AddParameter("@pFechaOperacion", mOperacion.FechaOperacion);

            return DBH.ExecuteNonQuery("OperacionCajadiaria_Update");
        }

        public object Delete(int Id_Caja)
        {
            DBH.AddParameter("@pId_Caja", Id_Caja);
            return DBH.ExecuteNonQuery("OperacionCajadiaria_Delete");
        }

        public object CloseCaja(DateTime FechaOperacion)
        {

            DateTime FechaOperacionPost = FechaOperacion.AddDays(1);
            DBH.AddParameter("@pFechaOperacion", FechaOperacion);
            DBH.AddParameter("@pFechaOperacionPost", FechaOperacionPost);
            return DBH.ExecuteNonQuery("OperacionCajadiaria_Close");
        }
    }
}
