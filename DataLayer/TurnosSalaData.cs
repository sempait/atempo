﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataBaseAccess;
using EntitiesLayer.SalaDeEnsayo;
using EntitiesLayer.SalaDeEnsayo.DataMapperClasses;

namespace DataLayer
{
    public class TurnosSalaData
    {
        DataBaseHelper DBH = DataBaseHelper.Instance;

        public TurnosSala SelectOne(int ResourceID)
        {
            DBH.AddParameter("@pResourceID", ResourceID);

            return new TurnoSalaDataMapper().DataMapper(DBH.ExecuteDataTable("TurnosSala_SelectOne_ItemsTurnoSala"))[0];
        }

        public object Insert(int ResourceID, float ImpTot)
        {
            DBH.AddParameter("@pResourceID", ResourceID);
            DBH.AddParameter("@pImporteTotal", ImpTot);
            return DBH.ExecuteNonQuery("TurnosSalaPago_Insert");
        }
    }
}
