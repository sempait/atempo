﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer.SalaDeEnsayo;

namespace BusinessLayer
{
    public class SalasBusiness
    {
        public EntitiesLayer.SalaDeEnsayo.Salas Select_One(int UniqueID)
        {
            return new SalasData().SelectOne(UniqueID);
        }

        public void Update(Salas oSalas)
        {
            new SalasData().Update(oSalas);
        }

        public Salas SelectOnePorResourceID(int ResourceID)
        {
            return new SalasData().SelectOnePorResourceID(ResourceID);
        }
    }
}
