﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;
using System.Transactions;

namespace BusinessLayer
{
    public class CursosBusiness
    {
        public List<Cursos> SelectAll()
        {
            return new CursosData().SelectAll();
        }

        public void Delete(int ID)
        {
            new CursosData().Delete(ID);
        }

        public List<Cursos> SelectAllCursosNoAsignados(int IDProfesor)
        {
            return new CursosData().SelectAllCursosNoAsignados(IDProfesor);
        }

        public Cursos SelectOne(int IdCurso)
        {
            return new CursosData().SelectOne(IdCurso);
        }

        public void Insert(Cursos oCursoActual)
        {

            new CursosData().Insert(oCursoActual);

               
        }

        public void Update(Cursos oCursoActual)
        {
            new CursosData().Update(oCursoActual);
        }
    }
}
