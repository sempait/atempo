﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataLayer;

namespace BusinessLayer
{
    public class ParametrosBusiness
    {
        public Parametros Select(int IdParametro)
        {
            return new ParametrosData().Select(IdParametro);
        }
        
        public void Insert(Parametros oParametro)
        {
            new ParametrosData().Insert(oParametro);
        }

        public Parametros SelectOneParaPagosMensuales(int Mes, int Anio)
        {
            return new ParametrosData().SelectOneParaPagosMensuales(Mes, Anio);
        }

        public void Update(Parametros oParametrosEdit)
        {
            new ParametrosData().Update(oParametrosEdit);
        }
    }
}
