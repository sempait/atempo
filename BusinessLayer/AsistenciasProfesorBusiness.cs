﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataLayer;

namespace BusinessLayer
{
    public class AsistenciasProfesorBusiness
    {
        public void Insert(AsistenciasProfesores oAsistenciaProfesor)
        {
            new AsistenciasProfesoresData().Insert(oAsistenciaProfesor);
        }

        public void Delete(int IDAsistenciaProfesor)
        {
            new AsistenciasProfesoresData().Delete(IDAsistenciaProfesor);
        }
    }
}
