﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataLayer;

namespace BusinessLayer
{
    public class PagoProfesoresBusiness
    {
        public DataTable RecuperarPagoProfesores(DateTime fechaDesde, DateTime fechaHasta)
        {
            return new PagoProfesoresData().RecuperarPagoProfesoresDatos(fechaDesde, fechaHasta);
        }
    }
}
