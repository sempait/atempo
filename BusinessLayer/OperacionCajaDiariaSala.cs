﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;

namespace BusinessLayer
{
   public class OperacionCajaDiariaSala
    {

        public void OperacionInsert(EntitiesLayer.OperacionCajaDiariaSala operacion)
        {
            new OperacionCajaDiariaSalaData().Insert(operacion);
        }

        public void Update(EntitiesLayer.OperacionCajaDiariaSala mOperacion)
        {
            new OperacionCajaDiariaSalaData().Update(mOperacion);
        }

        public EntitiesLayer.OperacionCajaDiariaSala Select_One(int IdOperacion)
        {

            return new OperacionCajaDiariaSalaData().SelectOne(IdOperacion);
        }

        public void Delete(int Id_Caja)
        {
            new OperacionCajaDiariaSalaData().Delete(Id_Caja);
        }

        public void CloseCaja(DateTime FechaOperacion)
        {
            new OperacionCajaDiariaSalaData().CloseCaja(FechaOperacion);
        }
    }
}
