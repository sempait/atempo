﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.SalaDeEnsayo;
using DataLayer;

namespace BusinessLayer
{
    public class TurnosSalaBusiness
    {
        public TurnosSala SelectOne(int ResourceID)
        {
            return new TurnosSalaData().SelectOne(ResourceID);
        }

        public void InsertPago(int ResourceID, float ImpTot)
        {
            new TurnosSalaData().Insert(ResourceID, ImpTot);
        }
    }
}
