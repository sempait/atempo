﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer.Bar;

namespace BusinessLayer
{
    public class GastosBarBusiness
    {


        public void Insert(GastosBar oGasto)
        {
            new GastosBarData().Insert(oGasto);
        }

        public void Update(GastosBar oGasto)
        {
            new GastosBarData().Update(oGasto);
        }

        public GastosBar Select_One(int Id_Gasto)
        {
            return new GastosBarData().SelectOne(Id_Gasto);
        }

        public void Delete(int Id_Gasto)
        {
            new GastosBarData().Delete(Id_Gasto);
        }
    }
}
