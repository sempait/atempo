﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer.Bar;

namespace BusinessLayer
{
    public class TiposArticulosBusiness
    {
        public TiposArticulos SelectOne(int IdTipoArticulo)
        {
            return new TiposArticulosData().SelectOne(IdTipoArticulo);
        }

        public void Delete(int IdTipoArticulo)
        {
            new TiposArticulosData().Delete(IdTipoArticulo);
        }

        public void Insert(TiposArticulos oTiposArticulos)
        {
            new TiposArticulosData().Insert(oTiposArticulos);
        }

        public void Update(TiposArticulos oTiposArticulos)
        {
            new TiposArticulosData().Update(oTiposArticulos);
        }

        public List<TiposArticulos> SelectAll()
        {
            return new TiposArticulosData().SelectAll();
        }
    }
}
