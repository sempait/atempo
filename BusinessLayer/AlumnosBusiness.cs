﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;

namespace BusinessLayer
{
    public class AlumnosBusiness
    {
        public List<Alumnos> AlumnosSelectAll()
        {
            return new AlumnosData().SelectAll();
        }

        public void AlumnosInsert(Alumnos alumno)
        {
            new AlumnosData().Insert(alumno);
        }

        public void Update(Alumnos oAlumno)
        {
            new AlumnosData().Update(oAlumno);
        }

        public Alumnos SelectOne(int ID)
        {
            return new AlumnosData().SelectOne(ID);
        }

        public void Delete(int ID)
        {
            new AlumnosData().Delete(ID);
        }

        public List<Alumnos> SelectAllConCuotasPendientes()
        {
            return new AlumnosData().SelectAllConCuotasPendientes();
        }

        public void UpdateBajaAlumno(Alumnos alumnoActual)
        {
            new AlumnosData().UpdateBajaAlumno(alumnoActual);
        }
    }
}
