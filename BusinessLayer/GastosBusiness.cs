﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer.EscuelaDeMusica;

namespace BusinessLayer
{
    public class GastosBusiness
    {


        public void Insert(Gastos oGasto)
        {
            new GastosData().Insert(oGasto);
        }

        public void Update(Gastos oGasto)
        {
            new GastosData().Update(oGasto);
        }

        public Gastos Select_One(int Id_Gasto)
        {
            return new GastosData().SelectOne(Id_Gasto);
        }

        public void Delete(int Id_Gasto)
        {
            new GastosData().Delete(Id_Gasto);
        }
    }
}
