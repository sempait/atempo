﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;

namespace BusinessLayer
{
    public class UsuariosBusiness
    {

        public List<Usuarios> SelectAll()
        {
            return new UsuariosData().SelectAll();
        }

        public string Insert(Usuarios oUsuario)
        {
            UsuariosData oUsuarioData = new UsuariosData();

            if (oUsuarioData.SelectOneDisponibilidadUsuario(oUsuario) == null)
            {
                oUsuarioData.Insert(oUsuario);
                return string.Empty;
            }
            else
                return "El nombre de Usuario se encuentra en uso";
        }

        public string Update(Usuarios oUsuario)
        {
            UsuariosData oUsuarioData = new UsuariosData();

            if (oUsuarioData.SelectOneDisponibilidadUsuario(oUsuario) == null)
            {
                oUsuarioData.Update(oUsuario);
                return string.Empty;
            }
            else
                return "El nombre de Usuario se encuentra en uso";
        }

        public Usuarios SelectOne(int ID)
        {
            return new UsuariosData().SelectOne(ID);
        }

        public void Delete(int idUsuario)
        {
            new UsuariosData().Delete(idUsuario);
        }

        public Usuarios SelectOne_ValidarUsuario(string u, string p)
        {
            return new UsuariosData().SelectOne_ValidarUsuario(u,p);
        }
    }
}
