﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;
using System.Transactions;

namespace BusinessLayer
{
    public class InscripcionesBusiness
    {
        public string Insert(Inscripciones oInscripcion)
        {
            string OutPut = string.Empty;

            using (TransactionScope oTransactionScope = new TransactionScope())
            {
                if (new InscripcionesData().SelectOneValidarUsuarioInscripto(oInscripcion))
                    new InscripcionesData().Insert(oInscripcion);
                else
                    OutPut = "El Alumno ya se encuentra inscripto al curso elegido";

                oTransactionScope.Complete();
            }

            return OutPut;
        }

        public List<Inscripciones> SelectAllConCuotasPendientesDelAlumno(int IDAlumno)
        {
            return new InscripcionesData().SelectAllConCuotasPendientesDelAlumno(IDAlumno);
        }

        public Inscripciones SelectOne(int IDInscripcion)
        {
            return new InscripcionesData().SelectOne(IDInscripcion);
        }

        public void Update(int IDInscripcion,int IDHorarioCurso)
        {
            new InscripcionesData().Update(IDInscripcion, IDHorarioCurso);
        }

        public void Delete(int IDInscripcion)
        {
            new InscripcionesData().Delete(IDInscripcion);
        }

        public HorariosCursos SelectHorarioCursoInscripcion(string IDHorarioCurso)
        {
            return new HorariosCursosData().SelectOne(int.Parse(IDHorarioCurso));
        }

        public void UpdateModalidad(string IDInscripcionSeleccionada,string Modalidad)
        {
            new InscripcionesData().UpdateModalidad(int.Parse(IDInscripcionSeleccionada), Modalidad);
        }
    }
}
