﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;

namespace BusinessLayer
{
   public class OperacionCajaDiariaBar
    {

        public void OperacionInsert(EntitiesLayer.OperacionCajaDiariaBar operacion)
        {
            new OperacionCajaDiariaBarData().Insert(operacion);
        }

        public void Update(EntitiesLayer.OperacionCajaDiariaBar mOperacion)
        {
            new OperacionCajaDiariaBarData().Update(mOperacion);
        }

        public EntitiesLayer.OperacionCajaDiariaBar Select_One(int IdOperacion) {

            return new OperacionCajaDiariaBarData().SelectOne(IdOperacion);
        }

        public void Delete(int Id_Caja)
        {
            new OperacionCajaDiariaBarData().Delete(Id_Caja);
        }

        public void CloseCaja(DateTime FechaOperacion)
        {
            new OperacionCajaDiariaBarData().CloseCaja(FechaOperacion);
        }
    }
}
