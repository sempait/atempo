﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;

namespace BusinessLayer
{
    public class VentasBusiness
    {
        public void InsertVenta(DateTime fecha,DateTime hora,float importetotal,float costototal)
        {

            new VentasData().Insert(fecha,hora,importetotal,costototal);
        }

        public void InsertItemVenta(int Cantidad, string IdArticulo)
        {
            new VentasData().InsertItemVenta(Cantidad,IdArticulo);
        }

        public object SelectAllItemVenta(int IdVenta)
        {
            return new VentasData().SelectAllItemVenta(IdVenta);

        }

        public object SellecAllVentas()
        {
            return new VentasData().SelectAll();
        }

        public void Delete(int IdVenta)
        {
            new VentasData().DeleteVenta(IdVenta);
        }
    }
}
