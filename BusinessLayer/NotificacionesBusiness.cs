﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;

namespace BusinessLayer
{
    public class NotificacionesBusiness
    {
        public Notificaciones SelectOne(int ID)
        {
            return new NotificacionesData().SelectOne(ID);
        }


        public void Update(Notificaciones oNotificaciones)
        {
            new NotificacionesData().Update(oNotificaciones);
        }
    }
}
