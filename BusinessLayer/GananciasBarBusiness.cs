﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataLayer;

namespace BusinessLayer
{
    public class GananciasBarBusiness
    {

        public DataTable RecuperarGanancias(DateTime fechaDesde, DateTime fechaHasta)
        {
            return new GananciasBarDataLayer().RecuperarOperaciones(fechaDesde, fechaHasta);
        }
    }
}
