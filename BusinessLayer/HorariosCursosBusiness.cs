﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataLayer;
using System.Transactions;

namespace BusinessLayer
{
    public class HorariosCursosBusiness
    {
        public List<HorariosCursos> SelectAllCursosActuales(int IDCurso,string TipoCurso)
        {
            return new HorariosCursosData().SelectAllCursosActuales(IDCurso, TipoCurso);
        }

        public List<HorariosCursos> SelectAllProfesor(int IDProfesor)
        {
            return new HorariosCursosData().SelectAllProfesor(IDProfesor);
        }

        public void Insert(HorariosCursos oHorarioCurso)
        {
            new HorariosCursosData().Insert(oHorarioCurso);
        }

        public List<HorariosCursos> ValidarHorarioCurso(string IDHorarioDisponibilidad)
        {
            return new HorariosCursosData().ValidarHorarioCurso(int.Parse(IDHorarioDisponibilidad));
        }

        public void Delete(string IDHorarioCurso)
        {
            new HorariosCursosData().Delete(int.Parse(IDHorarioCurso));
        }

        public List<HorariosCursos> SelectHorariosCursoAsistencia(DateTime fecha, string NombreDia, string IDProfesor)
        {
           return new HorariosCursosData().SelectHorariosCursoAsistencia(fecha,NombreDia,IDProfesor);
        }

        public HorariosCursos SelectOne(int IDHorarioCurso)
        {
            return new HorariosCursosData().SelectOne(IDHorarioCurso);
        }

        public void Update(HorariosCursos oHorariosCursos)
        {
            new HorariosCursosData().Update(oHorariosCursos);
        }
    }
}
