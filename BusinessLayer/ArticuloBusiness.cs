﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.Bar;
using DataLayer;

namespace BusinessLayer
{
    public class ArticuloBusiness
    {
        public Articulos SelectOne(int ID)
        {
            return new ArticulosData().SelectOne(ID);
        }
    }
}
