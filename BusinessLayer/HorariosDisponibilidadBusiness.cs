﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;

namespace BusinessLayer
{
    public class HorariosDisponibilidadBusiness
    {
        public void Insert(HorariosDisponibilidad oHorariosDisponibilidad)
        {
            new HorariosDisponibilidadData().Insert(oHorariosDisponibilidad);
        }

        public List<HorariosDisponibilidad> SelectAllDisponibilidadProfesor(int IDProfesor)
        {
            return new HorariosDisponibilidadData().SelectAllProfesor(IDProfesor);
        }

        public HorariosDisponibilidad SelectOne(int IDHorarioDisponibilidad)
        {
            return new HorariosDisponibilidadData().SelectOne(IDHorarioDisponibilidad);
        }


        public void Delete(string IDHorarioDisponibilidad)
        {
            new HorariosDisponibilidadData().Delete(int.Parse(IDHorarioDisponibilidad));
        }

        public void Update(HorariosDisponibilidad oHorarioDisponibilidad)
        {
            new HorariosDisponibilidadData().Update(oHorarioDisponibilidad);
        }
    }
}
