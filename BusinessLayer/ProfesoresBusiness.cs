﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;

namespace BusinessLayer
{
    public class ProfesoresBusiness
    {
        public List<Profesores> ProfesoresSelectAll()
        {
            return new ProfesoresData().SelectAll();
        }

        public void ProfesoresInsert(Profesores profesor)
        {
            new ProfesoresData().Insert(profesor);
        }

        public Profesores SelectOne(int ID)
        {
            return new ProfesoresData().SelectOne(ID);
        }

        public void Delete(int ID)
        {
            new ProfesoresData().Delete(ID);
        }


        public void Update(Profesores profesor) {

            new ProfesoresData().Update(profesor);
        }
    }
}
