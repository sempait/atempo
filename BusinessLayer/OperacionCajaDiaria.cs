﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;

namespace BusinessLayer
{
   public class OperacionCajaDiaria
    {

        public void OperacionInsert(EntitiesLayer.OperacionCajaDiaria operacion)
        {
            new OperacionCajaDiariaData().Insert(operacion);
        }

        public void Update(EntitiesLayer.OperacionCajaDiaria mOperacion)
        {
            new OperacionCajaDiariaData().Update(mOperacion);
        }

        public EntitiesLayer.OperacionCajaDiaria Select_One(int IdOperacion) {

            return new OperacionCajaDiariaData().SelectOne(IdOperacion);
        }

        public void Delete(int Id_Caja)
        {
            new OperacionCajaDiariaData().Delete(Id_Caja);
        }

        public void CloseCaja(DateTime FechaOperacion)
        {
            new OperacionCajaDiariaData().CloseCaja(FechaOperacion);
        }
    }
}
