﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer.SalaDeEnsayo;
using DataLayer;
using System.Data;

namespace BusinessLayer
{
    public class ItemTurnoSalaBusiness
    {
        public void Insert(ItemTurnoSala oItemTurnoSala)
        {
            new ItemTurnoSalaData().Insert(oItemTurnoSala);
        }


        public void Insert(int idTurnoSala, int idArticulo, int cantidadItemTurnoSala)
        {
            new ItemTurnoSalaData().InsertNuevo(idTurnoSala, idArticulo, cantidadItemTurnoSala);
        }

        public DataTable SelectAllPorTurnoSala(int idTurnoSala)
        {
            return new ItemTurnoSalaData().SelectAllPorTurnoSala(idTurnoSala);
        }

        public void DeletePorTurnoSala(int idTurnoSala)
        {
            new ItemTurnoSalaData().DeletePorTurnoSala(idTurnoSala);
        }
    }
}
