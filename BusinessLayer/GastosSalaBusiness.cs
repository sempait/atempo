﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer.SalaDeEnsayo;

namespace BusinessLayer
{
    public class GastosSalaBusiness
    {


        public void Insert(GastosSala oGasto)
        {
            new GastosSalaData().Insert(oGasto);
        }

        public void Update(GastosSala oGasto)
        {
            new GastosSalaData().Update(oGasto);
        }

        public GastosSala Select_One(int Id_Gasto)
        {
            return new GastosSalaData().SelectOne(Id_Gasto);
        }

        public void Delete(int Id_Gasto)
        {
            new GastosSalaData().Delete(Id_Gasto);
        }
    }
}
