﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;

namespace BusinessLayer
{
    public class HorarioDisponibilidadBusiness
    {
        public List<HorariosDisponibilidad> SelectAllProfesor(int IDProfesor)
        {
            return new HorariosDisponibilidadData().SelectAllProfesor(IDProfesor);
        }

        public void Insert(HorariosDisponibilidad oHorarioDisponibilidad)
        {
            new HorariosDisponibilidadData().Insert(oHorarioDisponibilidad);
        }
    }
}
