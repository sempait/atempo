﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;

namespace BusinessLayer
{
    public class RolesBusiness
    {
        public List<Roles> SelectAll()
        {
            return new RolesData().SelectAll();
        }
    }
}
