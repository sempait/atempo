﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer.Bar;
using System.Transactions;

namespace BusinessLayer
{
    public class ArticulosBusiness
    {

        public List<Articulos> SelectAll()
        {

            return new ArticulosData().SelectAllEnStock();
        }

        public void Delete(int IdArticulo)
        {
            new ArticulosData().Delete(IdArticulo);
        }

        public Articulos SelectOne(int IdArticulo)
        {
            return new ArticulosData().SelectOne(IdArticulo);
        }

        public void Update(Articulos oArticulos)
        {
            new ArticulosData().Update(oArticulos);
        }

        public void Insert(Articulos oArticulos)
        {
            using (TransactionScope oTransactionScope = new TransactionScope())
            {
                int IDArticulo = int.Parse(new ArticulosData().Insert(oArticulos).ToString());

                oArticulos.HistoricoCostoArticulo[0].Articulo.ID = IDArticulo;
                oArticulos.HistoricoPrecioArticulo[0].Articulo.ID = IDArticulo;
                new HistoricoCostosArticuloData().Insert(oArticulos.HistoricoCostoArticulo[0]);

                new HistoricoPrecioArticuloData().Insert(oArticulos.HistoricoPrecioArticulo[0]);

                oTransactionScope.Complete();
            }
        }
    }
}
