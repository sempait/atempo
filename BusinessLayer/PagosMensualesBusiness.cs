﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntitiesLayer;
using DataLayer;
using System.Data;

namespace BusinessLayer
{
    public class PagosMensualesBusiness
    {
        public List<PagosMensuales> SelectAllHorariosCursoDelAlumno(int IDInscripcion)
        {
            return new PagosMensualesData().SelectAllHorariosCursoDelAlumno(IDInscripcion);
        }

        public PagosMensuales SelectOne(int IDPagoMensual)
        {
            return new PagosMensualesData().SelectOne(IDPagoMensual);
        }

        public void Update(int IDPagoMensual, float Monto,DateTime FechaPagoCuota)
        {
            new PagosMensualesData().Update(IDPagoMensual, Monto,FechaPagoCuota);
        }

        public void InsertAll()
        {
            new PagosMensualesData().InsertAll();
        }

        public void SelectUltimasCuotasGeneradas()
        {
            DataTable dt = new PagosMensualesData().SelectUltimasCuotasGeneradas();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //si la inscripcion debe una cuota, entonces hago el insert de la cuota
                    if (Convert.ToInt32(row["MesPago"]) < DateTime.Now.Month && Convert.ToInt32(row["AñoPago"]) == DateTime.Now.Year)
                    {
                        for (int i = Convert.ToInt32(row["MesPago"]); i < DateTime.Now.Month; i++)
                        {
                        int mesPago = i + 1;
                        int añoPago = Convert.ToInt32(row["AñoPago"]);
                        int IDInscripcion = Convert.ToInt32(row["IDInscripcion"]);
                        new PagosMensualesData().InsertCuota(IDInscripcion,mesPago,añoPago);
                        }
                    }
                    else//si la inscripcion tiene todas las cuotas pagas
                    { }
                }
            }
            else //si dt 0, entonces esta vacio, quiere decir q cambio de año
            {
                DataTable dtUltimasDelAño = new PagosMensualesData().SelectUltimasDelAño();
                foreach (DataRow row in dtUltimasDelAño.Rows)
                {
                    int IDInscripcion = Convert.ToInt32(row["IDInscripcion"]);
                    new PagosMensualesData().InsertPrimeraCuotaDelAño(IDInscripcion);
                }
            }
        }

        //public void InsertCuota(int IDInscripciones)
        //{

        //    new PagosMensualesData().InsertCuota(IDInscripciones);

        //}

        public void DeletePagoMensual(int IDCuotaAlumno)
        {
            new PagosMensualesData().DeletePagoMensual(IDCuotaAlumno);
        }
    }
}
