﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataLayer;

namespace BusinessLayer
{
    public class GananciasBusiness
    {
        public DataTable RecuperarGanancias(DateTime fechaDesde, DateTime fechaHasta)
        {
            return new GananciasDataLayer().RecuperarOperaciones(fechaDesde, fechaHasta);
        }
    }
}
