﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using EntitiesLayer;

namespace BusinessLayer
{
    public class CursosProfesoresBusiness
    {
        public List<CursosProfesores> SelectAll(int IDProfesor)
        {
            return new CursosProfesoresData().SelectAll(IDProfesor);
        }

        public void Delete(int IDCursoProfesor)
        {
            new CursosProfesoresData().Delete(IDCursoProfesor);
        }

        public void Insert(CursosProfesores oCursoProfesorActual)
        {
            new CursosProfesoresData().Insert(oCursoProfesorActual);
        }

        public List<CursosProfesores> SelectAllCursosAsignados(int ID)
        {
            return new CursosProfesoresData().SelectAllCursosAsignados(ID);
        }
    }
}
