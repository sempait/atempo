﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vistaClientes.aspx.cs" Inherits="sala_vistaClientes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>"La Triada" - A Tempo Musica</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="icon" href="../images/logo-atempo.ico">
    <link rel="shortcut icon" href="../images/logo-atempo.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>



            <dx:ASPxScheduler ID="ASPxScheduler1" runat="server"
                AppointmentDataSourceID="SqlDataSource1" ClientIDMode="AutoID"
                ResourceDataSourceID="SqlDataSource2"
                GroupType="Resource" Theme="Default">
                <Storage>
                    <Appointments>
                        <Mappings AllDay="AllDay" AppointmentId="UniqueID" Description="Description"
                            End="EndDate" Label="Label" Location="Location" RecurrenceInfo="RecurrenceInfo"
                            ReminderInfo="ReminderInfo" ResourceId="ResourceID" Start="StartDate"
                            Status="Status" Subject="Subject" Type="Type" />
                    </Appointments>
                    <Resources>
                        <Mappings Caption="ResourceName" Color="Color" ResourceId="ResourceID" />
                    </Resources>
                </Storage>
                <Views>
                    <%--<DayView ShowWorkTimeOnly="true" DayCount="2">
                        <TimeRulers>
                            <dx:TimeRuler></dx:TimeRuler>
                        </TimeRulers>
                        <WorkTime Start="12:00" End="23:59" />
                    </DayView>--%>
                    <WorkWeekView>
                        <TimeRulers>
                            <dx:TimeRuler></dx:TimeRuler>
                        </TimeRulers>
                    </WorkWeekView>
                    <WeekView Enabled="False">
                    </WeekView>
                    <MonthView CompressWeekend="False">
                    </MonthView>
                    <TimelineView Enabled="False">
                    </TimelineView>
                </Views>
                <OptionsForms AppointmentFormVisibility="None" />
            </dx:ASPxScheduler>

            <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                SelectCommand="SELECT * FROM [Salas]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                SelectCommand="SELECT * FROM [TurnosSala]"></asp:SqlDataSource>

        </div>
        <div class="footer">
            <div class="footer-inner">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            &copy; 2014 <a href="http://www.sempait.com.ar" target="_blank">@SempaIT | La Triada - A Tempo Musica</a>.
                        </div>
                        <!-- /span12 -->
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /footer-inner -->
        </div>
        <!-- /footer -->
    </form>
</body>
</html>
