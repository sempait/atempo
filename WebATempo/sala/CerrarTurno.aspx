﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sala/inicio.master" AutoEventWireup="true"
    CodeFile="CerrarTurno.aspx.cs" Inherits="sala_CerrarTurno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Cerrar Turno</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <!-- /.col-lg-6 -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Información de la Banda
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="form-group">
                            <p>
                                <strong>Nombre de la Sala: </strong>
                                <asp:Label ID="lblNombreSala" runat="server" Text=""></asp:Label>
                            </p>
                            <p>
                                <strong>Nombre de la Banda: </strong>
                                <asp:Label ID="txtNombreBanda" runat="server" Text=""></asp:Label>
                            </p>
                            <p>
                                <strong>Hora Desde: </strong>
                                <asp:Label ID="lblHoraDesde" runat="server" Text=""></asp:Label>
                            </p>
                            <p>
                                <strong>Hora Hasta: </strong>
                                <asp:Label ID="lblHoraHasta" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>



            <div class="panel-body">
                <div class="panel-group" id="accordion3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion3" href="#collapseSeven">Agregar Items consumidos del Bar</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            1 - Seleccionar Artículo
                                        </div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <dx:ASPxGridView ID="gdvArticulos" runat="server" Width="100%" AutoGenerateColumns="False"
                                                KeyFieldName="IDArticulo" EnableTheming="True" Theme="Aqua" DataSourceID="SqlDataSourceArticulos">
                                                <Columns>
                                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True"
                                                        VisibleIndex="0" Caption="Seleccionar">
                                                    </dx:GridViewCommandColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDArticulo"
                                                        VisibleIndex="1" Visible="False" ReadOnly="True">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Descripcion"
                                                        FieldName="DescripcionArticulo" VisibleIndex="3">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDTipoArticulo"
                                                        VisibleIndex="4" Visible="False">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Stock" FieldName="StockArticulo"
                                                        VisibleIndex="7">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FechaBajaArticulo"
                                                        Visible="False" VisibleIndex="5">
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDTipoArticulo1" ReadOnly="True"
                                                        Visible="False" VisibleIndex="6">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Tipo Articulo"
                                                        FieldName="DescripcionTipoArticulo" VisibleIndex="2">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FechaBajaTipoArticulo"
                                                        Visible="False" VisibleIndex="8">
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDHistoricoPrecioArticulo"
                                                        ReadOnly="True" Visible="False" VisibleIndex="9">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FechaHastaPrecioArticulo"
                                                        Visible="False" VisibleIndex="10">
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataTextColumn Caption="Precio" FieldName="PrecioArticulo"
                                                        VisibleIndex="11">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDArticulo1"
                                                        Visible="False" VisibleIndex="12">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDHistoricoCostoArticulo" ReadOnly="True"
                                                        Visible="False" VisibleIndex="13">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FechaHastaCostoArticulo"
                                                        Visible="False" VisibleIndex="14">
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataTextColumn FieldName="CostoArticulo"
                                                        Visible="False" VisibleIndex="15">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDArticulo2"
                                                        Visible="False" VisibleIndex="16">
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsBehavior AllowFocusedRow="True" />
                                                <Settings ShowFilterRow="True" />
                                            </dx:ASPxGridView>
                                            <asp:SqlDataSource ID="SqlDataSourceArticulos" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                                SelectCommand="Articulos_SelectAll_EnStock" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                            <p>
                                            </p>
                                            <p style="text-align: right">
                                                <asp:Button ID="btnSeleccionar" runat="server" Text="=>" type="button" class="btn btn-outline btn-warning"
                                                    OnClick="btnSeleccionar_Click" Width="50px" />
                                            </p>
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>
                                <!-- /.col-lg-6 -->

                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            2 - Confirme Artículos
                                        </div>
                                        <div class="panel-body">

                                            <dx:ASPxGridView ID="gdvVenta" runat="server" Theme="Aqua" AutoGenerateColumns="False" KeyFieldName="codigo"
                                                Width="100%">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="IDArticulo" VisibleIndex="0" FieldName="codigo"
                                                        Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="DescripcionArticulo" VisibleIndex="1" FieldName="descripcion">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="PrecioArticulo" VisibleIndex="2" FieldName="precio">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataSpinEditColumn Caption="Cantidad" VisibleIndex="3" FieldName="cantidad">
                                                        <PropertiesSpinEdit DisplayFormatString="g">
                                                        </PropertiesSpinEdit>
                                                        <DataItemTemplate>
                                                            <dx:ASPxSpinEdit ID="txtTitle" runat="server" Width="100px" MinValue="1" MaxValue="100">
                                                            </dx:ASPxSpinEdit>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataSpinEditColumn>
                                                </Columns>
                                                <SettingsBehavior AllowFocusedRow="True" />

                                            </dx:ASPxGridView>
                                            <p>
                                            </p>
                                            <p style="text-align: left">
                                                <asp:Button ID="btnEliminar" runat="server" Text="<=" type="button" class="btn btn-outline btn-warning"
                                                    Width="50px" OnClick="btnEliminar_Click" />
                                            </p>
                                            <p style="text-align: right">
                                                <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" type="button" class="btn btn-outline btn-primary"
                                                    OnClick="btnAceptar_Click" Width="100px" />
                                            </p>
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                    <!-- /.panel -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="pago">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detalle de Pago
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="form-group">
                            <p>
                                <strong>Cantidad de Horas:
                                    <asp:TextBox ID="txtCantidadHoras" runat="server" OnTextChanged="txtCantidadHoras_TextChanged" Width="50px"></asp:TextBox></strong>
                            </p>
                            <p>
                            <p>
                                <strong>Importe por cantidad de Horas: </strong>
                                <asp:Label ID="lblImporteTotalHoras" runat="server"></asp:Label>
                            </p>
                            <p>
                                <strong>Importe por Items Consumidos: </strong>
                                <asp:Label ID="lblImporteTotalItems" runat="server" Text="0"></asp:Label>
                            </p>
                            <p>
                                <strong>Importe adicional: 
                                    <asp:TextBox ID="txtImporteAdicional" runat="server" Width="50px" OnTextChanged="txtImporteAdicional_TextChanged" Text="0"></asp:TextBox>
                                </strong>
                               
                            </p>
                            <p>
                                <strong>Descuento: 
                                    <asp:TextBox ID="txtDescuento" runat="server" Width="50px" OnTextChanged="txtDescuento_TextChanged" Text="0"></asp:TextBox>
                                </strong>
                               
                            </p>
                            <p>
                                <strong>Importe Total: </strong>
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                            </p>
                            <p>
                            </p>
                            <p>
                                <asp:Button ID="btnGuardarSinCerrar" runat="server" Text="Guardar sin cerrar" type="button" class="btn btn-outline btn-primary"
                                    Width="180px" onclick="btnGuardarSinCerrar_Click" />
                                <asp:Button ID="btnConfirmarPago" runat="server" Text="Cerrar" type="button" class="btn btn-outline btn-primary"
                                    OnClick="btnConfirmarPago_Click" Width="100px" />
                                <asp:Button ID="btnVolver" runat="server" Text="Volver" type="button" class="btn btn-outline btn-danger"
                                    Width="100px" onclick="btnVolver_Click" />
                            </p>
                            <p>
                                <asp:Label ID="lblCosto" runat="server" Text="0" Visible="False"></asp:Label>
                                
                            </p>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
            <%-- <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Moving Line Chart Example
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-line-chart-moving"></div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Bar Chart Example
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-bar-chart"></div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>--%>
        </div>
        <!-- /.row -->
    </div>

    <dx:ASPxPopupControl ID="pcMensajeCerrarTurno" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensajeCerrarTurno" HeaderText="Nuevo Turno"
        AllowDragging="True" PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblpcMensajeCerrarTurno" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%--<dx:ASPxButton ID="btnAceptarMensaje" runat="server" Text="Aceptar" 
                                AutoPostBack="False" Width="100%" OnClick="btnAceptarMensaje_Click">
                                <ClientSideEvents Click="function(s, e) { pcMensajeCerrarTurno.Hide(); }" />
                            </dx:ASPxButton>--%>
                            
                            <dx:ASPxButton ID="btnCancelarMensaje" runat="server" Text="Aceptar">
                                <ClientSideEvents Click="function(s, e) { pcMensajeCerrarTurno.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <!-- /#page-wrapper -->
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
