﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sala_IngresosEgresosCaja : System.Web.UI.Page
{
    private string Id_Caja_Sala;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["Id_Caja_Sala"] = null;
    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("AgregarEditarOperacion.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Id_Caja_Sala = gdvIngresosEgresos.GetRowValues(gdvIngresosEgresos.FocusedRowIndex, "Id_Caja_Sala").ToString();
        Session.Add("Id_Caja_Sala", Id_Caja_Sala);
        Response.Redirect("AgregarEditarOperacion.aspx");
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        Id_Caja_Sala = gdvIngresosEgresos.GetRowValues(gdvIngresosEgresos.FocusedRowIndex, "Id_Caja_Sala").ToString();

        new BusinessLayer.OperacionCajaDiariaSala().Delete(int.Parse(Id_Caja_Sala));

        gdvIngresosEgresos.DataBind();

        
    }
}