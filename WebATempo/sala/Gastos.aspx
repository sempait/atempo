﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/sala/inicio.master"
    CodeFile="Gastos.aspx.cs" Inherits="sala_Gastos" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Gestion de Gastos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Gastos
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar" type="button" class="btn btn-outline btn-primary"
                                    Width="100px" onclick="btnAgregar_Click" />
                                <asp:Button ID="btnEditar" runat="server" Text="Editar" type="button" class="btn btn-outline btn-warning"
                                    Width="100px" onclick="btnEditar_Click" />
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" type="button" class="btn btn-outline btn-danger"
                                    Width="100px" onclick="btnEliminar_Click" />
                            </p>
                            <p>
                                <dx:ASPxGridView ID="gdvGasto" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGastos"
                                    EnableTheming="True" Theme="Aqua" Width="100%" KeyFieldName="Id_Gasto">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Id_Gasto" Visible="False" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DescripcionGasto" VisibleIndex="2">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Improte" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataCheckColumn FieldName="AftectaCaja" VisibleIndex="4">
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaGasto" VisibleIndex="1">
                                        </dx:GridViewDataDateColumn>
                                    </Columns>
                                    <SettingsBehavior AllowFocusedRow="True" />
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="SqlDataSourceGastos" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                    SelectCommand="SELECT * FROM [GastosSala] order by 1 desc"></asp:SqlDataSource>
                            </p>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
