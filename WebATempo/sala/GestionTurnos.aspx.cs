﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;
using EntitiesLayer.Bar;
using EntitiesLayer.SalaDeEnsayo;

public partial class sala_GestionTurnos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["tablaGrilla"] = null;
        Session["MyResourceID"] = null;

        if (!IsPostBack)
        {
            CargarBotones();
        }        
    }

    private void CargarBotones()
    {
        //BUTTON SALA 1
        try
        {
            TurnosSala oTurnoSala1 = new TurnosSala();
            oTurnoSala1 = new TurnosSalaBusiness().SelectOne(1);
            if (oTurnoSala1.ID != 0)
                btnSala1.CssClass = "btn btn-danger btn-circle btn-xl";
        }
        catch
        {
            btnSala1.CssClass = "btn btn-default btn-circle btn-xl";
            btnCerrarTurno1.CssClass = "btn btn-default disabled";
        }

        //BUTTON SALA 2
        try
        {
            TurnosSala oTurnoSala2 = new TurnosSala();
            oTurnoSala2 = new TurnosSalaBusiness().SelectOne(2);
            if (oTurnoSala2.ID != 0)
                btnSala2.CssClass = "btn btn-danger btn-circle btn-xl";
        }
        catch
        {
            btnSala2.CssClass = "btn btn-default btn-circle btn-xl";
            btnCerrarTurno2.CssClass = "btn btn-default disabled";

        }
        //BUTTON SALA 3
        try
        {
            TurnosSala oTurnoSala3 = new TurnosSala();
            oTurnoSala3 = new TurnosSalaBusiness().SelectOne(3);
            if (oTurnoSala3.ID != 0)
                btnSala3.CssClass = "btn btn-danger btn-circle btn-xl";
        }
        catch
        {
            btnSala3.CssClass = "btn btn-default btn-circle btn-xl";
            btnCerrarTurno3.CssClass = "btn btn-default disabled";

        }
        //BUTTON SALA 4
        try
        {
            TurnosSala oTurnoSala4 = new TurnosSala();
            oTurnoSala4 = new TurnosSalaBusiness().SelectOne(4);
            if (oTurnoSala4.ID != 0)
                btnSala4.CssClass = "btn btn-danger btn-circle btn-xl";
        }
        catch
        {
            btnSala4.CssClass = "btn btn-default btn-circle btn-xl";
            btnCerrarTurno4.CssClass = "btn btn-default disabled";
        }
        //btnSala2.CssClass = "btn btn-default btn-circle btn-xl";
        //btnSala3.CssClass = "btn btn-danger btn-circle btn-xl";
        //btnSala4.CssClass = "btn btn-default btn-circle btn-xl";
        //btn btn-success btn-circle btn-xl

    }

    
    protected void btnCerrarTurno1_Click(object sender, EventArgs e)
    {
        //pcCerrarTurno1.ShowOnPageLoad = true;
        //cargaDatosTurno(1);
        //pcCerrarTurno1.HeaderText = "Cerrar Turno Sala 1";
        Session.Add("MyResourceID", 1); 
        Response.Redirect("CerrarTurno.aspx");
    }

    protected void btnCerrarTurno2_Click(object sender, EventArgs e)
    {
        //pcCerrarTurno1.ShowOnPageLoad = true;
        //cargaDatosTurno(2);
        //pcCerrarTurno1.HeaderText = "Cerrar Turno Sala 2";
        Session.Add("MyResourceID", 2); 
        Response.Redirect("CerrarTurno.aspx");
    }
    protected void btnCerrarTurno3_Click(object sender, EventArgs e)
    {
        //pcCerrarTurno1.ShowOnPageLoad = true;
        //cargaDatosTurno(3);
        //pcCerrarTurno1.HeaderText = "Cerrar Turno Sala 3";
        Session.Add("MyResourceID", 3);
        Response.Redirect("CerrarTurno.aspx");
    }
    protected void btnCerrarTurno4_Click(object sender, EventArgs e)
    {
        //pcCerrarTurno1.ShowOnPageLoad = true;
        //cargaDatosTurno(4);
        //pcCerrarTurno1.HeaderText = "Cerrar Turno Sala 4";
        Session.Add("MyResourceID", 4);
        Response.Redirect("CerrarTurno.aspx");
    }

    
    protected void btnSeleccionar_Click(object sender, EventArgs e)
    {
        //string IDArticulo = gdvArticulos.GetRowValues(gdvArticulos.FocusedRowIndex, "IDArticulo").ToString();
        List<object> fieldValues = gdvArticulos.GetSelectedFieldValues(new string[] { "IDArticulo", "DescripcionArticulo", "StockArticulo", "PrecioArticulo" });
        //gdvItemsTurno.DataSource = fieldValues;
        // Convert to DataTable.
        DataTable table = ConvertListToDataTable(fieldValues);
        gdvItemsTurno.DataSource = table;
        gdvItemsTurno.DataBind();
    }

    private DataTable ConvertListToDataTable(List<object> fieldValues)
    {
        // New table.
        DataTable table = new DataTable();

        // Get max columns.
        int columns = 0;
        foreach (object[] array in fieldValues)
        {
            if (array.Length > columns)
            {
                columns = array.Length;
            }
        }

        // Add columns.
        for (int i = 0; i < columns; i++)
        {
            table.Columns.Add();
        }

        // Add rows.
        foreach (object[] array in fieldValues)
        {
            table.Rows.Add(array);
        }

        return table;
    }

    protected void btnGuardarItems_Click(object sender, EventArgs e)
    {

    }
    
}