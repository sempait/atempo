﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sala_Salas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["UniqueID"] = null;
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        string UniqueID = gdvSalas.GetRowValues(gdvSalas.FocusedRowIndex, "UniqueID").ToString();
        Session.Add("UniqueID", UniqueID);
        Response.Redirect("SalasEditar.aspx");
    }
}