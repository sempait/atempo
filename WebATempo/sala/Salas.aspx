﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sala/inicio.master" AutoEventWireup="true"
    CodeFile="Salas.aspx.cs" Inherits="sala_Salas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Salas</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de Salas
                    </div>
                    <!-- /.panel-heading -->

                    <!-- /#page-wrapper -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>

                                <asp:Button ID="btnEditar" runat="server" Text="Editar" type="button" class="btn btn-outline btn-warning"
                                    Width="100px" OnClick="btnEditar_Click" />

                            </p>
                            <p>
                                <dx:ASPxGridView ID="gdvSalas" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGastos"
                                    EnableTheming="True" Theme="Aqua" Width="100%" KeyFieldName="UniqueID">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="UniqueID" Visible="False" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ResourceID" VisibleIndex="2" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="ResourceName" VisibleIndex="3" Caption="Nombre del Recurso">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="PrecioSala" VisibleIndex="4" Caption="Precio">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior AllowFocusedRow="True" />
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="SqlDataSourceGastos" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                    SelectCommand="SELECT * FROM [Salas]"></asp:SqlDataSource>
                            </p>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
