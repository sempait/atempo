﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sala/inicio.master" AutoEventWireup="true"
    CodeFile="TurnosSalas.aspx.cs" Inherits="sala_TurnosSalas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Emitir Turnos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       Emitir Turnos
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <dx:ASPxScheduler ID="ASPxScheduler1" runat="server" AppointmentDataSourceID="SqlDataSourceTurnosSalas"
                                ClientIDMode="AutoID" ResourceDataSourceID="SqlDataSourceSalas" Theme="Aqua"
                                GroupType="Resource">
                                <Storage>
                                    <Appointments AutoRetrieveId="True">
                                        <Mappings AllDay="AllDay" AppointmentId="UniqueID" Description="Description" End="EndDate"
                                            Label="Label" Location="Location" RecurrenceInfo="RecurrenceInfo" ReminderInfo="ReminderInfo"
                                            ResourceId="ResourceID" Start="StartDate" Status="Status" Subject="Subject" Type="Type" />
                                    </Appointments>
                                    <Resources>
                                        <Mappings Caption="ResourceName" Color="Color" Image="Image" ResourceId="ResourceID" />
                                    </Resources>
                                </Storage>
                                <Views>
                                    <%--<DayView ShowWorkTimeOnly="true" TimeScale="01:00:00">
                                        <TimeRulers>
                                            <dx:TimeRuler></dx:TimeRuler>
                                        </TimeRulers>
                                        <VisibleTime End="23:59:00" Start="12:00:00" />
                                        <WorkTime Start="12:00" End="23:59:00" />
                                    </DayView>--%>
                                    <WorkWeekView Enabled="False">
                                        <TimeRulers>
                                            <dx:TimeRuler></dx:TimeRuler>
                                        </TimeRulers>
                                    </WorkWeekView>
                                    <WeekView Enabled="False">
                                    </WeekView>
                                    <MonthView CompressWeekend="False" WeekCount="7">
                                    </MonthView>
                                    <%--<TimelineView>
                                        <WorkTime End="23:59:00" Start="12:00:00" />
                                    </TimelineView>--%>
                                </Views>
                                <OptionsForms AppointmentFormTemplateUrl="~/DevExpress/ASPxSchedulerForms/AppointmentForm.ascx"
                                    AppointmentInplaceEditorFormTemplateUrl="~/DevExpress/ASPxSchedulerForms/InplaceEditor.ascx"
                                    GotoDateFormTemplateUrl="~/DevExpress/ASPxSchedulerForms/GotoDateForm.ascx" RecurrentAppointmentDeleteFormTemplateUrl="~/DevExpress/ASPxSchedulerForms/RecurrentAppointmentDeleteForm.ascx"
                                    RecurrentAppointmentEditFormTemplateUrl="~/DevExpress/ASPxSchedulerForms/RecurrentAppointmentEditForm.ascx"
                                    RemindersFormTemplateUrl="~/DevExpress/ASPxSchedulerForms/ReminderForm.ascx" />
                                <OptionsToolTips AppointmentDragToolTipUrl="~/DevExpress/ASPxSchedulerForms/AppointmentDragToolTip.ascx"
                                    AppointmentToolTipUrl="~/DevExpress/ASPxSchedulerForms/AppointmentToolTip.ascx"
                                    SelectionToolTipUrl="~/DevExpress/ASPxSchedulerForms/SelectionToolTip.ascx" />
                            </dx:ASPxScheduler>
                            <asp:SqlDataSource ID="SqlDataSourceSalas" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT * FROM [Salas]"></asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSourceTurnosSalas" runat="server" ConflictDetection="CompareAllValues"
                                ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>" DeleteCommand="DELETE FROM [TurnosSala] WHERE [UniqueID] = @original_UniqueID"
                                InsertCommand="INSERT INTO [TurnosSala] ([Type], [StartDate], [EndDate], [AllDay], [Subject], [Location], [Description], [Status], [Label], [ResourceID], [ResourceIDs], [ReminderInfo], [RecurrenceInfo], [CustomField1]) VALUES (@Type, @StartDate, @EndDate, @AllDay, @Subject, @Location, @Description, @Status, @Label, @ResourceID, @ResourceIDs, @ReminderInfo, @RecurrenceInfo, @CustomField1)"
                                OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [TurnosSala]"
                                UpdateCommand="UPDATE [TurnosSala] SET [Type] = @Type, [StartDate] = @StartDate, [EndDate] = @EndDate, [AllDay] = @AllDay, [Subject] = @Subject, [Location] = @Location, [Description] = @Description, [Status] = @Status, [Label] = @Label, [ResourceID] = @ResourceID, [ResourceIDs] = @ResourceIDs, [ReminderInfo] = @ReminderInfo, [RecurrenceInfo] = @RecurrenceInfo, [CustomField1] = @CustomField1 WHERE [UniqueID] = @original_UniqueID ">
                                <DeleteParameters>
                                    <asp:Parameter Name="original_UniqueID" Type="Int32" />
                                    <asp:Parameter Name="original_Type" Type="Int32" />
                                    <asp:Parameter Name="original_StartDate" Type="DateTime" />
                                    <asp:Parameter Name="original_EndDate" Type="DateTime" />
                                    <asp:Parameter Name="original_AllDay" Type="Boolean" />
                                    <asp:Parameter Name="original_Subject" Type="String" />
                                    <asp:Parameter Name="original_Location" Type="String" />
                                    <asp:Parameter Name="original_Description" Type="String" />
                                    <asp:Parameter Name="original_Status" Type="Int32" />
                                    <asp:Parameter Name="original_Label" Type="Int32" />
                                    <asp:Parameter Name="original_ResourceID" Type="Int32" />
                                    <asp:Parameter Name="original_ResourceIDs" Type="String" />
                                    <asp:Parameter Name="original_ReminderInfo" Type="String" />
                                    <asp:Parameter Name="original_RecurrenceInfo" Type="String" />
                                    <asp:Parameter Name="original_CustomField1" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="Type" Type="Int32" />
                                    <asp:Parameter Name="StartDate" Type="DateTime" />
                                    <asp:Parameter Name="EndDate" Type="DateTime" />
                                    <asp:Parameter Name="AllDay" Type="Boolean" />
                                    <asp:Parameter Name="Subject" Type="String" />
                                    <asp:Parameter Name="Location" Type="String" />
                                    <asp:Parameter Name="Description" Type="String" />
                                    <asp:Parameter Name="Status" Type="Int32" />
                                    <asp:Parameter Name="Label" Type="Int32" />
                                    <asp:Parameter Name="ResourceID" Type="Int32" />
                                    <asp:Parameter Name="ResourceIDs" Type="String" />
                                    <asp:Parameter Name="ReminderInfo" Type="String" />
                                    <asp:Parameter Name="RecurrenceInfo" Type="String" />
                                    <asp:Parameter Name="CustomField1" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Type" Type="Int32" />
                                    <asp:Parameter Name="StartDate" Type="DateTime" />
                                    <asp:Parameter Name="EndDate" Type="DateTime" />
                                    <asp:Parameter Name="AllDay" Type="Boolean" />
                                    <asp:Parameter Name="Subject" Type="String" />
                                    <asp:Parameter Name="Location" Type="String" />
                                    <asp:Parameter Name="Description" Type="String" />
                                    <asp:Parameter Name="Status" Type="Int32" />
                                    <asp:Parameter Name="Label" Type="Int32" />
                                    <asp:Parameter Name="ResourceID" Type="Int32" />
                                    <asp:Parameter Name="ResourceIDs" Type="String" />
                                    <asp:Parameter Name="ReminderInfo" Type="String" />
                                    <asp:Parameter Name="RecurrenceInfo" Type="String" />
                                    <asp:Parameter Name="CustomField1" Type="String" />
                                    <asp:Parameter Name="original_UniqueID" Type="Int32" />
                                    <asp:Parameter Name="original_Type" Type="Int32" />
                                    <asp:Parameter Name="original_StartDate" Type="DateTime" />
                                    <asp:Parameter Name="original_EndDate" Type="DateTime" />
                                    <asp:Parameter Name="original_AllDay" Type="Boolean" />
                                    <asp:Parameter Name="original_Subject" Type="String" />
                                    <asp:Parameter Name="original_Location" Type="String" />
                                    <asp:Parameter Name="original_Description" Type="String" />
                                    <asp:Parameter Name="original_Status" Type="Int32" />
                                    <asp:Parameter Name="original_Label" Type="Int32" />
                                    <asp:Parameter Name="original_ResourceID" Type="Int32" />
                                    <asp:Parameter Name="original_ResourceIDs" Type="String" />
                                    <asp:Parameter Name="original_ReminderInfo" Type="String" />
                                    <asp:Parameter Name="original_RecurrenceInfo" Type="String" />
                                    <asp:Parameter Name="original_CustomField1" Type="String" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
