﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;
using EntitiesLayer.Bar;
using EntitiesLayer.SalaDeEnsayo;
using DevExpress.Web.ASPxEditors;

public partial class sala_CerrarTurno : System.Web.UI.Page
{
    int ResourceID;
    TurnosSala oTurnoSalaActual = new TurnosSala();
    private DataTable dtVentaActual;
    string HoraDesde;
    string HoraHasta;
    int CantHorasSala;
    float ImporteHoraSala;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                ResourceID = int.Parse(Session["MyResourceID"].ToString());

                //Cargo el form
                if (ResourceID != 0)
                    CargarDatosTurno(ResourceID);
            }
            catch
            {
                ResourceID = 0;
            }
        }
    }

    private void CargarDatosTurno(int ResourceID)
    {
        oTurnoSalaActual = new TurnosSalaBusiness().SelectOne(ResourceID);
        lblNombreSala.Text = "Sala " + oTurnoSalaActual.ResourceID;
        txtNombreBanda.Text = oTurnoSalaActual.Subject;
        lblHoraDesde.Text = oTurnoSalaActual.StarDate.ToString();
        lblHoraHasta.Text = oTurnoSalaActual.EndDate.ToString();

        HoraDesde = oTurnoSalaActual.StarDate.Value.Hour.ToString();
        HoraHasta = oTurnoSalaActual.EndDate.Value.Hour.ToString();
        if (int.Parse(HoraHasta) == 0)
            HoraHasta = "24";
        CantHorasSala = int.Parse(HoraHasta) - int.Parse(HoraDesde);
        txtCantidadHoras.Text = CantHorasSala.ToString();
        ActualizarImportePorCantidadDeHoras();
        ActualizarImporteTotal();

        DataTable tablaItemTurnoSala = new ItemTurnoSalaBusiness().SelectAllPorTurnoSala(oTurnoSalaActual.ID);
        gdvVenta.DataSource = tablaItemTurnoSala;
        gdvVenta.DataBind();
        Session["tablaGrilla"] = tablaItemTurnoSala;

        for (int i = 0; i < tablaItemTurnoSala.Rows.Count; i++)
        {
            ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
            //spin.Text = tablaItemTurnoSala.Rows[i]["cantidad"].ToString();
            spin.Value = tablaItemTurnoSala.Rows[i]["cantidad"];
        }
    }

    private void ActualizarImportePorCantidadDeHoras()
    {
        Salas oSala = new Salas();
        oSala = new SalasBusiness().SelectOnePorResourceID(ResourceID);
        //Busco datos para enviar email
        //arametros oParametros = new Parametros();
        //oParametros = new ParametrosBusiness().Select(1);

        ImporteHoraSala = oSala.PrecioSala;

        lblImporteTotalHoras.Text = Convert.ToString(int.Parse(txtCantidadHoras.Text) * int.Parse(ImporteHoraSala.ToString()));


    }

    protected void txtCantidadHoras_TextChanged(object sender, EventArgs e)
    {
        ActualizarImportePorCantidadDeHoras();
        ActualizarImporteTotal();
    }

    protected void btnSeleccionar_Click(object sender, EventArgs e)
    {
        oTurnoSalaActual = new TurnosSalaBusiness().SelectOne(int.Parse(Session["MyResourceID"].ToString()));
        List<object> fieldValues = gdvArticulos.GetSelectedFieldValues(new string[] { "IDArticulo", "DescripcionArticulo", "PrecioArticulo", "CostoArticulo", "StockArticulo" });
        DataTable tabla = (DataTable)Session["tablaGrilla"];
        List<int> listaCodigos = (from t in tabla.AsEnumerable() select Convert.ToInt32(t["codigo"])).ToList();
        DataTable tablaSeleccionados = ConvertListToDataTable(fieldValues);
        foreach (DataRow fila in tablaSeleccionados.Rows)
        {
            if (!listaCodigos.Contains(Convert.ToInt32(fila["codigo"])))
            {
                DataRow r = tabla.NewRow();
                r["codigo"] = fila["codigo"];
                r["descripcion"] = fila["descripcion"];
                r["precio"] = fila["precio"];
                r["costo"] = fila["costo"];
                r["stock"] = fila["stock"];
                r["cantidad"] = fila["cantidad"];
                r["IDTurnosSala"] = oTurnoSalaActual.ID;
                tabla.Rows.Add(r);
            }
        }
        // Convert to DataTable.
        dtVentaActual = tabla;
        gdvVenta.DataSource = dtVentaActual;
        gdvVenta.DataBind();
        Session["tablaGrilla"] = dtVentaActual;

        for (int i = 0; i < tabla.Rows.Count; i++)
        {
            ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
            //spin.Text = tablaItemTurnoSala.Rows[i]["cantidad"].ToString();
            spin.Value = tabla.Rows[i]["cantidad"];
        }
    }

    private DataTable ConvertListToDataTable(List<object> fieldValues)
    {
        // New table.
        DataTable table = new DataTable();
        table.Columns.Add("codigo");
        table.Columns.Add("descripcion");
        table.Columns.Add("precio");
        table.Columns.Add("costo");
        table.Columns.Add("stock");
        table.Columns.Add("cantidad").DefaultValue = 1;
        // Add rows.
        foreach (object[] array in fieldValues)
        {
            table.Rows.Add(array);
        }
        return table;
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        List<object> fieldValues = gdvVenta.GetSelectedFieldValues(new string[] { "IDArticulo", "DescripcionArticulo", "PrecioArticulo", "CostoArticulo", "StockArticulo" });

        // Convert to DataTable.
        dtVentaActual = ConvertListToDataTableRemove(fieldValues);
        gdvVenta.DataSource = dtVentaActual;
        gdvVenta.DataBind();
        Session["tablaGrilla"] = dtVentaActual;
    }

    private DataTable ConvertListToDataTableRemove(List<object> fieldValues)
    {
        //// New table.
        DataTable table = new DataTable();

        foreach (object[] array in fieldValues)
        {
            fieldValues.Remove(table.Rows);
        }
        return table;
    }

    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        DataTable tabla = (DataTable)Session["tablaGrilla"];
        double total = 0;
        double costo = 0;
        for (int i = 0; i < tabla.Rows.Count; i++)
        {
            ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
            double cantidad = Convert.ToDouble(spin.Value);

            if (Convert.ToInt32(tabla.Rows[i]["stock"]) >= cantidad)
            {
                //ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
                //double cantidad = Convert.ToDouble(spin.Value);
                tabla.Rows[i]["cantidad"] = cantidad;
                total = total + (Convert.ToDouble(tabla.Rows[i]["precio"]) * cantidad);
                costo = costo + (Convert.ToDouble(tabla.Rows[i]["costo"]) * cantidad);
            }
            else
            {
                pcMensajeCerrarTurno.ShowOnPageLoad = true;
                lblpcMensajeCerrarTurno.Text = "Hay artículos seleccionados que superan el stock disponible. Por favor, revise las cantidades y vuelva a intentar.";
                //btnAceptarMensaje.Visible = false;
                btnCancelarMensaje.Visible = true;
            }
        }

        lblImporteTotalItems.Text = total.ToString();
        lblCosto.Text = costo.ToString();
        Session["tablaGrilla"] = tabla;

        ActualizarImporteTotal();
    }

    protected void txtImporteAdicional_TextChanged(object sender, EventArgs e)
    {
        ActualizarImporteTotal();
    }

    private void ActualizarImporteTotal()
    {
        float ImpTotal = (int.Parse(lblImporteTotalHoras.Text) + int.Parse(lblImporteTotalItems.Text) + int.Parse(txtImporteAdicional.Text)) * (100 - (int.Parse(txtDescuento.Text))) / 100;
        lblTotal.Text = Convert.ToString(ImpTotal);
    }

    protected void btnConfirmarPago_Click(object sender, EventArgs e)
    {
        registrarPagoTurno();
        confirmarVenta();
        //pcMensajeVenta.ShowOnPageLoad = true;
        gdvArticulos.DataBind();
        gdvVenta.DataBind();
        lblImporteTotalHoras.Text = "0";
        lblImporteTotalItems.Text = "0";
        lblTotal.Text = "0";
        Response.Redirect("GestionTurnos.aspx");
    }

    protected void btnGuardarSinCerrar_Click(object sender, EventArgs e)
    {
        oTurnoSalaActual = new TurnosSalaBusiness().SelectOne(int.Parse(Session["MyResourceID"].ToString()));

        new ItemTurnoSalaBusiness().DeletePorTurnoSala(oTurnoSalaActual.ID);

        DataTable tabla = (DataTable)Session["tablaGrilla"];
        for (int i = 0; i < tabla.Rows.Count; i++)
        {
            ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
            int cantidad = Convert.ToInt32(spin.Value);
            tabla.Rows[i]["cantidad"] = cantidad;
            new ItemTurnoSalaBusiness().Insert(oTurnoSalaActual.ID, Convert.ToInt32(tabla.Rows[i]["codigo"]), cantidad);
        }
    }

    private void registrarPagoTurno()
    {
        oTurnoSalaActual = new TurnosSalaBusiness().SelectOne(int.Parse(Session["MyResourceID"].ToString()));
        float ImpTot = (int.Parse(lblImporteTotalHoras.Text) + int.Parse(txtImporteAdicional.Text)) * (100 - (int.Parse(txtDescuento.Text))) / 100;
        new TurnosSalaBusiness().InsertPago(oTurnoSalaActual.ID, ImpTot);
    }

    private void confirmarVenta()
    {
        DataTable tabla = (DataTable)Session["tablaGrilla"];

        if (tabla != null)
        {
            //Hago la insercion de la venta con monto total 0.
            new VentasBusiness().InsertVenta(DateTime.Parse(String.Format("{0:d}", DateTime.Now)), DateTime.Parse(String.Format("{0:t}", DateTime.Now)), CalcularImporteTotal(), CalcularCostoVenta());

            //Tengo que hacer el insert para cada item ,de la venta en la tabla detalle venta
            
            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
                double cantidad = Convert.ToDouble(spin.Value);
                tabla.Rows[i]["cantidad"] = cantidad;
                string IdArticulo = tabla.Rows[i]["Codigo"].ToString();
                new VentasBusiness().InsertItemVenta(int.Parse(cantidad.ToString()), IdArticulo);

            }
        }
    }

    private float CalcularCostoVenta()
    {

        return float.Parse(lblCosto.Text);

    }

    private float CalcularImporteTotal()
    {
        float ImporteTotal = int.Parse(lblImporteTotalItems.Text) * (100 - (int.Parse(txtDescuento.Text))) / 100;
        return ImporteTotal;
    }

    protected void txtDescuento_TextChanged(object sender, EventArgs e)
    {
        ActualizarImporteTotal();
    }
    protected void btnVolver_Click(object sender, EventArgs e)
    {
        Response.Redirect("GestionTurnos.aspx");
    }
}