﻿<%@ Page Language="C#" MasterPageFile="~/sala/inicio.master" AutoEventWireup="true"
    CodeFile="IngresosEgresosCaja.aspx.cs" Inherits="sala_IngresosEgresosCaja" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Gestion de Ingresos/Egresos de caja</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Caja
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar" type="button" class="btn btn-outline btn-primary"
                                    Width="100px" OnClick="btnAgregar_Click" />
                                <asp:Button ID="btnEditar" runat="server" Text="Editar" type="button" class="btn btn-outline btn-warning"
                                    Width="100px" onclick="btnEditar_Click" />
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" type="button" class="btn btn-outline btn-danger"
                                    Width="100px" onclick="btnEliminar_Click" />
                            </p>
                            <p>
                            </p>
                            <dx:ASPxGridView ID="gdvIngresosEgresos" runat="server" 
                                AutoGenerateColumns="False" DataSourceID="SqlDataEgresosIngresos"
                                EnableTheming="True" Theme="Aqua" Width="100%" KeyFieldName="Id_Caja_Sala">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="Id_Caja_Sala" Visible="False" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaOperacion" VisibleIndex="0">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="Importe" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Operacion" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DescripcionOperacion" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataEgresosIngresos" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT FechaOperacion,
CASE Ingreso
WHEN 0 THEN Egreso 
ELSE Ingreso
END AS Importe ,
CASE Ingreso
WHEN 0 THEN 'Egreso' 
ELSE 'Ingreso'
END AS Operacion, DescripcionOperacion,Id_Caja_Sala 
FROM CajaDiariaSala WHERE (CajaInicial = 0) ORDER BY FechaOperacion DESC">
                            </asp:SqlDataSource>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
