﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sala/inicio.master" AutoEventWireup="true"
    CodeFile="GestionTurnos.aspx.cs" Inherits="sala_GestionTurnos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Gestion de Turnos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Sala 1
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-area-chart">
                            <table class="nav-justified">
                                <tr>
                                    <td rowspan="3">
                                        <asp:Button ID="btnSala1" runat="server" Text="" type="button" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCerrarTurno1" runat="server" Text="Gestion de Turno" type="button"
                                            class="btn btn-outline btn-primary" OnClick="btnCerrarTurno1_Click" Width="150px" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Sala 2
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-bar-chart">
                            <table class="nav-justified">
                                <tr>
                                    <td rowspan="3">
                                        <asp:Button ID="btnSala2" runat="server" Text="" type="button" class="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCerrarTurno2" runat="server" Text="Gestion de Turno" type="button"
                                            class="btn btn-outline btn-primary" OnClick="btnCerrarTurno2_Click" Width="150px" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Sala 3
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-line-chart">
                            <table class="nav-justified">
                                <tr>
                                    <td rowspan="3">
                                        <asp:Button ID="btnSala3" runat="server" Text="" type="button" class="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCerrarTurno3" runat="server" Text="Gestion de Turno" type="button"
                                            class="btn btn-outline btn-primary" OnClick="btnCerrarTurno3_Click" Width="150px" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Sala 4
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-donut-chart">
                            <table class="nav-justified">
                                <tr>
                                    <td rowspan="3">
                                        <asp:Button ID="btnSala4" runat="server" Text="" type="button" class="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCerrarTurno4" runat="server" Text="Gestion de Turno" type="button"
                                            class="btn btn-outline btn-primary" OnClick="btnCerrarTurno4_Click" Width="150px" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Indicadores
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <p><button type="button" class="btn btn-default btn-circle btn">
                            </button> Sala Vacia. Sin Turnos para cerrar</p>
                            <p><button type="button" class="btn btn-danger btn-circle btn">
                            </button> Sala con turnos para cerrar.</p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
    <%--FORM PARA CERRAR EL TURNO 1--%>
    <dx:ASPxPopupControl ID="pcCerrarTurno1" runat="server" CloseAction="CloseButton"
        Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcCerrarTurno1" AllowDragging="True" PopupAnimationType="None"
        EnableViewState="False" Theme="Metropolis" Width="600px">
        <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('entryGroup'); pcCerrarTurno1.Focus(); }" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" Width="400px"
                Height="600px">
                <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btOK" Width="600px" Height="600px">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent1" runat="server">
                            <div class="container">
                                <div class="form-group">
                                    <label for="disabledSelect">
                                        Nombre Banda</label>
                                    <asp:Label ID="txtNombreBanda" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">
                                        Hora Desde:</label>
                                    <asp:Label ID="lblHoraDesde" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">
                                        Hora Hasta:</label>
                                    <asp:Label ID="lblHoraHasta" runat="server" Text=""></asp:Label>
                                </div>
                                <h1>
                                    Items</h1>
                                <table class="auto-style1">
                                    <tr>
                                        <td rowspan="2">
                                            <dx:ASPxGridView ID="gdvArticulos" runat="server" Width="100px" AutoGenerateColumns="False"
                                                KeyFieldName="IDArticulo" EnableTheming="True" Theme="Aqua" DataSourceID="SqlDataSourceArticulos">
                                                <Columns>
                                                    <dx:GridViewCommandColumn ShowInCustomizationForm="True" ShowSelectCheckbox="True"
                                                        VisibleIndex="0" Caption="Seleccion">
                                                    </dx:GridViewCommandColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDArticulo" ShowInCustomizationForm="True"
                                                        VisibleIndex="1" Visible="False" ReadOnly="True">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Descripcion" FieldName="DescripcionArticulo"
                                                        ShowInCustomizationForm="True" VisibleIndex="3">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDTipoArticulo" ShowInCustomizationForm="True"
                                                        VisibleIndex="6" Visible="False">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Stock" FieldName="StockArticulo" ShowInCustomizationForm="True"
                                                        VisibleIndex="4">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FechaBajaArticulo" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="7">
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDTipoArticulo1" ReadOnly="True" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="8">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Tipo Articulo" FieldName="DescripcionTipoArticulo"
                                                        ShowInCustomizationForm="True" VisibleIndex="2">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FechaBajaTipoArticulo" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="9">
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDHistoricoPrecioArticulo" ReadOnly="True"
                                                        ShowInCustomizationForm="True" Visible="False" VisibleIndex="10">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FechaHastaPrecioArticulo" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="11">
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataTextColumn Caption="Precio" FieldName="PrecioArticulo" ShowInCustomizationForm="True"
                                                        VisibleIndex="5">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDArticulo1" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="12">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDHistoricoCostoArticulo" ReadOnly="True" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="13">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FechaHastaCostoArticulo" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="14">
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataTextColumn FieldName="CostoArticulo" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="15">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="IDArticulo2" ShowInCustomizationForm="True"
                                                        Visible="False" VisibleIndex="16">
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsBehavior AllowFocusedRow="True" />
                                                <Settings ShowFilterRow="True" />
                                            </dx:ASPxGridView>
                                            <asp:SqlDataSource ID="SqlDataSourceArticulos" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                                SelectCommand="Articulos_SelectAll_EnStock" SelectCommandType="StoredProcedure">
                                            </asp:SqlDataSource>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="auto-style2">
                                            <dx:ASPxButton ID="btnSeleccionar" runat="server" Text=">>" OnClick="btnSeleccionar_Click">
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td rowspan="2">
                                            <dx:ASPxGridView ID="gdvItemsTurno" runat="server" Width="100px" AutoGenerateColumns="False"
                                                KeyFieldName="Articulo" EnableTheming="True" Theme="Aqua">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ID" ShowInCustomizationForm="True" Visible="False"
                                                        VisibleIndex="0" FieldName="Column1">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Descripcion" ShowInCustomizationForm="True" VisibleIndex="1"
                                                        FieldName="Column2">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Stock" ShowInCustomizationForm="True" VisibleIndex="4"
                                                        FieldName="Column4" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Precio" ShowInCustomizationForm="True" VisibleIndex="2"
                                                        FieldName="Column3">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataSpinEditColumn Caption="Cantidad" VisibleIndex="3" FieldName="Cantidad">
                                                        <PropertiesSpinEdit DisplayFormatString="g">
                                                        </PropertiesSpinEdit>
                                                        <DataItemTemplate>
                                                            <dx:ASPxSpinEdit ID="txtCantidad" runat="server" Width="100px" MinValue="1" MaxValue="100"
                                                                NullText="1">
                                                            </dx:ASPxSpinEdit>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataSpinEditColumn>
                                                </Columns>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="auto-style2">
                                            <dx:ASPxButton ID="btnEliminarItem" runat="server" Text="<<">
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnGuardarItems" runat="server" Text="Guardar Items" Width="120px"
                                                AutoPostBack="False" Style="float: left; margin-right: 8px" OnClick="btnGuardarItems_Click">
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                                <div class="form-group">
                                </div>
                                <h1>
                                    Detalle Pago</h1>
                                <div class="form-group">
                                    <label for="disabledSelect">
                                        Importe por Cantidad de Horas</label>
                                    <asp:Label ID="lblImporteHora" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">
                                        Importe por Items Consumidos</label>
                                    <asp:Label ID="lblItems" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="form-group">
                                    <label for="disabledSelect">
                                        Importe Total</label>
                                    <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="pcmButton">
                                    <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Cerrar Turno" Width="120px"
                                        AutoPostBack="False" Style="float: left; margin-right: 8px">
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Cancel" Width="80px" AutoPostBack="False"
                                        Style="float: left; margin-right: 8px">
                                        <ClientSideEvents Click="function(s, e) { pcCerrarTurno1.Hide(); }" />
                                    </dx:ASPxButton>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
