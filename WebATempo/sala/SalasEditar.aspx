﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sala/inicio.master" AutoEventWireup="true" CodeFile="SalasEditar.aspx.cs" Inherits="sala_SalasEditar" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Editar Sala</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Editar Sala
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout ID="ASPxFormLayoutSalas" runat="server" EnableTheming="True" Theme="Aqua"
                                    Width="100%" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption" AlignItemCaptionsInAllGroups="True">
                                    <items>
                                        <dx:LayoutGroup Caption="Datos" GroupBoxDecoration="HeadingLine">
                                            <Items>
                                                <dx:LayoutItem Caption="Nombre de la Sala">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer1" runat="server"
                                                            SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtNombreSala" runat="server">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Precio Sala">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainerGasto" runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtPrecioSala" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                    </items>
                                    <styles>
                                        <LayoutGroupBox>
                                            <Caption CssClass="layoutGroupBoxCaption">
                                            </Caption>
                                        </LayoutGroupBox>
                                    </styles>
                                </dx:ASPxFormLayout>
                                <p style="text-align: right">
                                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                        Width="120px" OnClick="btnGuardar_Click" />
                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                        Width="120px" OnClick="btnCancelar_Click" />
                                </p>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
</asp:Content>

