﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sala_CajaSala : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnCerrarCaja_Click(object sender, EventArgs e)
    {
        DateTime FechaOperacion = DateTime.Parse(gdvCajaDiaria.GetRowValues(gdvCajaDiaria.FocusedRowIndex, "Fecha").ToString());

        FechaOperacion = DateTime.Parse(FechaOperacion.ToShortDateString());

        new BusinessLayer.OperacionCajaDiariaSala().CloseCaja(FechaOperacion);

        gdvCajaDiaria.DataBind();
    }
}