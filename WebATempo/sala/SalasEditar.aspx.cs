﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer.SalaDeEnsayo;
using BusinessLayer;

public partial class sala_SalasEditar : System.Web.UI.Page
{
    Salas oSalas = new Salas();
    private int UniqueID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                UniqueID = int.Parse(Session["UniqueID"].ToString());

                //Cargo el form
                if (UniqueID != 0)
                    CargarDatos(UniqueID);
            }
            catch
            {
                UniqueID = 0;
            }
        }
    }

    private void CargarDatos(int UniqueID)
    {
        oSalas = new BusinessLayer.SalasBusiness().Select_One(UniqueID);

        txtNombreSala.Value = oSalas.ResourceName;
        txtPrecioSala.Value = oSalas.PrecioSala;
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("Salas.aspx");
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            UniqueID = int.Parse(Session["UniqueID"].ToString());

        }

        catch
        {
            UniqueID = 0;
        }

        oSalas.ResourceName = txtNombreSala.Text;
        oSalas.PrecioSala = float.Parse(txtPrecioSala.Text.ToString());
        
        if (UniqueID != 0)

            //new GastosSalaBusiness().Insert(oGasto);

        
        {
            oSalas.ID = UniqueID;
            new SalasBusiness().Update(oSalas);

        }

        Response.Redirect("Salas.aspx");
    }
}