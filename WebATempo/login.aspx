﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE HTML>
<html>

<head>
    <title>A Tempo Música</title>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'/>
    <link href="css/style2.css" rel="stylesheet"/>
    <link rel="icon" href="images/logo-atempo.png"/>
    <link rel="shortcut icon" href="images/logo-atempo.png" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    
    <meta property="og:title" content="A Tempo Música" />
    <meta property="og:site_name" content="A Tempo Música" />
    <meta property="og:image" content="http://atempo.sempait.com.ar/images/Atempo1.jpg" />
    <meta property="og:image" content="http://atempo.sempait.com.ar/images/Atempo2.jpg" />
    <meta property="og:image" content="http://atempo.sempait.com.ar/images/Atempo3.jpg" />
    <meta property="og:image" content="http://atempo.sempait.com.ar/images/Atempo4.jpg" />
    <meta property="og:image" content="http://atempo.sempait.com.ar/images/Atempo5.jpg" />
    <meta property="og:url" content="http://atempo.sempait.com.ar/login.aspx" />
    <meta property="og:type" content="article" />
    <meta property="article:author" content="https://www.facebook.com/sempait" />
    <meta property="article:publisher" content="https://www.facebook.com/sempait" />
</head>
<body>
    <!-- contact-form -->
    <div class="message warning">
    <asp:Label ID="lblMsjError" runat="server"></asp:Label>
        <div class="inset">
            <div class="login-head">
                <h1>
                    A Tempo Música</h1>
                <%--<div class="alert-close">
                </div>--%>
            </div>
            <form runat="server">
            <li>
                <asp:TextBox type="text" ID="txtUsuario" runat="server" placeholder="Usuario"></asp:TextBox><a
                    class=" icon user"></a>
                <%--<input type="text" class="text" value="Username" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}"><a href="#" class=" icon user"></a>--%>
            </li>
            <div class="clear">
            </div>
            <li>
                <asp:TextBox type="password" ID="txtPassword" runat="server" placeholder="Contraseña"></asp:TextBox><a
                    class="icon lock"></a>
                <%--<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}"> <a href="#" class="icon lock"></a>
                --%></li>
            <div class="clear">
            </div>
            <div class="submit">
                <%--<input type="submit" onclick="myFunction()" value="Sign in" >--%>
                <asp:Button type="submit" ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" />
                <h4>
                    <a href="#"></a>
                </h4>
                <div class="clear">
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="clear">
    </div>
    <!--- footer --->
    <div class="footer">
        <p style="text-align:center">
            <%--Design by <a href="http://www.sempait.com.ar" target="_blank">SempaIT</a>--%>
            <a href="http://www.sempait.com.ar" target="_blank" style="text-align:center">
                <img src="images/isologoSempaIT.png" alt="SempaIT"/></a>
            
            </p>
    </div>
</body>
</html>
