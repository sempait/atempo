﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Collections;
using EntitiesLayer.Bar;
using DevExpress.Web.ASPxEditors;
using BusinessLayer;

using DevExpress.Web.ASPxGridView;



public partial class bar_Venta : System.Web.UI.Page
{
    private DataTable dtVentaActual;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            deFechaVenta.Date = DateTime.Now;
        }

    }
    protected void btnSeleccionar_Click(object sender, EventArgs e)
    {
        List<object> fieldValues = gdvArticulos.GetSelectedFieldValues(new string[] { "IDArticulo", "DescripcionArticulo", "PrecioArticulo", "CostoArticulo", "StockArticulo" });

        // Convert to DataTable.
        dtVentaActual = ConvertListToDataTable(fieldValues);
        gdvVenta.DataSource = dtVentaActual;
        gdvVenta.DataBind();
        Session["tablaGrilla"] = dtVentaActual;

    }

    private DataTable ConvertListToDataTable(List<object> fieldValues)
    {
        // New table.
        DataTable table = new DataTable();
        table.Columns.Add("codigo");
        table.Columns.Add("descripcion");
        table.Columns.Add("precio");
        table.Columns.Add("costo");
        table.Columns.Add("stock");
        table.Columns.Add("cantidad").DefaultValue = 1;
        
        // Add rows.
        foreach (object[] array in fieldValues)
        {
            table.Rows.Add(array);
        }
        return table;
    }

    protected void btnAceptar_Click(object sender, EventArgs e)
    {

        DataTable tabla = (DataTable)Session["tablaGrilla"];
        double total = 0;
        double costo = 0;
        for (int i = 0; i < tabla.Rows.Count; i++)
        {
            ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
            double cantidad = Convert.ToDouble(spin.Value);

            if (Convert.ToInt32(tabla.Rows[i]["stock"]) >= cantidad)
            {
                //ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
                //double cantidad = Convert.ToDouble(spin.Value);
                tabla.Rows[i]["cantidad"] = cantidad;
                total = total + (Convert.ToDouble(tabla.Rows[i]["precio"]) * cantidad);
                costo = costo + (Convert.ToDouble(tabla.Rows[i]["costo"]) * cantidad);
            }
            else
            {
                pcMensajeVenta.ShowOnPageLoad = true;
                lblpcMensajeVenta.Text = "Hay artículos seleccionados que superan el stock disponible. Por favor, revise las cantidades y vuelva a intentar.";
                btnAceptarMensaje.Visible = false;
                btnCancelarMensaje.Visible = true;
            }
        }

        lblImporte.Text = total.ToString();
        lblCosto.Text = costo.ToString();
        Session["tablaGrilla"] = tabla;
    }
    protected void btnConfirmarVenta_Click(object sender, EventArgs e)
    {

        confirmarVenta();
        lblpcMensajeVenta.Text = "La venta fue realizada con éxito.";
        btnCancelarMensaje.Visible = false;
        btnAceptarMensaje.Visible = true;
        pcMensajeVenta.ShowOnPageLoad = true;
        gdvArticulos.DataBind();
        gdvVenta.DataBind();
        lblImporte.Value = 0;
        txtDescuento.Value = 0;
    }


    private void confirmarVenta()
    {
        //Hago la insercion de la venta
        new VentasBusiness().InsertVenta(DateTime.Parse(String.Format("{0:d}", deFechaVenta.Value)), DateTime.Parse(String.Format("{0:t}", deFechaVenta.Value)), CalcularImporteTotal(),CalcularCostoVenta());

        //Tengo que hacer el insert para cada item ,de la venta en la tabla detalle venta

        DataTable tabla = (DataTable)Session["tablaGrilla"];

        for (int i = 0; i < tabla.Rows.Count; i++)
        {
            ASPxSpinEdit spin = (ASPxSpinEdit)gdvVenta.FindRowCellTemplateControl(i, null, "txtTitle");
            double cantidad = Convert.ToDouble(spin.Value);
            tabla.Rows[i]["cantidad"] = cantidad;
            string IdArticulo = tabla.Rows[i]["Codigo"].ToString();
            new VentasBusiness().InsertItemVenta(int.Parse(cantidad.ToString()), IdArticulo);

        }
    }


    private float CalcularImporteTotal()
    {
        float ImporteTotal = int.Parse(lblImporte.Text) * (100 - (int.Parse(txtDescuento.Text))) / 100;
        return ImporteTotal;
    }

    private float CalcularCostoVenta()
    {

        return float.Parse(lblCosto.Text);

    }

    protected void btnAceptarMensaje_Click(object sender, EventArgs e)
    {
        pcMensajeVenta.ShowOnPageLoad = false;
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        List<object> fieldValues = gdvVenta.GetSelectedFieldValues(new string[] { "IDArticulo", "DescripcionArticulo", "PrecioArticulo" });

        // Convert to DataTable.
        dtVentaActual = ConvertListToDataTableRemove(fieldValues);
        gdvVenta.DataSource = dtVentaActual;
        gdvVenta.DataBind();
        Session["tablaGrilla"] = dtVentaActual;

    }


    private DataTable ConvertListToDataTableRemove(List<object> fieldValues)
    {
        //// New table.
        DataTable table = new DataTable();
        //table.Columns.Remove("codigo");
        //table.Columns.Remove("descripcion");
        //table.Columns.Remove("precio");


        foreach (object[] array in fieldValues)
        {
            fieldValues.Remove(table.Rows);
        }
        return table;
    }
}