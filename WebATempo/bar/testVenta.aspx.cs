﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using EntitiesLayer.Bar;

public partial class bar_testVenta : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSeleccionar_Click(object sender, EventArgs e)
    {
        List<object> fieldValues = gdvArticulos.GetSelectedFieldValues(new string[] { "IDArticulo", "DescripcionArticulo", "PrecioArticulo","Cantidad" });

        //foreach (object[] item in fieldValues)
        //{
        //    DataRow 
        //    ASPxListBox1.Items.Add(item[0].ToString());
        //    ASPxListBox2.Items.Add(item[1].ToString());
        //}

        // Convert to DataTable.
        DataTable table = ConvertListToDataTable(fieldValues);
        gdvVenta.DataSource = table;
        gdvVenta.DataBind();
    }

    private DataTable ConvertListToDataTable(List<object> fieldValues)
    {
        // New table.
        DataTable table = new DataTable();

        // Get max columns.
        int columns = 0;
        foreach (object[] array in fieldValues)
        {
            if (array.Length > columns)
            {
                columns = array.Length;
            }
        }

        // Add columns.
        for (int i = 0; i < columns; i++)
        {
            table.Columns.Add();
        }

        // Add rows.
        foreach (object[] array in fieldValues)
        {
            table.Rows.Add(array);
        }

        return table;
    }
    protected void btnEliminarItem_Click(object sender, EventArgs e)
    {
    }
    protected void btnGuardarItems_Click(object sender, EventArgs e)
    {

    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        //List<object> fieldValues = gdvVenta.getrowce
        //List<object> fieldValues = gdvVenta.GetSelectedFieldValues(new string[] { "IDArticulo","Column3", "Cantidad" });
    }
    protected void btnConfirmarVenta_Click(object sender, EventArgs e)
    {

    }
}