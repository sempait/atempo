﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bar/inicio.master" AutoEventWireup="true" CodeFile="testVenta.aspx.cs" Inherits="bar_testVenta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="page-wrapper">
        <div id="Div1">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Punto de venta</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            1 - Seleccionar Articulo
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                          
                                        <dx:ASPxGridView ID="gdvArticulos" runat="server" Width="100%" AutoGenerateColumns="False"
                                            KeyFieldName="IDArticulo" EnableTheming="True" Theme="Aqua" 
                                            DataSourceID="SqlDataSourceArticulos">
                                            <Columns>
                                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" ShowSelectCheckbox="True"
                                                    VisibleIndex="0">
                                                </dx:GridViewCommandColumn>
                                                <dx:GridViewDataTextColumn FieldName="IDArticulo" ShowInCustomizationForm="True"
                                                    VisibleIndex="1" Visible="False" ReadOnly="True">
                                                    <EditFormSettings Visible="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Descripcion" FieldName="DescripcionArticulo"
                                                    ShowInCustomizationForm="True" VisibleIndex="3">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="IDTipoArticulo" ShowInCustomizationForm="True"
                                                    VisibleIndex="4" Visible="False">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Stock" FieldName="StockArticulo" ShowInCustomizationForm="True"
                                                    VisibleIndex="7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="FechaBajaArticulo" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="5">
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="IDTipoArticulo1" ReadOnly="True" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="6">
                                                    <EditFormSettings Visible="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Tipo Articulo" FieldName="DescripcionTipoArticulo"
                                                    ShowInCustomizationForm="True" VisibleIndex="2">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="FechaBajaTipoArticulo" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="8">
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="IDHistoricoPrecioArticulo" ReadOnly="True"
                                                    ShowInCustomizationForm="True" Visible="False" VisibleIndex="9">
                                                    <EditFormSettings Visible="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="FechaHastaPrecioArticulo" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="10">
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Precio" FieldName="PrecioArticulo" ShowInCustomizationForm="True"
                                                    VisibleIndex="11">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="IDArticulo1" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="12">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="IDHistoricoCostoArticulo" ReadOnly="True" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="13">
                                                    <EditFormSettings Visible="False" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn FieldName="FechaHastaCostoArticulo" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="14">
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn FieldName="CostoArticulo" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="15">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="IDArticulo2" ShowInCustomizationForm="True"
                                                    Visible="False" VisibleIndex="16">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataSpinEditColumn Caption="Cantidad" VisibleIndex="17" FieldName="Cantidad">
<PropertiesSpinEdit DisplayFormatString="g"></PropertiesSpinEdit>
                                         <DataItemTemplate>
                                            <dx:ASPxSpinEdit ID="txtCantidad" runat="server" Width="100px" MinValue="1" MaxValue="100" NullText="1"></dx:ASPxSpinEdit> 
                                             </DataItemTemplate>
                                    </dx:GridViewDataSpinEditColumn>      
                                            </Columns>
                                            
                                            <SettingsBehavior AllowFocusedRow="True" />
                                            <Settings ShowFilterRow="True" />
                                        </dx:ASPxGridView>
                                        <asp:SqlDataSource ID="SqlDataSourceArticulos" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                            SelectCommand="Articulos_SelectAll_EnStock" SelectCommandType="StoredProcedure">
                                        </asp:SqlDataSource>
                                        <p></p>
                                        <p>
                                            <dx:ASPxButton ID="btnSeleccionar" runat="server" Theme="Aqua" 
                                                Text="Seleccionar" onclick="btnSeleccionar_Click">
                                                </dx:ASPxButton>
                                        </p>
                                    
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Detalle de Venta
                        </div>
                        <div class="panel-body">
                           <dx:ASPxGridView ID="gdvVenta" runat="server" Theme="Aqua" 
                                AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    
                                    <dx:GridViewDataTextColumn Caption="IDArticulo" VisibleIndex="0" FieldName="Column1" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="DescripcionArticulo" VisibleIndex="1" FieldName="Column2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="PrecioArticulo" VisibleIndex="2" FieldName="Column3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataSpinEditColumn Caption="Cantidad" VisibleIndex="3" FieldName="Cantidad">
                                            <PropertiesSpinEdit DisplayFormatString="g"></PropertiesSpinEdit>                                         
                                    </dx:GridViewDataSpinEditColumn>                                    
                                </Columns>
                                <Settings ShowFooter="True" />
                                <TotalSummary>
                                  <dx:ASPxSummaryItem FieldName="Column3" SummaryType="Sum" />
                                  <dx:ASPxSummaryItem FieldName="Cantidad" SummaryType="Sum" />
                             </TotalSummary>
                            </dx:ASPxGridView>
                            <p></p>
                                        <p>
                                            <dx:ASPxButton ID="btnAceptar" runat="server" Theme="Aqua" 
                                                Text="Aceptar" onclick="btnAceptar_Click">
                                                </dx:ASPxButton>
                                        </p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Importe
                        </div>
                        <div class="panel-body">
                            <%--<dl>
                                <dt>Standard Description List</dt>
                                <dd>Description Text</dd>
                                <dt>Description List Title</dt>
                                <dd>Description List Text</dd>
                            </dl>--%>
                            <dl class="dl-horizontal">
                                <dt>Importe Total</dt>
                                <dd runat="server" id="lblImporteTotal"></dd>
                                <%--<dt>Importe Total</dt>
                                <dd>$150</dd>--%>
                            </dl>
                            <p></p>
                                        <p>
                                            <dx:ASPxButton ID="btnConfirmarVenta" runat="server" Theme="Aqua" 
                                                Text="Confirmar Venta" onclick="btnConfirmarVenta_Click">
                                                </dx:ASPxButton>
                                        </p>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->

</asp:Content>

