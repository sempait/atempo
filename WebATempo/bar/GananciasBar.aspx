﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bar/inicio.master" AutoEventWireup="true"
    CodeFile="GananciasBar.aspx.cs" Inherits="bar_GananciasBar" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Gestión de ganancias bar</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <tbody>
                    <tr class="odd gradeX">
                        <td align="center">
                            Fecha Desde:
                        </td>
                        <td align="center">
                            <dx:ASPxDateEdit ID="deFechaDesde" runat="server" Width="200" EditFormatString="dd/MM/yyyy"
                                Theme="Aqua">
                                <TimeSectionProperties>
                                    <TimeEditProperties DisplayFormatString="dd/MM/yyyy" />
                                </TimeSectionProperties>
                            </dx:ASPxDateEdit>
                        </td>
                        <td align="center">
                            Fecha Hasta:
                        </td>
                        <td align="center">
                            <dx:ASPxDateEdit ID="deFechaHasta" runat="server" Width="200" EditFormatString="dd/MM/yyyy"
                                Theme="Aqua">
                                <TimeSectionProperties>
                                    <TimeEditProperties DisplayFormatString="dd/MM/yyyy" />
                                </TimeSectionProperties>
                            </dx:ASPxDateEdit>
                        </td>
                        <td align="center">
                            <asp:Button ID="btnEmitirReporte" runat="server" Text="Emitir" type="button" 
                                class="btn btn-outline btn-primary" onclick="btnEmitirReporte_Click" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <p>
        </p>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ganancias bar
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="800px" Font-Names="Verdana"
                        Font-Size="8pt" InteractiveDeviceInfos="(Colección)" WaitMessageFont-Names="Verdana"
                        WaitMessageFont-Size="14pt">
                        <LocalReport ReportPath="App_Code\ReporteGananciasBar.rdlc">
                        </LocalReport>
                    </rsweb:ReportViewer>
                </div>
            </div>
    </div>
</asp:Content>
