﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class bar_index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CargarNotificacion();
    }

    private void CargarNotificacion()
    {
        //Cargo el Contenido
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oN1 = new Notificaciones();
        oN1 = oNotfBusiness.SelectOne(7);

        lblNotificacion1.Text = oN1.Notificacion;
        lblFechaNoti1.Value = Convert.ToString(oN1.FechaPublicacionNotificacion.ToShortDateString());

        Notificaciones oN2 = new Notificaciones();
        oN2 = oNotfBusiness.SelectOne(8);

        lblNotificacion2.Text = oN2.Notificacion;
        lblFechaNoti2.Value = Convert.ToString(oN2.FechaPublicacionNotificacion.ToShortDateString());


        Notificaciones oN3 = new Notificaciones();
        oN3 = oNotfBusiness.SelectOne(9);

        lblNotificacion3.Text = oN3.Notificacion;
        lblFechaNoti3.Value = Convert.ToString(oN3.FechaPublicacionNotificacion.ToShortDateString());
    }
}