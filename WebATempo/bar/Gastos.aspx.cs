﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class bar_Gastos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["Id_Gasto"] = null;
    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("GastosEditar.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        string Id_Gasto = gdvGasto.GetRowValues(gdvGasto.FocusedRowIndex, "Id_Gasto").ToString();
        Session.Add("Id_Gasto", Id_Gasto);
        Response.Redirect("GastosEditar.aspx");
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        string Id_Gasto = gdvGasto.GetRowValues(gdvGasto.FocusedRowIndex, "Id_Gasto").ToString();

        new BusinessLayer.GastosBarBusiness().Delete(int.Parse(Id_Gasto));

        gdvGasto.DataBind();
    }
}