﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bar/inicio.master" AutoEventWireup="true"
    CodeFile="Articulo.aspx.cs" Inherits="bar_Articulo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        function ShowPopUp() {
            pcEliminar.Show();
        }
             
          
    </script>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Articulos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de Articulos
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar" type="button" class="btn btn-outline btn-primary"
                                    OnClick="btnAgregar_Click" Width="100px" />
                                <asp:Button ID="btnEditar" runat="server" Text="Editar" type="button" class="btn btn-outline btn-warning"
                                    OnClick="btnEditar_Click" Width="100px" />
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" type="button" class="btn btn-outline btn-danger"
                                    OnClick="btnEliminar_Click" Width="100px" />
                            </p>
                            <dx:ASPxGridView ID="gdvArticulos" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                EnableTheming="True" KeyFieldName="IDArticulo" Theme="Aqua" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="IDArticulo" ReadOnly="True" VisibleIndex="0"
                                        Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DescripcionArticulo" VisibleIndex="2" Caption="Descripcion">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="IDTipoArticulo" VisibleIndex="3" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="StockArticulo" VisibleIndex="4" Caption="Stock">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaBajaArticulo" VisibleIndex="5" Visible="False">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="IDTipoArticulo1" ReadOnly="True" Visible="False"
                                        VisibleIndex="6">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Tipo Articulo" FieldName="DescripcionTipoArticulo"
                                        VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaBajaTipoArticulo" Visible="False" VisibleIndex="7">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="IDHistoricoPrecioArticulo" ReadOnly="True"
                                        Visible="False" VisibleIndex="8">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaHastaPrecioArticulo" Visible="False" VisibleIndex="9">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="Precio" FieldName="PrecioArticulo" VisibleIndex="11">
                                        <%--<PropertiesTextEdit DisplayFormatString="c">
                                        </PropertiesTextEdit>--%>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="IDArticulo1" Visible="False" VisibleIndex="12">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="IDHistoricoCostoArticulo" ReadOnly="True" Visible="False"
                                        VisibleIndex="13">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaHastaCostoArticulo" Visible="False" VisibleIndex="14">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="Costo" FieldName="CostoArticulo" VisibleIndex="10">
                                        <%--<PropertiesTextEdit DisplayFormatString="c">
                                        </PropertiesTextEdit>--%>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="IDArticulo2" Visible="False" VisibleIndex="15">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" />
                                <SettingsPager PageSize="20">
                                </SettingsPager>
                                <Settings ShowFilterRow="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="Articulos_SelectAll" SelectCommandType="StoredProcedure">
                            </asp:SqlDataSource>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
    <dx:ASPxPopupControl ID="pcEliminar" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcEliminar" HeaderText="Eliminar Articulo" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblpcEliminar" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelar" runat="server" Text="Cancelar">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
