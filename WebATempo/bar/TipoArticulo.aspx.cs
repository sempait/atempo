﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class bar_TipoArticulo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IdTipoArticulo"] = null;
    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("editarTipoArticulo.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Editar();
    }

    private void Editar()
    {
        string IDTipoArticulo = gdvTipoArticulos.GetRowValues(gdvTipoArticulos.FocusedRowIndex, "IDTipoArticulo").ToString();
        Session.Add("IDTipoArticulo", IDTipoArticulo);
        Response.Redirect("EditarTipoArticulo.aspx");
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        try
        {
            string IDTipoArticulo = gdvTipoArticulos.GetRowValues(gdvTipoArticulos.FocusedRowIndex, "IDTipoArticulo").ToString();

            TiposArticulosBusiness oTiposArticulos = new TiposArticulosBusiness();
            oTiposArticulos.Delete(int.Parse(IDTipoArticulo));

            Response.Redirect("TipoArticulo.aspx");
        }
        catch
        {
            lblpcEliminar.Text = "El tipo de artículo que desea eliminar posee artículos vinculados. Elimine primero los artículos asociados a este tipo de artículo para continuar.";
            btnAceptar.Visible = false;
            pcEliminar.ShowOnPageLoad = true;
        }
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        lblpcEliminar.Text = "¿Desea eliminar el Tipo Articulo?";
        pcEliminar.ShowOnPageLoad = true;
    }
}