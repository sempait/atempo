﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer;
using System.Globalization;

public partial class bar_AgregarEditarOperacion : System.Web.UI.Page
{

    private int Id_Caja_Bar;

    OperacionCajaDiariaBar oOperacionActual = new OperacionCajaDiariaBar();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            try
            {
                Id_Caja_Bar = int.Parse(Session["Id_Caja_Bar"].ToString());

                //Cargo el form
                if (Id_Caja_Bar != 0)
                    CargarDatos(Id_Caja_Bar);
            }

            catch
            {
                Id_Caja_Bar = 0;
                FechaOperacion.Value = DateTime.Now;
            }

        }
    }

    private void CargarDatos(int IdOperacion)
    {

        oOperacionActual = new BusinessLayer.OperacionCajaDiariaBar().Select_One(IdOperacion);

        FechaOperacion.Value = oOperacionActual.FechaOperacion;
        DescripcionOperacion.Value = oOperacionActual.DescripcionOperacion;
        
        if (oOperacionActual.Ingreso != 0)
        {

            TipoOperacion.Text = "Ingreso";
            ImporteOperacion.Value = oOperacionActual.Ingreso;
        }
        else
        {
            TipoOperacion.Text = "Egreso";
            ImporteOperacion.Value = oOperacionActual.Egreso;
        }



    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        GuardarDatos();
    }

    private void GuardarDatos()
    {
        try
        {
            Id_Caja_Bar = int.Parse(Session["Id_Caja_Bar"].ToString());

        }

        catch
        {
            Id_Caja_Bar = 0;
        }


        OperacionCajaDiariaBar mOperacion = new OperacionCajaDiariaBar();

        mOperacion.DescripcionOperacion = DescripcionOperacion.Text;
        mOperacion.FechaOperacion = DateTime.Parse(FechaOperacion.Value.ToString());

        mOperacion.FechaOperacion = DateTime.Parse(mOperacion.FechaOperacion.Value.ToShortDateString());

        if (TipoOperacion.Text == "Ingreso")
        {
            mOperacion.Egreso = 0;
            mOperacion.Ingreso = float.Parse(ImporteOperacion.Text);
        }
        else
        {
            mOperacion.Ingreso = 0;
            mOperacion.Egreso = float.Parse(ImporteOperacion.Text);
        }


        if (Id_Caja_Bar == 0)
        {
            new BusinessLayer.OperacionCajaDiariaBar().OperacionInsert(mOperacion);
        }
        else
        {

            mOperacion.ID = Id_Caja_Bar;
            new BusinessLayer.OperacionCajaDiariaBar().Update(mOperacion);
        }



        Response.Redirect("IngresosEgresosCaja.aspx");

    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("IngresosEgresosCaja.aspx");
    }

}