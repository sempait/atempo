﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using DevExpress.Web.ASPxGridView;

public partial class bar_Ventas : System.Web.UI.Page
{

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("Venta.aspx");
    }
    protected void bEliminarInscripcion_Click(object sender, EventArgs e)
    {
        string Id_Venta = gvMain.GetRowValues(gvMain.FocusedRowIndex, "IDVenta").ToString();

        new BusinessLayer.VentasBusiness().Delete(int.Parse(Id_Venta));

        gvMain.DataBind();
    }

    protected void gvDetails_Init(object sender, EventArgs e)
    {
        ASPxGridView gvDetails = (ASPxGridView)sender;
        int nIdVenta = Convert.ToInt32(gvDetails.GetMasterRowKeyValue());
        gvDetails.DataSource = new VentasBusiness().SelectAllItemVenta(nIdVenta);
        gvDetails.DataBind();
    }


}