﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bar/inicio.master" AutoEventWireup="true"
    CodeFile="EditarTipoArticulo.aspx.cs" Inherits="bar_EditarTipoArticulo" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Tipo de Articulo</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tipo de Articulo
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout runat="server" RequiredMarkDisplayMode="Auto" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption"
                                    AlignItemCaptionsInAllGroups="True" Theme="Aqua" Width="100%">
                                    <Items>
                                        <dx:LayoutGroup Caption="Datos Tipo Articulo" GroupBoxDecoration="HeadingLine" SettingsItemCaptions-HorizontalAlign="Right">
                                            <Items>
                                                <dx:LayoutItem Caption="Descripcion Tipo Articulo">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox ID="txtTipoArticulo" runat="server" NullText="" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                            <SettingsItemCaptions HorizontalAlign="Right"></SettingsItemCaptions>
                                        </dx:LayoutGroup>
                                    </Items>

<Styles>
<LayoutGroupBox>
<Caption CssClass="layoutGroupBoxCaption"></Caption>
</LayoutGroupBox>
</Styles>
                                </dx:ASPxFormLayout>
                                <p>
                                </p>
                                <p style="text-align: right">
                                    <asp:Button ID="btnGurardar" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                        Width="150px" OnClick="btnGuardar_Click" />
                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                        OnClick="btnCancelar_Click" />
                                </p>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
