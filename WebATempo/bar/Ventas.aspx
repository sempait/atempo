﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bar/inicio.master" AutoEventWireup="true"
    CodeFile="Ventas.aspx.cs" Inherits="bar_Ventas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Listado de Ventas</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de Ventas
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregar" runat="server" Text="Nueva Venta" type="button" class="btn btn-outline btn-primary"
                                    OnClick="btnAgregar_Click" Width="110px" />
                                <asp:Button ID="bEliminarInscripcion" runat="server" Text="Eliminar" type="button"
                                    class="btn btn-outline btn-danger" Width="110px" OnClick="bEliminarInscripcion_Click" />
                                <dx:ASPxGridView ID="gvMain" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                    EnableTheming="True" KeyFieldName="IDVenta" Theme="Aqua" Width="100%">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="IDVenta" ReadOnly="True" Visible="False" VisibleIndex="0">
                                            <EditFormSettings Visible="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="FechaVenta" VisibleIndex="1">
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="HoraVenta" VisibleIndex="2">
                                            <PropertiesDateEdit DisplayFormatString="{0:t}">
                                            </PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="ImporteTotalVenta" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior AllowFocusedRow="True" />
                                    <SettingsDetail ShowDetailRow="True" />
                                    <Templates>
                                        <DetailRow>
                                            <dx:ASPxGridView ID="gvDetails" runat="server" AutoGenerateColumns="False" OnInit="gvDetails_Init"
                                                Theme="Aqua" Width="50%" Style="margin: auto">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="Descripcion" FieldName="Articulo.DescripcionArticulo"
                                                        VisibleIndex="0">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Cantidad" FieldName="Cantidad" VisibleIndex="1">
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                            </dx:ASPxGridView>
                                        </DetailRow>
                                    </Templates>
                                </dx:ASPxGridView>
                            </p>
                            <%--<asp:GridView ID="gvMain" DataKeyNames="ID" runat="server" AutoGenerateColumns="False"
                                EnableTheming="True" Theme="Aqua" Width="100%" CellPadding="4" OnRowDataBound="gvMain_RowDataBound"
                                GridLines="None" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                                OnRowCommand="gvMain_RowCommand">
                                <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <img id="<%# Eval("ID") %>" alt="Click para mostrar u ocultar los detalles <%# Eval("ID") %>"
                                                border="0" src="../images/detail.png" title="Detalle" />
                                        </ItemTemplate>
                                        <ItemStyle Width="10px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" DataFormatString="{0:d}"></asp:BoundField>
                                    <asp:BoundField DataField="Hora" HeaderText="Hora" DataFormatString="{0:t}"></asp:BoundField>
                                    <asp:BoundField DataField="Importe" HeaderText="Importe"></asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr style="height: 0px;">
                                                <td colspan="100%" align="center" style="height: 0px;">
                                                    <center>
                                                        <div id="div<%# Eval("ID") %>" style="display: none;">
                                                            <asp:GridView ID="gvDetails" DataKeyNames="ID" AutoGenerateColumns="false" runat="server"
                                                                GridLines="Horizontal">
                                                                <Columns>
                                                                    <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" HeaderStyle-Width="250px">
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Articulo.DescripcionArticulo" HeaderText="Articulo" DataFormatString="{0:d}"
                                                                        HeaderStyle-Width="120px"></asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </center>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pgr"></PagerStyle>
                                <SelectedRowStyle BackColor="#99CCFF" BorderStyle="Double" />
                            </asp:GridView>--%>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT * FROM [Ventas] ORDER BY FechaVenta DESC,HoraVenta DESC"></asp:SqlDataSource>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <dx:ASPxPopupControl ID="pcEliminarVenta" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcEliminarVenta" HeaderText="Eliminar Venta" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False" Width="325px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2" width="100%">
                            ¿Desea eliminar por completo la Venta?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptarEliminar" runat="server" Text="Aceptar">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelarEliminar" runat="server" Text="Cancelar">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcMensaje" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="Modificar la Modalidad" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnpcMensajeAcetar" runat="server" Text="Aceptar">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnpcMensajeCancelar" runat="server" Text="Cancelar">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
