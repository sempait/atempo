﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bar/inicio.master" AutoEventWireup="true"
    CodeFile="Venta.aspx.cs" Inherits="bar_Venta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Punto de venta</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="alert alert-success">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" AssociatedControlID="dateEdit" Text="Fecha Venta:" />
            <dx:ASPxDateEdit ID="deFechaVenta" runat="server" EditFormat="Custom" Width="200"
                Theme="Aqua">
                <TimeSectionProperties>
                    <TimeEditProperties EditFormatString="hh:mm tt" />
                </TimeSectionProperties>
            </dx:ASPxDateEdit>
        </div>
        <p>
        </p>
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        1 - Seleccionar Articulo
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <dx:ASPxGridView ID="gdvArticulos" runat="server" Width="100%" AutoGenerateColumns="False"
                            KeyFieldName="IDArticulo" EnableTheming="True" Theme="Aqua" DataSourceID="SqlDataSourceArticulos">
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="True"
                                    VisibleIndex="0" Caption="Seleccionar">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="IDArticulo"
                                    VisibleIndex="1" Visible="False" ReadOnly="True">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Descripcion" 
                                    FieldName="DescripcionArticulo" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="IDTipoArticulo"
                                    VisibleIndex="4" Visible="False">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Stock" FieldName="StockArticulo"
                                    VisibleIndex="7">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn FieldName="FechaBajaArticulo"
                                    Visible="False" VisibleIndex="5">
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn FieldName="IDTipoArticulo1" ReadOnly="True"
                                    Visible="False" VisibleIndex="6">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Tipo Articulo" 
                                    FieldName="DescripcionTipoArticulo" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn FieldName="FechaBajaTipoArticulo"
                                    Visible="False" VisibleIndex="8">
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn FieldName="IDHistoricoPrecioArticulo" 
                                    ReadOnly="True" Visible="False" VisibleIndex="9">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn FieldName="FechaHastaPrecioArticulo"
                                    Visible="False" VisibleIndex="10">
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn Caption="Precio" FieldName="PrecioArticulo"
                                    VisibleIndex="11">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="IDArticulo1"
                                    Visible="False" VisibleIndex="12">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="IDHistoricoCostoArticulo" ReadOnly="True"
                                    Visible="False" VisibleIndex="13">
                                    <EditFormSettings Visible="False" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn FieldName="FechaHastaCostoArticulo"
                                    Visible="False" VisibleIndex="14">
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn FieldName="CostoArticulo"
                                    Visible="False" VisibleIndex="15">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="IDArticulo2"
                                    Visible="False" VisibleIndex="16">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" />
                            <Settings ShowFilterRow="True" />
                        </dx:ASPxGridView>
                        <asp:SqlDataSource ID="SqlDataSourceArticulos" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                            SelectCommand="Articulos_SelectAll_EnStock" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                        <p>
                        </p>
                        <p style="text-align: right">
                            <asp:Button ID="btnSeleccionar" runat="server" Text="=>" type="button" class="btn btn-outline btn-warning"
                                OnClick="btnSeleccionar_Click" Width="50px" />
                        </p>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        2 - Confirme Artículos
                    </div>
                    <div class="panel-body">

                        <dx:ASPxGridView ID="gdvVenta" runat="server" Theme="Aqua" AutoGenerateColumns="False" KeyFieldName="codigo"
                            Width="100%">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="IDArticulo" VisibleIndex="0" FieldName="codigo"
                                    Visible="false">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="DescripcionArticulo" VisibleIndex="1" FieldName="descripcion">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="PrecioArticulo" VisibleIndex="2" FieldName="precio">

                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataSpinEditColumn Caption="Cantidad" VisibleIndex="4" FieldName="cantidad">
                                    <PropertiesSpinEdit DisplayFormatString="g">
                                    </PropertiesSpinEdit>
                                    <DataItemTemplate>
                                        <dx:ASPxSpinEdit ID="txtTitle" runat="server" Width="100px" MinValue="1" MaxValue="100">
                                        </dx:ASPxSpinEdit>
                                    </DataItemTemplate>
                                </dx:GridViewDataSpinEditColumn>
                                <dx:GridViewDataTextColumn Caption="CostoArticulo" FieldName="costo" Visible="False" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" />

                        </dx:ASPxGridView>
                        <p>
                        </p>
                        <p style="text-align: left">
                            <asp:Button ID="btnEliminar" runat="server" Text="<=" type="button" class="btn btn-outline btn-warning"
                               Width="50px" onclick="btnEliminar_Click" />
                        </p>
                        <p style="text-align: right">
                            <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" type="button" class="btn btn-outline btn-primary"
                                OnClick="btnAceptar_Click" Width="100px" />
                        </p>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        3 - Detalle de Venta y Confirmar
                    </div>
                    <div class="panel-body">
                        <dl class="dl-horizontal">
                            <dt>Importe</dt>
                            <dd>
                                <dx:ASPxLabel ID="lblImporte" runat="server" Text="0">
                                </dx:ASPxLabel>
                            </dd>
                            <dt>Descuento %</dt>
                            <dd>
                                <dx:ASPxTextBox ID="txtDescuento" runat="server" Width="50px" Text="0" >
                                </dx:ASPxTextBox>
                            </dd>
                            <dt>
                                <asp:Label ID="lblCosto" runat="server" Text="Costo" Visible="False"></asp:Label>
                            </dt>
                        </dl>

                        <p>
                        </p>
                        <p style="text-align: right">
                            <asp:Button ID="btnConfirmarVenta" runat="server" Text="Confirmar Venta" type="button"
                                class="btn btn-outline btn-primary" OnClick="btnConfirmarVenta_Click" Width="150px" />
                        </p>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->

        <!-- /#wrapper -->
    </div>
    <dx:ASPxPopupControl ID="pcMensajeVenta" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcEliminar" HeaderText="Nueva Venta"
        AllowDragging="True" PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblpcMensajeVenta" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptarMensaje" runat="server" Text="Aceptar" 
                                AutoPostBack="False" Width="100%" OnClick="btnAceptarMensaje_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                            
                            <dx:ASPxButton ID="btnCancelarMensaje" runat="server" Text="Aceptar">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
