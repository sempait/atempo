﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer.Bar;
using BusinessLayer;

public partial class bar_GastosEditar : System.Web.UI.Page
{
    GastosBar oGasto = new GastosBar();
    private int Id_Gasto;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            try
            {
                Id_Gasto = int.Parse(Session["Id_Gasto"].ToString());

                //Cargo el form
                if (Id_Gasto != 0)
                    CargarDatos(Id_Gasto);
            }

            catch
            {
                Id_Gasto = 0;
                FechaGasto.Value = DateTime.Now;
            }

        }
    }

    private void CargarDatos(int Id_Gasto)
    {
        oGasto = new BusinessLayer.GastosBarBusiness().Select_One(Id_Gasto);

        DescripcionGasto.Value = oGasto.DescripcionGasto;
        FechaGasto.Value = oGasto.FechaGasto;
        ImporteGasto.Value = oGasto.Improte;
        chkAfecta.Checked = oGasto.AftectaCaja;


    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            Id_Gasto = int.Parse(Session["Id_Gasto"].ToString());

        }

        catch
        {
            Id_Gasto = 0;
        }

        oGasto.DescripcionGasto = DescripcionGasto.Text;
        oGasto.Improte = float.Parse(ImporteGasto.Text.ToString());
        oGasto.FechaGasto = DateTime.Parse(FechaGasto.Value.ToString());
        oGasto.FechaGasto = DateTime.Parse(oGasto.FechaGasto.ToShortDateString());
        oGasto.AftectaCaja = chkAfecta.Checked;

        if (Id_Gasto == 0)

            new GastosBarBusiness().Insert(oGasto);

        else
        {
            oGasto.ID = Id_Gasto;
            new GastosBarBusiness().Update(oGasto);

        }

        Response.Redirect("Gastos.aspx");
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("Gastos.aspx");
    }
}