﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer.Bar;
using BusinessLayer;

public partial class bar_EditarArticulo : System.Web.UI.Page
{
    private int IdArticulo;
    Articulos oArticuloActual = new Articulos();
    private float mPrecioOld;
    private float mCostoOld;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["IDArticulo"] == null)
                {
                    IdArticulo = 0;

                }
                else
                {
                    IdArticulo = int.Parse(Session["IDArticulo"].ToString());

                    //Cargo el form
                    if (IdArticulo != 0)
                        CargarDatos(IdArticulo);
                }
            }
            catch
            {
                IdArticulo = 0;
            }
        }


    }

    private void CargarDatos(int IdArticulo)
    {
        //TiposArticulos oTipoArticulos = new TiposArticulos();
        //cbTipoArticulo.DataSource = new TiposArticulosBusiness().SelectAll();
        //cbTipoArticulo.TextField = "DescripcionTipoArticulo";
        //cbTipoArticulo.ValueField = "IDTipoArticulo"; 
        ////DropDownList1.DataTextField = "QUIZ_Name";
        ////DropDownList1.DataValueField = "QUIZ_ID"
        //cbTipoArticulo.DataBind();

        Articulos oArticuloActual = new ArticulosBusiness().SelectOne(IdArticulo);

        txtDescripcionArticulo.Value = oArticuloActual.DescripcionArticulo;
        txtStock.Value = oArticuloActual.Stock;
        txtPrecio.Value = Convert.ToString(oArticuloActual.HistoricoPrecioArticulo[0].Precio);
        txtCosto.Value = Convert.ToString(oArticuloActual.HistoricoCostoArticulo[0].Costo);

        cbTipoArticulo.Value = oArticuloActual.TipoArticulo.DescripcionTipoArticulo;

    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        GuardarArticulo();
    }

    private void GuardarArticulo()
    {
        try
        {
            IdArticulo = int.Parse(Session["IdArticulo"].ToString());
        }
        catch
        {
            IdArticulo = 0;
        }

        ArticulosBusiness oArticulosBussines = new ArticulosBusiness();

        oArticuloActual.DescripcionArticulo = txtDescripcionArticulo.Text;
        oArticuloActual.Stock = int.Parse(txtStock.Text);


        TiposArticulos oTipoArt = new TiposArticulos();

        int i = 0;

        bool result = int.TryParse(cbTipoArticulo.Value.ToString(), out i);
        if (!result)
        { oTipoArt.ID = int.Parse(Session["IDTipoArticulo"].ToString()); }
        else
        { oTipoArt.ID = int.Parse(cbTipoArticulo.Value.ToString()); }


        oArticuloActual.TipoArticulo = oTipoArt;

        HistoricoPreciosArticulos oPrecioActualizar = new HistoricoPreciosArticulos();

        if (txtPrecio.Text == "")
        { oPrecioActualizar.Precio = 0; }
        else
        { oPrecioActualizar.Precio = float.Parse(txtPrecio.Text); }

        oPrecioActualizar.Articulo = oArticuloActual;
        oArticuloActual.HistoricoPrecioArticulo.Add(oPrecioActualizar);

        HistoricoCostosArticulos oCostosActualizar = new HistoricoCostosArticulos();
        if (txtCosto.Text == "")
        { oCostosActualizar.Costo = 0; }
        else
        { oCostosActualizar.Costo = float.Parse(txtCosto.Text); }

        oCostosActualizar.Articulo = oArticuloActual;
        oArticuloActual.HistoricoCostoArticulo.Add(oCostosActualizar);

        oArticuloActual.TipoArticulo = oTipoArt;

        if (IdArticulo == 0)
        {
            oArticulosBussines.Insert(oArticuloActual);

        }
        else
        {

            oArticuloActual.ID = IdArticulo;
            oArticulosBussines.Update(oArticuloActual);
            actualizarCostoArticulo();
            actualizarPrecioArticulo();
        }

        Session["IdArticulo"] = null;
        Response.Redirect("Articulo.aspx");
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Session["IdArticulo"] = null;
        Response.Redirect("Articulo.aspx");
    }

    private void actualizarPrecioArticulo()
    {

        HistoricoPreciosArticulos oPrecioActualizar = new HistoricoPreciosArticulos();

        oArticuloActual.ID = IdArticulo;
        oArticuloActual.HistoricoPrecioArticulo[0].Precio = int.Parse(txtPrecio.Text);
        oPrecioActualizar.Articulo = oArticuloActual;
        oPrecioActualizar.Precio = float.Parse(txtPrecio.Text);


        new HistoricoPreciosArticulosBusiness().Insert(oPrecioActualizar);

    }




    private void actualizarCostoArticulo()
    {

        HistoricoCostosArticulos oCostosActualizar = new HistoricoCostosArticulos();
        oArticuloActual.ID = IdArticulo;
        oArticuloActual.HistoricoPrecioArticulo[0].Precio = int.Parse(txtPrecio.Text);
        oCostosActualizar.Articulo = oArticuloActual;
        oCostosActualizar.Costo = float.Parse(txtCosto.Text);

        new HistoricoCostosArticulosBusiness().Insert(oCostosActualizar);

    }
}