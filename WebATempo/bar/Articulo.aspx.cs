﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;

public partial class bar_Articulo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IDArticulo"] = null;

        // Necesario para traducir los componentes a Español
        /*1º linea*/
        System.Threading.Thread.CurrentThread.CurrentCulture =
        new System.Globalization.CultureInfo("es-ES");
        /*2º linea*/
        System.Threading.Thread.CurrentThread.CurrentUICulture =
        new System.Globalization.CultureInfo("es");
    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditarArticulo.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Editar();
    }

    private void Editar()
    {
        string IDArticulo = gdvArticulos.GetRowValues(gdvArticulos.FocusedRowIndex, "IDArticulo").ToString();
        string IDTipoArticulo = gdvArticulos.GetRowValues(gdvArticulos.FocusedRowIndex, "IDTipoArticulo").ToString();
        Session.Add("IDArticulo", IDArticulo);
        Session.Add("IDTipoArticulo", IDTipoArticulo);
        Response.Redirect("EditarArticulo.aspx");
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        try
        {
            string IDArticulo = gdvArticulos.GetRowValues(gdvArticulos.FocusedRowIndex, "IDArticulo").ToString();

            ArticulosBusiness oArticulo = new ArticulosBusiness();
            oArticulo.Delete(int.Parse(IDArticulo));
    
            Response.Redirect("Articulo.aspx");
        }
        catch
        {
            lblpcEliminar.Text = "El artículo posee ventas asociadas. Elimine primero las ventas asociadas a este artículo para continuar.";
            btnAceptar.Visible = false;
            pcEliminar.ShowOnPageLoad = true;
        }
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        lblpcEliminar.Text = "¿Desea eliminar el Articulo?";
        pcEliminar.ShowOnPageLoad = true;
    }
}