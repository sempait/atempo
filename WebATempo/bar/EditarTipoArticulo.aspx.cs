﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer.Bar;
using BusinessLayer;

public partial class bar_EditarTipoArticulo : System.Web.UI.Page
{
    private int IdTipoArticulo;
    TiposArticulos oTiposArticulosActual = new TiposArticulos();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                IdTipoArticulo = int.Parse(Session["IdTipoArticulo"].ToString());

                //Cargo el form
                if (IdTipoArticulo != 0)
                    CargarDatos(IdTipoArticulo);
            }
            catch
            {
                IdTipoArticulo = 0;
            }
        }
    }

    private void CargarDatos(int IdTipoArticulo)
    {
        oTiposArticulosActual = new TiposArticulosBusiness().SelectOne(IdTipoArticulo);

        txtTipoArticulo.Value = oTiposArticulosActual.DescripcionTipoArticulo;
        //deFechaBaja.Value = oTiposArticulosActual.FechaBaja;
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        GuardarTipoArticulo();
    }

    private void GuardarTipoArticulo()
    {
        try
        {
            IdTipoArticulo = int.Parse(Session["IdTipoArticulo"].ToString());
        }
        catch
        {
            IdTipoArticulo = 0;
        }

        TiposArticulosBusiness oTiposArticulosBussines = new TiposArticulosBusiness();
        TiposArticulos oTiposArticulos = new TiposArticulos();

        oTiposArticulos.DescripcionTipoArticulo = txtTipoArticulo.Text;
        //oTiposArticulos.FechaBaja = Convert.ToDateTime(deFechaBaja.Text);
    
        if (IdTipoArticulo == 0)
        {
            oTiposArticulosBussines.Insert(oTiposArticulos);
        }
        else
        {
            oTiposArticulos.ID = IdTipoArticulo;
            oTiposArticulosBussines.Update(oTiposArticulos);
        }

        Session["IdTipoArticulo"] = null;
        Response.Redirect("TipoArticulo.aspx");
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("TipoArticulo.aspx");
    }
    
}