﻿<%@ Page Title="" Language="C#" MasterPageFile="~/bar/inicio.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="bar_index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page-wrapper">
        
        
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Panel de Notificaciones</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Bar - Notificación 1</div>
                        <div class="panel-body">
                            <p>
                                <dx:ASPxLabel ID="lblNotificacion1" runat="server" Text="">
                                </dx:ASPxLabel>
                            </p>
                        </div>
                        <div class="panel-footer">
                            Fecha de publicación
                            <dx:ASPxLabel ID="lblFechaNoti1" runat="server" Text="">
                            </dx:ASPxLabel>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Bar - Notificación 2
                        </div>
                        <div class="panel-body">
                            <p>
                                <dx:ASPxLabel ID="lblNotificacion2" runat="server" Text="">
                                </dx:ASPxLabel>
                            </p>
                        </div>
                        <div class="panel-footer">
                            Fecha de publicación
                            <dx:ASPxLabel ID="lblFechaNoti2" runat="server" Text="">
                            </dx:ASPxLabel>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Bar - Notificacion 3
                        </div>
                        <div class="panel-body">
                            <p>
                                <dx:ASPxLabel ID="lblNotificacion3" runat="server" Text="">
                                </dx:ASPxLabel>
                            </p>
                        </div>
                        <div class="panel-footer">
                            Fecha de publicación
                            <dx:ASPxLabel ID="lblFechaNoti3" runat="server" Text="">
                            </dx:ASPxLabel>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
            </div>
        
        <!-- /.row -->
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>


