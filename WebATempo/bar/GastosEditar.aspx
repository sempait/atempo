﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GastosEditar.aspx.cs" MasterPageFile="~/bar/inicio.master"
    Inherits="bar_GastosEditar" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Agregar/Editar Gastos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Agregar/Editar Gastos</div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout ID="ASPxFormLayoutGatos" runat="server" EnableTheming="True" Theme="Aqua"
                                    Width="100%" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption" AlignItemCaptionsInAllGroups="True">
                                    <Items>
                                        <dx:LayoutGroup Caption="Datos" GroupBoxDecoration="HeadingLine">
                                            <Items>
                                                <dx:LayoutItem Caption="Fecha Gasto">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer1" runat="server"
                                                            SupportsDisabledAttribute="True">
                                                            <dx:ASPxDateEdit ID="FechaGasto" runat="server">
                                                            </dx:ASPxDateEdit>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Descripcion Gasto">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainerGasto" runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="DescripcionGasto" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Importe">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer2" runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="ImporteGasto" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Afecta a caja diaria">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer3" runat="server" 
                                                            SupportsDisabledAttribute="True">
                                                            <dx:ASPxCheckBox ID="chkAfecta" runat="server" 
                                                                CheckState="Unchecked">
                                                            </dx:ASPxCheckBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                    </Items>
                                    <Styles>
                                        <LayoutGroupBox>
                                            <Caption CssClass="layoutGroupBoxCaption">
                                            </Caption>
                                        </LayoutGroupBox>
                                    </Styles>
                                </dx:ASPxFormLayout>
                                <p style="text-align: right">
                                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                        Width="120px" onclick="btnGuardar_Click" />
                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                        Width="120px" onclick="btnCancelar_Click" />
                                </p>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
</asp:Content>