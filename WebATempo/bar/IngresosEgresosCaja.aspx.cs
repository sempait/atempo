﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IngresosEgresosCaja : System.Web.UI.Page
{
    private string Id_Caja_Bar;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["Id_Caja_Bar"] = null;
    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("AgregarEditarOperacion.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Id_Caja_Bar = gdvIngresosEgresos.GetRowValues(gdvIngresosEgresos.FocusedRowIndex, "Id_Caja_Bar").ToString();
        Session.Add("Id_Caja_Bar", Id_Caja_Bar);
        Response.Redirect("AgregarEditarOperacion.aspx");
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        Id_Caja_Bar = gdvIngresosEgresos.GetRowValues(gdvIngresosEgresos.FocusedRowIndex, "Id_Caja_Bar").ToString();

        new BusinessLayer.OperacionCajaDiariaBar().Delete(int.Parse(Id_Caja_Bar));

        gdvIngresosEgresos.DataBind();

        
    }
}