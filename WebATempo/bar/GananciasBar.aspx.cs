﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using BusinessLayer;

public partial class bar_GananciasBar : System.Web.UI.Page
{
    DSReporteGananciasBar dsReporte = new DSReporteGananciasBar();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            deFechaDesde.Value = DateTime.Today;
            deFechaHasta.Value = DateTime.Today;


            CargarReporte(Convert.ToDateTime(deFechaDesde.Value), Convert.ToDateTime(deFechaHasta.Value));
        }

    }




    private void CargarReporte(DateTime fechaDesde, DateTime fechaHasta)
    {
        DataTable tablaReporte = new GananciasBarBusiness().RecuperarGanancias(fechaDesde, fechaHasta);
        foreach (DataRow fila in tablaReporte.Rows)
        {
            DataRow filaReporte = dsReporte.DSReporte.NewRow();
            filaReporte["FechaOperacion"] = Convert.ToDateTime(fila["FechaOperacion"]).ToString("dd/MM/yyyy");
            filaReporte["DescripcionOperacion"] = fila["DescripcionOperacion"];
            filaReporte["TipoOperacion"] = fila["TipoOperacion"];
            filaReporte["Importe"] = fila["Importe"];
            dsReporte.DSReporte.Rows.Add(filaReporte);

        }

        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        DSReporteGananciasBar dsReporte1 = dsReporte;
        ReportDataSource datasource = new ReportDataSource("DSReporteGananciasBar", dsReporte1.Tables[0]);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(datasource);

    }
    protected void btnEmitirReporte_Click(object sender, EventArgs e)
    {
        CargarReporte(Convert.ToDateTime(deFechaDesde.Value), Convert.ToDateTime(deFechaHasta.Value));
    }
}