﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;
using DataLayer;
using Utilities;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnIngresar_Click(object sender, EventArgs e)
    {
        Usuarios oUsuarioActual = new UsuariosBusiness().SelectOne_ValidarUsuario(txtUsuario.Text, txtPassword.Text);

        if (oUsuarioActual != null)
        {
            if (oUsuarioActual.Rol.DescripcionRol == "Admin")
            {
                Session.Add("IDUsuario", oUsuarioActual.ID);
                Session.Add("Privilegio", true);
                Session.Add("Rol", "Admin");
                Response.Redirect("~/Escuela/Index.aspx");
            }
            else
            {
                Session.Add("IDUsuario", oUsuarioActual.ID);
                Session.Add("Rol", oUsuarioActual.Rol.DescripcionRol);
                Response.Redirect("~/" + oUsuarioActual.Rol.DescripcionRol + "/Index.aspx");
            }
        }
        else
        {
            lblMsjError.Text = "Usuario o Contraseña Incorrecta";
        }
    }
}