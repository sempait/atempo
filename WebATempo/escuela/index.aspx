﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="Index.aspx.cs" Inherits="escuela_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="fb-root">
    </div>
    <div id="page-wrapper">
        <%--<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Inicio</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div id="Div1">--%>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Panel de Notificaciones</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Escuela - Notificación 1</div>
                        <div class="panel-body">
                            <p>
                                <dx:ASPxLabel ID="lblNotificacion1" runat="server" Text="">
                                </dx:ASPxLabel>
                            </p>
                        </div>
                        <div class="panel-footer">
                            Fecha de publicación
                            <dx:ASPxLabel ID="lblFechaNoti1" runat="server" Text="">
                            </dx:ASPxLabel>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Escuela - Notificación 2
                        </div>
                        <div class="panel-body">
                            <p>
                                <dx:ASPxLabel ID="lblNotificacion2" runat="server" Text="">
                                </dx:ASPxLabel>
                            </p>
                        </div>
                        <div class="panel-footer">
                            Fecha de publicación
                            <dx:ASPxLabel ID="lblFechaNoti2" runat="server" Text="">
                            </dx:ASPxLabel>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Escuela - Notificacion 3
                        </div>
                        <div class="panel-body">
                            <p>
                                <dx:ASPxLabel ID="lblNotificacion3" runat="server" Text="">
                                </dx:ASPxLabel>
                            </p>
                        </div>
                        <div class="panel-footer">
                            Fecha de publicación
                            <dx:ASPxLabel ID="lblFechaNoti3" runat="server" Text="">
                            </dx:ASPxLabel>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-4 -->
            </div>
        </div>
        <!-- /.row -->

        

        <!-- /#wrapper -->
    <%--</div>--%>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
