﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.IO;

public partial class escuela_cuotaAlumno : System.Web.UI.Page
{
    string Html;
    //string emailToAlumno;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 
             new PagosMensualesBusiness().SelectUltimasCuotasGeneradas();
            deFechaCuotaAlumno.Value = DateTime.Now;
        }
       
        LoadGridViewAlumnos();
    }

    private void LoadGridViewAlumnos()
    {
        gdvAlumnos.DataSource = new AlumnosBusiness().SelectAllConCuotasPendientes();
        gdvAlumnos.DataBind();
    }

    private void LoadGridViewCursosDelAlumno()
    {
        if (gdvAlumnos.FocusedRowIndex > -1)
        {
            string IDAlumno = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "ID").ToString();

            gdvCursosAlumno.DataSource = new InscripcionesBusiness().SelectAllConCuotasPendientesDelAlumno(int.Parse(IDAlumno));
            gdvCursosAlumno.DataBind();
        }
    }

    private void LoadGridViewCuotasPendientes()
    {
        if (gdvCursosAlumno.FocusedRowIndex > -1)
        {
            string IDInscripcion = gdvCursosAlumno.GetRowValues(gdvCursosAlumno.FocusedRowIndex, "ID").ToString();
            gdvCuotasPendientes.DataSource = new PagosMensualesBusiness().SelectAllHorariosCursoDelAlumno(int.Parse(IDInscripcion));
            gdvCuotasPendientes.DataBind();
        }
    }

    private void LoadLabelsForm()
    {
        if (gdvCuotasPendientes.FocusedRowIndex > -1)
        {
            string IDPagoMensual = gdvCuotasPendientes.GetRowValues(gdvCuotasPendientes.FocusedRowIndex, "ID").ToString();
            string Precio = gdvCuotasPendientes.GetRowValues(gdvCuotasPendientes.FocusedRowIndex, "MontoMes").ToString();


            PagosMensuales oPagoMensual = new PagosMensualesBusiness().SelectOne(int.Parse(IDPagoMensual));

            Parametros oParametros = new ParametrosBusiness().SelectOneParaPagosMensuales(oPagoMensual.Mes, oPagoMensual.Anio);

            this.txtPrecioCurso.Text = Precio;
            this.txtDescuento.Text = "";
        }
    }

    protected void btnSeleccionarAlumno_Click(object sender, EventArgs e)
    {
        this.LoadGridViewCursosDelAlumno();
    }

    protected void btnSeleccionarInscripcion_Click(object sender, EventArgs e)
    {
        this.LoadGridViewCuotasPendientes();
    }

    protected void btnSeleccionarCuotaPendiente_Click(object sender, EventArgs e)
    {
        this.LoadLabelsForm();
    }

    protected void btnAbonarCuota_Click(object sender, EventArgs e)
    {

        if (gdvCuotasPendientes.FocusedRowIndex > -1)
        {
            int importeTotal;
            if (txtDescuento.Text != "")
                importeTotal = Convert.ToInt32(txtPrecioCurso.Text) - (((Convert.ToInt32(txtPrecioCurso.Text) * (Convert.ToInt32(txtDescuento.Text)) / 100)));
            else
                importeTotal = Convert.ToInt32(txtPrecioCurso.Text);

            string DescripcionTipoCurso;
            if (gdvCursosAlumno.GetRowValues(gdvCursosAlumno.FocusedRowIndex, "Modalidad").ToString() == "I")
                DescripcionTipoCurso = "Individial";
            else
                DescripcionTipoCurso = "Grupal";

            pcMensaje.ShowOnPageLoad = true;
            pcMensaje.HeaderText = "Datos el Pago";
            lblAlumno.Text = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "NombreYApellido").ToString();
            lblCurso.Text = string.Concat(gdvCursosAlumno.GetRowValues(gdvCursosAlumno.FocusedRowIndex, "HorarioCurso.Curso.DescripcionCurso").ToString(), " - ", DescripcionTipoCurso);

            lblMes.Text = gdvCuotasPendientes.GetRowValues(gdvCuotasPendientes.FocusedRowIndex, "MesString").ToString();
            lblImporteTotal.Text = Convert.ToString(importeTotal);
        }
    }

    protected void btnEliminarCuotaPendiente_Click(object sender, EventArgs e)
    {
        if (gdvCuotasPendientes.FocusedRowIndex > -1)
        {
            int IDCuotaAlumno = int.Parse(gdvCuotasPendientes.GetRowValues(gdvCuotasPendientes.FocusedRowIndex, "ID").ToString());
            new PagosMensualesBusiness().DeletePagoMensual(IDCuotaAlumno);
            LoadGridViewCuotasPendientes();
        }
    }
    

    private void RegistrarPagoCuotaAlumno()
    {
        string IDPagoMensual = gdvCuotasPendientes.GetRowValues(gdvCuotasPendientes.FocusedRowIndex, "ID").ToString();

        PagosMensuales oPagoMensual = new PagosMensualesBusiness().SelectOne(int.Parse(IDPagoMensual));

        DateTime FechaPagoCuota = DateTime.Parse(deFechaCuotaAlumno.Value.ToString());

        if (txtDescuento.Text != "")
            new PagosMensualesBusiness().Update(int.Parse(IDPagoMensual), ((Convert.ToInt32(txtPrecioCurso.Text) - (((Convert.ToInt32(txtPrecioCurso.Text) * (Convert.ToInt32(txtDescuento.Text)) / 100))))), FechaPagoCuota);
        else
            new PagosMensualesBusiness().Update(int.Parse(IDPagoMensual), Convert.ToInt32(txtPrecioCurso.Text), FechaPagoCuota);

        LoadGridViewAlumnos();
        gdvAlumnos.DataBind();
        gdvCuotasPendientes.DataBind();
        gdvCursosAlumno.DataBind();
    }
    protected void btnAceptarMensaje_Click(object sender, EventArgs e)
    {
        if (ckbMail.Checked == true)
        {
            cargarHTML();
            enviaEmail();
        }
        RegistrarPagoCuotaAlumno();
    }

    private void cargarHTML()
    {
        /*varNombreYApellido
         * varCurso
         * varDiaCurso
         * varHoraDesde
         * varHoraHasta
         * varProfesor
         * varTelefonoATempo
         * varMailATempo
         * 
         * varCuotaMes
         * varFechaPago
         * varImporteTotal
         * lblAlumno.Text = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "NombreYApellidoAlumno").ToString();
            lblCurso.Text = string.Concat(gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Curso.DescripcionCurso").ToString(), " - ", DescripcionTipoCurso);
            lblDia.Text = gdvHorariosCursos.GetRowValues(gdvAlumnos.FocusedRowIndex, "Dia").ToString();
            lblHoraDesde.Text = gdvHorariosCursos.GetRowValues(gdvAlumnos.FocusedRowIndex, "HoraDesde").ToString();
            lblHoraHasta.Text = gdvHorariosCursos.GetRowValues(gdvAlumnos.FocusedRowIndex, "HoraHasta").ToString();
            lblProfesor.Text = gdvHorariosCursos.GetRowValues(gdvAlumnos.FocusedRowIndex, "Profesor.NombreYApellidoProfesor").ToString();
         */
        int importeTotal;
        if (txtDescuento.Text != "")
            importeTotal = Convert.ToInt32(txtPrecioCurso.Text) - (((Convert.ToInt32(txtPrecioCurso.Text) * (Convert.ToInt32(txtDescuento.Text)) / 100)));
        else
            importeTotal = Convert.ToInt32(txtPrecioCurso.Text);

        string DescripcionTipoCurso;
        if (gdvCursosAlumno.GetRowValues(gdvCursosAlumno.FocusedRowIndex, "Modalidad").ToString() == "I")
            DescripcionTipoCurso = "Individial";
        else
            DescripcionTipoCurso = "Grupal";

        //string textHTML = File.ReadAllText(@"C:\Users\Nicolas\SempaIT\atempo\WebATempo\escuela\mailAtempo\atempoNewPagoAlumno.txt");
        string textHTML = File.ReadAllText(Server.MapPath("/atempo/escuela/mailForm/atempoNewPagoAlumno.txt"));

        textHTML = textHTML.Replace("varNombreYApellido", gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "NombreYApellido").ToString());
        textHTML = textHTML.Replace("varCurso", string.Concat((gdvCursosAlumno.GetRowValues(gdvCursosAlumno.FocusedRowIndex, "HorarioCurso.Curso.DescripcionCurso").ToString()), " - ", DescripcionTipoCurso));
        textHTML = textHTML.Replace("varDiaCurso", gdvCursosAlumno.GetRowValues(gdvCursosAlumno.FocusedRowIndex, "HorarioCurso.Dia").ToString());
        textHTML = textHTML.Replace("varHoraDesde", gdvCursosAlumno.GetRowValues(gdvCursosAlumno.FocusedRowIndex, "HorarioCurso.HoraDesde").ToString());
        textHTML = textHTML.Replace("varHoraHasta", gdvCursosAlumno.GetRowValues(gdvCursosAlumno.FocusedRowIndex, "HorarioCurso.HoraHasta").ToString());
        //textHTML = textHTML.Replace("varProfesor", gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Profesor.NombreYApellidoProfesor").ToString());
        textHTML = textHTML.Replace("varProfesor", "");
        textHTML = textHTML.Replace("Profesor:", "");
        textHTML = textHTML.Replace("varCuotaMes", gdvCuotasPendientes.GetRowValues(gdvCuotasPendientes.FocusedRowIndex, "MesString").ToString());
        textHTML = textHTML.Replace("varFechaPago", DateTime.Now.ToShortTimeString());
        textHTML = textHTML.Replace("varImporteTotal", Convert.ToString(importeTotal));
        Html = textHTML;
        //File.WriteAllText(@"C:\Users\Nicolas\SempaIT\atempo\WebATempo\escuela\mailAtempo\atempoNewPagoAlumno.txt", textHTML);
        //System.IO.StreamReader sr = new System.IO.StreamReader(@"C:\Users\Nicolas\SempaIT\atempo\WebATempo\escuela\mailAtempo\atempoNewPagoAlumno.txt", System.Text.Encoding.Default);
        //File.WriteAllText((Server.MapPath("/atempo/escuela/mailForm/atempoNewPagoAlumno.txt")), textHTML);
        //System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("/atempo/escuela/mailForm/atempoNewPagoAlumno.txt"), System.Text.Encoding.Default);
        //Html = sr.ReadToEnd();
        //sr.Close();
    }

    private void enviaEmail()
    {
        Alumnos oAlumnoActual = new Alumnos();
        oAlumnoActual = new AlumnosBusiness().SelectOne(int.Parse(gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "ID").ToString()));
        string emailToAlumno = oAlumnoActual.Email;

        if (validar())
        {
            //Busco datos para enviar email
            Parametros oParametros = new Parametros();
            oParametros = new ParametrosBusiness().Select(1);

            string direccionEmail = oParametros.Email;
            string usuarioEmail = oParametros.Usuario;
            string passEmail = oParametros.Contrsenia;
            string emailSempait = "sistemas@sempait.com.ar";

            //create the message
            MailMessage mail = new MailMessage();
            //add the email address we will be sending the message to
            //if (emailToAlumno == "")
            //    mail.To.Add(direccionEmail);
            //else
            //mail.To.Add("" + emailToAlumno + "," + direccionEmail + "");
            mail.To.Add(emailToAlumno);
            mail.Bcc.Add("" + emailSempait + "," + direccionEmail + "");
            //mail.To.Add("nico_poche@hotmail.com,nico.pochettino@gmail.com,npochettino@sempait.com.ar,npochettino@laboratorioturner.com.ar,nicolas.pochettino@yahoo.com.ar");
            //add our email here
            mail.From = new MailAddress(direccionEmail, "A Tempo Musica");
            //email's subject
            mail.Subject = "A Tempo Musica - Pago Cuota";
            //email's body, this is going to be html. note that we attach the image as using cid
            mail.Body = Html;
            //set email's body to html
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.Normal;
            //setup our smtp client, these are Gmail specific settings
            //SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            //SmtpClient client = new SmtpClient("localhost",25); 
            //client.EnableSsl = true; 
            SmtpClient smtp = new SmtpClient();
            smtp.Host = System.Configuration.ConfigurationManager.AppSettings["SMTP_SERVER"].ToString();
            //smtp.Send(mail);
            //ssl must be enabled for Gmail
            //our Gmail account credentials
            //NetworkCredential credentials = new NetworkCredential(direccionEmail, passEmail);
            //add credentials to our smtp client
            //client.Credentials = credentials;
            try
            {
                //try to send the mail message
                //client.Send(mail);
                //Response.Redirect("Gracias.aspx");
                smtp.Send(mail);
            }
            catch
            {
                //some feedback if it does not work
                //Button1.Text = "Fail";
                //Response.Redirect("Gracias.aspx");
            }
        }
        else
        {

        }
    }

    private bool validar()
    {
        bool bandera = true;
        //if (string.IsNullOrEmpty(txtemail.Text))
        //    bandera = false;
        return bandera;
    }

    protected void btnCancelarMensaje_Click(object sender, EventArgs e)
    {

    }
}