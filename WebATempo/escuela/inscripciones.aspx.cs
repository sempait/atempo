﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_inscripciones : System.Web.UI.Page
{
    string IDInscripcion;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (rdbActivos.Checked)
                gdvInscripciones.FilterExpression = "FechaFinInscripcion IS NULL";

        }


        Session["IDInscripcion"] = null;
    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("inscripcion.aspx");

    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        pcEliminar.ShowOnPageLoad = true;
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        IDInscripcion = gdvInscripciones.GetRowValues(gdvInscripciones.FocusedRowIndex, "IDInscripcion").ToString();
        string IDHorarioCurso = gdvInscripciones.GetRowValues(gdvInscripciones.FocusedRowIndex, "IDHorariosCurso").ToString();

        new InscripcionesBusiness().Update(int.Parse(IDInscripcion), int.Parse(IDHorarioCurso));

        pcEliminar.ShowOnPageLoad = false;

        gdvInscripciones.DataBind();
    }


    protected void rbActivos_CheckedChanged(object sender, EventArgs e)
    {
        gdvInscripciones.FilterExpression = "FechaFinInscripcion IS NULL";
    }



    protected void rdbActivos_CheckedChanged(object sender, EventArgs e)
    {

        if (rdbActivos.Checked)
            gdvInscripciones.FilterExpression = "FechaFinInscripcion IS NULL";


    }
    protected void rdbBajas_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbBajas.Checked)
            gdvInscripciones.FilterExpression = "FechaFinInscripcion IS NOT NULL";
    }
    protected void bEliminarInscripcion_Click(object sender, EventArgs e)
    {
        pcEliminarInscripcion.ShowOnPageLoad = true;
    }
    protected void btnAceptarEliminar_Click(object sender, EventArgs e)
    {
        IDInscripcion = gdvInscripciones.GetRowValues(gdvInscripciones.FocusedRowIndex, "IDInscripcion").ToString();
        string FechaFinInscripcion = gdvInscripciones.GetRowValues(gdvInscripciones.FocusedRowIndex, "FechaFinInscripcion").ToString();

        
        
        
        
        if (FechaFinInscripcion.Length == 0)
        {

            new InscripcionesBusiness().Delete(int.Parse(IDInscripcion));

            gdvInscripciones.DataBind();


        }
        else
            pcMensajeError.ShowOnPageLoad = true;

        pcEliminarInscripcion.ShowOnPageLoad = false;


    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        pcEliminar.ShowOnPageLoad = false;
    }
    protected void btnEditarModalidad_Click(object sender, EventArgs e)
    {
        string Modalidad = gdvInscripciones.GetRowValues(gdvInscripciones.FocusedRowIndex, "Modalidad").ToString();

        lblMensaje.Text = "Se va a modificar la modalidad: " + Modalidad;
        pcMensaje.HeaderText = "Modificar Modalidad";
        pcMensaje.ShowOnPageLoad = true;

    }
    protected void AceptarDialog_Click(object sender, EventArgs e)
    {
        pcPopAppEdicionModalidad.ShowOnPageLoad = false;
    }
    protected void btnpcMensajeAcetar_Click(object sender, EventArgs e)
    {
        string IDInscripcionSeleccionada = gdvInscripciones.GetRowValues(gdvInscripciones.FocusedRowIndex, "IDInscripcion").ToString();

        string Modalidad = gdvInscripciones.GetRowValues(gdvInscripciones.FocusedRowIndex, "Modalidad").ToString();
        string IDHorariosCurso = gdvInscripciones.GetRowValues(gdvInscripciones.FocusedRowIndex, "IDHorariosCurso").ToString();

        if (Modalidad.Equals("G"))
        {
            HorariosCursos mHorarioCursoActual = new InscripcionesBusiness().SelectHorarioCursoInscripcion(IDHorariosCurso);

            if (mHorarioCursoActual.CantidadAlumnos > 1)
            {
                pcPopAppEdicionModalidad.ShowOnPageLoad = true;
            }

            else
            {
                new InscripcionesBusiness().UpdateModalidad(IDInscripcionSeleccionada, "I");
            }
        }
        else
        {
            new InscripcionesBusiness().UpdateModalidad(IDInscripcionSeleccionada, "G");
        }



        gdvInscripciones.DataBind();

    }

    protected void ASPxButtonError_Click(object sender, EventArgs e)
    {
        pcMensajeError.ShowOnPageLoad = false;
    }
    protected void btnCancelarEliminar_Click(object sender, EventArgs e)
    {
        pcEliminarInscripcion.ShowOnPageLoad = false;
    }
}