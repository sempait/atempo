﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer;
using BusinessLayer;

public partial class escuela_AgregarDisponiblidad : System.Web.UI.Page
{
    int IdProfesor = 0;
    int IDHorarioDisponibilidad = 0;
    Profesores oProfesorActual = new Profesores();
    HorariosDisponibilidad oHorarioDisponibilidad = new HorariosDisponibilidad();

    protected void Page_Load(object sender, EventArgs e)
    {
        IdProfesor = int.Parse(Session["IDProfesor"].ToString());
        IDHorarioDisponibilidad = int.Parse(Session["IDHorarioDisponibilidad"].ToString());


        if (!IsPostBack)
        {

            try
            {
                //Cargo el form
                if (IdProfesor != 0)
                {
                    CargarDatos(IdProfesor, IDHorarioDisponibilidad);
                }
            }

            catch
            {
                IdProfesor = 0;
                IDHorarioDisponibilidad = 0;
            }
        }
    }

    private void CargarDatos(int IdProfesor, int IDHorarioDisponibilidad)
    {
        oProfesorActual = new ProfesoresBusiness().SelectOne(IdProfesor);
        txtNombreYApellidoProfesor.Text = oProfesorActual.NombreYApellidoProfesor;

        if (IDHorarioDisponibilidad != 0)
        {

            oHorarioDisponibilidad = new HorariosDisponibilidadBusiness().SelectOne(IDHorarioDisponibilidad);


          /// /get the data into the list you can set it
            CmbDia.SelectedItem.Text = oHorarioDisponibilidad.Dia;

            TmeHoraDesde.DateTime = DateTime.Parse(oHorarioDisponibilidad.HoraDesde.ToString());
            TmeHoraHasta.DateTime = DateTime.Parse(oHorarioDisponibilidad.HoraHasta.ToString());
            
            //TmeHoraDesde.Value = oHorarioDisponibilidad.HoraDesde.ToString();
            //TmeHoraHasta.Value = oHorarioDisponibilidad.HoraHasta.ToString();
        }

    }

    
    protected void btnCancelarCurso_Click(object sender, EventArgs e)
    {
        Response.Redirect("horariosDisp.aspx");
    }

    protected void btnSelecionarCurso_Click(object sender, EventArgs e)
    {
        GuardarDisponibilidad();
        Session["IDProfesor"] = null;
        Session["IDHorarioDisponibilidad"] = null;
        Response.Redirect("horariosDisp.aspx");
    }

    protected void btnGuardarAgregar_Click(object sender, EventArgs e)
    {
        GuardarDisponibilidad();
        Response.Redirect("AgregarDisponiblidad.aspx");
    }

    private void GuardarDisponibilidad()
    {
        HorariosDisponibilidad oHorarioDisponible = new HorariosDisponibilidad();
        Profesores oProfesor = new Profesores();

        if (IDHorarioDisponibilidad == 0)
        {
            oHorarioDisponible.Dia = CmbDia.SelectedValue.ToString();
            oHorarioDisponible.HoraDesde = TimeSpan.Parse(TmeHoraDesde.Text); // TimeSpan.Parse(TmeHoraDesde.Value.ToString().Split(' ')[1]);
            oHorarioDisponible.HoraHasta = TimeSpan.Parse(TmeHoraHasta.Text); //TimeSpan.Parse(TmeHoraHasta.Value.ToString().Split(' ')[1]);
            oProfesor.ID = IdProfesor;
            oHorarioDisponible.Profesor = oProfesor;
            new HorariosDisponibilidadBusiness().Insert(oHorarioDisponible);
        }

        else
        {
            oHorarioDisponible.ID = IDHorarioDisponibilidad;
            oHorarioDisponible.Dia = CmbDia.SelectedItem.ToString();
            //oHorarioDisponible.HoraDesde = TimeSpan.Parse(TmeHoraDesde.Value.ToString().Split(' ')[1]);
            //oHorarioDisponible.HoraHasta = TimeSpan.Parse(TmeHoraHasta.Value.ToString().Split(' ')[1]);
            oHorarioDisponible.HoraDesde = TimeSpan.Parse(TmeHoraDesde.Text);
            oHorarioDisponible.HoraHasta = TimeSpan.Parse(TmeHoraHasta.Text);

            oProfesor.ID = IdProfesor;
            oHorarioDisponible.Profesor = oProfesor;

            new HorariosDisponibilidadBusiness().Update(oHorarioDisponible);
        }
    }
}