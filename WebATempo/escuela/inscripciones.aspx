﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="inscripciones.aspx.cs" Inherits="escuela_inscripciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Listado de Inscripciones</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de Inscripciones
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar" type="button" class="btn btn-outline btn-primary"
                                    OnClick="btnAgregar_Click" Width="100px" />
                                <asp:Button ID="btnEditarModalidad" runat="server" Text="Editar Mod" type="button"
                                    class="btn btn-outline btn-warning" Width="100px" OnClick="btnEditarModalidad_Click" />
                                <asp:Button ID="btnDarDeBaja" runat="server" Text="Dar de Baja/Alta" type="button"
                                    class="btn btn-outline btn-danger" OnClick="btnEliminar_Click" Width="133px" />
                                &nbsp;&nbsp;&nbsp;<asp:Button ID="bEliminarInscripcion" runat="server" Text="Eliminar"
                                    type="button" class="btn btn-outline btn-danger" Width="100px" 
                                    OnClick="bEliminarInscripcion_Click" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="rdbActivos" runat="server" Checked="True" OnCheckedChanged="rdbActivos_CheckedChanged"
                                    Text="Activos" AutoPostBack="true" GroupName="Estado" />
                                &nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="rdbBajas" runat="server" OnCheckedChanged="rdbBajas_CheckedChanged"
                                    Text="Bajas" AutoPostBack="true" GroupName="Estado" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </p>
                            <dx:ASPxGridView ID="gdvInscripciones" runat="server" AutoGenerateColumns="False"
                                EnableTheming="True" KeyFieldName="IDInscripcion" Theme="Aqua" Width="100%" DataSourceID="SqlDataSourceInscripcion"
                                SettingsPager-NumericButtonCount="20">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="IDInscripcion" VisibleIndex="0" ReadOnly="True"
                                        Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Alumno" FieldName="NombreYApellidoAlumno" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Curso" FieldName="DescripcionCurso" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Modalidad" FieldName="Modalidad" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Profesor" FieldName="NombreYApellidoProfesor"
                                        VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Dia Curso" FieldName="DiaCurso" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="Fecha Inicio" FieldName="FechaInicioInscripcion"
                                        VisibleIndex="2">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Fecha fin" FieldName="FechaFinInscripcion" VisibleIndex="2">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="IdHorarioCurso" FieldName="IDHorariosCurso" Visible="False"
                                        VisibleIndex="24">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Hora Desde" FieldName="HoraDesdeCurso" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Hora Hasta" FieldName="HoraHastaCurso" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="Control" />
                                <SettingsPager PageSize="20">
                                </SettingsPager>
                                <Settings ShowFilterRow="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSourceInscripcion" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT * 
FROM Inscripciones 
INNER JOIN Alumnos ON 
Inscripciones.IDAlumno = Alumnos.IDAlumno 
INNER JOIN HorariosCurso HC ON HC.IDHorarioCurso=Inscripciones.IDHorariosCurso
INNER JOIN Profesores P ON P.IDProfesor = HC.IDProfesor

INNER JOIN Cursos C ON C.IDCurso=HC.IDCurso WHERE Alumnos.FechaBaja IS NULL ORDER BY FechaInicioInscripcion DESC">
                            </asp:SqlDataSource>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <dx:ASPxPopupControl ID="pcEliminar" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcEliminar" HeaderText="Dar de Baja/Alta Inscripción" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            ¿Desea dar de baja/Alta la Inscripción?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcEliminarInscripcion" runat="server" EnableTheming="True"
        Theme="RedWine" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ClientInstanceName="pcEliminar" HeaderText="Eliminar Inscripción"
        AllowDragging="True" PopupAnimationType="None" EnableViewState="False" Width="325px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2" width="100%">
                            ¿Desea eliminar por completo la Inscripción?
                            <br />
                            Tenga en cuenta que se eliminarán las cuotas pendientes.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptarEliminar" runat="server" Text="Aceptar" OnClick="btnAceptarEliminar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelarEliminar" runat="server" Text="Cancelar" 
                                OnClick="btnCancelarEliminar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcPopAppEdicionModalidad" runat="server" EnableTheming="True"
        Theme="RedWine" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ClientInstanceName="pcEliminar" HeaderText="Editar Modalidad Inscripción"
        AllowDragging="True" PopupAnimationType="None" EnableViewState="False" Width="325px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2" width="100%">
                            El Horario Curso Tiene mas de 1 inscripto no puede modificar su modalidad.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="AceptarDialog" runat="server" Text="Aceptar" Width="100%" OnClick="AceptarDialog_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcMensaje" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="Modificar la Modalidad" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnpcMensajeAcetar" runat="server" Text="Aceptar" OnClick="btnpcMensajeAcetar_Click">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnpcMensajeCancelar" runat="server" Text="Cancelar" OnClick="btnpcMensajeAcetar_Click">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="pcMensajeError" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="Eliminar Inscripción" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="LabelError" runat="server" Text="Para Eliminar una inscripción, ésta debe estar activa"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="ASPxButtonError" runat="server" Text="Aceptar" 
                                OnClick="ASPxButtonError_Click" Width="100%">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
