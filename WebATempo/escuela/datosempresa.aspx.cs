﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;
using DevExpress.Web.ASPxEditors;

public partial class escuela_datosempresa : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cargarDatos();
        }
    }

    private void cargarDatos()
    {
        Parametros oParametros = new Parametros();
        oParametros = new ParametrosBusiness().Select(1);

        txtEmail.Value = oParametros.Email;
        txtUsuarioEmail.Value = oParametros.Usuario;
        txtContraseñaEmail.Value = oParametros.Contrsenia;
        txtCantMaxAlumnosGrupo.Value = oParametros.CantidadMaximaAlumnos;
        teHoraAperturaEscuela.DateTime = DateTime.Parse(oParametros.HoraAperturaEscuela.ToString());
        teHoraCierreEscuela.DateTime = DateTime.Parse(oParametros.HoraCierreEscuela.ToString());
        teHoraAperturaSala.DateTime = DateTime.Parse(oParametros.HoraAperturaSala.ToString());
        teHoraCierreSala.DateTime = DateTime.Parse(oParametros.HoraCierreSala.ToString());
        teHoraAperturaSalaSabados.DateTime = DateTime.Parse(oParametros.HoraAperturaSalaSabado.ToString());
        teHoraCierreSalaSabados.DateTime = DateTime.Parse(oParametros.HoraCierreSalaSabado.ToString());
        txtDireccion.Value = oParametros.Direccion;
        txtTelefono.Value = oParametros.Telefono;
        txtCelular.Value = oParametros.Celular;
        txtPrecioHoraGrupal.Value = oParametros.PrecioHoraGrupal;
        txtPrecioHoraIndividual.Value = oParametros.PrecioHoraIndividual;
        TxtPorcentajePlus.Value = oParametros.PorcentajePlusProfesor;
        txtPrecioHoraSala.Value = oParametros.PrecioHoraSala;
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        ParametrosBusiness oParametrosBussines = new ParametrosBusiness();
        Parametros oParametrosEdit = new Parametros();

        oParametrosEdit.ID = 1;
        oParametrosEdit.Email = txtEmail.Text;
        oParametrosEdit.Usuario = txtUsuarioEmail.Text;
        oParametrosEdit.Contrsenia = txtContraseñaEmail.Text;
        oParametrosEdit.Celular = txtCelular.Text;
        oParametrosEdit.Telefono = txtTelefono.Text;
        oParametrosEdit.Direccion = txtDireccion.Text;
        oParametrosEdit.CantidadMaximaAlumnos = int.Parse(txtCantMaxAlumnosGrupo.Text);
        oParametrosEdit.HoraAperturaEscuela = TimeSpan.Parse(teHoraAperturaEscuela.Text);
        oParametrosEdit.HoraCierreEscuela = TimeSpan.Parse(teHoraCierreEscuela.Text);
        oParametrosEdit.HoraAperturaSala = TimeSpan.Parse(teHoraAperturaSala.Text);
        oParametrosEdit.HoraCierreSala = TimeSpan.Parse(teHoraCierreSala.Text);
        oParametrosEdit.HoraAperturaSalaSabado = TimeSpan.Parse(teHoraAperturaSalaSabados.Text);
        oParametrosEdit.HoraCierreSalaSabado = TimeSpan.Parse(teHoraCierreSalaSabados.Text);
        oParametrosEdit.PrecioHoraIndividual = float.Parse(txtPrecioHoraIndividual.Text);
        oParametrosEdit.PrecioHoraGrupal = float.Parse(txtPrecioHoraGrupal.Text);
        oParametrosEdit.PorcentajePlusProfesor = int.Parse(TxtPorcentajePlus.Text);
        oParametrosEdit.PrecioHoraSala = float.Parse(txtPrecioHoraSala.Text);

        oParametrosBussines.Update(oParametrosEdit);
    }
    protected void btnVaciar_Click(object sender, EventArgs e)
    {
        pcVaciar.ShowOnPageLoad = true;
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        pcVaciar.Dispose();
        txtEmail.Text = "";
        txtUsuarioEmail.Text = "";
        txtContraseñaEmail.Text = "";
    }
}