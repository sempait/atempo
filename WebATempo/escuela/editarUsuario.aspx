﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="~/escuela/editarUsuario.aspx.cs" Inherits="admin_editarUsuario" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Usuarios</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Usuarios
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <%--Form de Alta y Modificacion de Usuario--%>
                            <script type="text/javascript">
    // <![CDATA[
                                var passwordMinLength = 6;
                                function GetPasswordRating(password) {
                                    var result = 0;
                                    if (password) {
                                        result++;
                                        if (password.length >= passwordMinLength) {
                                            if (/[a-z]/.test(password))
                                                result++;
                                            if (/[A-Z]/.test(password))
                                                result++;
                                            if (/\d/.test(password))
                                                result++;
                                            if (!(/^[a-z0-9]+$/i.test(password)))
                                                result++;
                                        }
                                    }
                                    return result;
                                }
                                function OnPasswordTextBoxInit(s, e) {
                                    ApplyCurrentPasswordRating();
                                }
                                function OnPassChanged(s, e) {
                                    ApplyCurrentPasswordRating();
                                }
                                function ApplyCurrentPasswordRating() {
                                    var password = passwordTextBox.GetText();
                                    var passwordRating = GetPasswordRating(password);
                                    ApplyPasswordRating(passwordRating);
                                }
                                function ApplyPasswordRating(value) {
                                    ratingControl.SetValue(value);
                                    switch (value) {
                                        case 0:
                                            ratingLabel.SetValue("Nivel de Seguridad");
                                            break;
                                        case 1:
                                            ratingLabel.SetValue("Muy Simple");
                                            break;
                                        case 2:
                                            ratingLabel.SetValue("Insegura");
                                            break;
                                        case 3:
                                            ratingLabel.SetValue("Normal");
                                            break;
                                        case 4:
                                            ratingLabel.SetValue("Segura");
                                            break;
                                        case 5:
                                            ratingLabel.SetValue("Muy Segura");
                                            break;
                                        default:
                                            ratingLabel.SetValue("Nivel de Seguridad");
                                    }
                                }
                                function GetErrorText(editor) {
                                    if (editor === passwordTextBox) {
                                        if (ratingControl.GetValue() === 1)
                                            return "La contraseña es muy simple";
                                    } else if (editor === confirmPasswordTextBox) {
                                        if (passwordTextBox.GetText() !== confirmPasswordTextBox.GetText())
                                            return "La contraseña ingresada no coincide";
                                    }
                                    return "";
                                }
                                function OnPassValidation(s, e) {
                                    var errorText = GetErrorText(s);
                                    if (errorText) {
                                        e.isValid = false;
                                        e.errorText = errorText;
                                    }
                                }
    // ]]> 
                            </script>
                            <div class="formLayoutContainer">
                            <dx:ASPxFormLayout runat="server" RequiredMarkDisplayMode="Auto" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption"
                                AlignItemCaptionsInAllGroups="true" Theme="Aqua" Width="100%">
                                <Items>
                                    <dx:LayoutGroup Caption="Datos de Usuario" GroupBoxDecoration="HeadingLine" SettingsItemCaptions-HorizontalAlign="Right">
                                        <Items>
                                            <dx:LayoutItem Caption="Nombre y Apellido">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <table>
                                                            <tr>
                                                                <td style="padding-right: 5px;">
                                                                    <dx:ASPxTextBox ID="txtNombreYApellido" runat="server" Width="370">
                                                                        <ValidationSettings Display="Dynamic" RequiredField-IsRequired="true" ErrorDisplayMode="Text"
                                                                            SetFocusOnError="true">
                                                                            <RequiredField IsRequired="True"></RequiredField>
                                                                        </ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Rol" RequiredMarkDisplayMode="Required">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <table>
                                                            <tr>
                                                                <td style="padding-right: 5px;">
                                                                    <dx:ASPxComboBox runat="server" ID="cbRoles" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                                        TextField="DescripcionRol" ValueField="IDRol" DataSourceID="SqlDataSource1" />
                                                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                                                        SelectCommand="SELECT DescripcionRol,IDRol FROM [Roles]"></asp:SqlDataSource>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                        <SettingsItemCaptions HorizontalAlign="Right"></SettingsItemCaptions>
                                    </dx:LayoutGroup>
                                    <dx:LayoutGroup Caption="Datos de Acceso" GroupBoxDecoration="HeadingLine" SettingsItemCaptions-HorizontalAlign="Right"
                                        ColCount="2">
                                        <Items>
                                            <dx:LayoutItem Caption="Usuario" ColSpan="2">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox runat="server" ID="txtUsuario" Width="170">
                                                            <ValidationSettings ErrorDisplayMode="Text" Display="Dynamic" ErrorTextPosition="Bottom"
                                                                SetFocusOnError="true">
                                                                <%--<RegularExpression ErrorText="Invalid e-mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />--%>
                                                                <RequiredField IsRequired="True" ErrorText="The value is required" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Contraseña">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="txtContraseña" tipe="" runat="server" ClientInstanceName="passwordTextBox"
                                                            Width="170">
                                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="Text" Display="Dynamic"
                                                                SetFocusOnError="true">
                                                                <RequiredField IsRequired="True" ErrorText="Campo requerido" />
                                                            </ValidationSettings>
                                                            <ClientSideEvents Init="OnPasswordTextBoxInit" KeyUp="OnPassChanged" Validation="OnPassValidation" />
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem ShowCaption="False" RequiredMarkDisplayMode="Required">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxRatingControl ID="ratingControl" runat="server" ReadOnly="true" ItemCount="5"
                                                                        Value="0" ClientInstanceName="ratingControl" />
                                                                </td>
                                                                <td style="padding-left: 5px; width: 100px">
                                                                    <dx:ASPxLabel ID="ratingLabel" runat="server" ClientInstanceName="ratingLabel" Text="Nivel de Seguridad" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                            <dx:LayoutItem Caption="Confirmar Contraseña" ColSpan="2">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="txtConfirmarContraseña" TextMode="Password" runat="server" ClientInstanceName="confirmPasswordTextBox"
                                                            Width="170">
                                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="Text" Display="Dynamic"
                                                                SetFocusOnError="true">
                                                                <RequiredField IsRequired="True" ErrorText="Campo requerido" />
                                                            </ValidationSettings>
                                                            <ClientSideEvents Validation="OnPassValidation" />
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                        <SettingsItemCaptions HorizontalAlign="Right"></SettingsItemCaptions>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>
                            <p style="text-align: right">
                                <asp:Button ID="btnGurardar" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                    Width="150px" OnClick="btnGuardar_Click" />
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                    Width="150px" OnClick="btnCancelar_Click" />
                            </p>
                        </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
