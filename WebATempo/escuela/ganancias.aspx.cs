﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLayer;
using Microsoft.Reporting.WebForms;

public partial class escuela_ganancias : System.Web.UI.Page
{

    DSReporteGananciasEscuela dsReporte = new DSReporteGananciasEscuela();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            deFechaDesde.Value = DateTime.Today;
            deFechaHasta.Value = DateTime.Today;


            CargarReporte(Convert.ToDateTime(deFechaDesde.Value), Convert.ToDateTime(deFechaHasta.Value));
        }

    }


    protected void btnEmitirReporte_Click(object sender, EventArgs e)
    {
        CargarReporte(Convert.ToDateTime(deFechaDesde.Value), Convert.ToDateTime(deFechaHasta.Value));
    }

    private void CargarReporte(DateTime fechaDesde, DateTime fechaHasta)
    {
        DataTable tablaReporte = new GananciasBusiness().RecuperarGanancias(fechaDesde, fechaHasta);



        foreach (DataRow fila in tablaReporte.Rows)
        {
            DataRow filaReporte = dsReporte.TablaOperaciones.NewRow();

            if (fila["TipoOperacion"] != "TG" && fila["TipoOperacion"] != "TP")
            {


                filaReporte["FechaOperacion"] = Convert.ToDateTime(fila["FechaOperacion"]).ToString("dd/MM/yyyy");
                filaReporte["DescripcionOperacion"] = fila["DescripcionOperacion"];
                filaReporte["TipoOperacion"] = fila["TipoOperacion"];
                filaReporte["Importe"] = fila["Importe"];
                filaReporte["TotalOperacion"] = (from t in tablaReporte.AsEnumerable() where t["TipoOperacion"] == "TP" select Convert.ToDouble(t["Importe"])).SingleOrDefault() - (from t in tablaReporte.AsEnumerable() where t["TipoOperacion"] == "TG" select Convert.ToDouble(t["Importe"])).SingleOrDefault();

                dsReporte.TablaOperaciones.Rows.Add(filaReporte);
            }

            

        }



        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        DSReporteGananciasEscuela dsReporteGananciaEscuela = dsReporte;
        ReportDataSource datasource = new ReportDataSource("DSGanancias", dsReporteGananciaEscuela.Tables[0]);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(datasource);

    }
}