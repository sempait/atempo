﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;
using DevExpress.Web.ASPxEditors;

public partial class escuela_editarProfesor : System.Web.UI.Page
{
    int IdProfesor = 0;
    Profesores oProfesorActual = new Profesores();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack){
        
        try
        {
            IdProfesor = int.Parse(Session["IDProfesor"].ToString());

            //Cargo el form
            if (IdProfesor != 0)
                CargarDatos(IdProfesor);
        }

        catch
        {
            IdProfesor = 0;
        }

            }
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (Validar())
        {
            ProfesoresBusiness oProfesoresBussines = new ProfesoresBusiness();

            try { IdProfesor = int.Parse(Session["IDProfesor"].ToString()); }
            catch { IdProfesor = 0; }


            Profesores oProfesor = new Profesores();
            oProfesor.NombreYApellidoProfesor = txtNombreYApellido.Value.ToString();
            if (txtCiudad.Text == null)
                oProfesor.Ciudad = "";
            else
                oProfesor.Ciudad = txtCiudad.Text;
            if (txtEmail.Text == null)
                oProfesor.Email = "";
            else
                oProfesor.Email = txtEmail.Text;
            if (txtTelefono.Text == null)
                oProfesor.Telefono = "";
            else
                oProfesor.Telefono = txtTelefono.Text;
            if (txtDireccion == null)
                oProfesor.Direccion = "";
            else
               oProfesor.Direccion = txtDireccion.Text;
            //oProfesor.FechaBaja = Convert.ToDateTime(dtmFechaBaja.Value);
            if (txtDNI.Text == null)
                oProfesor.DNI = "";
            else
                oProfesor.DNI = txtDNI.Text;
            if (deFechaNacimiento.Text == "")
                oProfesor.FechaNacimiento = null;
            else
                oProfesor.FechaNacimiento = Convert.ToDateTime(deFechaNacimiento.Value);
            
        
            if (IdProfesor == 0)
            {
           
                oProfesoresBussines.ProfesoresInsert(oProfesor);
            }
            else
            {
            
                oProfesor.ID = IdProfesor;
                oProfesoresBussines.Update(oProfesor);
            }

            Response.Redirect("profesores.aspx");
        }
        lblMensaje.Text = "Debe completar los datos requeridos";
        pcMensaje.HeaderText = "Advertencia";
        pcMensaje.ShowOnPageLoad = true;
    }

    private bool Validar()
    {
        if (txtNombreYApellido.Text == "" && deFechaNacimiento.Text == "")
            return false;
        else
            return true;
    }

    protected void NameTextBox_Validation(object sender, ValidationEventArgs e)
    {
        if ((e.Value as string).Length < 2)
            e.IsValid = false;
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("profesores.aspx");
    }

    private void CargarDatos(int IdProfesor)
    {

        
        oProfesorActual = new ProfesoresBusiness().SelectOne(IdProfesor);

        txtNombreYApellido.Text = oProfesorActual.NombreYApellidoProfesor;
        txtCiudad.Text = oProfesorActual.Ciudad;
        txtEmail.Text = oProfesorActual.Email;
        txtDireccion.Text = oProfesorActual.Direccion;
        txtTelefono.Text = oProfesorActual.Telefono;
        txtDNI.Text = oProfesorActual.DNI;
        deFechaNacimiento.Value = oProfesorActual.FechaNacimiento;
        

    }
    
}