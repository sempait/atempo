﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer;
using BusinessLayer;

public partial class escuela_asistenacias : System.Web.UI.Page
{

    string NombreDia = "";
    string IDProfesor;
    //DateTime fecha;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            deFechaAsistencia.Value = DateTime.Now;
        }


    }


    protected void btnSelecionarProfesor_Click(object sender, EventArgs e)
    {



        CargarGrillaHorarioCurso();



    }

    private void CargarGrillaHorarioCurso()
    {
        //Obtengo la fecha, el id del profesor

        NombreDia = "";
        DateTime fecha = DateTime.Parse(deFechaAsistencia.Value.ToString().Split(' ')[0]);

        IDProfesor = gdvProfesores.GetRowValues(gdvProfesores.FocusedRowIndex, "IDProfesor").ToString();

        switch (fecha.DayOfWeek.ToString())
        {
            case "Monday":
                NombreDia = "LUNES";
                break;
            case "Tuesday":
                NombreDia = "MARTES";
                break;
            case "Wednesday":
                NombreDia = "MIERCOLES";
                break;
            case "Thursday":
                NombreDia = "JUEVES";
                break;
            case "Friday":
                NombreDia = "VIERNES";
                break;
            case "Saturday":
                NombreDia = "SABADO";
                break;
            case "Sunday":
                NombreDia = "DOMINGO";
                break;

        }

        List<HorariosCursos> oListHorarioCurso = new List<HorariosCursos>();


        gdvHorarioCursos.DataSource = new HorariosCursosBusiness().SelectHorariosCursoAsistencia(fecha, NombreDia, IDProfesor);
        gdvHorarioCursos.DataBind();
    }




    protected void btnRegistrarAsistencia_Click(object sender, EventArgs e)
    {
        //Obtengo el HorarioCurso seleccionado y luego realizo la insercion con la fecha seleccionada

        string IDHorarioCurso = gdvHorarioCursos.GetRowValues(gdvHorarioCursos.FocusedRowIndex, "ID").ToString();

        HorariosCursos oHorarioCurso = new HorariosCursos();

        oHorarioCurso.ID = int.Parse(IDHorarioCurso);

        AsistenciasProfesores oAsistenciasProfesores = new AsistenciasProfesores();

        oAsistenciasProfesores.FechaAsistencia = DateTime.Parse(deFechaAsistencia.Value.ToString());

        oAsistenciasProfesores.HorarioCurso = oHorarioCurso;

        new AsistenciasProfesorBusiness().Insert(oAsistenciasProfesores);

        CargarGrillaHorarioCurso();

    }
}