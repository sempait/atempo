﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="editarCurso.aspx.cs" Inherits="escuela_editarCurso" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Cursos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Cursos
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" RequiredMarkDisplayMode="Auto"
                                    Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption" AlignItemCaptionsInAllGroups="True"
                                    Theme="Aqua" Width="100%">
                                    <Items>
                                        <dx:TabbedLayoutGroup Caption="Tabbed Group" Width="100%">
                                            <Items>
                                                <dx:LayoutGroup Caption="Curso" ShowCaption="False">
                                                    <Items>
                                                        <dx:LayoutGroup Caption="Datos del Curso" ColCount="2" GroupBoxDecoration="HeadingLine">
                                                            <Items>
                                                                <dx:LayoutItem Caption="Descripcion del Curso" ColSpan="2">
                                                                    <LayoutItemNestedControlCollection>
                                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer1" runat="server"
                                                                            SupportsDisabledAttribute="True">
                                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtDescripcionCurso">
                                                                            </dx:ASPxTextBox>
                                                                        </dx:LayoutItemNestedControlContainer>
                                                                    </LayoutItemNestedControlCollection>
                                                                </dx:LayoutItem>
                                                            </Items>
                                                        </dx:LayoutGroup>
                                                    </Items>
                                                </dx:LayoutGroup>
                                                <dx:LayoutGroup Caption="Precio" ShowCaption="False">
                                                    <Items>
                                                        <dx:LayoutGroup Caption="Precio Curso" GroupBoxDecoration="HeadingLine">
                                                            <Items>
                                                                <dx:LayoutItem Caption="Precio Individual">
                                                                    <LayoutItemNestedControlCollection>
                                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer5" runat="server"
                                                                            SupportsDisabledAttribute="True">
                                                                            <dx:ASPxTextBox ID="txtPrecioIndividual" runat="server" Width="200px">
                                                                            </dx:ASPxTextBox>
                                                                        </dx:LayoutItemNestedControlContainer>
                                                                    </LayoutItemNestedControlCollection>
                                                                </dx:LayoutItem>
                                                                <dx:LayoutItem Caption="Precio Grupal">
                                                                    <LayoutItemNestedControlCollection>
                                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer4" runat="server"
                                                                            SupportsDisabledAttribute="True">
                                                                            <dx:ASPxTextBox ID="txtPrecioGrupal" runat="server" Width="200px">
                                                                            </dx:ASPxTextBox>
                                                                        </dx:LayoutItemNestedControlContainer>
                                                                    </LayoutItemNestedControlCollection>
                                                                </dx:LayoutItem>
                                                                <dx:LayoutItem Caption=" ">
                                                                    <LayoutItemNestedControlCollection>
                                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                                        </dx:LayoutItemNestedControlContainer>
                                                                    </LayoutItemNestedControlCollection>
                                                                </dx:LayoutItem>
                                                            </Items>
                                                        </dx:LayoutGroup>
                                                    </Items>
                                                </dx:LayoutGroup>
                                            </Items>
                                        </dx:TabbedLayoutGroup>
                                    </Items>
                                    <Styles>
                                        <LayoutGroupBox>
                                            <Caption CssClass="layoutGroupBoxCaption">
                                            </Caption>
                                        </LayoutGroupBox>
                                    </Styles>
                                </dx:ASPxFormLayout>
                                <p>
                                </p>
                                <p style="text-align: right">
                                    <asp:Button ID="btnGurardar" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                        Width="150px" OnClick="btnGuardar_Click" />
                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                        Width="150px" OnClick="btnCancelar_Click" />
                                </p>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
