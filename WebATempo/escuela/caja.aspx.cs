﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer;
using System.Globalization;

public partial class escuela_caja : System.Web.UI.Page
{

    //private int Id_itemDia;

    OperacionCajaDiaria oOperacionActual = new OperacionCajaDiaria();

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void btnCerrarCaja_Click(object sender, EventArgs e)
    {
        DateTime FechaOperacion = DateTime.Parse(gdvCajaDiaria.GetRowValues(gdvCajaDiaria.FocusedRowIndex, "Fecha").ToString());

        FechaOperacion = DateTime.Parse(FechaOperacion.ToShortDateString());

        new BusinessLayer.OperacionCajaDiaria().CloseCaja(FechaOperacion);



        gdvCajaDiaria.DataBind();
    }
}