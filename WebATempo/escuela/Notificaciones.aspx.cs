﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_Notificaciones : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                CargarNotificacion();
            }
            catch
            { 
            }
        }   
    }

    private void CargarNotificacion()
    {
        //Cargo el Contenido
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oN1 = new Notificaciones();
        oN1 = oNotfBusiness.SelectOne(1);

        txtNotificacion1.Value = oN1.Notificacion;
        DateTime dt1 = oN1.FechaPublicacionNotificacion;
        deNotificacion1.Date = dt1;


        Notificaciones oN2 = new Notificaciones();
        oN2 = oNotfBusiness.SelectOne(2);

        txtNotificacion2.Value = oN2.Notificacion;
        DateTime dt2 = oN2.FechaPublicacionNotificacion;
        deNotificacion2.Date = dt2;


        Notificaciones oN3 = new Notificaciones();
        oN3 = oNotfBusiness.SelectOne(3);

        txtNotificacion3.Value = oN3.Notificacion;
        DateTime dt3 = oN3.FechaPublicacionNotificacion;
        deNotificacion3.Date = dt3;

        //Sala
        Notificaciones oN4 = new Notificaciones();
        oN4 = oNotfBusiness.SelectOne(4);

        txtNotificacion4.Value = oN4.Notificacion;
        DateTime dt4 = oN4.FechaPublicacionNotificacion;
        deNotificacion4.Date = dt4;

        Notificaciones oN5 = new Notificaciones();
        oN5 = oNotfBusiness.SelectOne(5);

        txtNotificacion5.Value = oN5.Notificacion;
        DateTime dt5 = oN5.FechaPublicacionNotificacion;
        deNotificacion5.Date = dt5;

        Notificaciones oN6 = new Notificaciones();
        oN6 = oNotfBusiness.SelectOne(6);

        txtNotificacion6.Value = oN3.Notificacion;
        DateTime dt6 = oN6.FechaPublicacionNotificacion;
        deNotificacion6.Date = dt6;

        //Bar
        Notificaciones oN7 = new Notificaciones();
        oN7 = oNotfBusiness.SelectOne(7);

        txtNotificacion7.Value = oN3.Notificacion;
        DateTime dt7 = oN7.FechaPublicacionNotificacion;
        deNotificacion7.Date = dt7;

        Notificaciones oN8 = new Notificaciones();
        oN8 = oNotfBusiness.SelectOne(8);

        txtNotificacion8.Value = oN3.Notificacion; 
        DateTime dt8 = oN8.FechaPublicacionNotificacion;
        deNotificacion8.Date = dt8;

        Notificaciones oN9 = new Notificaciones();
        oN9 = oNotfBusiness.SelectOne(9);

        txtNotificacion9.Value = oN3.Notificacion;
        DateTime dt9 = oN9.FechaPublicacionNotificacion;
        deNotificacion9.Date = dt9;
    }

    /** Escuela - Notificaciones **/
    protected void btnGuardar1_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();
        
        oNotificacion.ID = 1;
        oNotificacion.Notificacion = txtNotificacion1.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion1.Value);
            //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion);
    }
    protected void btnGuardar2_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();

        oNotificacion.ID = 2;
        oNotificacion.Notificacion = txtNotificacion2.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion2.Value);
        //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion);
    }
    protected void btnGuardar3_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();

        oNotificacion.ID = 3;
        oNotificacion.Notificacion = txtNotificacion3.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion3.Value);
        //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion);        
    }

    /** Sala - Notificaciones **/
    protected void btnGuardar4_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();

        oNotificacion.ID = 4;
        oNotificacion.Notificacion = txtNotificacion4.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion4.Value);
        //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion); 
    }
    protected void btnGuardar5_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();

        oNotificacion.ID = 5;
        oNotificacion.Notificacion = txtNotificacion5.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion5.Value);
        //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion); 
    }
    protected void btnGuardar6_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();

        oNotificacion.ID = 6;
        oNotificacion.Notificacion = txtNotificacion6.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion6.Value);
        //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion); 
    }

    /** Bar - Notificaciones **/
    protected void btnGuardar7_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();

        oNotificacion.ID = 7;
        oNotificacion.Notificacion = txtNotificacion7.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion7.Value);
        //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion); 
    }
    protected void btnGuardar8_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();

        oNotificacion.ID = 8;
        oNotificacion.Notificacion = txtNotificacion8.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion8.Value);
        //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion); 
    }
    protected void btnGuardar9_Click(object sender, EventArgs e)
    {
        NotificacionesBusiness oNotfBusiness = new NotificacionesBusiness();
        Notificaciones oNotificacion = new Notificaciones();

        oNotificacion.ID = 9;
        oNotificacion.Notificacion = txtNotificacion9.Text;
        oNotificacion.FechaPublicacionNotificacion = Convert.ToDateTime(deNotificacion9.Value);

        //Convert.ToDateTime(deNotificacion1.Text);

        oNotfBusiness.Update(oNotificacion); 
    }
}