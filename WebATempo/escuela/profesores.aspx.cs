﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_profesores : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IDProfesor"] = null;
    }        

    private void Editar()
    {
        string IdProfesor = gdvProfesores.GetRowValues(gdvProfesores.FocusedRowIndex, "IDProfesor").ToString();
        Session.Add("IDProfesor", IdProfesor); 
        Response.Redirect("editarProfesor.aspx");
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        pcEliminar.ShowOnPageLoad = true;
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Editar();
    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("editarProfesor.aspx");
    }
    
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        string IdProfesor = gdvProfesores.GetRowValues(gdvProfesores.FocusedRowIndex, "IDProfesor").ToString();


        ProfesoresBusiness oAlumno = new ProfesoresBusiness();
        oAlumno.Delete(int.Parse(IdProfesor));

        Response.Redirect("profesores.aspx");
    }
    protected void btnEliminar_Click2(object sender, EventArgs e)
    {

    }
}