﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="editarProfesor.aspx.cs" Inherits="escuela_editarProfesor" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
    // <![CDATA[
        function OnNameValidation(s, e) {
            var name = e.value;
            if (name == null)
                return;
            if (name.length < 2)
                e.isValid = false;
        }
        // ]]>
    </script>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Profesor</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Profesor
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout runat="server" EnableViewState="False" EncodeHtml="False" Width="100%"
                                    Theme="Aqua" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption" AlignItemCaptionsInAllGroups="True">
                                    <Items>
                                        <dx:LayoutGroup Caption="Datos Profesor" GroupBoxDecoration="HeadingLine" SettingsItemCaptions-HorizontalAlign="Left"
                                            Width="100%" HorizontalAlign="Center">
                                            <Items>
                                                <dx:LayoutItem Caption="Nombre y Apellido">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox ID="txtNombreYApellido" runat="server" NullText="" Width="170px">
                                                                <ValidationSettings SetFocusOnError="True" ErrorText="Nombre es requerido">
                                                                    <RequiredField IsRequired="True" ErrorText="Nombre es requerido" />
                                                                </ValidationSettings>
                                                                <ClientSideEvents Validation="OnNameValidation" />
                                                                <InvalidStyle BackColor="LightPink" />
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="DNI">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtDNI">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Fecha de Nacimiento">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxDateEdit ID="deFechaNacimiento" runat="server">
                                                                <%--<ValidationSettings SetFocusOnError="True" ErrorText="Fecha de Nacimiento es requerido">
                                                                    <RequiredField IsRequired="True" ErrorText="Fecha de Nacimiento es requerido" />
                                                                </ValidationSettings>--%>
                                                            </dx:ASPxDateEdit>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Ciudad">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtCiudad">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Direccion">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtDireccion" class="editable">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Telefono">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtTelefono" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="E-mail">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtEmail" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                            <SettingsItemCaptions HorizontalAlign="Right"></SettingsItemCaptions>
                                            <SettingsItems HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </dx:LayoutGroup>
                                    </Items>
                                    <SettingsItemCaptions HorizontalAlign="Center" Location="Left" VerticalAlign="Top" />
                                    <Styles>
                                        <LayoutGroupBox>
                                            <Caption CssClass="layoutGroupBoxCaption">
                                            </Caption>
                                        </LayoutGroupBox>
                                    </Styles>
                                </dx:ASPxFormLayout>
                                <p>
                                </p>
                                <p style="text-align: right">
                                    <asp:Button ID="Button2" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                        Width="150px" OnClick="btnGuardar_Click" />
                                    <asp:Button ID="Button3" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                        Width="150px" OnClick="btnCancelar_Click" />
                                </p>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
    </div>

    <dx:ASPxPopupControl ID="pcMensaje" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="" AllowDragging="True" PopupAnimationType="None"
        EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <dx:ASPxLabel ID="lblMensaje" runat="server" Text="" Theme="Aqua">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxButton ID="btnAceptarMsj" runat="server" Text="Aceptar">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <!--HASTA ACA EL CONTENT-->
</asp:Content>
