﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer.EscuelaDeMusica;
using BusinessLayer;
using EntitiesLayer;

using System.Net.Mail;
using System.Text;
using System.Net;
using System.IO;
using System.Configuration;

public partial class escuela_inscripcion : System.Web.UI.Page
{
    string IdTipoCurso;
    string DescripcionTipoCurso;
    int IDInscripcion;
    string OutPut;
    Inscripciones oInscripcionActual = new Inscripciones();
    Inscripciones oInscripcionNew = new Inscripciones();
    string Html;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                IDInscripcion = int.Parse(Session["IDInscripcion"].ToString());

                //Cargo el form
                if (IDInscripcion != 0)
                    CargarDatos(IDInscripcion);
            }
            catch
            {
                IDInscripcion = 0;
            }
            deFechaInscripcion.Date = DateTime.Now;
        }
    }

    private void CargarDatos(int IDInscripcion)
    {
        oInscripcionActual = new InscripcionesBusiness().SelectOne(IDInscripcion);

        int rowAlumno = gdvAlumnos.FindVisibleIndexByKeyValue(oInscripcionActual.Alumno.ID);
        gdvAlumnos.FocusedRowIndex = rowAlumno;
    }
    protected void btnSelecionarCurso_Click(object sender, EventArgs e)
    {
        LoadDataCursosHorarios();
    }

    private void LoadDataCursosHorarios()
    {
        string IDCurso = gdvCursos.GetRowValues(gdvCursos.FocusedRowIndex, "IDCurso").ToString();


        if (rdbtnGrupal.Checked)
        {
            IdTipoCurso = "G";
        }
        else
        {
            IdTipoCurso = "I";

        }
        gdvHorariosCursos.DataSource = new HorariosCursosBusiness().SelectAllCursosActuales(int.Parse(IDCurso), IdTipoCurso);
        gdvHorariosCursos.DataBind();
    }

    protected void btnInscribir_Click(object sender, EventArgs e)
    {
        if (gdvHorariosCursos.FocusedRowIndex != -1 && gdvAlumnos.FocusedRowIndex != -1)
        {
            if (rdbtnGrupal.Checked)
            {
                DescripcionTipoCurso = "Grupal";
            }
            else
            {
                DescripcionTipoCurso = "Individual";
            }

            pcMensaje.ShowOnPageLoad = true;
            pcMensaje.HeaderText = "Datos de la Inscripcion";
            lblAlumno.Text = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "NombreYApellidoAlumno").ToString();
            lblCurso.Text = string.Concat(gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Curso.DescripcionCurso").ToString(), " - ", DescripcionTipoCurso);
            lblDia.Text = gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Dia").ToString();
            lblHoraDesde.Text = gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "HoraDesde").ToString();
            lblHoraHasta.Text = gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "HoraHasta").ToString();
            lblProfesor.Text = gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Profesor.NombreYApellidoProfesor").ToString();
        }
        else
        {
            if (gdvHorariosCursos.FocusedRowIndex == -1)
            {
                //Mensaje de error: "Debe seleccionarse un HorarioCurso"
            }
            if (gdvAlumnos.FocusedRowIndex == -1)
            {
                //Mensaje de error: "Debe seleccionarse un Alumno"
            }
        }
    }

    private void RegistroInscripcion()
    {
        try
        {
            IDInscripcion = int.Parse(Session["IDInscripcion"].ToString());
        }
        catch
        {
            IDInscripcion = 0;
        }

        if (rdbtnGrupal.Checked)
        {
            IdTipoCurso = "G";
        }
        else
        {
            IdTipoCurso = "I";
        }

        string IDAlumno = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "IDAlumno").ToString();
        string IDHorarioCurso = gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "ID").ToString();

        //LLAMAR AL METODO DEL DATA MAPPER QUE GENERA LA INSCRIPCION CON ESTOS DOS PARAMETROS.
        Alumnos oAlumno = new Alumnos();
        oAlumno.ID = int.Parse(IDAlumno);

        HorariosCursos oHorarioCurso = new HorariosCursos();
        oHorarioCurso.ID = int.Parse(IDHorarioCurso);

        Inscripciones oInscripcion = new Inscripciones();
        oInscripcion.Alumno = oAlumno;
        oInscripcion.HorarioCurso = oHorarioCurso;
        oInscripcion.Modalidad = IdTipoCurso;

        oInscripcion.FechaInicio = DateTime.Parse(deFechaInscripcion.Date.ToString());

        //CONFIRMOO LA INSCRIPCION 
        string OutPut = new InscripcionesBusiness().Insert(oInscripcion);
        //new PagosMensualesBusiness().InsertCuota(oInscripcion.ID);


        if (IDInscripcion != 0)
        {
            oInscripcion.ID = IDInscripcion;
          //  new InscripcionesBusiness().Update(oInscripcion);
            Response.Redirect("inscripciones.aspx");
            Session["IDInscripcion"] = null;
        }
        else
        {
            // OutPut = new InscripcionesBusiness().Insert(oInscripcion);

            if (String.IsNullOrEmpty(OutPut))
            {
                //Mensaje registro satisfactirio
            }
            else
            {
                //Mensa de error OutPut
            }
            Session["IDInscripcion"] = null;
        }
        LoadDataCursosHorarios();
    }



    protected void btnAceptarMensaje_Click(object sender, EventArgs e)
    {
        if (ckbMail.Checked == true)
        {
            cargarHTML();
            enviaEmail();
        }
        RegistroInscripcion();
    }

    private void cargarHTML()
    {
        /*varNombreYApellido
         * varCurso
         * varDiaCurso
         * varHoraDesde
         * varHoraHasta
         * varProfesor
         * varTelefonoATempo
         * varMailATempo
         * 
         * varCuotaMes
         * varFechaPago
         * varImporteTotal
         * lblAlumno.Text = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "NombreYApellidoAlumno").ToString();
            lblCurso.Text = string.Concat(gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Curso.DescripcionCurso").ToString(), " - ", DescripcionTipoCurso);
            lblDia.Text = gdvHorariosCursos.GetRowValues(gdvAlumnos.FocusedRowIndex, "Dia").ToString();
            lblHoraDesde.Text = gdvHorariosCursos.GetRowValues(gdvAlumnos.FocusedRowIndex, "HoraDesde").ToString();
            lblHoraHasta.Text = gdvHorariosCursos.GetRowValues(gdvAlumnos.FocusedRowIndex, "HoraHasta").ToString();
            lblProfesor.Text = gdvHorariosCursos.GetRowValues(gdvAlumnos.FocusedRowIndex, "Profesor.NombreYApellidoProfesor").ToString();
         */
        if (rdbtnGrupal.Checked)
        {
            DescripcionTipoCurso = "Grupal";
        }
        else
        {
            DescripcionTipoCurso = "Individual";
        }

        //string textHTML = File.ReadAllText(@"C:\Users\Nicolas\SempaIT\atempo\WebATempo\escuela\mailAtempo\atempoNewInscripcion.txt");
        string textHTML = File.ReadAllText(Server.MapPath("/atempo/escuela/mailForm/atempoNewInscripcion.txt"));
        
        textHTML = textHTML.Replace("varNombreYApellido", gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "NombreYApellidoAlumno").ToString());
        textHTML = textHTML.Replace("varCurso", string.Concat((gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Curso.DescripcionCurso").ToString())," - ",DescripcionTipoCurso));
        textHTML = textHTML.Replace("varDiaCurso", gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Dia").ToString());
        textHTML = textHTML.Replace("varHoraDesde", gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "HoraDesde").ToString());
        textHTML = textHTML.Replace("varHoraHasta", gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "HoraHasta").ToString());
        textHTML = textHTML.Replace("varProfesor", gdvHorariosCursos.GetRowValues(gdvHorariosCursos.FocusedRowIndex, "Profesor.NombreYApellidoProfesor").ToString());

        Html = textHTML;
        //File.WriteAllText(@"C:\Users\Nicolas\SempaIT\atempo\WebATempo\escuela\mailAtempo\atempoNewInscripcion.txt", textHTML);
        //System.IO.StreamReader sr = new System.IO.StreamReader(@"C:\Users\Nicolas\SempaIT\atempo\WebATempo\escuela\mailAtempo\atempoNewInscripcion.txt", System.Text.Encoding.Default);
        //File.WriteAllText((Server.MapPath("/atempo/escuela/mailForm/atempoNewInscripcion.txt")), textHTML);
        //System.IO.StreamReader sr = new System.IO.StreamReader(Server.MapPath("/atempo/escuela/mailForm/atempoNewInscripcion.txt"), System.Text.Encoding.Default);
        //Html = sr.ReadToEnd();
        //sr.Close();
    }

    private void enviaEmail()
    {
        string emailToAlumno = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "EmailAlumno").ToString();
        if (validar())
        {
            //Busco datos para enviar email
            Parametros oParametros = new Parametros();
            oParametros = new ParametrosBusiness().Select(1);

            string direccionEmail = oParametros.Email;
            string usuarioEmail = oParametros.Usuario;
            string passEmail = oParametros.Contrsenia;
            string emailSempait = "sistemas@sempait.com.ar";

            //create the message
            MailMessage mail = new MailMessage();
            //add the email address we will be sending the message to
            //mail.To.Add("" + emailToAlumno + "," + direccionEmail + "");
            mail.To.Add(emailToAlumno);
            mail.Bcc.Add(""+ emailSempait + ","+ direccionEmail +"");
            //mail.To.Add("nico_poche@hotmail.com,nico.pochettino@gmail.com,npochettino@sempait.com.ar,npochettino@laboratorioturner.com.ar,nicolas.pochettino@yahoo.com.ar");
            //add our email here
            mail.From = new MailAddress(direccionEmail,"A Tempo Musica");
            //email's subject
            mail.Subject = "A Tempo Musica - Inscripcion";
            //email's body, this is going to be html. note that we attach the image as using cid
            mail.Body = Html; 
            //set email's body to html
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.Normal;
            //setup our smtp client, these are Gmail specific settings
            //SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            //SmtpClient client = new SmtpClient("localhost",25); 
            //client.EnableSsl = true; 
            SmtpClient smtp = new SmtpClient();
            smtp.Host = System.Configuration.ConfigurationManager.AppSettings["SMTP_SERVER"].ToString();
            //smtp.Send(mail);
            //ssl must be enabled for Gmail
            //our Gmail account credentials
            //NetworkCredential credentials = new NetworkCredential(direccionEmail, passEmail);
            //add credentials to our smtp client
            //client.Credentials = credentials;
            
        

                    


            try
            {
                //try to send the mail message
                //client.Send(mail);
                //Response.Redirect("Gracias.aspx");
                smtp.Send(mail);
            }
            catch
            {
                //some feedback if it does not work
                //Button1.Text = "Fail";
                //Response.Redirect("Gracias.aspx");
            }
        }
        else
        {
 
        }
    }

    private bool validar()
    {
        bool bandera = true;
        //if (string.IsNullOrEmpty(txtemail.Text))
        //    bandera = false;
        return bandera;
    }
    
    protected void btnCancelarMensaje_Click(object sender, EventArgs e)
    {
        pcMensaje.ShowOnPageLoad = false;
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {

    }
}