﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IngresosEgresosCaja : System.Web.UI.Page
{
    private string Id_Caja;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["Id_Caja"] = null;
    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("AgregarEditarOperacion.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Id_Caja = gdvIngresosEgresos.GetRowValues(gdvIngresosEgresos.FocusedRowIndex, "Id_Caja").ToString();
        Session.Add("Id_Caja", Id_Caja);
        Response.Redirect("AgregarEditarOperacion.aspx");
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        Id_Caja = gdvIngresosEgresos.GetRowValues(gdvIngresosEgresos.FocusedRowIndex, "Id_Caja").ToString();

        new BusinessLayer.OperacionCajaDiaria().Delete(int.Parse(Id_Caja));

        gdvIngresosEgresos.DataBind();

        
    }
}