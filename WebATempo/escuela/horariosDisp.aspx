﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="horariosDisp.aspx.cs" Inherits="escuela_horariosDisp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function OnRowClick(fila) {

            gdvHorarioProfesor.PerformCallback(fila);
        }
    </script>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Disponibilidad Horaria de un profesor
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        1 - Seleccionar Profesor
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnSelecionarCurso" runat="server" Text="Seleccionar" type="button"
                                    class="btn btn-outline btn-primary" OnClick="btnSelecionarCurso_Click" />
                            </p>
                            <p>
                            </p>
                            <dx:ASPxGridView ID="gdvProfesores" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                EnableTheming="True" KeyFieldName="IDProfesor" Theme="Aqua" Width="100%" OnHtmlRowPrepared="gdvProfesores_HtmlRowPrepared">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="IDProfesor" VisibleIndex="0" ReadOnly="True"
                                        Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoProfesor" VisibleIndex="1" Caption="Nombre y Apellido Profesor">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaBajaProfesor" VisibleIndex="2" Visible="False">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="Telefono" VisibleIndex="3" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Email" Visible="False" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Direccion" Visible="False" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Ciudad" Visible="False" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" ProcessFocusedRowChangedOnServer="True"
                                    ProcessSelectionChangedOnServer="True" />
                                <Settings ShowFilterRow="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT * FROM [Profesores] WHERE FechaBajaProfesor IS NULL ORDER BY NombreYApellidoProfesor">
                            </asp:SqlDataSource>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        2 - Disponibilidad Horaria del Profesor
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar" Width="100px" class="btn btn-outline btn-primary"
                                    OnClick="btnAgregar_Click" />
                                <asp:Button ID="btnEditar" runat="server" OnClick="btnEditar_Click" class="btn btn-outline btn-warning  "
                                    Text="Editar" Width="100px" />
                                <asp:Button ID="btnEliminar" runat="server" OnClick="btnEliminar_Click" class="btn btn-outline btn-danger"
                                    Text="Eliminar" Width="100px" />
                            </p>
                            <dx:ASPxGridView ID="gdvHorarioProfesor" runat="server" AutoGenerateColumns="False"
                                EnableTheming="True" KeyFieldName="ID" Theme="Aqua" Width="100%" AutoPostBack="true"
                                ClientInstanceName="gdvHorarioProfesor" OnCustomCallback="gdvHorarioProfesor_CustomCallback">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="Dia" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="HoraDesde" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="HoraHasta" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Visible="False" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="False" />
                            </dx:ASPxGridView>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <dx:ASPxPopupControl ID="pcMensaje" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="" AllowDragging="True" PopupAnimationType="None"
        EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <dx:ASPxLabel ID="lblMensaje" runat="server" Text="" Theme="Aqua">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <dx:ASPxButton ID="ASPxButton4" runat="server" Text="Aceptar">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcEliminar" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcEliminar" HeaderText="Eliminar Disponibilidad Horaria"
        AllowDragging="True" PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            ¿La disponibilidad horaria tiene asignado cursos dentro de su rango horario, Esta
                            segura que desea eliminarla?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcEliminarSinCursoRelacionado" runat="server" EnableTheming="True"
        Theme="RedWine" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ClientInstanceName="pcEliminarSinCursoRelacionado"
        HeaderText="Eliminar Disponibilidad Horaria" AllowDragging="True" PopupAnimationType="None"
        EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            ¿Esta segura que desea eliminar la Disponibilidad Horaria?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminarSinCursoRelacionado.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Cancelar" OnClick="btnCancelar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminarSinCursoRelacionado.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
