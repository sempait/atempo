﻿<%@ Page Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="AgregarDisponiblidad.aspx.cs" Inherits="escuela_AgregarDisponiblidad" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Asiganar Disponibilidad Horaria a Profesor</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Asignar Curso a Profesor
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout ID="ASPxFormLayout2" runat="server" EnableTheming="True" Theme="Aqua"
                                    Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption" AlignItemCaptionsInAllGroups="True"
                                    Width="100%">
                                    <Items>
                                        <dx:LayoutGroup Caption="Asignar" GroupBoxDecoration="HeadingLine">
                                            <Items>
                                                <dx:LayoutItem Caption="Profesor">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer1" runat="server"
                                                            SupportsDisabledAttribute="True">
                                                            <asp:TextBox ID="txtNombreYApellidoProfesor" runat="server" Enabled="false" Width="150px"></asp:TextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Dia">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="CmbDiaass" runat="server" SupportsDisabledAttribute="True">
                                                            <asp:DropDownList ID="CmbDia" runat="server" Width="150px">
                                                                <asp:ListItem Value="LUNES">Lunes</asp:ListItem>
                                                                <asp:ListItem Value="MARTES">Martes</asp:ListItem>
                                                                <asp:ListItem Value="MERCOLES">Miercoles</asp:ListItem>
                                                                <asp:ListItem Value="JUEVES">Jueves</asp:ListItem>
                                                                <asp:ListItem Value="VIERNES">Viernes</asp:ListItem>
                                                                <asp:ListItem Value="SABADOS">Sabados</asp:ListItem>
                                                                <asp:ListItem Value="DOMINGOS">Domingo</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Horario Desde">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer3" runat="server"
                                                            SupportsDisabledAttribute="True">
                                                            <dx:ASPxTimeEdit ID="TmeHoraDesde" runat="server" Width="150px" EditFormatString="HH:mm:ss">
                                                            </dx:ASPxTimeEdit>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Horario Hasta" runat="server">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer4" runat="server"
                                                            SupportsDisabledAttribute="True">
                                                            <dx:ASPxTimeEdit ID="TmeHoraHasta" runat="server" Width="150px" EditFormatString="HH:mm:ss">
                                                            </dx:ASPxTimeEdit>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                    </Items>
                                    <SettingsItems HorizontalAlign="Left" />
                                    <SettingsItemCaptions HorizontalAlign="Left" Location="Left" />
                                    <SettingsItemHelpTexts HorizontalAlign="Left" />
                                </dx:ASPxFormLayout>
                                <p style="text-align: right">
                                    <asp:Button ID="btnSelecionarCurso" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                        OnClick="btnSelecionarCurso_Click" Width="120px" />
                                         <asp:Button ID="btnGuardarAgregar" runat="server" Text="Guardar y Agregar" type="button" class="btn btn-outline btn-primary"
                                        Width="150px" OnClick="btnGuardarAgregar_Click" />
                                    <asp:Button ID="btnCancelarCurso" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                        Width="120px" OnClick="btnCancelarCurso_Click" />
                                </p>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
</asp:Content>
