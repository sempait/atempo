﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="asignaCursos.aspx.cs" Inherits="escuela_asignaCursos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Asignar Horario Curso a Profesores</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        1 - Seleccionar Profesor
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnSeleccionar" runat="server" Text="Seleccionar" type="button" class="btn btn-outline btn-primary"
                                    OnClick="btnSeleccionar_Click" />
                            </p>
                            <p>
                            </p>
                            <dx:ASPxGridView ID="gdvProfesores" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                EnableTheming="True" KeyFieldName="IDProfesor" Theme="Aqua" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="IDProfesor" VisibleIndex="0" ReadOnly="True"
                                        Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoProfesor" VisibleIndex="1" Caption="Nombre y Apellido Profesor">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaBajaProfesor" VisibleIndex="2" Visible="False">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="Telefono" VisibleIndex="3" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Email" Visible="False" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Direccion" Visible="False" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Ciudad" Visible="False" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT * FROM [Profesores] WHERE FechaBajaProfesor IS NULL ORDER BY NombreYApellidoProfesor">
                            </asp:SqlDataSource>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        2 - Cursos del Profesor
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar" type="button" class="btn btn-outline btn-primary"
                                    OnClick="btnAgregar_Click" Width="100px" />
                                <asp:Button ID="btnEditar" runat="server" Text="Editar" type="button" class="btn btn-outline btn-warning"
                                    OnClick="btnEditar_Click" Width="100px" />
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" type="button" class="btn btn-outline btn-danger"
                                    OnClick="btnEliminar_Click" Width="100px" />
                            </p>
                            <dx:ASPxGridView ID="gdvCursosProfesor" runat="server" AutoGenerateColumns="False"
                                EnableTheming="True" KeyFieldName="ID" Theme="Aqua" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="CantidadAlumnos" VisibleIndex="0" Caption="Cantidad"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="IdHorarioCurso" VisibleIndex="1" Caption="IdHorarioCurso"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Dia" VisibleIndex="2" Caption="Dia">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="HoraDesde" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="HoraHasta" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Curso" FieldName="Curso.DescripcionCurso" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="IDCurso" FieldName="Curso.ID" VisibleIndex="6" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="False" />
                            </dx:ASPxGridView>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <dx:ASPxPopupControl ID="pcEliminarSinInscripcion" runat="server" EnableTheming="True"
        Theme="RedWine" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ClientInstanceName="pcEliminarSinInscripcion" HeaderText="Eliminar Curso Profesor"
        AllowDragging="True" PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            ¿Desea eliminar la asignacion del curso al profesor?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminarSinInscripcion.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminarSinInscripcion.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcEliminarConInscripcionRelacionado" runat="server" EnableTheming="True"
        Theme="RedWine" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" ClientInstanceName="pcEliminarConInscripcionRelacionado" HeaderText="Eliminar Disponibilidad Horaria"
        AllowDragging="True" PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            ¿El Horario Curso tiene alumnos inscriptos, está seguro que desea eliminarlo?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminarConInscripcionRelacionado.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Cancelar" OnClick="btnCancelar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminarConInscripcionRelacionado.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="pcMensaje" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="" AllowDragging="True" PopupAnimationType="None"
        EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <dx:ASPxLabel ID="lblMensaje" runat="server" Text="" Theme="Aqua">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxButton ID="btnAceptarMsj" runat="server" Text="Aceptar">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
