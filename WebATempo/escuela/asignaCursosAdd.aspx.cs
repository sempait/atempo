﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_asignaCursosAdd : System.Web.UI.Page
{
    private int IdProfesor;
    private int IDHorarioCurso;
    private string DescripcionCurso;

    private int IDCurso;
    Profesores oProfesorActual = new Profesores();

    protected void Page_Load(object sender, EventArgs e)
    {

        IdProfesor = int.Parse(Session["IDProfesor"].ToString());

        if (Session["IDHorarioCurso"] == null)
        {
            IDHorarioCurso = 0;
        }
        else
        {
            IDHorarioCurso = int.Parse(Session["IDHorarioCurso"].ToString());

            if (Session["DescripcionCurso"] == null)
                DescripcionCurso = "";
            else
                DescripcionCurso = Session["DescripcionCurso"].ToString();


            if (Session["IDCurso"] == null)
                IDCurso = 0;
            IDCurso = int.Parse(Session["IDCurso"].ToString());
        }
        if (!IsPostBack)
        {
            CargarDatos();
        }
    }

    private void CargarDatos()
    {
        ProfesoresBusiness oPB = new ProfesoresBusiness();
        oProfesorActual = oPB.SelectOne(IdProfesor);

        txtNombreYApellidoProfesor.Value = oProfesorActual.NombreYApellidoProfesor;

        cbDescripcionCurso.TextField = "DescripcionCurso";
        cbDescripcionCurso.ValueField = "ID";
        cbDescripcionCurso.DataSource = new CursosBusiness().SelectAllCursosNoAsignados(oProfesorActual.ID);
        cbDescripcionCurso.DataBind();
        //para la edicion
        if (DescripcionCurso != null)
        {
            HorariosCursos oHorariosCursos = new HorariosCursos();
            HorariosCursosBusiness oHorariosCursosBusiness = new HorariosCursosBusiness();
            oHorariosCursos = oHorariosCursosBusiness.SelectOne(IDHorarioCurso);
            TmeHoraDesde.DateTime = DateTime.Parse(oHorariosCursos.HoraDesde.ToString());
            TmeHoraDesdeCurso.DateTime = DateTime.Parse(oHorariosCursos.HoraHasta.ToString());
            cbDiaCurso.Value = oHorariosCursos.Dia;
            cbDescripcionCurso.Value = DescripcionCurso;//oHorariosCursos.Curso.DescripcionCurso.ToString();
        } 
        cargarDisponibilidadHoraria();
    }

    private void cargarDisponibilidadHoraria()
    {
        lblNombreYApellidoProfesor.Text = oProfesorActual.NombreYApellidoProfesor;
        gdvHorarioProfesor.DataSource = new HorariosDisponibilidadBusiness().SelectAllDisponibilidadProfesor(IdProfesor);
        gdvHorarioProfesor.DataBind();
    }



    public void GuardarHorarioCurso()
    {
        if (IDHorarioCurso == 0)
        {
            HorariosCursos oHorariosCursos = new HorariosCursos();
            Cursos oCurso = new Cursos();
            oCurso.ID = Convert.ToInt32(cbDescripcionCurso.Value.ToString());

            oHorariosCursos.Curso = oCurso;

            oProfesorActual.ID = IdProfesor;
            oHorariosCursos.Profesor = oProfesorActual;

            oHorariosCursos.Dia = cbDiaCurso.SelectedItem.Text;

            oHorariosCursos.HoraDesde = TimeSpan.Parse(TmeHoraDesde.Text);
            oHorariosCursos.HoraHasta = TimeSpan.Parse(TmeHoraDesdeCurso.Text);

            oHorariosCursos.Individual = false;

            HorariosCursosBusiness oHorariosCursosBusiness = new HorariosCursosBusiness();

            oHorariosCursos.Individual = false;
            oHorariosCursosBusiness.Insert(oHorariosCursos);
        }
        else
        {
            HorariosCursos oHorariosCursos = new HorariosCursos();
            oHorariosCursos.Individual = false;
            oHorariosCursos.ID = IDHorarioCurso;

            oHorariosCursos.Dia = cbDiaCurso.SelectedItem.Text;

            oHorariosCursos.HoraDesde = TimeSpan.Parse(TmeHoraDesde.Text);
            oHorariosCursos.HoraHasta = TimeSpan.Parse(TmeHoraDesdeCurso.Text);

            Cursos oCurso = new Cursos();
            if (cbDescripcionCurso.Value != null)
                oCurso.ID = IDCurso;
            else
                oCurso.ID = Convert.ToInt32(cbDescripcionCurso.Value.ToString());

            oHorariosCursos.Curso = oCurso;

            oProfesorActual.ID = IdProfesor;
            oHorariosCursos.Profesor = oProfesorActual;

            HorariosCursosBusiness oHorariosCursosBusiness = new HorariosCursosBusiness();
            oHorariosCursosBusiness.Update(oHorariosCursos);
        }

    }


    protected void btnSelecionar_Click(object sender, EventArgs e)
    {
        GuardarHorarioCurso();
        Session["IDProfesor"] = null;
        Session["IdCurso"] = null;
        Session["DescripcionCurso"] = null;
        Session["DiaHorarioCurso"] = null;
        Session["IDHorarioCurso"] = null;
        Response.Redirect("asignaCursos.aspx");
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("asignaCursos.aspx");
    }
    protected void btnCancelar_Click1(object sender, EventArgs e)
    {
        Response.Redirect("asignaCursos.aspx");
    }
    protected void btnGuardarAgregar_Click(object sender, EventArgs e)
    {
        GuardarHorarioCurso();
        Response.Redirect("asignaCursosAdd.aspx");
    }
}