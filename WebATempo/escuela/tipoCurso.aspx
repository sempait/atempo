﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="tipoCurso.aspx.cs" Inherits="escuela_tipoCurso" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        function ShowPopUp() {
            pcEliminar.Show();
        }
             
          
    </script>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Tipo Curso</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de Tipo Curso
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="table-responsive">
                                <p>
                                    <asp:Button ID="btnAgregar" runat="server" Text="Agregar" type="button" class="btn btn-outline btn-primary"
                                        OnClick="btnAgregar_Click" Width="100px" />
                                    <asp:Button ID="btnEditar" runat="server" Text="Editar" type="button" class="btn btn-outline btn-warning"
                                        OnClick="btnEditar_Click" Width="100px" />
                                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" type="button" class="btn btn-outline btn-danger"
                                        OnClick="btnEliminar_Click" Width="100px" />
                                </p>
                                <p>
                                </p>
                                <dx:ASPxGridView ID="gdvCursos" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                    EnableTheming="True" KeyFieldName="IDTipoCurso" Theme="Aqua" Width="100%">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="IDTipoCurso" ReadOnly="True" VisibleIndex="0"
                                            Visible="False">
                                            <EditFormSettings Visible="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DescripcionTipoCurso" VisibleIndex="1" Caption="Descripcion Tipo Curso">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior AllowFocusedRow="True" />
                                    <SettingsPager Visible="False">
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" />
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                    SelectCommand="TiposCurso_SelectAll" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
    </div>
    <dx:ASPxPopupControl ID="pcEliminar" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcEliminar" HeaderText="Eliminar Tipo Curso" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            ¿Desea eliminar el Tipo de Curso?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
