﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer;
using System.Globalization;

public partial class escuela_AgregarEditarOperacion : System.Web.UI.Page
{

    private int Id_Caja;

    OperacionCajaDiaria oOperacionActual = new OperacionCajaDiaria();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            try
            {
                Id_Caja = int.Parse(Session["Id_Caja"].ToString());

                //Cargo el form
                if (Id_Caja != 0)
                    CargarDatos(Id_Caja);
            }

            catch
            {
                Id_Caja = 0;
                FechaOperacion.Value = DateTime.Now;
            }

        }
    }

    private void CargarDatos(int IdOperacion)
    {

        oOperacionActual = new BusinessLayer.OperacionCajaDiaria().Select_One(IdOperacion);

        FechaOperacion.Value = oOperacionActual.FechaOperacion;
        DescripcionOperacion.Value = oOperacionActual.DescripcionOperacion;

        if (oOperacionActual.Ingreso != 0)
        {

            TipoOperacion.Text = "Ingreso";
            ImporteOperacion.Value = oOperacionActual.Ingreso;
        }
        else
        {
            TipoOperacion.Text = "Egreso";
            ImporteOperacion.Value = oOperacionActual.Egreso;
        }



    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        GuardarDatos();
    }

    private void GuardarDatos()
    {
        try
        {
            Id_Caja = int.Parse(Session["Id_Caja"].ToString());

        }

        catch
        {
            Id_Caja = 0;
        }


        OperacionCajaDiaria mOperacion = new OperacionCajaDiaria();

        mOperacion.DescripcionOperacion = DescripcionOperacion.Text;
        mOperacion.FechaOperacion = DateTime.Parse(FechaOperacion.Value.ToString());

        mOperacion.FechaOperacion = DateTime.Parse(mOperacion.FechaOperacion.Value.ToShortDateString());

        if (TipoOperacion.Text == "Ingreso")
        {
            mOperacion.Egreso = 0;
            mOperacion.Ingreso = float.Parse(Math.Round(float.Parse(ImporteOperacion.Text.ToString()), 2).ToString());

        }
        else
        {
            mOperacion.Ingreso = 0;
            mOperacion.Egreso = float.Parse(Math.Round(float.Parse(ImporteOperacion.Text.ToString()), 2).ToString());
        }


        if (Id_Caja == 0)
        {
            new BusinessLayer.OperacionCajaDiaria().OperacionInsert(mOperacion);
        }
        else
        {

            mOperacion.ID = Id_Caja;
            new BusinessLayer.OperacionCajaDiaria().Update(mOperacion);
        }



        Response.Redirect("IngresosEgresosCaja.aspx");

    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("IngresosEgresosCaja.aspx");
    }

}