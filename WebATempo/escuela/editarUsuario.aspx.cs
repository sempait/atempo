﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using BusinessLayer;
using EntitiesLayer;
using Utilities;

public partial class admin_editarUsuario : System.Web.UI.Page
{
    private int IDUsuarioABM;
    Usuarios oUsuarioActual = new Usuarios();

    protected void Page_Load(object sender, EventArgs e)
    {      
        if (!IsPostBack)
        {
            try
            {
                IDUsuarioABM = int.Parse(Session["IDUsuarioABM"].ToString());

                //Cargo el form
                if (IDUsuarioABM != 0)
                    CargarDatos(IDUsuarioABM);
            }
            catch
            {
                IDUsuarioABM = 0;
            }
        }        
    }

    private void LoadRoles()
    {
        cbRoles.ValueField = "ID";
        cbRoles.DisplayFormatString = "DescripcionRol";
        cbRoles.DataSource = new RolesBusiness().SelectAll();
        cbRoles.DataBind();
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            IDUsuarioABM = int.Parse(Session["IDUsuarioABM"].ToString());
        }

        catch
        {
            IDUsuarioABM = 0;
        }

        UsuariosBusiness oUsuarioBussines = new UsuariosBusiness();
        Usuarios oUsuario = new Usuarios();

        oUsuario.NombreYApellido = txtNombreYApellido.Text;
        oUsuario.Usuario = txtUsuario.Text;
        oUsuario.Password = txtContraseña.Text;

        //Roles oRol = new Roles();
        //oRol.ID = cbRoles.SelectedIndex;
        //oUsuario.Rol = oRol;

        Roles oRol = new Roles(); ;
        oRol.ID = int.Parse(cbRoles.Value.ToString());
        //oRol.ID = int.Parse(cbRoles.SelectedIndex.ToString()) + 1;
        oUsuario.Rol = oRol;    

        if (IDUsuarioABM == 0)
        {
            oUsuarioBussines.Insert(oUsuario);
        }
        else
        {
            oUsuario.ID = IDUsuarioABM;
            oUsuarioBussines.Update(oUsuario);
        }

        Session["IDUsuarioABM"] = null;
        Response.Redirect("usuarios.aspx");
    }

    private Usuarios LoadEntity()
    {
        Usuarios oUsuario = new Usuarios();

        oUsuario.ID = IDUsuarioABM;
        oUsuario.NombreYApellido = txtNombreYApellido.Text;
        oUsuario.Usuario = Encode.GetMD5(txtUsuario.Text);
        oUsuario.Password = Encode.GetMD5(txtContraseña.Text);

        Roles oRol = new Roles();
        oRol.ID = int.Parse(cbRoles.SelectedItem.Value.ToString());

        oUsuario.Rol = oRol;

        return oUsuario;
    }

    protected void cbRoles_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void CargarDatos(int IDUsuarioABM)
    {
        oUsuarioActual = new UsuariosBusiness().SelectOne(IDUsuarioABM);
        
        txtUsuario.Value = oUsuarioActual.Usuario;
        txtContraseña.Value = oUsuarioActual.Password;
        txtNombreYApellido.Value = oUsuarioActual.NombreYApellido;
        cbRoles.Value = oUsuarioActual.Rol.DescripcionRol;        
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("usuarios.aspx");
    }
}