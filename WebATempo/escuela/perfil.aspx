﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="perfil.aspx.cs" Inherits="escuela_perfil" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Perfil de Usuario</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Datos Usuario
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" EnableTheming="True" Theme="Aqua"
                                    Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption" AlignItemCaptionsInAllGroups="True"
                                    Width="100%">
                                    <Items>
                                        <dx:LayoutGroup Caption="Datos del Usuario" ColCount="2" GroupBoxDecoration="HeadingLine">
                                            <Items>
                                                <dx:LayoutItem Caption="Nombre y Apellido" ColSpan="2">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtNombreYApellido" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Usuario" ColSpan="2">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtUsuario" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Contraseña" ColSpan="2">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtContraseña" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                    </Items>
                                    <Styles>
                                        <LayoutGroupBox>
                                            <Caption CssClass="layoutGroupBoxCaption">
                                            </Caption>
                                        </LayoutGroupBox>
                                    </Styles>
                                </dx:ASPxFormLayout>
                            </div>
                            <p style="text-align: right">
                                <asp:Button ID="btnAceptar" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                    Width="150px" OnClick="btnAceptar_Click" />
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                    Width="150px" OnClick="btnCancelar_Click" />
                            </p>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
