﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;
using System.Data;

public partial class escuela_asignaCursos : System.Web.UI.Page
{

    string IDHorarioCurso;
    int CantidadAlumnos;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IDProfesor"] = null;
        Session["IDCurso"] = null;
        Session["IDHorarioCurso"] = null;
        Session["DescripcionCurso"] = null;
        Session["Dia"] = null;
        Session["IDCurso"] = null;

        if (!IsPostBack)
        {

        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        string IDProfesor = gdvProfesores.GetRowValues(gdvProfesores.FocusedRowIndex, "IDProfesor").ToString();
        Session.Add("IDProfesor", IDProfesor);
        Session.Add("IDCurso", 0);

        Response.Redirect("asignaCursosAdd.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {

        if (gdvCursosProfesor.FocusedRowIndex == -1)
        {
            pcMensaje.HeaderText = "Mensaje";
            lblMensaje.Text = "Debe seleccionar un Horario Curso del Profesor";
            pcMensaje.ShowOnPageLoad = true; 
        }
        else
        {Editar(); }       
    }

    private void Editar()
    {
        
        string IDProfesor = gdvProfesores.GetRowValues(gdvProfesores.FocusedRowIndex, "IDProfesor").ToString();
        Session.Add("IDProfesor", IDProfesor);

        string IDHorarioCurso = gdvCursosProfesor.GetRowValues(gdvCursosProfesor.FocusedRowIndex, "ID").ToString();
        Session.Add("IDHorarioCurso", IDHorarioCurso);

        string DescripcionCurso = gdvCursosProfesor.GetRowValues(gdvCursosProfesor.FocusedRowIndex, "Curso.DescripcionCurso").ToString();
        Session.Add("DescripcionCurso", DescripcionCurso);

        string Dia = gdvCursosProfesor.GetRowValues(gdvCursosProfesor.FocusedRowIndex, "Dia").ToString();
        Session.Add("DiaHorarioCurso", DescripcionCurso);

        string IDCurso = gdvCursosProfesor.GetRowValues(gdvCursosProfesor.FocusedRowIndex, "Curso.ID").ToString();
        Session.Add("IDCurso", IDCurso);

        Response.Redirect("asignaCursosAdd.aspx");
    }

    //BUSCO LOS CURSOS QUE TIENE ASIGNADO EL PROFESOR Y LOS CARGO EN LA GRILLA  
    protected void btnSeleccionar_Click(object sender, EventArgs e)
    {
        cargarHorarioCursosProfesores();
    }

    private void cargarHorarioCursosProfesores()
    {
        string IDProfesor = gdvProfesores.GetRowValues(gdvProfesores.FocusedRowIndex, "IDProfesor").ToString();

        gdvCursosProfesor.DataSource = new HorariosCursosBusiness().SelectAllProfesor(int.Parse(IDProfesor));
        gdvCursosProfesor.DataBind();
    }

    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        try
        {
            IDHorarioCurso = gdvCursosProfesor.GetRowValues(gdvCursosProfesor.FocusedRowIndex, "ID").ToString();
            new HorariosCursosBusiness().Delete(IDHorarioCurso);

            cargarHorarioCursosProfesores();
           
        }
        catch (Exception ex)
        {
            pcMensaje.ShowOnPageLoad = true;
            lblMensaje.Text = "El Horario Curso tiene inscripciones asignadas. Elimine las inscripciones para poder eliminar el horario seleccionado.";
            btnAceptar.Visible = false;
            btnCancelar.Visible = false;

        }
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        if (gdvCursosProfesor.FocusedRowIndex == -1)
        {
            pcMensaje.HeaderText = "Mensaje";
            lblMensaje.Text = "Debe seleccionar un Horario Curso del Profesor";
            pcMensaje.ShowOnPageLoad = true;
        }
        else
        {   
            IDHorarioCurso = gdvCursosProfesor.GetRowValues(gdvCursosProfesor.FocusedRowIndex, "ID").ToString();
            CantidadAlumnos = int.Parse(gdvCursosProfesor.GetRowValues(gdvCursosProfesor.FocusedRowIndex, "CantidadAlumnos").ToString());


            if (CantidadAlumnos != 0)
            {
                pcEliminarConInscripcionRelacionado.ShowOnPageLoad = true;
            }
            else
            {
                pcEliminarSinInscripcion.ShowOnPageLoad = true;
            }
        }   
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        //pcEliminarConInscripcionRelacionado.ShowOnPageLoad = false;
        //pcEliminarSinInscripcion.ShowOnPageLoad = false;
    }
   
}