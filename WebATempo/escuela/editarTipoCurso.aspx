﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="editarTipoCurso.aspx.cs" Inherits="escuela_editarTipoCurso" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Tipo Curso</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tipo Curso
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <dx:ASPxFormLayout runat="server" RequiredMarkDisplayMode="Auto" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption"
                                AlignItemCaptionsInAllGroups="True" Width="100%">
                                <Items>
                                    <dx:LayoutGroup Caption="Datos del Tipo de Curso" GroupBoxDecoration="HeadingLine"
                                        SettingsItemCaptions-HorizontalAlign="Right">
                                        <Items>
                                            <dx:LayoutItem Caption="Descripcion">
                                                <LayoutItemNestedControlCollection>
                                                    <dx:LayoutItemNestedControlContainer>
                                                        <dx:ASPxTextBox ID="txtNombreYApellido" runat="server" NullText="" Width="370px">
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" SetFocusOnError="True">
                                                                <RequiredField IsRequired="True" />
                                                                <RequiredField IsRequired="True"></RequiredField>
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </dx:LayoutItemNestedControlContainer>
                                                </LayoutItemNestedControlCollection>
                                            </dx:LayoutItem>
                                        </Items>
                                        <SettingsItemCaptions HorizontalAlign="Right"></SettingsItemCaptions>
                                    </dx:LayoutGroup>
                                </Items>
                            </dx:ASPxFormLayout>
                            <p>
                            </p>
                            <p style="text-align: right">
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                    Width="150px" OnClick="btnGuardar_Click" />
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                    Width="150px" OnClick="btnCancelar_Click" />
                            </p>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
