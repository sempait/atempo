﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="cuotaAlumno.aspx.cs" Inherits="escuela_cuotaAlumno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Pago Cuota Alumno</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="alert alert-success">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" AssociatedControlID="dateEdit" Text="Fecha Pago de Cuota:" />
            <dx:ASPxDateEdit ID="deFechaCuotaAlumno" runat="server" EditFormat="Custom" Width="200"
                Theme="Aqua">
                <TimeSectionProperties>
                    <TimeEditProperties EditFormatString="hh:mm tt" />
                </TimeSectionProperties>
            </dx:ASPxDateEdit>
        </div>
        <p>
        </p>
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        1 - Seleccione el Alumno
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-area-chart">
                            <dx:ASPxGridView ID="gdvAlumnos" runat="server" AutoGenerateColumns="False" EnableTheming="True"
                                KeyFieldName="ID" Theme="Aqua" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" VisibleIndex="0" Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellido" VisibleIndex="1" Caption="Alumno">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Email" VisibleIndex="2" Caption="Email">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Telefono" VisibleIndex="3" Caption="Tel. Alumno" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoPadre" VisibleIndex="5" Caption="Nom. Padre"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoMadre" VisibleIndex="6" Caption="Nom. Madre"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TrabajoPadre" VisibleIndex="7" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TrabajoMadre" VisibleIndex="8" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoTrabajoPadre" VisibleIndex="9" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoTrabajoMadre" VisibleIndex="10" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Comentario" VisibleIndex="11" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoPadre" VisibleIndex="12" Caption="Tel. Padre"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoMadre" VisibleIndex="13" Caption="Tel. Madre"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DomicilioAlumno" VisibleIndex="4" Caption="Domicilio"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Tutor" VisibleIndex="14" Caption="Tutor" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoTutor" VisibleIndex="15" Caption="Tel. Tutor"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                            </dx:ASPxGridView>
                        </div>
                        <p>
                        </p>
                        <p style="text-align: right">
                            <asp:Button ID="btnSeleccionarAlumno" runat="server" Text="Seleccionar" type="button"
                                class="btn btn-outline btn-primary" OnClick="btnSeleccionarAlumno_Click" />
                        </p>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        2 - Seleccione el Curso al que esta inscripto
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-bar-chart">
                            <dx:ASPxGridView ID="gdvCursosAlumno" runat="server" AutoGenerateColumns="False"
                                EnableTheming="True" KeyFieldName="ID" Theme="Aqua" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Visible="False" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Día" FieldName="HorarioCurso.Dia" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Hora Desde" FieldName="HorarioCurso.HoraDesde"
                                        VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Hora Hasta" FieldName="HorarioCurso.HoraHasta"
                                        VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="DescripcionCurso" FieldName="HorarioCurso.Curso.DescripcionCurso"
                                        VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Modalidad" FieldName="Modalidad" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                            </dx:ASPxGridView>
                        </div>
                        <p>
                        </p>
                        <p style="text-align: right">
                            <asp:Button ID="btnSeleccionarInscripcion" runat="server" Text="Seleccionar" type="button"
                                class="btn btn-outline btn-primary" OnClick="btnSeleccionarInscripcion_Click" />
                        </p>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        3 - Seleccione la cuota pendiente a abonar
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        
                        <dx:ASPxGridView ID="gdvCuotasPendientes" runat="server" AutoGenerateColumns="False"
                            EnableTheming="True" KeyFieldName="ID" Theme="Aqua" Width="100%">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Visible="False" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Mes" FieldName="MesString" VisibleIndex="0">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Año" FieldName="Anio" VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Precio" FieldName="MontoMes" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" />
                            <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                        </dx:ASPxGridView>
                        <p>
                        </p>
                        <p style="text-align: right">
                            <asp:Button ID="btnEliminarCuotaPendiente" runat="server" Text="Eliminar Cuota" type="button"
                                class="btn btn-outline btn-danger" OnClick="btnEliminarCuotaPendiente_Click" />
                        
                            <asp:Button ID="btnSeleccionarCuotaPendiente" runat="server" Text="Seleccionar" type="button"
                                class="btn btn-outline btn-primary" OnClick="btnSeleccionarCuotaPendiente_Click" />
                        </p>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    4 - Realizar Pago
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <p style="text-align: center">
                        <asp:Label ID="Label1" runat="server" Text="Label">Precio Curso: </asp:Label>
                        <asp:TextBox ID="txtPrecioCurso" runat="server" Width="60px" Enabled="False"></asp:TextBox>
                    </p>
                    <p style="text-align: center">
                        <asp:Label ID="Label3" runat="server" Text="Label">% Descuento: </asp:Label>
                        <asp:TextBox ID="txtDescuento" runat="server" Width="60px"></asp:TextBox>
                    </p>
                    <p style="text-align: right">
                        <asp:Button ID="btnAbonarCuota" runat="server" Text="Abonar Cuota" type="button"
                            class="btn btn-outline btn-primary" OnClick="btnAbonarCuota_Click" />
                    </p>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
    <!-- /#wrapper -->
   
    <!--HASTA ACA EL CONTENT-->
    <dx:ASPxPopupControl ID="pcMensaje" runat="server" EnableTheming="True" Theme="Metropolis"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="" AllowDragging="True" PopupAnimationType="None"
        EnableViewState="False" Width="200px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <fieldset>
                    <div class="form-group">
                        <label>
                            Alumno
                        </label>
                        <asp:Label ID="lblAlumno" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="form-group">
                        <label>
                            Curso
                        </label>
                        <asp:Label ID="lblCurso" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="form-group">
                        <label>
                            Cuota del Mes
                        </label>
                        <asp:Label ID="lblMes" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="form-group">
                        <label>
                            Importe Total
                        </label>
                        <asp:Label ID="lblImporteTotal" runat="server" Text=""></asp:Label>
                    </div>                    
                    <div class="checkbox">
                        <label>
                            <dx:ASPxCheckBox ID="ckbMail" runat="server" Text="Envio de Email">
                            </dx:ASPxCheckBox>
                        </label>
                    </div>
                    <table>
                        <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptarMensaje" runat="server" Text="Aceptar" 
                                OnClick="btnAceptarMensaje_Click" Theme="Metropolis">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton></td>
                            <td>
                            <dx:ASPxButton ID="btnCancelarMensaje" runat="server" Text="Cancelar" 
                                    OnClick="btnCancelarMensaje_Click" Theme="Metropolis">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton></td>
                        </tr>
                    </table>
                </fieldset>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
