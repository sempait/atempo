﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="Notificaciones.aspx.cs" Inherits="escuela_Notificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Edicion del Panel de Notificaciones</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Notificaciones Escuela
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion3" href="#collapseSeven">Notificacion
                                                1</a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion1" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion1" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar1" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar1_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion3" href="#collapseEight">Notificacion
                                                2</a>
                                        </h4>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion2" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion2" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar2" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar2_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion3" href="#collapseNine">Notificacion
                                                3</a>
                                        </h4>
                                    </div>
                                    <div id="collapseNine" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion3" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion3" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar3" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar3_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Notificaciones Sala
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion2">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">Notificacion
                                                1</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion4" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion4" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar4" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar4_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">Notificacion
                                                2</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion5" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion5" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar5" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar5_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">Notificacion
                                                3</a>
                                        </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion6" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion6" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar6" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar6_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Notificaciones Bar
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Notificacion
                                                1</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion7" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion7" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar7" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar7_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Notificacion
                                                2</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion8" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion8" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar8" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar8_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Notificacion
                                                3</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <dx:ASPxMemo ID="txtNotificacion9" runat="server" Height="71px" Width="100%">
                                            </dx:ASPxMemo>
                                            <p>
                                            </p>
                                            <dx:ASPxDateEdit ID="deNotificacion9" runat="server" Width="100%">
                                            </dx:ASPxDateEdit>
                                            <p>
                                            </p>
                                            <table style="width: 100%;" align="right">
                                                <tr>
                                                    <td align="right" style="width: 100%;">
                                                        <asp:Button ID="btnGuardar9" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                                            OnClick="btnGuardar9_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- /#wrapper -->
        </div>
</asp:Content>
