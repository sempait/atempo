﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_horariosDisp : System.Web.UI.Page
{
    string IDProfesor = "";
    string IDHorarioDisponibilidad;
    Boolean flag = false;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        //Llamar al formulario alta de Disponibilidad horaria

        IDProfesor = gdvProfesores.GetRowValues(gdvProfesores.FocusedRowIndex, "IDProfesor").ToString();
        Session.Add("IDProfesor", IDProfesor);
        Session["IDHorarioDisponibilidad"] = 0;


        Response.Redirect("AgregarDisponiblidad.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        if (gdvHorarioProfesor.FocusedRowIndex == -1)
        {
            pcMensaje.HeaderText = "Mensaje";
            lblMensaje.Text = "Debe seleccionar una Disponibilidad Horaria del Profesor para poder Editar";
            pcMensaje.ShowOnPageLoad = true;
        }
        else
        { Editar(); }
    }

    private void Editar()
    {
        IDHorarioDisponibilidad = gdvHorarioProfesor.GetRowValues(gdvHorarioProfesor.FocusedRowIndex, "ID").ToString();

        Session.Add("IDHorarioDisponibilidad", IDHorarioDisponibilidad);

        Response.Redirect("AgregarDisponiblidad.aspx");

    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        if (gdvHorarioProfesor.FocusedRowIndex == -1)
        {
            pcMensaje.HeaderText = "Mensaje";
            lblMensaje.Text = "No selecciono ninguna Disponibilidad Horaria para eliminar";
            pcMensaje.ShowOnPageLoad = true;
        }
        else
        {
            Eliminar();
        }
    }

    private void Eliminar()
    {
        IDHorarioDisponibilidad = gdvHorarioProfesor.GetRowValues(gdvHorarioProfesor.FocusedRowIndex, "ID").ToString();

        //ACA TENGO QUE IR A PEGARLE A LA BASE Y VER SI TIENE HorariosCursos Dentro de su rango

        List<HorariosCursos> OListHorarioCurso = new List<HorariosCursos>();

        OListHorarioCurso = new HorariosCursosBusiness().ValidarHorarioCurso(IDHorarioDisponibilidad);

        if (OListHorarioCurso.Count != 0)
        {

            pcEliminar.ShowOnPageLoad = true;

        }

        else
        {
            //Aca tengo que hacer el eliminar 

            pcEliminarSinCursoRelacionado.ShowOnPageLoad = true;


        }

    }
    protected void btnSelecionarCurso_Click(object sender, EventArgs e)
    {
        cargarDisponibilidadHoraria();
    }

    private void cargarDisponibilidadHoraria()
    {

        IDProfesor = gdvProfesores.GetRowValues(gdvProfesores.FocusedRowIndex, "IDProfesor").ToString();

        Session.Add("IDProfesor", IDProfesor);

        gdvHorarioProfesor.DataSource = new HorariosDisponibilidadBusiness().SelectAllDisponibilidadProfesor(int.Parse(IDProfesor));
        gdvHorarioProfesor.DataBind();


    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        IDHorarioDisponibilidad = gdvHorarioProfesor.GetRowValues(gdvHorarioProfesor.FocusedRowIndex, "ID").ToString();
        new HorariosDisponibilidadBusiness().Delete(IDHorarioDisponibilidad);
        //pcEliminarSinCursoRelacionado.ShowOnPageLoad = false;
        //pcEliminar.ShowOnPageLoad = false;
        cargarDisponibilidadHoraria();
        //gdvHorarioProfesor.DataBind();
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        //pcEliminarSinCursoRelacionado.ShowOnPageLoad = false;
        //pcEliminar.ShowOnPageLoad = false;
    }
    
    protected void gdvProfesores_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        e.Row.Attributes.Add("onclick", "OnRowClick(" + "'" + e.Row.ID + "'" + ")");
    }
    protected void gdvHorarioProfesor_CustomCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
    {
        //IDProfesor = gdvProfesores.GetRowValues(int.Parse(e.Parameters), "IDProfesor").ToString();

        gdvHorarioProfesor.DataSource = new HorariosDisponibilidadBusiness().SelectAllDisponibilidadProfesor(int.Parse(e.Parameters));
        gdvHorarioProfesor.DataBind();


    }
}