﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="editarAlumno.aspx.cs" Inherits="escuela_editarAlumno" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Alumnos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Alumnos
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout runat="server" RequiredMarkDisplayMode="Auto" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption"
                                    AlignItemCaptionsInAllGroups="True" Width="100%" Theme="Aqua">
                                    <Items>
                                        <dx:LayoutGroup Caption="Datos del Alumno" GroupBoxDecoration="HeadingLine" SettingsItemCaptions-HorizontalAlign="Right"
                                            ColCount="2">
                                            <Items>
                                                <dx:LayoutItem Caption="Nombre y Apellido" ColSpan="2">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox ID="txtNombreYApellido" runat="server" NullText="" Width="100%">
                                                                <ValidationSettings SetFocusOnError="True" ErrorText="Nombre es requerido">
                                                                    <RequiredField IsRequired="True" ErrorText="Nombre es requerido" />
                                                                </ValidationSettings>
                                                                <InvalidStyle BackColor="LightPink" />
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="DNI">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtDNI">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Fecha de Nacimiento">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxDateEdit ID="deFechaNacimiento" runat="server">
                                                            </dx:ASPxDateEdit>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Direccion">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtDireccion">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="E-mail">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtEmail" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Telefono">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtTelefono" runat="server" Width="170px">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Colegio">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtColegio">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                            <SettingsItemCaptions HorizontalAlign="Right"></SettingsItemCaptions>
                                        </dx:LayoutGroup>
                                        <dx:LayoutGroup Caption="Datos de los Padres" GroupBoxDecoration="HeadingLine" SettingsItemCaptions-HorizontalAlign="Right"
                                            ColCount="2">
                                            <Items>
                                                <dx:LayoutItem Caption="Nombre y Apellido del Padre">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtNomApellidoPadre">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Tel. Padre">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtTelPadre">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Trabajo">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtTrabajoPadre">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Tel. Trabajo">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtTelTrabajoPadre">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Nombre y Apellido de la Madre">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtNomApeMadre">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Tel. Madre">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtTelMadre">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Trabajo">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtTrabajoMadre">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Tel. Trabajo">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtTelTrabajoMadre">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                            <SettingsItemCaptions HorizontalAlign="Right"></SettingsItemCaptions>
                                        </dx:LayoutGroup>
                                        <dx:LayoutGroup SettingsItemCaptions-HorizontalAlign="Right" Caption="Datos del Tutor"
                                            ColCount="2" GroupBoxDecoration="HeadingLine">
                                            <Items>
                                                <dx:LayoutItem Caption="Nombre y Apellido Tutor">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtNomApeTutor">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Tel. Tutor">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer>
                                                            <dx:ASPxTextBox runat="server" Width="170px" ID="txtTelTutor">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                            <SettingsItemCaptions HorizontalAlign="Right"></SettingsItemCaptions>
                                        </dx:LayoutGroup>
                                        <dx:LayoutItem Caption="Comentario">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer runat="server" 
                                                    SupportsDisabledAttribute="True">
                                                    <dx:ASPxMemo ID="txtComentario" runat="server" Height="71px" Width="100%">
                                                    </dx:ASPxMemo>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <Styles>
                                        <LayoutGroupBox>
                                            <Caption CssClass="layoutGroupBoxCaption">
                                            </Caption>
                                        </LayoutGroupBox>
                                    </Styles>
                                </dx:ASPxFormLayout>
                                <p>
                                </p>
                                <p style="text-align: right">
                                    <asp:Button ID="Button2" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                        Width="150px" OnClick="btnGuardar_Click" />
                                    <asp:Button ID="Button1" runat="server" Text="Guardar e Inscribir" type="button"
                                        class="btn btn-outline btn-primary" Width="150px" OnClick="btnGuardarInscribir_Click" />
                                    <asp:Button ID="Button3" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                        Width="150px" OnClick="btnCancelar_Click" />
                                </p>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <dx:ASPxPopupControl ID="pcMensaje" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="" AllowDragging="True" PopupAnimationType="None"
        EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            <dx:ASPxLabel ID="lblMensaje" runat="server" Text="" Theme="Aqua">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxButton ID="btnAceptarMsj" runat="server" Text="Aceptar">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
