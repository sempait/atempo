﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class admin_usuarios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IDUsuarioABM"] = null;
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("editarUsuario.aspx");
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        string IDUsuarioABM = gdvUsuarios.GetRowValues(gdvUsuarios.FocusedRowIndex, "IDUsuario").ToString();
        Session.Add("IDUsuarioABM", IDUsuarioABM);
        Response.Redirect("editarUsuario.aspx");
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        pcEliminar.ShowOnPageLoad = true;
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        string IDUsuarioABM = gdvUsuarios.GetRowValues(gdvUsuarios.FocusedRowIndex, "IDUsuario").ToString();
        UsuariosBusiness oUsuario = new UsuariosBusiness();
        oUsuario.Delete(int.Parse(IDUsuarioABM));

        Response.Redirect("usuarios.aspx");
    }
}