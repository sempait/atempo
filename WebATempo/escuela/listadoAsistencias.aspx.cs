﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_listadoAsistencias : System.Web.UI.Page
{
    string IDAsistenciaProfesor;

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IDAsistenciaProfesor"] = null;
    }
        
    protected void btnAgregarAsistencia_Click(object sender, EventArgs e)
    {
        Response.Redirect("asistenacias.aspx");
    }
    protected void btnEliminarAsistencia_Click(object sender, EventArgs e)
    {
        pcEliminar.ShowOnPageLoad = true;
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        IDAsistenciaProfesor = gdvAsistencias.GetRowValues(gdvAsistencias.FocusedRowIndex, "IDAsistenciaProfesor").ToString();

        new AsistenciasProfesorBusiness().Delete(int.Parse(IDAsistenciaProfesor));

        pcEliminar.ShowOnPageLoad = false;

        gdvAsistencias.DataBind();
    }
}