﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="~/escuela/usuarios.aspx.cs" Inherits="admin_usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        function ShowPopUp() {
            pcEliminar.Show();
        }
             
          
    </script>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Usuarios</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Datos Usuarios
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar" type="button" class="btn btn-outline btn-primary"
                                    OnClick="btnAgregar_Click" Width="100px" />
                                <asp:Button ID="btnEditar" runat="server" Text="Editar" type="button" class="btn btn-outline btn-warning"
                                    OnClick="btnEditar_Click" Width="100px" />
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" type="button" class="btn btn-outline btn-danger"
                                    OnClick="btnEliminar_Click" Width="100px" />
                            </p>
                            <dx:ASPxGridView ID="gdvUsuarios" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2"
                                KeyFieldName="IDUsuario" Theme="Aqua" EnableTheming="True" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="IDUsuario" ReadOnly="True" Visible="False"
                                        VisibleIndex="0">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Usuario" VisibleIndex="1" Visible="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Password" VisibleIndex="2" Visible="True">
                                        <PropertiesTextEdit Password="True">
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoUsuario" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="IDRol" VisibleIndex="4" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaBajaUsuario" Visible="False" VisibleIndex="5">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="Expr1" ReadOnly="True" Visible="False" VisibleIndex="6">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DescripcionRol" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn FieldName="ActivoRol" Visible="False" VisibleIndex="8">
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaBajaRol" Visible="False" VisibleIndex="9">
                                    </dx:GridViewDataDateColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT U.IDUsuario, U.Usuario, U.Password, U.NombreYApellidoUsuario, U.IDRol, U.FechaBajaUsuario, R.IDRol AS Expr1, R.DescripcionRol, R.ActivoRol, R.FechaBajaRol FROM Usuarios AS U INNER JOIN Roles AS R ON R.IDRol = U.IDRol
                                                            WHERE FechaBajaUsuario IS NULL"></asp:SqlDataSource>
                            
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
    <dx:ASPxPopupControl ID="pcEliminar" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcEliminar" HeaderText="Eliminar Usuario" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">
                            ¿Desea eliminar el usuario?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelar" runat="server" Text="Cancelar">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
