﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true" CodeFile="listadoAsistencias.aspx.cs" Inherits="escuela_listadoAsistencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Listado de Asistencias</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Listado de Asistencias
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnAgregarAsistencia" runat="server" Text="Agregar" type="button" class="btn btn-outline btn-primary"
                                    OnClick="btnAgregarAsistencia_Click" Width="100px" />
                                <asp:Button ID="btnEliminarAsistencia" runat="server"
                                    Text="Eliminar" type="button"
                                    class="btn btn-outline btn-danger" Width="100px"
                                    OnClick="btnEliminarAsistencia_Click"/>
                            </p>
                            <dx:ASPxGridView ID="gdvAsistencias" runat="server" AutoGenerateColumns="False" Width="100%" DataSourceID="SqlDataSource1" KeyFieldName="IDAsistenciaProfesor" Theme="Aqua">
                                <Columns>
                                    
                                    <dx:GridViewDataTextColumn FieldName="IDAsistenciaProfesor" ReadOnly="True" VisibleIndex="1" Visible="false">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaAsistenciaProfesor" VisibleIndex="2" Width="10px">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="HoraDesdeCurso" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="HoraHastaCurso" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DiaCurso" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoProfesor" VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DescripcionCurso" VisibleIndex="9">
                                    </dx:GridViewDataTextColumn>
                                    
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="True" />
                            </dx:ASPxGridView>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>" SelectCommand="SELECT AP.IDAsistenciaProfesor,AP.FechaAsistenciaProfesor,HC.DiaCurso,HC.HoraDesdeCurso,HC.HoraHastaCurso,P.NombreYApellidoProfesor,C.DescripcionCurso
FROM AsistenciasProfesores AP
INNER JOIN HorariosCurso HC 
ON AP.IDHorariosCurso = HC.IDHorarioCurso
INNER JOIN Profesores P
ON P.IDProfesor = HC.IDProfesor
INNER JOIN Cursos C
ON C.IDCurso = HC.IDCurso
ORDER BY FechaAsistenciaProfesor DESC"></asp:SqlDataSource>

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <dx:ASPxPopupControl ID="pcEliminar" runat="server" EnableTheming="True" Theme="RedWine"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcEliminar" HeaderText="Dar de Baja Asistencia" AllowDragging="True"
        PopupAnimationType="None" EnableViewState="False">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table class="nav-justified">
                    <tr>
                        <td colspan="2">¿Desea eliminar la Asistencia?
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancelar" runat="server" Text="Cancelar">
                                <ClientSideEvents Click="function(s, e) { pcEliminar.Hide(); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>



</asp:Content>

