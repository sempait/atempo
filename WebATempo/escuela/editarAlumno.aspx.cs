﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_editarAlumno : System.Web.UI.Page
{
    private int IdAlumno;
    Alumnos oAlumnoActual = new Alumnos();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            try
            {
                IdAlumno = int.Parse(Session["IDAlumno"].ToString());

                //Cargo el form
                if (IdAlumno != 0)
                    CargarDatos(IdAlumno);
            }

            catch
            {
                IdAlumno = 0;
                deFechaNacimiento.Value = DateTime.Now.ToShortDateString();
            }

        }
    }

    private void CargarDatos(int IdAlumno)
    {       
        oAlumnoActual = new AlumnosBusiness().SelectOne(IdAlumno);
            
        txtNombreYApellido.Value = oAlumnoActual.NombreYApellido;
        txtEmail.Value = oAlumnoActual.Email;
        txtTelefono.Value = oAlumnoActual.Telefono;
            
            
        txtNomApellidoPadre.Value = oAlumnoActual.NombreYApellidoPadre;
        txtNomApeMadre.Value = oAlumnoActual.NombreYApellidoMadre;
        txtTrabajoPadre.Value = oAlumnoActual.TrabajoPadre;
        txtTrabajoMadre.Value = oAlumnoActual.TrabajoMadre;
        txtTelTrabajoPadre.Value = oAlumnoActual.TelefonoTrabajoPadre;
        txtTelTrabajoMadre.Value = oAlumnoActual.TelefonoTrabajoMadre;
        txtComentario.Value = oAlumnoActual.Comentario;
        txtTelPadre.Value = oAlumnoActual.TelefonoPadre;            
        txtTelMadre.Value = oAlumnoActual.TelefonoMadre;
        txtDireccion.Value = oAlumnoActual.Domicilio;
            
        txtNomApeTutor.Value = oAlumnoActual.Tutor;
        txtTelTutor.Value = oAlumnoActual.TelefonoTutor;
        txtDNI.Value = oAlumnoActual.DNI;
        txtColegio.Value = oAlumnoActual.Colegio;
            
        deFechaNacimiento.Value = oAlumnoActual.FechaNacimiento;
     }

    private bool Validar()
    {
        if (txtNombreYApellido.Text == "" && deFechaNacimiento.Text == "")
            return false;
        else
            return true;
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {

        GuardarAlumno();


    }

    private void GuardarAlumno()
    {
        if (Validar())
        {
            try
            {
                IdAlumno = int.Parse(Session["IDAlumno"].ToString());
            }

            catch
            {
                IdAlumno = 0;
            }
            AlumnosBusiness oAlumnosBussines = new AlumnosBusiness();
            Alumnos oAlumno = new Alumnos();
            oAlumno.NombreYApellido = txtNombreYApellido.Text;
            oAlumno.Email = txtEmail.Text;
            oAlumno.Telefono = txtTelefono.Text;


            oAlumno.NombreYApellidoPadre = txtNomApellidoPadre.Text;
            oAlumno.NombreYApellidoMadre = txtNomApeMadre.Text;
            oAlumno.TrabajoPadre = txtTrabajoPadre.Text;
            oAlumno.TrabajoMadre = txtTrabajoMadre.Text;
            oAlumno.TelefonoTrabajoPadre = txtTelTrabajoPadre.Text;
            oAlumno.TelefonoTrabajoMadre = txtTelTrabajoMadre.Text;
            oAlumno.Comentario = txtComentario.Text;

            oAlumno.TelefonoPadre = txtTelPadre.Text;
            oAlumno.TelefonoMadre = txtTelMadre.Text;
            oAlumno.Domicilio = txtDireccion.Text;
            oAlumno.Tutor = txtNomApeTutor.Text;
            oAlumno.TelefonoTutor = txtTelTutor.Text;
            oAlumno.DNI = txtDNI.Text;
            oAlumno.Colegio = txtColegio.Text;
            //oAlumno.FechaNacimiento = Convert.ToDateTime(deFechaNacimiento.Value);
            if (deFechaNacimiento.Text == "")
                oAlumno.FechaNacimiento = null;
            else
                oAlumno.FechaNacimiento = Convert.ToDateTime(deFechaNacimiento.Value);

            if (IdAlumno == 0)
            {

                oAlumnosBussines.AlumnosInsert(oAlumno);
            }
            else
            {

                oAlumno.ID = IdAlumno;
                oAlumnosBussines.Update(oAlumno);
            }

            Session["IDAlumno"] = null;
            Response.Redirect("alumnos.aspx");
        }
        lblMensaje.Text = "Debe completar los datos requeridos";
        pcMensaje.HeaderText = "Advertencia";
        pcMensaje.ShowOnPageLoad = true;
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Cancelar();

    }

    private void Cancelar()
    {
        Response.Redirect("alumnos.aspx");

        Session["IDAlumno"] = null;
    }

    protected void btnGuardarInscribir_Click(object sender, EventArgs e)
    {
        AlumnosBusiness oAlumnosBussines = new AlumnosBusiness();
        Alumnos oAlumno = new Alumnos();

        if (IdAlumno == 0)
        {
            oAlumno.NombreYApellido = txtNombreYApellido.Text;
            //COMPLETAR DATOS FALTANTES

            oAlumnosBussines.AlumnosInsert(oAlumno);
            Session["IDAlumno"] = null;
        }
        else
        {
            oAlumno.NombreYApellido = txtNombreYApellido.Text;
            oAlumno.ID = IdAlumno;
            oAlumnosBussines.Update(oAlumno);
        }

        Session["IDAlumno"] = null;
        Response.Redirect("inscripcion.aspx");
    }
}