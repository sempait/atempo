﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;

public partial class admin_inicio : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Necesario para traducir los componentes a Español
        /*1º linea*/
        System.Threading.Thread.CurrentThread.CurrentCulture =
        new System.Globalization.CultureInfo("es-ES");
        /*2º linea*/
        System.Threading.Thread.CurrentThread.CurrentUICulture =
        new System.Globalization.CultureInfo("es");

        if (Session["IDUsuario"] == null)
        {
            //pcMensaje.HeaderText = "Mensaje";
            //lblMensaje.Text = "La sesión expiró. Vuelva a iniciar.";
            //pcMensaje.ShowOnPageLoad = true;
            Session.Clear();
            Session.Abandon();
            Response.Redirect("../login.aspx");
        }
        else
        {
            string Rol = Convert.ToString(Session["Rol"].ToString());
            if (Rol == "Escuela")
            {
                ddlSala.Visible = false;
                ddlBar.Visible = true;
            }
        }
        
    }
    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();
        Response.Redirect("../login.aspx");
    }

    
    
}
