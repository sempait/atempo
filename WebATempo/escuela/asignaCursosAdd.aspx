﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="asignaCursosAdd.aspx.cs" Inherits="escuela_asignaCursosAdd" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Asignar Horario Curso a Profesores</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Disponibilidad Horaria del Profesor: 
                        <asp:Label ID="lblNombreYApellidoProfesor" runat="server" Text=""></asp:Label>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <dx:ASPxGridView ID="gdvHorarioProfesor" runat="server" AutoGenerateColumns="False"
                                EnableTheming="True" KeyFieldName="ID" Theme="Aqua" Width="100%" AutoPostBack="true">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="Dia" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="HoraDesde" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="HoraHasta" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Visible="False" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="False" />
                            </dx:ASPxGridView>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Asignar Curso a Profesor
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="formLayoutContainer">
                                <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" EnableTheming="True" Theme="Aqua"
                                    Width="100%" Styles-LayoutGroupBox-Caption-CssClass="layoutGroupBoxCaption" AlignItemCaptionsInAllGroups="True">
                                    <Items>
                                        <dx:LayoutGroup Caption="Datos Profesor" GroupBoxDecoration="HeadingLine">
                                            <Items>
                                                <dx:LayoutItem Caption="Nombre y Apellido Profesor">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTextBox ID="txtNombreYApellidoProfesor" runat="server" Width="170px" Enabled="False">
                                                            </dx:ASPxTextBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                        <dx:LayoutGroup Caption="Agregar Curso" GroupBoxDecoration="HeadingLine">
                                            <Items>
                                                <dx:LayoutItem Caption="Descripcion Curso">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxComboBox ID="cbDescripcionCurso" runat="server" Width="170px">
                                                            </dx:ASPxComboBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                        <dx:LayoutGroup Caption="Horario Curso" GroupBoxDecoration="HeadingLine">
                                            <Items>
                                                <dx:LayoutItem Caption="Dia">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxComboBox ID="cbDiaCurso" runat="server" Width="170px">
                                                                <Items>
                                                                    <dx:ListEditItem Text="LUNES" Value="LUNES" />
                                                                    <dx:ListEditItem Text="MARTES" Value="MARTES" />
                                                                    <dx:ListEditItem Text="MIERCOLES" Value="MIERCOLES" />
                                                                    <dx:ListEditItem Text="JUEVES" Value="JUEVES" />
                                                                    <dx:ListEditItem Text="VIERNES" Value="VIERNES" />
                                                                    <dx:ListEditItem Text="SABADO" Value="SABADO" />
                                                                    <dx:ListEditItem Text="DOMINGO" Value="DOMINGO" />
                                                                </Items>
                                                            </dx:ASPxComboBox>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Horario Desde">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTimeEdit ID="TmeHoraDesde" runat="server" Width="170px" EditFormatString="HH:mm:ss">
                                                            </dx:ASPxTimeEdit>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                                <dx:LayoutItem Caption="Horario Hasta">
                                                    <LayoutItemNestedControlCollection>
                                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                                            <dx:ASPxTimeEdit ID="TmeHoraDesdeCurso" runat="server" Width="170px" EditFormatString="HH:mm:ss">
                                                            </dx:ASPxTimeEdit>
                                                        </dx:LayoutItemNestedControlContainer>
                                                    </LayoutItemNestedControlCollection>
                                                </dx:LayoutItem>
                                            </Items>
                                        </dx:LayoutGroup>
                                    </Items>
                                    <Styles>
                                        <LayoutGroupBox>
                                            <Caption CssClass="layoutGroupBoxCaption">
                                            </Caption>
                                        </LayoutGroupBox>
                                    </Styles>
                                </dx:ASPxFormLayout>
                                <p style="text-align: right">
                                    <asp:Button ID="btnSelecionar" runat="server" Text="Guardar" type="button" class="btn btn-outline btn-primary"
                                        Width="120px" OnClick="btnSelecionar_Click" />
                                    <asp:Button ID="btnGuardarAgregar" runat="server" Text="Guardar y Agregar" type="button"
                                        class="btn btn-outline btn-primary" Width="150px" OnClick="btnGuardarAgregar_Click" />
                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                        Width="120px" OnClick="btnCancelar_Click1" />
                                </p>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
</asp:Content>
