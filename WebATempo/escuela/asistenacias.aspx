﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="asistenacias.aspx.cs" Inherits="escuela_asistenacias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Asistencia Profesores</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="alert alert-success">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" AssociatedControlID="dateEdit" Text="Fecha Asistencia:" />
            <dx:ASPxDateEdit ID="deFechaAsistencia" runat="server" EditFormat="Custom" Width="200"
                Theme="Aqua">
                <TimeSectionProperties>
                    <TimeEditProperties EditFormatString="hh:mm tt" />
                </TimeSectionProperties>
            </dx:ASPxDateEdit>
        </div>
        <p>
        </p>
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Seleccione un Profesor
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-area-chart">
                            <dx:ASPxGridView ID="gdvProfesores" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                EnableTheming="True" KeyFieldName="IDProfesor" Theme="Aqua" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="IDProfesor" VisibleIndex="0" ReadOnly="True"
                                        Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoProfesor" VisibleIndex="1" Caption="Nombre y Apellido Profesor">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn FieldName="FechaBajaProfesor" VisibleIndex="2" Visible="False">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoProfesor" VisibleIndex="3" Visible="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="EmailProfesor" Visible="True" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Direccion" Visible="False" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Ciudad" Visible="False" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT * FROM [Profesores] WHERE FechaBajaProfesor IS NULL"></asp:SqlDataSource>
                            <p>
                            </p>
                            <p style="text-align: right">
                                <asp:Button ID="btnSelecionarProfesor" runat="server" Text="Seleccionar" type="button"
                                    class="btn btn-outline btn-primary" OnClick="btnSelecionarProfesor_Click" />
                            </p>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        2 - Seleccione el Horario Curso
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-bar-chart">
                            <dx:ASPxGridView ID="gdvHorarioCursos" runat="server" AutoGenerateColumns="False"
                                EnableTheming="True" Theme="Aqua" Width="100%" KeyFieldName="ID">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="IDHorarioCurso" FieldName="ID" Visible="False"
                                        VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Dia" FieldName="Dia" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="HoraDesde" FieldName="HoraDesde" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="HoraHasta" FieldName="HoraHasta" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Curso" FieldName="Curso.DescripcionCurso" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                            </dx:ASPxGridView>
                        </div>
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
        <!--HASTA ACA EL CONTENT-->
        <p>
            <asp:Button ID="btnRegistrarAsistencia" runat="server" Text="Registrar Asistencia"
                type="button" class="btn btn-outline btn-primary btn-lg btn-block" Width="100%"
                OnClick="btnRegistrarAsistencia_Click" />
        </p>
    </div>
</asp:Content>
