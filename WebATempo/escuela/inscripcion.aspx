﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="inscripcion.aspx.cs" Inherits="escuela_inscripcion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Agregar Inscripcion</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="alert alert-success">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" AssociatedControlID="dateEdit" Text="Fecha Inscripción::" />
            <dx:ASPxDateEdit ID="deFechaInscripcion" runat="server" EditFormat="Custom" Width="200"
                Theme="Aqua">
                <TimeSectionProperties>
                    <TimeEditProperties EditFormatString="hh:mm tt" />
                </TimeSectionProperties>
            </dx:ASPxDateEdit>
        </div>
        <p>
        </p>
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        1 - Seleccione el Alumno
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-area-chart">
                            <dx:ASPxGridView ID="gdvAlumnos" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                                EnableTheming="True" KeyFieldName="IDAlumno" Theme="Aqua" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="IDAlumno" ReadOnly="True" VisibleIndex="0"
                                        Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoAlumno" VisibleIndex="1" Caption="Alumno">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="EmailAlumno" VisibleIndex="2" Caption="Email"
                                        Visible="True">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoAlumno" VisibleIndex="3" Caption="Tel. Alumno" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoPadre" VisibleIndex="5" Caption="Nom. Padre"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NombreYApellidoMadre" VisibleIndex="6" Caption="Nom. Madre"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TrabajoPadre" VisibleIndex="7" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TrabajoMadre" VisibleIndex="8" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoTrabajoPadre" VisibleIndex="9" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoTrabajoMadre" VisibleIndex="10" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Comentario" VisibleIndex="11" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoPadre" VisibleIndex="12" Caption="Tel. Padre"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoMadre" VisibleIndex="13" Caption="Tel. Madre"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DomicilioAlumno" VisibleIndex="4" Caption="Domicilio"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Tutor" VisibleIndex="14" Caption="Tutor" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TelefonoTutor" VisibleIndex="15" Caption="Tel. Tutor"
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT * FROM [Alumnos] WHERE FechaBaja IS NULL ORDER BY NombreYApellidoAlumno"></asp:SqlDataSource>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        2 - Seleccione el Curso
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="morris-bar-chart">
                            <dx:ASPxGridView ID="gdvCursos" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceCursos"
                                EnableTheming="True" Theme="Aqua" Width="100%" KeyFieldName="IDCurso">
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="IDCurso" VisibleIndex="0" ReadOnly="True" Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DescripcionCurso" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Expr1" ReadOnly="True" Visible="False" VisibleIndex="5">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <Settings ShowFilterRow="True" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSourceCursos" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT c.IDCurso, c.DescripcionCurso, c.ActivoCurso, c.FechaBajaCurso FROM Cursos AS c  WHERE (c.FechaBajaCurso IS NULL) ORDER BY C.DescripcionCurso">
                            </asp:SqlDataSource>
                            <p>
                                <dx:ASPxRadioButton ID="rdbtnIndividual" runat="server" Text="Individual" Checked="True"
                                    GroupName="Modalidad">
                                </dx:ASPxRadioButton>
                                <dx:ASPxRadioButton ID="rdbtnGrupal" runat="server" Text="Grupal" GroupName="Modalidad">
                                </dx:ASPxRadioButton>
                            </p>
                        </div>
                        <p style="text-align: right">
                            <asp:Button ID="btnSelecionarCurso" runat="server" Text="Seleccionar" type="button"
                                class="btn btn-outline btn-primary" OnClick="btnSelecionarCurso_Click" />
                        </p>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            <!-- /.col-lg-6 -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Horarios Disponibles
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <p>
                            <asp:Button ID="btnInscribir" runat="server" Text="Inscribir" type="button" class="btn btn-outline btn-primary"
                                OnClick="btnInscribir_Click" />
                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" type="button" class="btn btn-outline btn-danger"
                                OnClick="btnCancelar_Click" />
                        </p>
                        <dx:ASPxGridView ID="gdvHorariosCursos" runat="server" Theme="Aqua" Width="100%"
                            AutoGenerateColumns="False" EnableTheming="True" KeyFieldName="ID">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Dia" FieldName="Dia" VisibleIndex="1" ReadOnly="True">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Hora Desde" FieldName="HoraDesde" VisibleIndex="2"
                                    ReadOnly="True">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Hora Hasta" FieldName="HoraHasta" VisibleIndex="3"
                                    ReadOnly="True">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Descripción Curso" FieldName="Curso.DescripcionCurso"
                                    VisibleIndex="4" ReadOnly="True">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Profesor" FieldName="Profesor.NombreYApellidoProfesor"
                                    VisibleIndex="5" ReadOnly="True">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="N° Alumnos Inscriptos" FieldName="CantidadAlumnos"
                                    VisibleIndex="7" ReadOnly="True">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="IDRegistro" FieldName="ID" Visible="False" VisibleIndex="0"
                                    ReadOnly="True">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsBehavior AllowFocusedRow="True" />
                            <SettingsPager PageSize="40">
                            </SettingsPager>
                            <Settings ShowFilterRow="True" />
                        </dx:ASPxGridView>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>
    <dx:ASPxPopupControl ID="pcMensaje" runat="server" EnableTheming="True" Theme="Metropolis"
        CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="pcMensaje" HeaderText="" AllowDragging="True" PopupAnimationType="None"
        EnableViewState="False" Width="200px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <fieldset>
                    <div class="form-group">
                        <label>
                            Alumno
                        </label>
                        <asp:Label ID="lblAlumno" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="form-group">
                        <label>
                            Curso
                        </label>
                        <asp:Label ID="lblCurso" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="form-group">
                        <label>
                            Dia
                        </label>
                        <asp:Label ID="lblDia" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="form-group">
                        <label>
                            Hora Desde
                        </label>
                        <asp:Label ID="lblHoraDesde" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="form-group">
                        <label>
                            Hora Hasta
                        </label>
                        <asp:Label ID="lblHoraHasta" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="form-group">
                        <label>
                            Profesor
                        </label>
                        <asp:Label ID="lblProfesor" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <dx:ASPxCheckBox ID="ckbMail" runat="server" Text="Envio de Email">
                            </dx:ASPxCheckBox>
                        </label>
                    </div>
                    <table>
                        <tr>
                        <td>
                            <dx:ASPxButton ID="btnAceptarMensaje" runat="server" Text="Aceptar" 
                                OnClick="btnAceptarMensaje_Click" Theme="Metropolis">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton></td>
                            <td>
                            <dx:ASPxButton ID="btnCancelarMensaje" runat="server" Text="Cancelar" 
                                    OnClick="btnCancelarMensaje_Click" Theme="Metropolis">
                                <ClientSideEvents Click="function(s, e) { pcMensaje.Hide(); }" />
                            </dx:ASPxButton></td>
                        </tr>
                    </table>
                </fieldset>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
