﻿<%@ Page Title="" Language="C#" MasterPageFile="~/escuela/inicio.master" AutoEventWireup="true"
    CodeFile="caja.aspx.cs" Inherits="escuela_caja" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Gestion de Caja</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Caja
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <p>
                                <asp:Button ID="btnCerrarCaja" runat="server" Text="CerrarCaja" type="button" class="btn btn-outline btn-primary"
                                    Width="100px" OnClick="btnCerrarCaja_Click" />
                            </p>
                            <dx:ASPxGridView ID="gdvCajaDiaria" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceCajaDiaria"
                                EnableTheming="True" Theme="Aqua" Width="100%" KeyFieldName="Fecha">
                                <Columns>
                                    <dx:GridViewDataDateColumn FieldName="Fecha" VisibleIndex="0">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="CajaInicial" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Ingresos" VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Egresos" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Gastos" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Pagos" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Resultado" ReadOnly="True" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="IdCaja" FieldName="Id_Caja" Visible="False" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" />
                                <SettingsPager PageSize="10">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSourceCajaDiaria" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                                SelectCommand="SELECT * FROM [ViResultadoCajaDiaria] order by 1 desc"></asp:SqlDataSource>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /#page-wrapper -->
                </div>
            </div>
        </div>
        <!-- /#wrapper -->
    </div>
    <!--HASTA ACA EL CONTENT-->
</asp:Content>
