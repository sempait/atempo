﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_cursos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IDCurso"] = null;

        // Necesario para traducir los componentes a Español
        /*1º linea*/
        System.Threading.Thread.CurrentThread.CurrentCulture =
        new System.Globalization.CultureInfo("es-ES");
        /*2º linea*/
        System.Threading.Thread.CurrentThread.CurrentUICulture =
        new System.Globalization.CultureInfo("es");
    }

    //private void CargarGrid()
    //{
    //    gdvCursos.DataSource = new CursosBusiness().SelectAll();
    //    gdvCursos.DataBind();
    //}
    //protected void btnNuevo_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("editarCurso.aspx");
    //}
    //protected void gdvCursos_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    int idCurso = Convert.ToInt32(gdvCursos.DataKeys[e.RowIndex].Value);

    //    CursosBusiness oCursos = new CursosBusiness();
    //    oCursos.Delete(idCurso);

    //    CargarGrid(); //luego de eliminar se recarga el grid
    //}
    //protected void gdvCursos_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{
    //    int idCurso = Convert.ToInt32(gdvCursos.DataKeys[e.NewSelectedIndex].Value);

    //    Response.Redirect(string.Format("editarCurso.aspx?id={0}", idCurso));
    //}
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("editarCurso.aspx");
    }
   
    
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Editar();
    }

    private void Editar()
    {
        string IDCurso = gdvCursos.GetRowValues(gdvCursos.FocusedRowIndex, "IDCurso").ToString();
        Session.Add("IDCurso", IDCurso);
        Response.Redirect("editarCurso.aspx");
    }
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        pcEliminar.ShowOnPageLoad = true;
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        pcEliminar.ShowOnPageLoad = false;
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        string IDCurso = gdvCursos.GetRowValues(gdvCursos.FocusedRowIndex, "IDCurso").ToString();

        CursosBusiness oCurso = new CursosBusiness();
        oCurso.Delete(int.Parse(IDCurso));

        Response.Redirect("cursos.aspx");
    }
}