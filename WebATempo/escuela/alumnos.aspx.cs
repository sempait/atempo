﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_alumnos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (rdbActivos.Checked)
                gdvAlumnos.FilterExpression = "FechaBaja IS NULL";

        }

        Session["IDAlumno"] = null;

    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        Response.Redirect("editarAlumno.aspx");
    }
    protected void btnEditar_Click(object sender, EventArgs e)
    {
        Editar();
    }

    private void Editar()
    {
        string IDAlumno = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "IDAlumno").ToString();
        Session.Add("IDAlumno", IDAlumno);
        Response.Redirect("editarAlumno.aspx");
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        pcEliminar.ShowOnPageLoad = true;
        lblMensajeAlumnos.Text = "¿Desea eliminar el Alumno?";

    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        try
        {
            string IDAlumno = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "IDAlumno").ToString();

            AlumnosBusiness oAlumno = new AlumnosBusiness();
            oAlumno.Delete(int.Parse(IDAlumno));
            Response.Redirect("alumnos.aspx");
        }
        catch (Exception ex)
        {
            pcEliminar.ShowOnPageLoad = true;
            lblMensajeAlumnos.Text = Server.HtmlEncode(ex.Message.ToString());
            btnAceptar.Visible = false;
            btnCancelar.Visible = false;
        }
    }
    protected void btnDarDeBajaAlumno_Click(object sender, EventArgs e)
    {
        pcDeBajaAlumno.ShowOnPageLoad = true;
        lblMensajeAlumnos.Text = "¿Desea dar de baja el Alumno?";
    }

    protected void rbActivos_CheckedChanged(object sender, EventArgs e)
    {
        gdvAlumnos.FilterExpression = "FechaFinInscripcion IS NULL";
    }
    protected void btnpcDeBajaAlumnoAceptar_Click(object sender, EventArgs e)
    {
        string IDAlumno = gdvAlumnos.GetRowValues(gdvAlumnos.FocusedRowIndex, "IDAlumno").ToString();
        Alumnos alumnoActual = new Alumnos();
        alumnoActual.ID = int.Parse(IDAlumno);

        new AlumnosBusiness().UpdateBajaAlumno(alumnoActual);

        gdvAlumnos.DataBind();
    }
    protected void rdbActivos_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbActivos.Checked)
            gdvAlumnos.FilterExpression = "FechaBaja IS NULL";

    }
    protected void rdbBajas_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbBajas.Checked)
            gdvAlumnos.FilterExpression = "FechaBaja IS NOT NULL";

    }
}