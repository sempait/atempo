﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using BusinessLayer;
using Microsoft.Reporting.WebForms;



public partial class escuela_PagoProfesor : System.Web.UI.Page
{
    DSReportePagoProfesores dsReporte = new DSReportePagoProfesores();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            deFechaDesde.Value = DateTime.Today;
            deFechaHasta.Value = DateTime.Today;


            CargarReporte(Convert.ToDateTime(deFechaDesde.Value), Convert.ToDateTime(deFechaHasta.Value));
        }


    }

    protected void btnEmitirReporte_Click(object sender, EventArgs e)
    {
        CargarReporte(Convert.ToDateTime(deFechaDesde.Value), Convert.ToDateTime(deFechaHasta.Value));
    }



    protected void Page_UnLoad(object sender, EventArgs e)
    {
        dsReporte.Dispose();
        dsReporte = null;
    }


    private void CargarReporte(DateTime fechaDesde, DateTime fechaHasta)
    {
        DataTable tablaReporte = new PagoProfesoresBusiness().RecuperarPagoProfesores(fechaDesde, fechaHasta);
        foreach (DataRow fila in tablaReporte.Rows)
        {
            DataRow filaReporte = dsReporte.tablaReporte.NewRow();
            filaReporte["profesor"] = fila["profesor"];
            filaReporte["modalidad"] = fila["modalidad"];
            filaReporte["fecha"] = fila["fecha"];
            filaReporte["curso"] = fila["curso"];
            filaReporte["horas"] = fila["horas"];
            filaReporte["importe"] = fila["importe"];
            filaReporte["pocentajePlus"] = fila["pocentajePlus"];
            dsReporte.tablaReporte.Rows.Add(filaReporte);

        }

        ReportViewer1.ProcessingMode = ProcessingMode.Local;
        //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReportPagoaProfesores.rdlc");
        DSReportePagoProfesores dsReporte1 = dsReporte;
        ReportDataSource datasource = new ReportDataSource("DSReportePagoProfesores", dsReporte1.Tables[0]);
        ReportViewer1.LocalReport.DataSources.Clear();
        ReportViewer1.LocalReport.DataSources.Add(datasource);

    }
}