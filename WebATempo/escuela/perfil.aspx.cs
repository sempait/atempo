﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLayer;
using EntitiesLayer;

public partial class escuela_perfil : System.Web.UI.Page
{
    private int IdUsuario;
    Usuarios oUsaurioActual = new Usuarios();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           IdUsuario = int.Parse(Session["IDUsuario"].ToString());
            CargarDatos(IdUsuario);
        }
    }

    private void CargarDatos(int IdUsuario)
    {
        oUsaurioActual = new UsuariosBusiness().SelectOne(IdUsuario);

        txtNombreYApellido.Value = oUsaurioActual.NombreYApellido;
        txtUsuario.Value = oUsaurioActual.Usuario;
        txtContraseña.Value = oUsaurioActual.Password;
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        UsuariosBusiness oUsuariosBussines = new UsuariosBusiness();
        //Usuarios oUsuario = new Usuarios();

        //oUsaurioActual.ID = IdUsuario;
        oUsaurioActual.NombreYApellido = txtNombreYApellido.Text;
        oUsaurioActual.Usuario = txtUsuario.Text;
        oUsaurioActual.Password = txtContraseña.Text;

        string resultado = oUsuariosBussines.Update(oUsaurioActual);


        Response.Redirect("index.aspx");
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("index.aspx");
    }
}