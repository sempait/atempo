﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntitiesLayer;
using BusinessLayer;

public partial class escuela_editarCurso : System.Web.UI.Page
{
    private int IdCurso;
    Cursos oCursoActual = new Cursos();

    protected void Page_Load(object sender, EventArgs e)
    {
     
        if (!IsPostBack)
        {

            try
            {
                IdCurso = int.Parse(Session["IDCurso"].ToString());

                if (IdCurso != 0)
                    CargarDatos(IdCurso);
            }

            catch
            {
                IdCurso = 0;
                
            }

        }








    }

    private void CargarDatos(int IdCurso)
    {
        oCursoActual = new CursosBusiness().SelectOne(IdCurso);

        txtDescripcionCurso.Value = oCursoActual.DescripcionCurso;
        txtPrecioIndividual.Value = Convert.ToString(oCursoActual.PrecioIndividual);
        txtPrecioGrupal.Value = Convert.ToString(oCursoActual.PrecioGrupal);


    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        GuardarArticulo();
    }

    private void GuardarArticulo()
    {
        try
        {
            IdCurso = int.Parse(Session["IDCurso"].ToString());
        }
        catch
        {
            IdCurso = 0;
        }

        CursosBusiness oCursosBusiness = new CursosBusiness();


        oCursoActual.DescripcionCurso = txtDescripcionCurso.Text;

        if (txtPrecioIndividual.Text == "")
        { oCursoActual.PrecioIndividual = 0; }
        else
        { oCursoActual.PrecioIndividual = float.Parse(txtPrecioIndividual.Text.ToString()); }


        if (txtPrecioGrupal.Text == "")
        { oCursoActual.PrecioGrupal = 0; }
        else
        { oCursoActual.PrecioGrupal = float.Parse(txtPrecioGrupal.Text.ToString()); }



        if (IdCurso == 0)
        {
            oCursosBusiness.Insert(oCursoActual);

        }
        else
        {
            oCursoActual.ID = IdCurso;
            oCursosBusiness.Update(oCursoActual);
        }

        Session["IDCurso"] = null;
        Response.Redirect("cursos.aspx");
    }
    protected void btnActualizarPrecio_Click(object sender, EventArgs e)
    {
        actualizarPrecio();
    }

    private void actualizarPrecio()
    {
        HistoricoPreciosCursosBusiness hpcb = new HistoricoPreciosCursosBusiness();
        HistoricoPreciosCursos oPrecioActualizar = new HistoricoPreciosCursos();

        //string precioOld = Convert.ToString(oArticuloActual.HistoricoPrecioArticulo[0].Precio);
        //if (txtPrecio.Text != precioOld)
        //{
        //oArticuloActual.ID = IdArticulo;
        //oArticuloActual.HistoricoPrecioArticulo[0].Precio = int.Parse(txtPrecio.Text);
        oPrecioActualizar.Curso = oCursoActual;
        oPrecioActualizar.Precio = float.Parse(txtPrecioIndividual.Text);

        hpcb.Insert(oPrecioActualizar);
        //}    
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("cursos.aspx");
    }
}