﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="atempo_sala.aspx.cs" Inherits="sitio_atempo_sala" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>A Tempo Sala</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="icon" href="../images/logo-atempo.png" />
    <link rel="shortcut icon" href="../images/logo-atempo.png" />
    
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery.scrolly.min.js" type="text/javascript"></script>
    <script src="js/jquery.poptrox.min.js" type="text/javascript"></script>
    <script src="js/skel.min.js" type="text/javascript"></script>
    <script src="js/init.js" type="text/javascript"></script>
    <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <!-- Header -->
        <section id="header">
				<header>
					<h1>A Tempo Sala</h1>
					<%--<p>Centro de Estudios Musicales</p>--%>
                    <p>Lunes a Viernes 20:00 a 24:00 Hs</p>
                <p>Sabados 14:00 a 22:00 Hs</p>
				</header>
				<footer>
					<a href="#banner2" class="button style2 scrolly scrolly-centered">Turnos Disponibles</a>
				</footer>
			</section>
        <!-- Banner -->
        <div id="banner2">
            <dx:ASPxScheduler ID="ASPxScheduler1" runat="server" AppointmentDataSourceID="SqlDataSource1"
                ClientIDMode="AutoID" ResourceDataSourceID="SqlDataSource2" GroupType="Resource"
                Theme="Default">
                <Storage>
                    <Appointments>
                        <Mappings AllDay="AllDay" AppointmentId="UniqueID" Description="Description" End="EndDate"
                            Label="Label" Location="Location" RecurrenceInfo="RecurrenceInfo" ReminderInfo="ReminderInfo"
                            ResourceId="ResourceID" Start="StartDate" Status="Status" Subject="Subject" Type="Type" />
                    </Appointments>
                    <Resources>
                        <Mappings Caption="ResourceName" Color="Color" ResourceId="ResourceID" />
                    </Resources>
                </Storage>
                <Views>
                    <%--<DayView ShowWorkTimeOnly="true" DayCount="1">
                        <TimeRulers>
                            <dx:TimeRuler></dx:TimeRuler>
                        </TimeRulers>
                        <WorkTime Start="14:00" End="23:59" />
                    </DayView>--%>
                    <WorkWeekView>
                        <TimeRulers>
                            <dx:TimeRuler></dx:TimeRuler>
                        </TimeRulers>
                    </WorkWeekView>
                    <WeekView Enabled="False">
                    </WeekView>
                    <MonthView CompressWeekend="False">
                    </MonthView>
                    <TimelineView Enabled="False">
                    </TimelineView>
                </Views>
                <OptionsForms AppointmentFormVisibility="None" />
            </dx:ASPxScheduler>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                SelectCommand="SELECT * FROM [Salas]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EscuelaDeMusicaConnectionString %>"
                SelectCommand="SELECT * FROM [TurnosSala]"></asp:SqlDataSource>
        </div>
        <!-- Contact -->
        <article class="container box style3">
				<header>
					<h2>Contáctenos</h2>
                    <p>Buenos Aires 911 - Planta Alta<br />Tel. 341 - 6794423</p>
					
				</header>
				
					<div class="row half">
						<div class="6u"><%--<input type="text" class="text" name="name" placeholder="Nombre" />--%>
                            <asp:TextBox ID="txtNombreContacto" runat="server" type="text" class="text" placeholder="Nombre" ></asp:TextBox>
                        </div>
						<div class="6u"><%--<input type="text" class="text" name="email" placeholder="Email" />--%>
                            <asp:TextBox ID="txtEmailContacto" runat="server" type="text" class="text" placeholder="Email"></asp:TextBox>
                        </div>
					</div>
					<div class="row half">
						<div class="12u">
							<%--<textarea name="message" placeholder="Mensaje"></textarea>--%>
                            <asp:TextBox id="txtTextArea" TextMode="multiline" Columns="50" Rows="5" runat="server" placeholder="Mensaje"/>
						</div>
					</div>
					<div class="row">
						<div class="12u">
							<ul class="actions">
								<li><%--<input type="submit" value="Enviar Mensaje" />--%>
                                    <asp:Button ID="btnEnviarMensaje" runat="server" Text="Enviar Mensaje" 
                                        type="submit" onclick="btnEnviarMensaje_Click"></asp:Button>
                                </li>
							</ul>
						</div>
					</div>
				
			</article>
        <section id="footer">
			<ul class="icons">
				<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
				<li><a href="https://www.facebook.com/atempo.musica.7" target="_blank" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
				<%--<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
				<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
				<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
				<li><a href="#" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>--%>
			</ul>
			<div class="copyright">
				<ul class="menu">
					<li>&copy; A Tempo Sala</li><li>Design by: <a href="http://www.sempait.com.ar" target="_blank">SempaIT</a></li>
				</ul>
			</div>
		</section>
    </div>
    </form>
</body>
</html>
