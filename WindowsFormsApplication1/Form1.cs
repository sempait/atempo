﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BusinessLayer;
using EntitiesLayer;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Alumnos alumno = new Alumnos();
            alumno.NombreYApellido  = "Tincho";
            new AlumnosBusiness().AlumnosInsert(alumno);
        }
    }
}
